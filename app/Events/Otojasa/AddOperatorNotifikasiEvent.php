<?php namespace App\Events\Otojasa;

use App\Events\Event;

use Illuminate\Queue\SerializesModels;

class AddOperatorNotifikasiEvent extends Event {

	use SerializesModels;

	public $user;

	public function __construct($user){
		$this->user = $user;
	}

	public function send(){
		\Mail::send('emails.AddOperator', ['data' => $this->user], function($email){
			$email->to($this->user->email);
		});
	}

}
