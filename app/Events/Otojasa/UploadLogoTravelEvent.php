<?php namespace App\Events\Otojasa;

use App\Events\Event;

use Illuminate\Queue\SerializesModels;

class UploadLogoTravelEvent extends Event {

	use SerializesModels;

	public $travel;
	public $path;

	public function __construct($travel){
		$this->travel = $travel;
		$this->path = storage_path() . '/app/images/travel/avatar/';
	}

	public function upload(){

		$file = file_get_contents($_FILES['logo']['tmp_name']);
		$basename = md5($_FILES['logo']['tmp_name']) . uniqid() . '.png';
		$dir = str_replace('-', '', $this->travel->slug);

		if(!is_dir($this->path . $dir))
			mkdir($this->path . $dir);

		if(!is_dir($this->path . 'thumb/' . $dir))
			mkdir($this->path . 'thumb/' . $dir);

		\Image::make($file)
			->fit(300, $this->newHight($_FILES['logo']['tmp_name'], 300))
			->save($this->path . $dir . '/' . $basename);

		\Image::make($file)
			->fit(50, $this->newHight($_FILES['logo']['tmp_name'], 50))
			->save($this->path . 'thumb/' . $dir . '/' . $basename);

		return $dir . '/' . $basename;
	}

	private function newHight($file, $nh){
		$size 		= getimagesize($file);
		$w 			= $size[0];
		$h			= $size[1];

		return ceil(($h / $w) * $nh);

	}

}
