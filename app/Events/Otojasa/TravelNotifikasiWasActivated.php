<?php namespace App\Events\Otojasa;

use App\User;
use App\Models\relasi_travel;

use App\Events\Event;

use Illuminate\Queue\SerializesModels;

class TravelNotifikasiWasActivated extends Event {

	use SerializesModels;

	public $travel, $emails = [];

	public function __construct($travel){
		$this->travel = $travel;
	}

	public function sendEmail(){

		foreach(relasi_travel::admintravel($this->travel->id)->get() as $admin){
			$this->emails[] = $admin->email;
		}

		\Mail::send('emails.AktivatedOtojasa', ['data' => $this->travel], function($email){
			$email->to($this->emails)->subject($this->travel->nama_travel . ' telah diaktifkan');
		});
	}

}
