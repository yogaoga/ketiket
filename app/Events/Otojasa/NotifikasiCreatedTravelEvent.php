<?php namespace App\Events\Otojasa;

use App\Events\Event;
use App\Models\travel;

use Illuminate\Queue\SerializesModels;

class NotifikasiCreatedTravelEvent extends Event {

	use SerializesModels;

	public function __construct($travel){
		$travel = travel::find($travel->id);

		\Mail::send('emails.CreateTravel',['data'=>$travel], function($email){
			$email->to(\Auth::user()->email);
		});

		\Mail::send('emails.CreateTravel',['data'=>$travel], function($email){
			$email->to(env('KETIKET_EMAIL_NOTIF'));
		});

	}

}
