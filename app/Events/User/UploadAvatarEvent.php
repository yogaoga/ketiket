<?php namespace App\Events\User;

use App\Events\Event;

use Illuminate\Queue\SerializesModels;

class UploadAvatarEvent extends Event {

	use SerializesModels;

	public function file($data){

		$dir = storage_path() . '/app/images/avatar/';
		// Membuat folder
		$paths = explode('@', \Auth::user()->email);
		$path = $paths[0] . \Auth::user()->id;
		// membuat nm_file
		$basename = md5($data) . '.png';

		//dd($dir . $path);

		$file = file_get_contents($data);

		if(!is_dir($dir . $path))
			mkdir($dir . $path);
		if(!is_dir($dir . 'thumb/' . $path))
			mkdir($dir . 'thumb/' . $path);

		\Image::make($file)
			->fit(300, $this->newHight($data, 300))
			->save($dir . $path . '/' . $basename);

		\Image::make($file)
			->fit(50, $this->newHight($data, 50))
			->save($dir . 'thumb/' . $path . '/' . $basename);

		// Delete File
		$this->deleteFile();

		return $path . '/' . $basename;
	}

	private function deleteFile(){
		$dir = storage_path() . '/app/images/avatar/';
		$file = \Auth::user()->foto;
		if(!empty($file)){
			if(file_exists($dir . $file)){
				unlink($dir . $file);
			}
			if(file_exists($dir . '/thumb/' . $file)){
				unlink($dir . '/thumb/' . $file);
			}
		}
	}

	private function newHight($file, $nh){
		$size 		= getimagesize($file);
		$w 			= $size[0];
		$h			= $size[1];

		return ceil(($h / $w) * $nh);

	}

}
