<?php namespace App\Events;

use App\User;
use App\Events\Event;

use Illuminate\Queue\SerializesModels;

class StatusLoginUser extends Event {

	use SerializesModels;
	private $time_limit;

	public function __construct(){
		$this->time_limit = $limit = time() + 900;
	}

	public function login(){
		return User::find(\Auth::user()->id)->update([
			'status_online' => 1,
			'time_online' => $this->time_limit
		]);
	}

	public function refresh(){
		if(\Auth::check()):
			$this->login();
		endif;
	}

	public function logout(){
		return User::find(\Auth::user()->id)->update([
			'status_online' => 0,
			'time_online' => 0
		]);
	}

}
