<?php namespace App\Events\Agen;

use App\Events\Event;

use Illuminate\Queue\SerializesModels;

class AddOperatorNotifikasiAgenEvent extends Event {

	use SerializesModels;

	public $user;

	public function __construct($user){
		$this->user = $user;
	}

	public function send(){
		\Mail::send('emails.AddOperatorAgen', ['data' => $this->user], function($email){
			$email->to($this->user->email);
		});
	}

}
