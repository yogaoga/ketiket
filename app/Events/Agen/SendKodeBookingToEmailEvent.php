<?php namespace App\Events\Agen;

use App\Events\Event;

use Illuminate\Queue\SerializesModels;

class SendKodeBookingToEmailEvent extends Event {

	use SerializesModels;

	public $req;
	public function __construct(array $req){
		$this->req = $req;
	}

}
