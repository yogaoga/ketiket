<?php namespace App\Handlers\Events\Agen;

use App\Models\reservasi;
use App\Models\reservasi_detail;

use App\Events\Agen\SendKodeBookingToEmailEvent;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class SendCodeBookingToEmailEventHandler {

	public function handle(SendKodeBookingToEmailEvent $event){
		$emails = explode(',', $event->req['email']);
		$travel = reservasi::join('route_detail', 'route_detail.id', '=', 'reservasi.route_detail_id')
		->join('kab_kota AS a', 'a.id', '=', 'route_detail.depart')
		->join('kab_kota AS b', 'b.id', '=', 'route_detail.destination')
		->join('hubs', 'hubs.id', '=', 'reservasi.hubs_id')
		->where('reservasi.id', $event->req['id'])
		->select(
			'a.nm_kab_kota AS nm_depart',
			'b.nm_kab_kota AS nm_destination',
			'reservasi.*',
			'hubs.lokasi',
			'hubs.waktu'
		)
		->first();

		$kursi = '';
		foreach(reservasi_detail::whereReservasi_id($event->req['id'])->get() as $detail){
			$kursi .= ' ' . $detail->kursi . ',';
		}

		\Mail::send('emails.emailKodeBooking', [
			'reservasi' => $travel,
			'kursi' => rtrim($kursi,',')
		], function($email) use ($emails){
			$email->to($emails)->subject('Kode Booking');
		});

	}

}
