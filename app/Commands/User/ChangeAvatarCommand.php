<?php namespace App\Commands\User;

use App\User;
use App\Commands\Command;

use Illuminate\Contracts\Bus\SelfHandling;

class ChangeAvatarCommand extends Command implements SelfHandling {

	public $req;
	public function __construct($req){
		$this->req = $req;
	}

	
	public function handle(){
		
		$user = User::find(\Auth::user()->id);
		$user->foto = $this->req;
		$user->save();

		return $user;
	}

}
