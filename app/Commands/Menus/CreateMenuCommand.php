<?php namespace App\Commands\Menus;

use App\Commands\Command;
use App\Models\frame_menu_nav;

use Illuminate\Contracts\Bus\SelfHandling;

class CreateMenuCommand extends Command implements SelfHandling {

	public $req;

	public function __construct(array $req){
		$this->req = $req;
	}

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle(){

		return frame_menu_nav::create([
			'title' 	=> $this->req['title'],
			'parent_id'	=> $this->req['idparent'],
			'slug' 		=> $this->req['slug'],
			'class' 	=> $this->req['class'],
			'status' 	=> $this->req['status'],
			'id' 		=> $this->req['id'],
			'ket' 		=> $this->req['ket'],
			'guest' 	=> empty($this->req['guest']) ? 0 : $this->req['guest'],
			'auth' 		=> empty($this->req['auth']) ? 0 : $this->req['auth']
		]);

	}

}
