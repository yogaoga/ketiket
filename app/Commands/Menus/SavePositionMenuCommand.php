<?php namespace App\Commands\Menus;

use App\Commands\Command;
use App\Models\frame_menu_nav;

use Illuminate\Contracts\Bus\SelfHandling;

class SavePositionMenuCommand extends Command implements SelfHandling {

	public $req;

	public function __construct(array $req){
		$this->req = $req;
	}

	
	public function handle(){
		$data = json_decode($this->req['update']);
		//dd($data);

		$seri = 1;
		foreach ($data as $menus) {
			
			$menu = frame_menu_nav::find($menus->id);
			$menu->parent_id 	= 0;
			$menu->seri 		= $seri;
			$menu->save();

			

			// Children 1
			if(!empty($menus->children)){
				foreach($menus->children as $child){
					$menu = frame_menu_nav::find($child->id);
					$menu->parent_id 	= $menus->id;
					$menu->seri 		= $seri;
					$menu->save();
					$seri++;

					// Children 2
					if(!empty($child->children)){
						foreach($child->children as $child2){
							$menu = frame_menu_nav::find($child2->id);
							$menu->parent_id 	= $child->id;
							$menu->seri 		= $seri;
							$menu->save();
							$seri++;


							// Children 3
							if(!empty($child2->children)){
								foreach($child2->children as $child3){
									$menu = frame_menu_nav::find($child3->id);
									$menu->parent_id 	= $child2->id;
									$menu->seri 		= $seri;
									$menu->save();
									$seri++;

									// Children 4
									if(!empty($child3->children)){
										foreach($child3->children as $child4){
											$menu = frame_menu_nav::find($child4->id);
											$menu->parent_id 	= $child3->id;
											$menu->seri 		= $seri;
											$menu->save();
											$seri++;
										}
									}
								}
							}

						}
					}


				}
			}

			$seri++;
		}

	}

}
