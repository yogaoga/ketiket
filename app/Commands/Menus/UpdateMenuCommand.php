<?php namespace App\Commands\Menus;

use App\Commands\Command;
use App\Models\frame_menu_nav;
use Illuminate\Contracts\Bus\SelfHandling;

class UpdateMenuCommand extends Command implements SelfHandling {

	public $req;
	public function __construct(array $req){
		$this->req = $req;
	}

	
	public function handle(){

		if(!empty($this->req['del'])){
			$id_menu = $this->req['id'];

			$menu = frame_menu_nav::find($id_menu);
			$menu->delete();

			frame_relasi_akses_user::whereFrame_menu_nav_id($id_menu)->delete();
		}

		
		$menu = frame_menu_nav::find($this->req['id']);

		$menu->title 		= trim($this->req['title']);
		$menu->parent_id	= trim($this->req['idparent']);
		$menu->slug			= trim($this->req['slug']);
		$menu->class 		= trim($this->req['class']);
		$menu->status 		= trim($this->req['status']);
		$menu->class_id		= trim($this->req['id']);
		$menu->ket 			= trim($this->req['ket']);
		$menu->auth 		= empty($this->req['auth']) ? 0 : $this->req['auth'];
		$menu->guest 		= empty($this->req['guest']) ? 0 : $this->req['guest'];
		$menu->save();

	}

}
