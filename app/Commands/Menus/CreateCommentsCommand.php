<?php namespace App\Commands\Menus;

use App\Commands\Command;
use App\Models\comment;

use Illuminate\Contracts\Bus\SelfHandling;

class CreateCommentsCommand extends Command implements SelfHandling {

	public $req;

	public function __construct(array $req){
		$this->req = $req;
	}

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle(){

		 comment::create([
			'comment' => $this->req['message'],
			'email' => $this->req['email'],
			'nama' => $this->req['nama'],
			'status' => 1,
			'travel_id' => $this->req['travel_id']

			]);

		return $this->req;
	}
}