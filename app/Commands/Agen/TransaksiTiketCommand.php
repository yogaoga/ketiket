<?php namespace App\Commands\Agen;

use App\Models\transaksi;

use App\Commands\Command;

use Illuminate\Contracts\Bus\SelfHandling;

class TransaksiTiketCommand extends Command implements SelfHandling {

	public $req;

	public function __construct(array $req){
		$this->req = $req;
	}

	public function handle(){
		return transaksi::create([
			'users_id' => \Auth::user()->id,
			'reservasi_id' => $this->req['id'],
			'tgl_transaksi' => date('Y-m-d'),
			'nominal' => $this->req['total'],
			'jenisbayar' => 1,
			'penambahan' => $this->req['total'],
			'total' => $this->req['penumpang'],
			'status_transaksi' => 1,
			'keterangan' => $this->req['keterangan'],
			'pengurangan' => 0
		]);
	}

}
