<?php namespace App\Commands\Agen;


use App\Models\reservasi;
use App\Models\reservasi_detail;

use App\Commands\Command;

use Illuminate\Contracts\Bus\SelfHandling;

class CreateCodeBookingCommand extends Command implements SelfHandling {

	public $req;

	public function __construct(array $req){
		$this->req = $req;
	}

	public function handle(){
		
		/* Kode booking */
		$i 		= substr(\Agen::data()->nama_travel, 0,2);
		$kode 	= strtoupper($i) . strtoupper(base_convert(rand(1111,9999), 10, 36));
		/* End Kode booking */

		$reservasi = reservasi::create([
			'users_id' => \Auth::user()->id,
			'tgl_reservasi' => date('Y-m-d', strtotime($this->req['tgl_reservasi'])),
			'tgl_limit' => date('Y-m-d H:i:s'),
			'tgl_bayar' => date('Y-m-d H:i:s'),
			'kode_booking' => $kode,
			'status_reservasi' => 0,
			'reservasi_by' => \Agen::data()->id,
			'hubs_id' => $this->req['hubs_id'],
			'route_detail_id' => $this->req['route_detail_id']
		]);

		$details = [];
		foreach($this->req['kode'] as $key => $val){
			$details[] = reservasi_detail::create([
				'users_id' => \Auth::user()->id,
				'reservasi_id' => $reservasi->id,
				'nama_penumpang' => $this->req['nama'][$key],
				'kursi' => $val,
				'harga' => $this->req['harga'],
				'discount' => $this->req['diskon'],
				'tlp' => $this->req['hp'][$key]
			]);
		}

		return [
			'reservasi' => $reservasi,
			'detail' => $details
		];

	}

}
