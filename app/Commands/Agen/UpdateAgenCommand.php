<?php namespace App\Commands\Agen;

use App\Models\travel;

use App\Commands\Command;

use Illuminate\Contracts\Bus\SelfHandling;

class UpdateAgenCommand extends Command implements SelfHandling {

	public $req;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(array $req){
		$this->req = $req;
	}

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle(){
		return travel::find(\Agen::data()->id)->update([
			'nama_travel' => $this->req['nama_travel'],
			'information' => $this->req['information'],
			'hint' => $this->req['hint'],
			'alamat' => $this->req['alamat'],
			'no_tlp' => $this->req['no_tlp'],
			'provinsi_id' => $this->req['provinsi'],
			'website' => $this->req['website']
		]);

	}

}
