<?php namespace App\Commands\Agen;

use App\Models\relasi_travel;
use App\Commands\Command;

use Illuminate\Contracts\Bus\SelfHandling;

class AddOperatorAgenCommand extends Command implements SelfHandling {

	public $req;
	public function __construct($req){
		$this->req = $req;
	}

	public function handle(){
		$user = relasi_travel::firstOrCreate([
			'user_id' => $this->req['id'],
			'travel_id' => \Agen::data()->id
		]);

		$user->update([
			'level' => 2,
			'status' => 0,
			'tipe' => 2,
			'token' => md5($this->req['id'] . \Agen::data()->id)
		]);

		return $user;
	}

}
