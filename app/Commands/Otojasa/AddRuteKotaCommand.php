<?php namespace App\Commands\Otojasa;

use App\Models\route_detail;
use App\Models\harga_tiket;
use App\Models\hubs;

use App\Commands\Command;

use Illuminate\Contracts\Bus\SelfHandling;

class AddRuteKotaCommand extends Command implements SelfHandling {

	public $req;

	public function __construct(array $req){
		$this->req = $req;
	}

	
	public function handle(){
		
		$rute = route_detail::create([
			'depart' => $this->req['depart'],
			'destination' => $this->req['destination'],
			'routes_id' => $this->req['id']
		]);

		harga_tiket::create([
			'route_detail_id' => $rute->id,
			'harga' => $this->req['harga']
		]);

		if(count($this->req['time']) > 0){
			foreach($this->req['time'] as $key => $val){
				hubs::create([
					'route_detail_id' => $rute->id,
					'kota_id' => $this->req['depart'],
					'lokasi' => $this->req['boardingpoint'][$key],
					'waktu' => $val
				]);
			}
		}

	}

}
