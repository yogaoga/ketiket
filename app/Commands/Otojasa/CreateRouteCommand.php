<?php namespace App\Commands\Otojasa;

use App\Models\hubs;
use App\Models\routes;
use App\Models\route_detail;
use App\Models\harga_tiket;
use App\Models\relasi_facility;

use App\Commands\Command;

use Illuminate\Contracts\Bus\SelfHandling;

class CreateRouteCommand extends Command implements SelfHandling {

	public $req;

	public function __construct(array $req){	
		$this->req = $req;
	}

	
	public function handle(){
		
		// Membaut nama hari
		$day_route = '';
		if(isset($this->req['day_route']) && is_array($this->req['day_route'])){
			foreach ($this->req['day_route'] as $day) {
				$day_route .= $day . ',';
			}
		}

		// Insert data ke Tavel Routes
		$route = routes::create([
			'kode_routes' 		=> $this->req['kode_routes'],
			'depart' 			=> $this->req['depart'],
			'destination' 		=> $this->req['destination'],
			'day_route' 		=> rtrim($day_route, ','),
			'tgl_aktif' 		=> date('Y-m-d', strtotime($this->req['tgl_aktif'])),
			'travel_id' 		=> \Travel::data()->id,
			'tgl_limit' 		=> date('Y-m-d', strtotime($this->req['tgl_limit'])),
			'template_mobile' 	=> $this->req['template_mobile'],
			'kelas' 			=> $this->req['kelas'],
			'keterangan' 		=> $this->req['keterangan'],
			'created_by' 		=> \Auth::user()->id
		]);

		foreach($this->req['departs'][0] as $from){

			foreach($this->req['destinations'][0] as $to ){

				// Insert data ke tabel routes_detail 
				$detail = route_detail::create([
					'routes_id' 		=> $route->id,
					'depart' 			=> $from,
					'destination' 		=> $to,
					// 'waktu' 			=> $this->req['time'][0]['time'][$from],
					// 'boarding_point' 	=> $this->req['boardingpoint'][0]['boardingpoint'][$from]
				]);

				// Boarding point
				foreach($this->req['boardingpoint'][0]['boardingpoint'][$from] as $key => $bp){
					$time = $this->req['boardingpoint'][0]['time'][$from][$key];
					hubs::create([
						'route_detail_id' => $detail->id,
						'kota_id' => $from,
						'lokasi' => $bp,
						'waktu' => $time
					]);
				}

				// Insert harga ke tabel harga_tiket
				harga_tiket::create([
					'route_detail_id' => $detail->id,
					'harga' => $this->req['Prices'][0]['harga'][$from . '_' . $to]
				]);

			}

		}


		// Insert Facilitys
		if(isset($this->req['facilitys']) && count($this->req['facilitys'][0]['facilitys']) > 0){
			foreach($this->req['facilitys'][0]['facilitys'] as $facility){
				relasi_facility::create([
					'routes_id' => $route->id,
					'ref_facility_id' => $facility
				]);
			}
		}

		return $this->req;
	}

}
