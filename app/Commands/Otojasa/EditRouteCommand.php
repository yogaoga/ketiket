<?php namespace App\Commands\Otojasa;

use App\Models\hubs;
use App\Models\harga_tiket;

use App\Commands\Command;

use Illuminate\Contracts\Bus\SelfHandling;

class EditRouteCommand extends Command implements SelfHandling {

	public $req;
	
	public function __construct($req){
		$this->req = $req;
	}

	public function handle(){
		
		harga_tiket::whereRoute_detail_id($this->req['id'])
		->update([
			'harga' => $this->req['harga']
		]);

		if(count($this->req['time']) > 0){
			foreach($this->req['time'] as $key => $val){
				hubs::create([
					'route_detail_id' => $this->req['id'],
					'kota_id' => $this->req['kota'],
					'lokasi' => $this->req['boardingpoint'][$key],
					'waktu' => $val
				]);
			}
		}

	}

}
