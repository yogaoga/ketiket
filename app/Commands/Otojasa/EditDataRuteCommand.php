<?php namespace App\Commands\Otojasa;

use App\Models\routes;
use App\Commands\Command;

use Illuminate\Contracts\Bus\SelfHandling;

class EditDataRuteCommand extends Command implements SelfHandling {

	public $req;

	public function __construct(array $req){
		$this->req = $req;
	}

	
	public function handle(){
		
		$hari = '';
		foreach($this->req['day_route'] as $day){
			$hari .= $day . ',';
		}

		return routes::find($this->req['id'])->update([
			// 'depart' 		=> $this->req['depart'],
			// 'destination' 	=> $this->req['destination'],
			'tgl_aktif' 	=> date('Y-m-d', strtotime($this->req['tgl_aktif'])),
			'tgl_limit' 	=> date('Y-m-d', strtotime($this->req['tgl_limit'])),
			'day_route' 	=> rtrim($hari, ','),
			'kelas' 		=> $this->req['kelas'],
			'template_mobile' => $this->req['template_mobile'],
			'keterangan' 	=> $this->req['keterangan'],
		]);

	}

}
