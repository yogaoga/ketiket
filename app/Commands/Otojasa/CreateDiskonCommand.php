<?php namespace App\Commands\Otojasa;

use App\Models\persenharga;

use App\Commands\Command;

use Illuminate\Contracts\Bus\SelfHandling;

class CreateDiskonCommand extends Command implements SelfHandling {

	public $req;
	public function __construct(array $req){
		$this->req = $req;
	}

	public function handle(){
		foreach($this->req['discount'] as $id => $val){
			if(!empty($val) && !empty($this->req['tgl_aktif'][$id]) && !empty($this->req['tgl_limit'][$id])){
				$diskon = persenharga::firstOrCreate([
					'route_detail_id' => $id
				]);
				$diskon->update([
					'tgl_aktif' => date('Y-m-d', strtotime($this->req['tgl_aktif'][$id])),
					'tgl_limit' => date('Y-m-d', strtotime($this->req['tgl_limit'][$id])),
					'persen' => $val
				]);
			}
		}
	}

}
