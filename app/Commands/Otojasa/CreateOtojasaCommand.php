<?php namespace App\Commands\Otojasa;

use App\Models\travel;
use App\Models\relasi_travel;
use App\Commands\Command;

use Illuminate\Contracts\Bus\SelfHandling;

class CreateOtojasaCommand extends Command implements SelfHandling {

	public $req;

	public function __construct(array $req){
		$this->req = $req;
	}

	
	public function handle(){

		$slug = str_slug($this->req['nama_travel']);
		$travel = travel::create([
			'nama_travel' => $this->req['nama_travel'],
			'information' => $this->req['information'],
			'hint' => $this->req['hint'],
			'alamat' => $this->req['alamat'],
			'no_tlp' => $this->req['no_tlp'],
			'provinsi_id' => $this->req['provinsi_id'],
			'website' => $this->req['website'],
			'slug' => $slug,
			'tipe' => $this->req['tipe']
		]);

		relasi_travel::firstOrCreate([
			'user_id' => \Auth::user()->id,
			'travel_id' => $travel->id,
			'level' => 1,
			'token' => bin2hex($this->req['nama_travel'])
		]);

		return $travel;
	}

}
