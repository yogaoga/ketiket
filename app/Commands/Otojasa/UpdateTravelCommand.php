<?php namespace App\Commands\Otojasa;

use App\Models\travel;

use App\Commands\Command;

use Illuminate\Contracts\Bus\SelfHandling;

class UpdateTravelCommand extends Command implements SelfHandling {

	public $req;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(array $req){
		$this->req = $req;
	}

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle(){
		return travel::find(\Travel::data()->id)->update([
			'nama_travel' => $this->req['nama_travel'],
			'information' => $this->req['information'],
			'hint' => $this->req['hint'],
			'alamat' => $this->req['alamat'],
			'no_tlp' => $this->req['no_tlp'],
			'provinsi_id' => $this->req['provinsi'],
			'website' => $this->req['website']
		]);
	}

}
