<?php namespace App\Commands\Reservasi;

use App\Commands\Command;
use App\Models\reservasi;
use App\Models\reservasi_detail;
use App\User;


use Illuminate\Contracts\Bus\SelfHandling;

class CreateReservasiCommand extends Command implements SelfHandling {

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */

	public $req;

	public function __construct(array $req)
	{
		$this->req = $req;
	}

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle()
	{
		// Insert Reservasi




	/*	$user = User::create([
			'name' => $this->req['nm_pemesan'],
			'email' => $this->req['email'],
			'alamat' => $this->req['alamat'],
			'hp' => $this->req['no_telp']
			]); */

		$reservasi = reservasi::create([
			'users_id' => 1,
			'tgl_reservasi' => date('Y-m-d',strtotime($this->req['tgl_reservasi'])),
			'kode_booking' => $this->req['kode_booking'],
			'status_reservasi' => 0,
			'hubs_id' => $this->req['hubs_id'],
			'route_detail_id' => $this->req['route_detail_id']

			]);

		for ($i=0; $i < count($this->req['kode']) ; $i++) { 

			reservasi_detail::create([
				'reservasi_id' => $reservasi->id,
				'nama_penumpang' => $this->req['nama'][$i],
				'kursi' => $this->req['kode'][$i],
				'harga' => $this->req['harga'],
				'status_bayar'=> 0,
				'tlp' => $this->req['hp'][$i]
				]);

		}



		return $this->req;



	}

}
