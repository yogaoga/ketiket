<?php

Route::get('/test', function(){
	return view('emails.emailKodeBooking');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@show');

Route::get('home', 'HomeController@index');

//CAR RENT BY SINTA
Route::get('/car-rent', 'CarRentController@index');
Route::get('/load-car-rent/{page}/{limit}', 'CarRentController@show');
Route::get('/input-car-rent/{edit}/{id}', 'CarRentController@create');
Route::post('/input-car-rent', 'CarRentController@store');
Route::post('/status-car-rent', 'CarRentController@updateStatus');
Route::get('/foto-car-rent/{idrent}', 'CarRentController@editFoto');
Route::post('/foto-car-rent', 'CarRentController@updateFoto');
Route::post('/primer-foto-car-rent', 'CarRentController@updateFotoPrimer');
Route::post('/delet-foto-car-rent', 'CarRentController@destroyFoto');
Route::get('/harga-car-rent/{id}/{edit}/{idcar}', 'CarRentController@editHarga');
Route::post('/harga-car-rent', 'CarRentController@updateHarga');
Route::post('/delet-harga-car-rent', 'CarRentController@destroyHarga');

Route::get('/car-book', 'CarBookController@show');
Route::get('/extra-car-book', 'CarBookController@extra');
Route::get('/biodata-car-book', 'CarBookController@biodata');
Route::post('/biodata-car-book', 'CarBookController@store');
Route::get('/finish-car-book/{no_invoice}', 'CarBookController@finish');
Route::get('/print-car-book/{no_invoice}', 'CarBookController@printInvoice');
Route::get('/lamasewa-car-book/{tgl}/{tgl1}/{cartime}', 'CarBookController@lamasewa');

//DAFTAR BOOKING 
Route::get('/load-car-book/{page}/{status}/{fieldsearch}/{search?}','CarRentController@loadBook');    
/////////////////////////////
/* Daftar Booking */

Route::get('/new-book','CarRentController@newBooking');
Route::get('/view_invoice/{id}/{noinvoice}/{status}','CarRentController@ViewInvoice');
Route::post('/saveBooking','CarRentController@saveBooking');

/* end yoga@valdlabs.com */

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

/* @hexters */

Route::controller('/menu', 'MenuSetting\MenuSettingController');
Route::controller('/ajax', 'AjaxController');
Route::controller('/user', 'UserController');
Route::controller('/otojasa', 'OtojasaController');
Route::controller('/agen', 'AgenController');
Route::controller('/pages', 'PagesSettingControll');

Route::get('/aktivasi', 'Auth\aktivasiController@index');
Route::get('/invite/{key}', 'PageController@confirmasiusertravel');
Route::get('/create-otojasa', 'PageController@createotojasa');
Route::post('/create-otojasa', 'PageController@saveotojasa');

/* End @hexters */

/* yoga@valdlabs.com */

Route::controller('/reservasi','ReservasiController');
Route::post('/saveComment','PageController@saveComment');

Route::get('gotofb',function(){
	return \Socialize::with('facebook')->redirect();
});
Route::get('showfb',function(){
	$user = \Socialize::with('facebook')->user();
	dd($user);
});


/* @Hexters
	Menampilkan detail rute
	ex : http://localhost:8000/cipaganti/rute/CI-00
	SEO : true
*/
Route::get('/{travel}/route/{kode_rute}', 'PageController@detailrute');
Route::get('/tentang-kami', 'PagesSettingControll@about_me');
Route::get('/privacy-policy', 'PagesSettingControll@privacy_policy');
Route::get('/disclaimer', 'PagesSettingControll@disclaimer');

/*
	============================= CATATAN ===========================
	@Yoga Buat halaman profile untuk Travel, tapi untuk status yang masih tidak akrif / 0 hanya user dengan level admin (1,2)
	dan orang yang mendaftarkan level saja

	dari setiap travel itu hanya ada 1 user admin, sisanya hanya operator.
	Yang menbedakan operator atau admin terletak pada tabel database relasi_travel
	+ relasi_travel
		- starus 1 berarti aktif
		- level 1:admin|2:operator
	
	Gunakan Controller PageController dengan method detailtravel yang di ambil berdasarkan field slug pada tabel travel
	+ travel
		- slug <-- fild yang berfusngsi sebagai ID untuk pemanggilan data travel "{travel} == slug"

	akses URL http:://ketiket.com/nama_travel

*/

Route::get('/{travel}', 'PageController@detailtravel');

