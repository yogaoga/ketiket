<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Socialize;

class SocialController extends Controller {

	//


	public function getFacebook(){
		return Socialize::with('facebook')->redirect();
	}

	public function facebook(){
		$user = Socialize::with('facebook')->user();

		print_r($user);
	}

}
