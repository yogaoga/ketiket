<?php namespace App\Http\Controllers;

use App\User;
use App\Http\Requests;
use App\Events\User\UploadAvatarEvent;
use App\Commands\User\ChangeAvatarCommand;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class UserController extends Controller {

	public function getEdit(){

		return view('User.EditUser');
	}

	public function postEdit(Request $req){
		User::find(\Auth::user()->id)->update($req->all());
		return redirect()->back()->withNotif([
			'label' => 'success',
			'err' => 'Pembaharuan akun berhasil tersimpan'
		]);
	}

	public function postAvatar(Request $req, UploadAvatarEvent $upload){
		//melakukan upload file
		$file = $upload->file($_FILES['avatar']['tmp_name']);

		// Melakukan update pada database
		$this->dispatch(
			new ChangeAvatarCommand($file)
		);
		
		return redirect()->back();
	}

}
