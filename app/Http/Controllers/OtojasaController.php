<?php namespace App\Http\Controllers;

/* -- @author hexters@gmail.com -- */

use App\Models\travel;
use App\Models\routes;
use App\Models\provinsi;
use App\Models\kab_kota;
use App\Models\persenharga;
use App\Models\route_detail;
use App\Models\ref_facility;
use App\Models\template_mobil;
use App\Models\relasi_facility;

use App\Commands\Otojasa\EditRouteCommand;
use App\Commands\Otojasa\CreateRouteCommand;
use App\Commands\Otojasa\EditDataRuteCommand;
use App\Commands\Otojasa\CreateOtojasaCommand;
use App\Commands\Otojasa\AddRuteKotaCommand;
use App\Commands\Otojasa\UpdateTravelCommand;
use App\Commands\Otojasa\CreateDiskonCommand;

use App\Events\Otojasa\UploadLogoTravelEvent;
use App\Events\Otojasa\NotifikasiCreatedTravelEvent;
use App\Events\Otojasa\TravelNotifikasiWasActivated;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\Otojasa\CreateOtojasaRequest;

use Illuminate\Http\Request;

class OtojasaController extends Controller {

	public function __construct(){
		$this->middleware('auth');
		$this->middleware('authTravel');
	}
	public function getIndex(){return redirect()->back();}

	/*
	Digunakan untuk membuat Travel dan Agen
	@access public
	@url otojasa/create
	*/
	// public function getCreate(){

	// 	if(\Travel::check())
	// 		return redirect('/home')->withNotif([
	// 			'label' => 'info',
	// 			'err' => 'Anda sudah terdaftar sebagai admin dari ' . \Travel::data()->nama_travel
	// 		]);

	// 	return view('Otojasa.CreateOtojasa', [
	// 		'provinsi' => Provinsi::orderBy('nm_provinsi', 'asc')->get()
	// 	]);
	// }

	/*
	proses penyimpanan data Travel / agen yang baru
	*/
	// public function postCreate(CreateOtojasaRequest $req){
	// 	// Menimpan data Travel Baru
	// 	$travel = $this->dispatch(
	// 		new CreateOtojasaCommand($req->all())
	// 	);
		
	// 	// Upload Logo travel
	// 	$file = new UploadLogoTravelEvent($travel);
	// 	travel::find($travel->id)->update([
	// 		'logo' => $file->upload()
	// 	]);
		
	// 	// Mengirim email notifikasi ke user dan admin
	// 	new NotifikasiCreatedTravelEvent($travel);

	// 	return redirect('/home')->withNotif([
	// 		'label' => 'info',
	// 		'err' => 'Pengajuan Otojasa anda berhasil terkirim. Periksa Email anda untuk melihat keterangan lebih lanjut.'
	// 	]);
	// }

	
	/*
	Dashboard dari otojasa
	@access Admin & operator Travel
	*/
	public function getDashboard(){
		
		if(\Travel::guest())
			return redirect('/home');

		return view('Otojasa.DashboardOtojasa');
	}

	/*
	Daftar otojasa yang baru
	@access Superadmin dan admin ketiket
	*/
	public function getNew(){
		if(\Auth::user()->frame_level_user_id > 2)
			return redirect()->back()->withNotif([
				'label' => 'info',
				'err' => 'Anda tidak memiliki akses ke halaman tersebut'
			]);

		$src = isset($_GET['src']) ? $_GET['src'] : "";
		$new = travel::listnewtravel($src)->paginate(20);
		return view('Otojasa.NewOtojasa', [
			'items' => $new,
			'src' => $src
		]);
	}
	
	
	/*
	Mengaktifkan Otojasa
	@access Superadmin dan admin ketiket
	*/
	public function postActive(Request $req){
		$travel = travel::find($req->id);
		$travel->status = 1;
		$travel->save();

		$email = new TravelNotifikasiWasActivated($travel);
		$email->sendEmail();
		
		return redirect()->back()->withNotif([
			'label' => 'info',
			'err' => $travel->nama_travel . ' telah berhasil dikatifkan'
		]);
	}

	/*
	daftar user yang ada di otojasa
	@access Admin & operator Travel
	*/
	public function getUsers(){

		if(\Travel::data()->level > 1)
			return redirect('/otojasa/dashboard');

		return view('Otojasa.UsersOtojasa', [
			'users' => \Travel::users(20)
		]);
	}

	/*
	Digunakan untuk menampilkan daftar Rute perjalanan
	@access admin Travel & operator Travel
	@url /otojasa/route
	*/
	public function getRoute(Request $req){

		if(!empty(\Session::get('create_route')))
			\Session::forget('create_route');

		$src = isset($_GET['src']) ? $req->src : '';
		return view('Otojasa.Route', [
			'routes' => routes::daftar($src)->paginate(10),
			'src' => $src
		]);
	}

	////////////////////////////////////// CREATE ROUTE /////////////////////////////////

	/*
	1. Pembuatan rute perjalanan
	@access Admin Travel
	*/
	public function getCreateroute(){
		
		if(!empty(\Session::get('create_route')))
			\Session::forget('create_route');

		return view('Otojasa.Route.CreateRoute',[
			'provinsi' => provinsi::all(),
			'mobils' => template_mobil::all()
		]);
	}

	/*
	2. Penambahan daftar rute perjalanan
	@access Admin Travel
	*/
	public function postRoutecity(Request $req){

		$this->validate($req, [
	        'day_route' => 'required',
	        'kode_routes' => 'required|unique:routes',
	    ]);

		\Session::put('create_route', $req->all());
		return view('soon', [
			'from' => kab_kota::find($req->depart)->nm_kab_kota,
			'to' => kab_kota::find($req->destination)->nm_kab_kota,

			'idfrom' => $req->depart,
			'idto' => $req->destination
		]);
	}
	public function getRoutecity(){return redirect('/otojasa/route');}

	/*
	3. Boarding Point
	@access Admin Travel
	*/
	public function postBoardingpoint(Request $req){
		\Session::push('create_route.departs', $req->add_depart);
		\Session::push('create_route.destinations', $req->add_destination);
		return view('soon', [
			'departs' => $req->add_depart
		]);
	}
	public function getBoardingpoint(){return redirect('/otojasa/route');}

	/*
	3. Set Harga
	@access Admin Travel
	*/
	public function postPriceroute(Request $req){
		
		\Session::push('create_route.boardingpoint', $req->all());
		//dd(\Session::get('create_route'));
		return view('soon', [
			'data' => \Session::get('create_route')
		]);
	}
	public function getPriceroute(){return redirect('/otojasa/route');}

	/*
	4. Waktu perkiraan
	@access Admin Travel
	//////////////////////////////////////////////////
	public function postTime(Request $req){
		
		\Session::push('create_route.Prices', $req->all());
		
		return view('Otojasa.Route.Time', [
			'data' => \Session::get('create_route')
		]);
	}
	public function getTime(){return redirect('/otojasa/route');}
	//////////////////////////////////////////////////
	*/

	/*
	4. Menambahkan Fasilitas pada kendaraan
	@access Admin Travel
	*/
	public function postFacilitys(Request $req){
		\Session::push('create_route.Prices', $req->all());

		$items = ref_facility::orderBy('id', 'desc')->get();
		return view('soon', [
			'items' => $items
		]);
	}
	public function getFacilitys(){return redirect('/otojasa/route');}

	/*
	5. Penyimpanan data rute
	@access Admin Travel
	*/
	public function postCreateroute(Request $req){
		
		if(count($req->facilitys[0]) > 0)
			\Session::push('create_route.facilitys', $req->all());
			
		// FILE --> App\Commands\Otojasa\CreateRouteCommand;
		$this->dispatch(
			new CreateRouteCommand(\Session::get('create_route'))
		);

		return redirect('/otojasa/route')->withNotif([
			'label' => 'info',
			'err' => 'Rute ' . $req->kode_routes . ' berhasil dibuat.'
		]);
	}

	///////////////////////////// END CREATE ROUTE /////////////////////////////////////
	/*
	Melakukan perubahan pada data rute
	@access Admin Travel
	*/
	public function getEditdataroute($id){
		$routes = routes::detailbysession($id);

		/* Permission */
		if($routes->count() == 0 || \Travel::data()->level == 2)
			return redirect('/home')->withNotif([
				'label' => 'success',
				'err' => 'Rute tidak ditemukan ?'
			]);

		$route = $routes->first();
		return view('soon', [
			'route' => $route,
			'mobils' => template_mobil::all(),
			'hari' => explode(',', $route->day_route)
		]);
	}
	/*
	Penyimpanan perbaharuan data Rute
	*/
	public function postEditdataroute(Request $req){
		$this->validate($req, [
			// 'depart' 		=> 'required',
			// 'destination' 	=> 'required',
			'tgl_aktif' 	=> 'required',
			'tgl_limit' 	=> 'required',
			'day_route' 	=> 'required',
			'kelas' 		=> 'required',
			'template_mobile' => 'required'
		]);

		$this->dispatch(
			new EditDataRuteCommand($req->all())
		);

		return redirect()->back()->withNotif([
			'label' => 'success',
			'err' => 'Pembaharuan data Rute telah berhasil.'
		]);

	}

	/*
	Pembaharuan data Rute perjalanan
	*/
	public function getEditroutes($id){
		$route = route_detail::listroute($id);

		/* Permission */
		if($route->count() == 0 || \Travel::data()->level == 2)
			return redirect('/home')->withNotif([
				'label' => 'success',
				'err' => 'Rute tidak ditemukan ?'
			]);

		$routes = $route->get();
		/* -- Boarding Points --*/
		$pools = [];
		foreach($routes as $r){
			$pools[$r->id] = route_detail::find($r->id)->boardingpoint()->get();
		}
		return view('Otojasa.Route.EditRoute', [
			'routes' => $routes,
			'pools' => $pools,
			'provinsi' => provinsi::all(),
			'id' => $id
		]);
	}

	public function postEditroutes(Request $req){
		$this->dispatch(
			new EditRouteCommand($req->all())
		);

		return redirect()->back()->withNotif([
			'label' => 'success',
			'err' => 'Rute berhasil diperbaharui'
		]);
	}

	public function postAddrutekota(Request $req){
		$this->dispatch(
			new AddRuteKotaCommand($req->all())
		);

		return redirect()->back()->withNotif([
			'label' => 'success',
			'err' => 'Rute berhasil ditemukan.'
		]);
	}
	
	/*
	Perubahan Pasilitas
	*/
	public function getEditfacility($id){
		$routes = routes::whereTravel_id(\Travel::data()->id)->whereId($id);
		/* -- Hak akases rute -- */
		if($routes->count() == 0)
			return redirect()->back()->withNotif([
				'label' => 'success',
				'err' => 'Data Pasilitas tidak ditemukan ?.'
			]);

		$route = $routes->first();
		/* -- Pasilitas -- */
		$items = ref_facility::orderBy('id', 'desc')->get();
		$pasilitas = routes::find($route->id)->facility()->get();
		$facilitys = [];
		foreach($pasilitas as $f){
			$facilitys[] = $f->ref_facility_id;
		}
		
		return view('Otojasa.Route.EditFacility', [
			'items' => $items,
			'route' => $route,
			'facilitys' => $facilitys
		]);
	}

	public function postEditfacility(Request $req){
		relasi_facility::whereRoutes_id($req->id)->delete();
		foreach ($req->facilitys as $id) {
			relasi_facility::create([
				'ref_facility_id' => $id,
				'routes_id' => $req->id
			]);
		}

		return redirect()->back()->withNotif([
				'label' => 'success',
				'err' => 'Pasilitas berhasil diperbaharui.'
			]);

	}

	/* 
	Discount Agen 
	@access Admin, Operator Travel
	*/
	public function getDiscount(){
		$src = isset($_GET['src']) ? $_GET['src'] : '';
		$persen = persenharga::me($src)->paginate(10);
		return view('soon', [
			'rutes' => $persen,
			'src' => $src
		]);
	}

	/*
	Membuat Discount
	@access Admin, Operator Travel
	*/
	public function getCreatediscount($id){
		$route = routes::find($id);
		/* -- Filter -- */
		if(empty($route) || $route->travel_id != \Travel::data()->id)
			return redirect()->back()->withNotif([
				'label' => 'success',
				'err' => 'Rute tidak ditemukan'
			]);

		$details = $route->detail()
		->join('kab_kota AS a', 'a.id', '=', 'route_detail.depart')
		->join('kab_kota AS b', 'b.id', '=', 'route_detail.destination')
		->join('harga_tiket', 'harga_tiket.route_detail_id', '=', 'route_detail.id')
		->leftJoin('persenharga', 'persenharga.route_detail_id', '=', 'route_detail.id')
		->where('route_detail.status',1)
		->select(
			'route_detail.*', 
			'a.nm_kab_kota AS nm_depart', 
			'b.nm_kab_kota AS nm_destination', 
			'harga_tiket.harga', 
			'persenharga.persen', 
			'persenharga.tgl_aktif AS tgl_d_aktif', 
			'persenharga.tgl_limit AS tgl_d_limit')
		->get();
		
		$result = [];
		foreach($details as $detail){
			if($detail->depart == $route->depart)
				$result['nm_depart'] = $detail->nm_depart;
			if($detail->destination == $route->destination)
				$result['nm_destination'] = $detail->nm_destination;
			$result['details'][] = $detail;
			$result['diskon'][$detail->id] = $detail->harga - (($detail->harga * (FLOAT) $detail->persen) / 100);
		}
		$result['route'] = $route;
		return view('Otojasa.Discount.CreateDiscount', $result);
	}
	/*
	Update Discount
	@access Admin, Operator Travel
	*/
	public function postUpdatediscount(Request $req){
		$this->dispatch(new CreateDiskonCommand($req->all()));
		return redirect()->back()->withNotif([
				'label' => 'success',
				'err' => 'Diskon berhasil disimpan'
			]);
	}


	/**
	* Setting Travel
	* @access Admin
	*/

	public function getSettings(){

		return view('Otojasa.Settings', [
			'provinsi' => provinsi::all()
		]);
	}
	/*Penyimpanan perubahan Travel*/
	public function postSettings(Request $req){
		$this->dispatch(new UpdateTravelCommand($req->all()));
		return redirect()->back()->withNotif([
			'label' => 'success',
			'err' => 'Travel berhasil diperbaharui'
		]);
	}

	public function postLogo(){
		$travel = \Travel::data();
		$file = new UploadLogoTravelEvent($travel);
		travel::find(\Travel::data()->id)->update([
			'logo' => $file->upload()
		]);
		return redirect()->back()->withNotif([
			'label' => 'success',
			'err' => 'Logo berhasil diperbaharui'
		]);
	}
}
