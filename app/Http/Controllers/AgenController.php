<?php namespace App\Http\Controllers;

use App\Models\hubs;
use App\Models\travel;
use App\Models\routes;
use App\Models\provinsi;
use App\Models\reservasi;
use App\Models\route_detail;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Events\Otojasa\UploadLogoTravelEvent;
use App\Commands\Agen\UpdateAgenCommand;
use App\Commands\Agen\TransaksiTiketCommand;
use App\Commands\Agen\CreateCodeBookingCommand;

use App\Events\Agen\SendKodeBookingToEmailEvent;

use Illuminate\Http\Request;

class AgenController extends Controller {

	public function __construct(){
		$this->middleware('auth');
		$this->middleware('authAgen');
	}

	// Daftar otojasa yang baru
	public function getNew(){
		$src = isset($_GET['src']) ? $_GET['src'] : "";
		$new = travel::listnewagen($src)->paginate(20);

		return view('Otojasa.NewOtojasa', [
			'items' => $new,
			'src' => $src
		]);
	}


	/*
	daftar user yang ada di otojasa
	@access Admin & operator Travel
	*/
	public function getUsers(){

		if(\Agen::data()->level > 1)
			return redirect('/otojasa/dashboard');

		return view('Agen.UsersAgen', [
			'users' => \Agen::users(20)
		]);
	}
	/////////////////////////////////////////////////////////////
	/*
		Halaman utama Agen
		@access Admin dan perator Agen
	*/
	public function getDashboard(){
		
		return view('Agen.Dashboard');
	}
	/*
		1. Pencarian Rute 
		@access Admin dan perator Agen
	*/
	public function getRoute(){

		if(\Session::get('booked'))
			\Session::forget('booked');

		return view('Agen.Route', [
			'provinsi' => provinsi::all()
		]);
	}

	/*
		2.Daftar Pencarian rute
		@access Admin dan perator Agen	
	*/
	public function getListroutes(Request $req){

		if(\Session::get('booked'))
			\Session::forget('booked');

		\Session::put('kembali', $req->all());

		if(empty($req->all()))
			return redirect('/agen/route');

		$res = route_detail::agensrc($req->all())->paginate(5);
		//dd($res);
		/* Boarding point */
		$pool 		= [];
		$facility 	= [];
		$hari		= [];
		foreach($res as $route){
			/* Boarding Point*/
			$pool[$route->id] = route_detail::find($route->id)->boardingpoint()->get();
			/* Facility*/
			$facility[$route->id] = routes::find($route->routes_id)->facility()
			->join('ref_facility', 'ref_facility.id', '=', 'relasi_facility.ref_facility_id')
			->get();
		}

		return view('Agen.Booking.ListRoutes', [
			'routes' => $res,
			'pools' => $pool,
			'req' => $req->all(),
			'facilitys' => $facility
		]);
	}

	/*
		3.Memilih Kursi
		@access Admin dan perator Agen	
	*/
	public function getSeat(Request $req){

		if(\Session::get('booked'))
			\Session::forget('booked');

		if(empty($req->all()))
			return redirect('/agen/route');

		$route = route_detail::Detailbyid($req->i);
		$pool = hubs::find($req->pool . $req->i);

		/* Back */
		$back = '';
		foreach (\Session::get('kembali') as $key => $value) {
			$back .= $key . '=' . $value . '&';
		}
		return view('Agen.Booking.seat', [
			'route' => $route,
			'pool' => $pool,
			'req' => $req->all(),
			'back' => rtrim($back, '&')
		]);
	}


	/*
		4. Pemilihan kursi
		@access Admin dan perator Agen	
	*/
	public function postSeat(Request $req){
		$result = $this->dispatch(new CreateCodeBookingCommand($req->all()));
		\Session::push('booked', $result);
		return redirect('/agen/kodebooking/');
	}

	/*
		5. Review kode Bookig
		@access Admin dan perator Agen	
	*/
	public function getKodebooking(){
		$data = \Session::get('booked');


		$rute = route_detail::join('routes', 'routes.id', '=', 'route_detail.routes_id')
		->join('travel', 'travel.id', '=', 'routes.travel_id')
		->join('kab_kota AS a', 'a.id', '=', 'route_detail.depart')
		->join('kab_kota AS b', 'b.id', '=', 'route_detail.destination')
		->where('route_detail.id', $data[0]['reservasi']->route_detail_id)
		->select(
			'travel.*',
			'a.nm_kab_kota AS nm_depart',
			'b.nm_kab_kota AS nm_destination',
			'routes.*'
			)
		->first();
		$bp = hubs::find($data[0]['reservasi']->hubs_id);

		/*Harga*/
		$total = 0;
		$kursi = '';
		foreach ($data[0]['detail'] as $item) {
			$total += $item->harga;
			$kursi .= $item->kursi . '&nbsp;';
		}
		
		$other = [
			'total' => $total,
			'kursi' => $kursi
		];

		return view('Agen.Booking.GetKode', [
			'data' => $data[0],
			'rute' => $rute,
			'pool' => $bp,
			'other' => $other
		]);
	}

	/* Menyimpan Konfirmasi pembayaran Tiket*/
	public function postSave(Request $req){

		/*Jika pembayaran sudah dilakuakn*/
		if($req->status_reservasi == 1){
			/* Validasi */
			$this->validate($req, [
				'total' => 'required|numeric'
			]);
			/* Execute pembayaran */
			$this->dispatch(new TransaksiTiketCommand($req->all()));
		}
		/* Perubahan Status pembayaran mendai 1:Pemumpang sudah melakukan pembayaran tapi belum naik */
		reservasi::find($req->id)->update([
			'status_reservasi' => $req->status_reservasi
		]);

		/* Redirect ke Step 6 */
		return redirect('/agen/finish?i=' . $req->id . '&b=' . $req->status_reservasi . '&_t=' . csrf_token());
	}

	/*
		6. Selesai
		@access Admin dan perator Agen
	*/
	public function getFinish(Request $req){
		$reservasi = reservasi::find($req->i);
		return view('Agen.Booking.Finish', [
			'reservasi' => $reservasi
		]);
	}

	public function postFinish(Request $req){
		$this->validate($req, [
			'email' => 'required'
		]);
		\Event::fire(new SendKodeBookingToEmailEvent($req->all()));
		return redirect()->back()->withNotif([
			'label' => 'success',
			'err' => 'Email berhasil terkirim'
		]);
	}

	public function getSettings(){
		return view('Agen.Settings', [
			'provinsi' => provinsi::all()
		]);
	}

	public function postSettings(Request $req){
		$this->dispatch(new UpdateAgenCommand($req->all()));
		return redirect()->back()->withNotif([
			'label' => 'success',
			'err' => 'Agen berhasil diperbaharui'
		]);
	}

	public function postLogo(){
		$travel = \Agen::data();
		$file = new UploadLogoTravelEvent($travel);
		travel::find(\Agen::data()->id)->update([
			'logo' => $file->upload()
		]);
		return redirect()->back()->withNotif([
			'label' => 'success',
			'err' => 'Logo berhasil diperbaharui'
		]);
	}

	public function getKendaraan(Request $req){

		return json_encode([
			'id' => $req->bus,

			/* -- Template Bus 32 seat 1 */
			'bus1' => '
				<div class="bodi-bus">
				    <div class="kendaraan-content">
				        <div class="row-kursi">
				            <div id="lineA-1" class="kursi"><a href="javascript:void(0);"><img src="' . asset('/images/sopir_24.png') . '" title="JURU MUDI"></a></div>
				        </div>

				        <div class="row-kursi">
				        	<div id="lineA-1" class="kursi"><a class="bus-booking" data-kode="4" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">4</span></a></div>
				            <div id="lineA-2" class="kursi"><a class="bus-booking" data-kode="3" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">3</span></a></div>
				            <div id="lineA-3" class="row-round"></div>
				            <div id="lineA-4" class="kursi"><a class="bus-booking" data-kode="2" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">2</span></a></div>
				            <div id="lineA-5" class="kursi"><a class="bus-booking" data-kode="1" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">1</span></a></div>
				        </div>

				        <div class="row-kursi">
				        	<div id="lineB-1" class="kursi"><a class="bus-booking" data-kode="8" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">8</span></a></div>
				            <div id="lineB-2" class="kursi"><a class="bus-booking" data-kode="7" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">7</span></a></div>
				            <div id="lineB-3" class="row-round"></div>
				            <div id="lineB-4" class="kursi"><a class="bus-booking" data-kode="6" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">6</span></a></div>
				            <div id="lineB-5" class="kursi"><a class="bus-booking" data-kode="5" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">5</span></a></div>
				        </div>
				        <div class="row-kursi">
				        	<div id="lineC-1" class="kursi"><a class="bus-booking" data-kode="12" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">12</span></a></div>
				            <div id="lineC-2" class="kursi"><a class="bus-booking" data-kode="11" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">11</span></a></div>
				            <div id="lineC-3" class="row-round"></div>
				            <div id="lineC-4" class="kursi"><a class="bus-booking" data-kode="10" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">10</span></a></div>
				            <div id="lineC-5" class="kursi"><a class="bus-booking" data-kode="9" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">9</span></a></div>
				        </div>
				        <div class="row-kursi">
				        	<div id="lineD-1" class="kursi"><a class="bus-booking" data-kode="16" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">16</span></a></div>
				            <div id="lineD-2" class="kursi"><a class="bus-booking" data-kode="15" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">15</span></a></div>
				            <div id="lineD-3" class="row-round"></div>
				            <div id="lineD-4" class="kursi"><a class="bus-booking" data-kode="14" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">14</span></a></div>
				            <div id="lineD-5" class="kursi"><a class="bus-booking" data-kode="13" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">13</span></a></div>
				        </div>
				        <div class="row-kursi">
				        	<div id="lineE-1" class="kursi"><a class="bus-booking" data-kode="20" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">20</span></a></div>
				            <div id="lineE-2" class="kursi"><a class="bus-booking" data-kode="19" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">19</span></a></div>
				            <div id="lineE-3" class="row-round"></div>
				            <div id="lineE-4" class="kursi"><a class="bus-booking" data-kode="18" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">18</span></a></div>
				            <div id="lineE-5" class="kursi"><a class="bus-booking" data-kode="17" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">17</span></a></div>
				        </div>
				        <div class="row-kursi">
				        	<div id="lineF-1" class="kursi"><a class="bus-booking" data-kode="24" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">24</span></a></div>
				            <div id="lineF-2" class="kursi"><a class="bus-booking" data-kode="23" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">23</span></a></div>
				            <div id="lineF-3" class="row-round"></div>
				            <div id="lineF-4" class="kursi"><a class="bus-booking" data-kode="22" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">22</span></a></div>
				            <div id="lineF-5" class="kursi"><a class="bus-booking" data-kode="21" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">21</span></a></div>
				        </div>
				        <div class="row-kursi">
				        	<div id="lineG-1" class="kursi"><a class="bus-booking" data-kode="28" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">28</span></a></div>
				            <div id="lineG-2" class="kursi"><a class="bus-booking" data-kode="27" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">27</span></a></div>
				            <div id="lineG-3" class="row-round"></div>
				            <div id="lineG-4" class="kursi"><a class="bus-booking" data-kode="26" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">26</span></a></div>
				            <div id="lineG-5" class="kursi"><a class="bus-booking" data-kode="25" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">25</span></a></div>
				        </div>
				        <div class="row-kursi">
				        	<div id="lineH-1" class="kursi"><a class="bus-booking" data-kode="30" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">30</span></a></div>
				            <div id="lineH-2" class="kursi"><a class="bus-booking" data-kode="29" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">29</span></a></div>
				            <div id="lineH-3" class="row-round"></div>
				            <div id="lineI-3" class="row-empty"></div>
				            <div id="lineI-3" class="row-empty"></div>
				            <div id="lineI-3" class="row-empty"></div>
				            <div id="lineH-4" class="kursi"></div>
				            <div id="lineH-5" class="kursi" style="border-bottom:5px solid javascript:void(0);333;"></div>
				        </div>
				        <div class="row-kursi">
				        	<div id="lineI-1" class="kursi"><a class="bus-booking" data-kode="32" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">32</span></a></div>
				            <div id="lineI-2" class="kursi"><a class="bus-booking" data-kode="31" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">31</span></a></div>
				            <div id="lineI-3" class="row-round"></div>
				            <div id="lineI-3" class="row-empty"></div>
				            <div id="lineI-3" class="row-empty"></div>
				            <div id="lineI-4" class="kursi"></div>
				            <div id="lineI-5" class="kursi"><img src="http://localhost:8000/images/toilet.png" title="Toilet"></div>
				        </div>
				    </div>
				</div>
			',
			/* -- Template Bus 32A seat 1 */
			'bus2' => '
				<div class="bodi-bus">
				    <div class="kendaraan-content">
				        <div class="row-kursi">
				            <div id="lineA-1" class="kursi"><a class="bus-booking" data-kode="0" href="javascript:void(0);"><img src="' . asset('/images/sopir_24.png') . '" title="JURU MUDI"></a></div>
				        </div>

				        <div class="row-kursi">
				        	<div id="lineA-1" class="kursi"><a class="bus-booking" data-kode="4A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">4A</span></a></div>
				            <div id="lineA-2" class="kursi"><a class="bus-booking" data-kode="3A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">3A</span></a></div>
				            <div id="lineA-3" class="row-round"></div>
				            <div id="lineA-4" class="kursi"><a class="bus-booking" data-kode="2A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">2A</span></a></div>
				            <div id="lineA-5" class="kursi"><a class="bus-booking" data-kode="1A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">1A</span></a></div>
				        </div>

				        <div class="row-kursi">
				        	<div id="lineB-1" class="kursi"><a class="bus-booking" data-kode="8A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">8A</span></a></div>
				            <div id="lineB-2" class="kursi"><a class="bus-booking" data-kode="7A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">7A</span></a></div>
				            <div id="lineB-3" class="row-round"></div>
				            <div id="lineB-4" class="kursi"><a class="bus-booking" data-kode="6A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">6A</span></a></div>
				            <div id="lineB-5" class="kursi"><a class="bus-booking" data-kode="5A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">5A</span></a></div>
				        </div>
				        <div class="row-kursi">
				        	<div id="lineC-1" class="kursi"><a class="bus-booking" data-kode="12A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">12A</span></a></div>
				            <div id="lineC-2" class="kursi"><a class="bus-booking" data-kode="11A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">11A</span></a></div>
				            <div id="lineC-3" class="row-round"></div>
				            <div id="lineC-4" class="kursi"><a class="bus-booking" data-kode="10A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">10A</span></a></div>
				            <div id="lineC-5" class="kursi"><a class="bus-booking" data-kode="9A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">9A</span></a></div>
				        </div>
				        <div class="row-kursi">
				        	<div id="lineD-1" class="kursi"><a class="bus-booking" data-kode="16A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">16A</span></a></div>
				            <div id="lineD-2" class="kursi"><a class="bus-booking" data-kode="15A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">15A</span></a></div>
				            <div id="lineD-3" class="row-round"></div>
				            <div id="lineD-4" class="kursi"><a class="bus-booking" data-kode="14A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">14A</span></a></div>
				            <div id="lineD-5" class="kursi"><a class="bus-booking" data-kode="13A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">13A</span></a></div>
				        </div>
				        <div class="row-kursi">
				        	<div id="lineE-1" class="kursi"><a class="bus-booking" data-kode="20A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">20A</span></a></div>
				            <div id="lineE-2" class="kursi"><a class="bus-booking" data-kode="19A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">19A</span></a></div>
				            <div id="lineE-3" class="row-round"></div>
				            <div id="lineE-4" class="kursi"><a class="bus-booking" data-kode="18A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">18A</span></a></div>
				            <div id="lineE-5" class="kursi"><a class="bus-booking" data-kode="17A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">17A</span></a></div>
				        </div>
				        <div class="row-kursi">
				        	<div id="lineF-1" class="kursi"><a class="bus-booking" data-kode="24A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">24A</span></a></div>
				            <div id="lineF-2" class="kursi"><a class="bus-booking" data-kode="23A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">23A</span></a></div>
				            <div id="lineF-3" class="row-round"></div>
				            <div id="lineF-4" class="kursi"><a class="bus-booking" data-kode="22A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">22A</span></a></div>
				            <div id="lineF-5" class="kursi"><a class="bus-booking" data-kode="21A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">21A</span></a></div>
				        </div>
				        <div class="row-kursi">
				        	<div id="lineG-1" class="kursi"><a class="bus-booking" data-kode="28A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">28A</span></a></div>
				            <div id="lineG-2" class="kursi"><a class="bus-booking" data-kode="27A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">27A</span></a></div>
				            <div id="lineG-3" class="row-round"></div>
				            <div id="lineG-4" class="kursi"><a class="bus-booking" data-kode="26A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">26A</span></a></div>
				            <div id="lineG-5" class="kursi"><a class="bus-booking" data-kode="25A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">25A</span></a></div>
				        </div>
				        <div class="row-kursi">
				        	<div id="lineH-1" class="kursi"><a class="bus-booking" data-kode="30A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">30A</span></a></div>
				            <div id="lineH-2" class="kursi"><a class="bus-booking" data-kode="29A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">29A</span></a></div>
				            <div id="lineH-3" class="row-round"></div>
				            <div id="lineI-3" class="row-empty"></div>
				            <div id="lineI-3" class="row-empty"></div>
				            <div id="lineI-3" class="row-empty"></div>
				            <div id="lineH-4" class="kursi"></div>
				            <div id="lineH-5" class="kursi" style="border-bottom:5px solid javascript:void(0);333;"></div>
				        </div>
				        <div class="row-kursi">
				        	<div id="lineI-1" class="kursi"><a class="bus-booking" data-kode="32A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">32A</span></a></div>
				            <div id="lineI-2" class="kursi"><a class="bus-booking" data-kode="31A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">31A</span></a></div>
				            <div id="lineI-3" class="row-round"></div>
				            <div id="lineI-3" class="row-empty"></div>
				            <div id="lineI-3" class="row-empty"></div>
				            <div id="lineI-4" class="kursi"></div>
				            <div id="lineI-5" class="kursi"><img src="http://localhost:8000/images/toilet.png" title="Toilet"></div>
				        </div>
				    </div>
				</div>
			'
		]);
	}

}
