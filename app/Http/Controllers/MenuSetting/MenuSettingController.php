<?php namespace App\Http\Controllers\MenuSetting;

use App\Models\frame_level_user;
use App\Models\frame_menu_nav;
use App\Models\frame_relasi_akses_user;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Commands\Menus\CreateMenuCommand;
use App\Commands\Menus\SavePositionMenuCommand;
use App\Commands\Menus\UpdateMenuCommand;

use Illuminate\Http\Request;

class MenuSettingController extends Controller {

	public function __construct(){
		$this->middleware('auth');
	}

	public function getIndex(){
		return redirect('/');
	}

	public function getSetting(){
		$menu = frame_menu_nav::whereStatus(1)
		->whereParent_id(0)
		->get();
		return view('MenuSetting.show', [
			'parent' => $menu
		]);
	}

	public function postSetting(Request $req){

		$save = $this->dispatch(
			new CreateMenuCommand($req->all())
		);

		return redirect()->back();
	}

	public function postSaveposition(Request $req){
		
		$save = $this->dispatch(
			new SavePositionMenuCommand($req->all())
		);

		if($save)
			return redirect()->back();
		else
			return redirect()->back()->withErrs("Gagal menyimpan posisi");
	}


	public function getEdit($id = 0){

		if(empty($id))
			return redirect()->back();

		$menu = frame_menu_nav::whereStatus(1)->whereParent_id(0)->get();
		$edit = frame_menu_nav::find($id);
		return view('MenuSetting.show', [
			'parent' 	=> $menu,
			'menu' 		=> $edit
		]);
	}

	public function postUpdate(Request $req){

		$this->dispatch(
			new UpdateMenuCommand($req->all())
		);

		return redirect()->back();

	}

	// Hak akses management
	public function getAccess($id = 0){
		return view('MenuSetting.ShowAccess', [
			'level' 	=> frame_level_user::whereStatus(1)->get(),
			'id' 		=> $id
		]);
	}

	public function postSaveaccessmenu(){

		try{

			frame_relasi_akses_user::whereFrame_level_user_id(\Input::get('id_level'))->delete();

			\DB::beginTransaction();
			
			foreach (\Input::get('id_menu') as $id_menu) {
				frame_relasi_akses_user::create([
					'frame_menu_nav_id' => $id_menu,
					'frame_level_user_id' => \Input::get('id_level')
				]);
			}

			\DB::commit();
			return redirect()->back()->withSess('Update Success!');
		}catch(Exception $e){
			\DB::rollback();
			return redirect()->back()->withErrs($e->getMessage());
		}

	}

}
