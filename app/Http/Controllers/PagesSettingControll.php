<?php namespace App\Http\Controllers;

use App\Models\ketiket_pages;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class PagesSettingControll extends Controller {

	public function getIndex(){

		if(\Auth::guest())
			return redirect('/');

		$data = ketiket_pages::find(1);

		return view('PageSettings.Show', [
			'data' => $data
		]);
	}

	public function postIndex(Request $req){
		ketiket_pages::find(1)->update($req->all());
		return redirect()->back()->withNotif([
			'label' => 'success',
			'err' => 'Berhasil diperbaharui'
		]);
	}


	/////////////////////////////////////////////////
	public function about_me(){
		$ket = ketiket_pages::find(1);
		$data = $ket->about_me;
		return view('PageSettings.Result', [
			'result' => $data,
			'judul' => 'Tentang Kami',
			'title' => 'Ketiket | Tentng Kami'
		]);
	}
	public function privacy_policy(){
		$ket = ketiket_pages::find(1);
		$data = $ket->privacy_policy;
		return view('PageSettings.Result', [
			'result' => $data,
			'judul' => 'Privacy Policy',
			'title' => 'Ketiket | Privacy Policy'
		]);
	}
	public function disclaimer(){
		$ket = ketiket_pages::find(1);
		$data = $ket->disclaimer;
		return view('PageSettings.Result', [
			'result' => $data,
			'judul' => 'Disclaimer',
			'title' => 'Ketiket | Disclaimer'
		]);
	}
}
