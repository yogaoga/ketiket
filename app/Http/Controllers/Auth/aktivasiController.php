<?php namespace App\Http\Controllers\Auth;

use App\User;
use App\Models\users_verifikasi;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class aktivasiController extends Controller {

	public function index(){

		if(\Input::has('t')){
			$kode = $_GET['t'];

			$verif = users_verifikasi::whereKode_verifikasi($kode);
			
			if($verif->count() > 0){
				$verif = $verif->first();
				$user = user::find($verif->user_id);
				$user->status = 1;
				$user->save();
				
				users_verifikasi::whereKode_verifikasi($kode)
				->update([
					'status' => 1
				]);
			}
			
		}


		return redirect('/home');

	}

}
