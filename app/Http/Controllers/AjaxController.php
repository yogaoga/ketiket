<?php namespace App\Http\Controllers;

use App\User;
use App\Models\hubs;
use App\Models\travel;
use App\Models\routes;
use App\Models\provinsi;
use App\Models\harga_tiket;
use App\Models\persenharga;
use App\Models\route_detail;
use App\Models\relasi_travel;

use App\Events\Otojasa\AddOperatorNotifikasiEvent;
use App\Events\Agen\AddOperatorNotifikasiAgenEvent;

use App\Commands\Otojasa\AddOperatorCommand;
use App\Commands\Agen\AddOperatorAgenCommand;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class AjaxController extends Controller {

	/*
		Method untuk mengambil semua notifikasi
	*/
	public function getNotif(){
		$result = [];

		if(\Request::ajax()):

			// Mengecek kelengkapan user
			if(\Input::has('complite_user')){
				$id = $_GET['complite_user'];
				$user = User::find($id);
				$result['result'] = false;
				if($user->status == 0){
					$result['result'] = true;
					$result['err'] = '<h5 class="text-muted"><i class="fa fa-envelope-o fa-lg"></i> Periksa email anda</h5>';
				}else if(empty($user->rekening) || empty($user->hp) || empty($user->alamat) || empty($user->cabang) || empty($user->bank)){
					$result['result'] = true;
					$result['err'] = '<h5 class="text-muted"><i class="glyphicon glyphicon-user fa-lg"></i> Silahkan untuk melengkapi identitas Anda <a href="' . url('/user/edit') . '" class="btn btn-info btn-xs">Perbaharui Akun</a></h5>';
				}
				
			}


		endif;
		return json_encode($result);
	}

	public function postAddusertrvel(Request $req){
		$result = [];
		$users = User::whereEmail($req->email);

		if($users->count() > 0){

			$user = $users->first();
			$cek = relasi_travel::whereUser_id($user->id);
			// Memastikan user tidak terdaftar pada agen atau travel
			if($cek->count() > 0){
				$result['result'] = false;
				$result['err'] = 'Permintaan di tolak ' . $user->name . ' sudah terdaftar sebagai pengelola Travel / Agen';
			}else{
				$result['result'] = true;
				$this->dispatch(
					new AddOperatorCommand($user)
				);

				$email = new AddOperatorNotifikasiEvent($user);
				$email->send();
			}

		}else{
			$result['result'] = false;
			$result['err'] = 'Email yang ada kirimkan tidak dapat ditemukan ?';
		}

		return json_encode($result);
	}

	public function postDeladdusertrvel(Request $req){
		relasi_travel::find($req->id)->delete();
		return json_encode([
			'result' => true
		]);
	}

	public function postGeneratecode(){
		$rute = routes::count();
		$alias = substr(\Travel::data()->nama_travel, 0,2);

		return json_encode([
			'result' => strtoupper($alias) . '-' . $rute . rand(0,9)
		]);
	}

	/*
	Berfungsi untuk mengaktifkan Rute
	*/
	public function postActiveroutes(Request $req){
		routes::find($req->routes)->update([
			'status' => $req->status
		]);

		return json_encode([
			'result' => $req->status
		]);
	}

	/*
	Berfungsi untuk menghapus Rute
	*/
	public function postDeleteroutes(Request $req){
		routes::find($req->routes)->update([
			'status' => 3 /* <--- stauts ini berarti di hapus daftar*/
		]);
		route_detail::whereRoutes_id($req->routes)->update([
			'status' => 3 /* <--- stauts ini berarti di hapus daftar*/
		]);

		return json_encode([
			'result' => true
		]);
	}


	/*
	Mengambil data Kabupaten Kota
	*/
	public function postGetkabkota(Request $req){
		$res = '
		<div class="form-group">
			 <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <label for="from">' . $req->label . '</label>
					<select class="form-select2 form-control" name="' . $req->name . '" required>
						<option value="">Pilih Kota</option>
					';
			foreach(provinsi::all() as $prov){
				$res .= '<optgroup label="' . $prov->nm_provinsi . '">';
						foreach(provinsi::find($prov->id)->kab_kota()->get() as $kab){
							$res .= '<option value="' . $kab->id .'">' . $kab->nm_kab_kota . '</option>';
						}
				$res .= '</optgroup>';
			}
				$res .= '</select>
				</div>
			</div>
		';

		return json_encode([
			'data' => $res
		]);
	}

	/* Menghapus detail rute */
	public function postDelrutedetail(Request $req){
		$route = route_detail::find($req->id);
		$route->status = 3;
		$route->save();

		return json_encode([
			'result' => true
		]);
	}
	/* Mengambil data rute detaul untuk proses Edit */
	public function postEditdetailrute(Request $req){
		$res = [];
		$route = route_detail::join('harga_tiket', function($join){
			return $join->on('harga_tiket.route_detail_id', '=', 'route_detail.id');
		})
		->join('kab_kota AS a', function($join){
			return $join->on('a.id', '=', 'route_detail.depart');
		})
		->join('kab_kota AS b', function($join){
			return $join->on('b.id', '=', 'route_detail.destination');
		})
		->where('route_detail.id', $req->id)
		->where('route_detail.status', 1)
		->select('route_detail.*', 'harga_tiket.*', 'a.nm_kab_kota AS nm_depart', 'b.nm_kab_kota AS nm_destination');
		if($route->count() == 0){
			$res['result'] = false;
			$res['err'] = 'Rute tidak ditemukan ?';
		}else{

			$routes = $route->first();

			$res['result'] = true;
			$res['judul'] = $routes->nm_depart . ' to ' . $routes->nm_destination;
			$res['content'] = '
				 <div class="form-group">
				    <label for="harga" class="col-sm-2 control-label">Harga</label>
				    <div class="col-sm-10">
			      		<input type="text" class="input-text text-right full-width" value="' . (INT) $routes->harga . '" id="harga" name="harga">
				    </div>
			  	</div>
			  	<button onclick="addpools()" class="button btn-mini sky-blue1 pull-right" type="button"><i class="fa fa-plus"></i> Tambah Boarding Point</button>
			  	<hr />
			  	<div class="add-pools"></div>
			  	<input type="hidden" name="id" value="' . $req->id . '" />
			  	<input type="hidden" name="kota" value="' . $routes->depart . '" />
			';
		}

		return json_encode($res);

	}
	/* -- Menghapus Boarding point -- */
	public function postDelpool(Request $req){
		hubs::find($req->id)->delete();
		return json_encode([
			'result' => true
		]);
	}

	/* -- Mengambil detail Route */
	public function postDiscountroutes(Request $req){
		$src = $req->src;
		$return = [];
		$html 	= '';
		$routes = routes::daftar($src)->paginate(5);
		foreach($routes as $route){
			$html .= '
				<article class="box" style="margin:10px 0;">
                    <div class="details col-xs-9 col-sm-12">
                        <div class="details-wrapper">
                            <div class="first-row">
                                <div>
                                    <h4 class="box-title">
                                    	<a href="' . url('/otojasa/creatediscount/' . $route->id) . '">
                                    		' . $route->dari . ' <i class="soap-icon-arrow-right"></i> ' . $route->ke . '
                                    	</a>
                                    	<small>Kode : ' . $route->kode_routes . ' | Oleh : ' . $route->oleh . '</small></h4>
                                    <a href="' . url('/otojasa/creatediscount/' . $route->id) . '" class="button btn-mini stop">Prosess</a>
                                </div>
                                <div>
                                    <span class="price"><small>Harga</small>Rp ' . number_format(harga_tiket::hargabyroute($route->depart,$route->destination)->price,0,',','.') . '</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
			';
		}

		if(count($routes) > 0){
			$return['reslut'] = true;
			$return['data'] 	= $html;
			$return['paginate'] = $routes->render();
		}else{
			$return['reslut'] = false;
			$return['data'] = '<hr /><div class="well"> Rute tidak ditemukan</div>';
		}
		
		return json_encode($return);
	}

	public function postDeldiscount(Request $req){
		persenharga::find($req->id)->delete();
		return json_encode([
			'result' => true
		]);
	}

	////////////////// AGEN ////////////////////////
	public function postAdduseragen(Request $req){
		$result = [];
		$users = User::whereEmail($req->email);

		if($users->count() > 0){

			$user = $users->first();
			$cek = relasi_travel::whereUser_id($user->id);
			// Memastikan user tidak terdaftar pada agen atau travel
			if($cek->count() > 0){
				$result['result'] = false;
				$result['err'] = 'Permintaan di tolak ' . $user->name . ' sudah terdaftar sebagai pengelola Travel / Agen';
			}else{
				$result['result'] = true;
				$this->dispatch(
					new AddOperatorAgenCommand($user)
				);

				$email = new AddOperatorNotifikasiAgenEvent($user);
				$email->send();
			}

		}else{
			$result['result'] = false;
			$result['err'] = 'Email yang ada kirimkan tidak dapat ditemukan ?';
		}

		return json_encode($result);
	}

	public function postDeladduseragen(Request $req){
		relasi_travel::find($req->id)->delete();
		return json_encode([
			'result' => true
		]);
	}
}
