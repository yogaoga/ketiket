<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\car_rent;
use Illuminate\Http\Request;
use App\Models\provinsi;
use App\Models\car_booking;
use App\Models\ref_car_time;
use App\ref_car_extra;
use DB;
use Validator;

class CarBookController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
            $validator = Validator::make($request->all(), [
                'nama' => 'required',
                'alamat' => 'required',
                'nokontak' => 'required',
                'idno' => 'required'
            ]);
             try {
                if ($validator->fails()) {
                    return $validator->errors()->all();
                }else{
                    DB::beginTransaction();//SUPAYA BISA DI ROLLBACK JIKA ADA SALAH SATU TRANSAKSI DB YANG GAGAL
                    $no_invoice = car_booking::insert($request);
                    DB::commit();
                    return redirect('/finish-car-book/'.$no_invoice);
                }
            }catch(\Exception $ex){
                return $ex;
            }
            
	}
        public function finish($no_invoice){
            $data = car_booking::get_detail($no_invoice);
            $norek = DB::table('ref_norek')->get();
            $npwp = DB::table('ref_npwp')->get();
            return view('client.car_rent_finish',compact('no_invoice','data','norek','npwp'));
        }
        public function printInvoice($no_invoice){
            $data = car_booking::get_detail($no_invoice);
            return view('client.car_rent_print',compact('no_invoice','data'));
        }

        /**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	
        public function show(Request $req)
	{
            $limit = 5;
            $arcar = car_rent::get_paging($req->page,$limit,'','book',$req);
            $arkota = provinsi::getWithKota();
            $cartype = DB::table('ref_car_type')->where('status','=',1)->get();
            $armanuf = DB::table('ref_car_manufacture')->where('status','=',1)->orderBy('nm_car_mnfcr')->get();
            $artrans = DB::table('ref_car_transmission')->where('status','=',1)->get();
               
            return view('client.car_rent',compact('arcar', 'arkota' ,'cartype','armanuf','artrans'));
	}
        public function extra(Request $req)
	{
            $arextra = DB::table('ref_car_extra')->where('status','=',1)->get();
            $driverdet = ref_car_extra::get_detail_bynama('Driver');
            return view('client.car_rent_extra', compact('arextra','driverdet'));
	}
        public function biodata(Request $req)
	{
            $aridcard = DB::table('ref_idcard')->get();
            return view('client.car_rent_biodata', compact('aridcard'));
	}
        public function lamasewa($tgl,$tgl1,$cartime){
            $tgl = new \DateTime($tgl);
            $tgl1 = new \DateTime($tgl1);
            return ref_car_time::get_detail($cartime, $tgl, $tgl1);
           
        }
        
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
