<?php namespace App\Http\Controllers;

use App\Models\provinsi;

use App\Models\travel;
use App\Models\routes;
use App\Models\route_detail;
use App\Models\relasi_travel;
use App\Models\comment;

use App\Commands\Otojasa\CreateOtojasaCommand;
use App\Commands\Menus\CreateCommentsCommand;

use App\Events\Otojasa\UploadLogoTravelEvent;
use App\Http\Requests\Otojasa\CreateOtojasaRequest;
use App\Events\Otojasa\NotifikasiCreatedTravelEvent;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class PageController extends Controller {

	public function detailtravel($slug){
		
		// Bisa dengan cara membuat pola di Model
		$travel = travel::whereSlug($slug)->first();
		$comment = comment::where('travel_id',$travel->id)->take(4)->get();


		/*
			Lakukan validasi
			Jika status masih 0/tidak aktif yang dapat mengakses hanya level 1,2
			dan orang yang mengajukan Otojasa bisa di lihat di relasi_travel berdasarkan travel_id,status=1,level=1 <--- Ini Admin Otojasa

			Beriketerangan jika masih tidak aktif Bahwa Otojasa ini masih dalam MODERATOR sebagai pemberitahuan sementara untuk user yang mengajukan

			Halaman ini bisa di akses pada saat login dan tidak login

			====================
			Tambahkan Atribut SEO
			Title ---> di isi dengan data dari nama_travel
			deskripsi ----> di isi dengan data dari field information

			klo bisa gunakan teknig grabing agar terlihat pas pada saat di share di socialmedia seperti tampilan gambar dan deskripsi
			tombol share button muncul pada saat travel sudah aktive
		*/
		return view('client.travel',[
			'travel' => $travel,
			'comment' => $comment
			]);
		// View nya buat di folder /view/Page/ 
	}


	// Mengaktifkan Operator masing-masing travel
	public function confirmasiusertravel($key){
		$user = relasi_travel::whereToken($key)
			->whereUserId(\Auth::user()->id);

		if($user->count() == 0)
			return redirect('/home');

		$user->update([
			'status' => 1
		]);

		\Travel::data();

		return redirect('/otojasa/dashboard')->withNotif([
			'label' => 'info',
			'err' => 'Selamat datang dan selamat bergabung bersama ' . \Travel::data()->nama_travel
		]);
	}

	
	/*
	Menampilkan detail Rute
	@access Public
	*/
	public function detailrute($domain, $kode){
		$travels = travel::whereSlug($domain);
		/*Memastikan Travel tidak kosong*/
		if($travels->count() == 0)
			return redirect('/')->withNotif([
				'label' => 'success',
				'err' => 'Travel / Agen tidak ditemukan ?'
			]);

		$travel = $travels->first();
		$routes = routes::show($travel->id, $kode);

		/*Memastikan Rute tidak kosong*/
		if($routes->count() == 0)
			return redirect('/')->withNotif([
				'label' => 'success',
				'err' => 'Rute tidak ditemukan ?'
			]);

		$route = $routes->first();
		$detail = $route->detail()
		->join('kab_kota AS a', function($join){
			return $join->on('a.id', '=', 'route_detail.depart');
		})
		->join('kab_kota AS b', function($join){
			return $join->on('b.id', '=', 'route_detail.destination');
		})
		->join('harga_tiket', function($join){
			return $join->on('harga_tiket.route_detail_id', '=', 'route_detail.id');
		})
		->where('route_detail.status', 1)
		->select('route_detail.*', 'a.nm_kab_kota AS nm_depart', 'b.nm_kab_kota AS nm_destination', 'harga_tiket.*')
		->get();
		/* -- Facilitys -- */
		$facilitys = $route->facility()
		->join('ref_facility', 'ref_facility.id', '=', 'relasi_facility.ref_facility_id')
		->orderBy('relasi_facility.id', 'asc')
		->get();
		
		/* -- Boarding Pints -- */
		$pools = [];
		foreach($detail as $details){
			$pools[$details->id] = route_detail::find($details->id)->boardingpoint()->get();
		}
		
		$status = $route->status > 0 ? '' : ' - Tidak aktif';
		return view('Otojasa.DetailRute', [
			'travel'	=> $travel,
			'route' 	=> $route,
			'details' 	=> $detail,
			'status'	=> $status,
			'facilitys' => $facilitys,
			'pools'		=> $pools,
			/* -- SEO -- */
			'title' 		=> $route->nm_depart . ' ke ' . $route->nm_destination . ' | ' . $travel->nama_travel,
			'description' 	=> $travel->hint,
			'author' 		=> $travel->nama_travel,
			'keywords' 		=> 'ketiket,Travel,Jasa Tiket,' . $travel->nama_travel . ',' . $route->nm_depart . ' ke ' . $route->nm_destination
		]);
	}


	//////////////////// Otojasa ///////////////////
	/*
	Digunakan untuk membuat Travel dan Agen
	@access public
	@url otojasa/create
	*/
	public function createotojasa(){
		if(\Travel::check())
			return redirect('/home')->withNotif([
				'label' => 'info',
				'err' => 'Anda sudah terdaftar sebagai admin dari ' . \Travel::data()->nama_travel
			]);

		return view('Otojasa.CreateOtojasa', [
			'provinsi' => Provinsi::orderBy('nm_provinsi', 'asc')->get()
		]);
	}
	/*
	proses penyimpanan data Travel / agen yang baru
	*/
	public function saveotojasa(CreateOtojasaRequest $req){
		// Menimpan data Travel Baru
		$travel = $this->dispatch(
			new CreateOtojasaCommand($req->all())
		);
		
		// Upload Logo travel
		$file = new UploadLogoTravelEvent($travel);
		travel::find($travel->id)->update([
			'logo' => $file->upload()
		]);
		
		// Mengirim email notifikasi ke user dan admin
		new NotifikasiCreatedTravelEvent($travel);

		return redirect('/home')->withNotif([
			'label' => 'info',
			'err' => 'Pengajuan Otojasa anda berhasil terkirim. Periksa Email anda untuk melihat keterangan lebih lanjut.'
		]);
	}

	/* 
	Simpan Komen
	*/

	public function saveComment(Request $req){

	$this->dispatch(new CreateCommentsCommand($req->all()));

	return redirect()->back()->withNotif([
			'label' => 'success',
			'err' => 'Comment Sudah Di Tambahkan!'
		]);
	}
}
