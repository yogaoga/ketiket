<?php namespace App\Http\Controllers;
use Input;
use DB;

use App\Models\provinsi;
use App\Models\kab_kota;
use App\Models\routes;
use App\Models\reservasi;
use App\Models\car_rent;

class WelcomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 * @author Yoga
	 */
	public function index()
	{

		return view('welcome', [
			'title' => 'Selamat datang di Ketiket'
			]);
	}

	public function show(){

		$data = DB::table('travel')->take(4)->get();

		$route_populer = routes::join('route_detail','route_detail.routes_id','=','routes.id')
		->join('kab_kota AS a','a.id','=','route_detail.depart')
		->join('kab_kota AS b','b.id','=','route_detail.destination')
		->select(
			'routes.*',
			'a.nm_kab_kota AS nm_depart',
			'b.nm_kab_kota AS nm_destination'
			)
		->take(9)
		->get();

		$route_last = reservasi::join('route_detail','route_detail.id','=','reservasi.route_detail_id')
		->join('routes','routes.id','=','route_detail.routes_id')
		->join('kab_kota AS a','a.id','=','route_detail.depart')
		->join('kab_kota AS b','b.id','=','route_detail.destination')
		->select(
			'reservasi.*',
			'a.nm_kab_kota AS nm_depart',
			'b.nm_kab_kota AS nm_destination'
			)
		->orderBy('route_detail.id','desc')
		->groupBy('route_detail.depart','route_detail.destination')
		->take(9)
		->get();
                
                $cartype = DB::table('ref_car_type')->where('status','=',1)->get();
                $carrent = car_rent::get_terkini();
return view('welcome',[
	'data' => $data,
	'provinsi' => Provinsi::all(),
	'route_populer' => $route_populer,
	'route_last' => $route_last,
        'cartype' => $cartype,
        'carrent' => $carrent
	]);

}

}
