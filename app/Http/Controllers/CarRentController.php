<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use DB;
use App\Models\ref_car_manufacture;
use App\Models\provinsi;
use App\Models\car_rent;
use App\Models\car_img;
use App\Models\car_price;
use App\Models\car_booking;
use Validator;
use Auth;

class CarRentController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct(){
		$this->middleware('auth');
		$this->middleware('authTravel');
	}
	public function index()
	{
		return view('CarRent.CarRent');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($edit,$id)
	{
		$armerk = ref_car_manufacture::getWithMerk();
		$artype = DB::table('ref_car_type')->where('status','=',1)->get();
		$artrans = DB::table('ref_car_transmission')->where('status','=',1)->get();
		$arfuel = DB::table('ref_car_fuel')->where('status','=',1)->get();
		$artime = DB::table('ref_car_time')->where('status','=',1)->get();
		$arkota = provinsi::getWithKota();
		$odata = "";
		if($edit==1){$odata = car_rent::get_detail($id);}
		return view('CarRent.InputCar',compact('armerk','artype','artrans','arfuel','artime','arkota','edit','id','odata'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{

		try {

                if($request->edit==0){//INPUT BARU
                    DB::beginTransaction();//SUPAYA BISA DI ROLLBACK JIKA ADA SALAH SATU TRANSAKSI DB YANG GAGAL

                        //CEK PLAT MOBIL TIDAK BOLEH SAMA
                    $ncar = car_rent::cekCar(str_replace(" ","",$request->plat));
                    if($ncar>0){return "Plat nomor yang sama sudah ada! Silahkan edit pada mobil tersebut";}

                        $lastid = car_rent::insert($request);//SIMPAN DATA MOBIL
                        car_price::insert($request->harga,$request->unit,$lastid);//SIMPAN DATA HARGA SEWA MOBIL
                        if($request->file('foto')){
                            car_img::insert($request->file('foto'),$lastid,$request->primer);//SIMPAN DATA FOTO MOBIL
                        }
                        DB::commit();
                        return redirect('/input-car-rent/0/0')->withNotif([
                        	'label' => 'success',
                        	'err' => ' Data berhasil disimpan!'
                        	]);
                }else{//EDIT 
                    //CEK PLAT MOBIL TIDAK BOLEH SAMA

                	$ncar = car_rent::cekCar_wtid(str_replace(" ","",$request->plat),$request->id);
                	if($ncar>0){return "Plat nomor yang sama sudah ada! Silahkan edit pada mobil tersebut";}

                    car_rent::updet($request);//SIMPAN DATA MOBIL
                    if($request->file('foto')){
                            car_img::change($request->file('foto'),$request->id,$request->primer);//UPDATE DATA FOTO MOBIL
                        }
                        return redirect('/car-rent')->withNotif([
                        	'label' => 'success',
                        	'err' => ' Data berhasil diupdate!'
                        	]);
                    }
                }catch(\Exception $ex){
                	return $ex;
                }


            }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($page,$limit)
	{

		$arcar = car_rent::get_paging($page,$limit,Auth::user()->id);
		return view('CarRent.ListCar', ['arcar' => $arcar]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function editFoto($idrent)
	{
		$data = car_img::get($idrent);
		return view('CarRent.EditFoto',compact('data','idrent'));
	}
        public function editHarga($id,$edit,$idcar)
	{
            $data = "";
            if($edit==1){$data = car_price::get_detail($id);}
            $artime = DB::table('ref_car_time')->where('status','=',1)->get();
            return view('CarRent.EditHarga',compact('data','id','edit','artime','idcar'));
	}
	public function updateHarga(Request $request)
	{
            $id_rentcar = $request->idcar;
            $price = $request->harga;
            $id_car_time = $request->unit;
            if($request->edit==0){
                car_price::create(compact('id_rentcar','price','id_car_time'));
            }else{
                car_price::where('id','=',$request->id)->update(['price'=>$price,'id_car_time'=>$id_car_time]);
            }
            return 1;
	}
	public function updateStatus(Request $request)
	{

            car_rent::where('id_rentcar','=',$request->id)->update(['status'=>$request->state]);
            return 1;
	}
	public function updateFoto(Request $request)
	{
		car_img::updet($request);
		return 1;
	}
	public function updateFotoPrimer(Request $request)
	{
		car_img::updetPrimer($request);
		return 1;
	}
	public function destroy($id)
	{
		//
	}
	public function destroyFoto(Request $request)
	{
		car_img::destroy($request->id);
		return 1;
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @author yoga@valdlabs.com
	 * @return Response
	 */
        //MERETURN VIEW LIST BOOKING:
	public function newBooking()
	{
		$items = car_booking::detail()
		->where('data_car_booking.status',1)
		->orWhere('data_car_booking.status',2)
		->orWhere('data_car_booking.status',3)
		->get();
		return view('CarRent.NewBook',[
			'items' => $items
			]);
	}
        
        //MERETURN DATA LIST BOOKING:
        public function loadBook($page,$status,$fieldsearch,$search='')
	{
            $limit = 10;
            $user = Auth::user()->id;
            $data = car_booking::get_paging($page,$limit,$status,$user,$search,$fieldsearch);
            return $data;
	}
        
	//MERETURN VIEW UNTUK MENGUBAH STATUS KE : BOOKING, CANCEL ATAU SELESAI:
	public function ViewInvoice($id,$no_invoice,$status){
            $data = car_booking::get_detail($no_invoice,$id);
            return view('CarRent.ViewInvoice',compact('id','no_invoice','status','data'));
	}
        
        //MENYIMPAN UPDATE STATUS BOOKING:
	public function saveBooking(Request $req){
            $status = $req->status;
            if($status==2){
                if($req->diskon){$diskon = $req->diskon;}
                else{$diskon=0;}
                $params = [
                    'status' => $status,
                    'diskon' => $diskon,
                    'adjustment' => $req->adj
                ];
                car_booking::where('no_invoice','=',$req->noinvoice)->where('status','<>',4)->update($params);
            }elseif($status==3){
                $params = [
		'status' => $status
		];
		car_booking::find($req->id)->update($params);
            }elseif($status==4){
                $params = [
		'adjustment' => $req->rpcancel,
		'status' => $status,
                'total_price' => 0
		];
		car_booking::find($req->id)->update($params);
                //GRAND TOTAL SETELAH DICANCEL = GRAND TOTAL SEBELUMNYA - TOTAL SEWA MOBIL YANG DICANCEL
                //GRAND TOTAL DIUPDATE DI SEMUA INVOICE YANG SAMA
                $grand = $req->ograndtotal-$req->ototal;
                $params = [
                'grand_total' => $grand
		];
                car_booking::where('no_invoice','=',$req->noinvoice)->update($params);
                
            }
            return 1;
	}

        public function destroyHarga(Request $request)
	{
            car_price::destroy($request->id);
            return 1;
	}
        public function listCancel()
	{
            $items = car_booking::detail()
            ->where('data_car_booking.status',4)
            ->get();
            return view('CarRent.NewBook',[
                    'items' => $items
                    ]);
	}
}
