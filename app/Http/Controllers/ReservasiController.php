<?php namespace App\Http\Controllers;

use App\Models\reservasi;
use App\Models\reservasi_detail;
use App\Models\hubs;
use App\Models\routes;
use App\Models\travel;
use App\Models\provinsi;
use App\Models\route_detail;
use App\Models\kab_kota;

use App\Http\Requests;
use App\Http\Controllers\Controller;


use App\Http\Requests\ReservasiRequest;
use Illuminate\Http\Request;
use App\Commands\Reservasi\CreateReservasiCommand;

use Input;
use DB;

class ReservasiController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 * @author Yoga [yoga@valdlabs.com]
	 * 
	 */
	public function getJadwal(Request $req)
	{
		\Session::put('booked',$req->all());

		if(empty($req->all()))
			return redirect('/');

		$res = route_detail::agen($req->all())->paginate(5);

		$pool 		= [];
		$facility 	= [];
		$hari		= [];

		foreach($res as $route){
			/* Boarding Point*/
			$pool[$route->id] = route_detail::find($route->id)->boardingpoint()->get();
			/* Facility*/
			$facility[$route->id] = routes::find($route->routes_id)->facility()
			->join('ref_facility', 'ref_facility.id', '=', 'relasi_facility.ref_facility_id')
			->get();
		}
		return view('client.jadwal', [
			'routes' => $res,
			'pools' => $pool,
			'req' => $req->all(),
			'facilitys' => $facility,
			'provinsi' => provinsi::all()
			]);
	}

	public function getSeat(Request $req){

		\Session::put('seat',$req->all());

		if(empty($req->all()))
			return redirect('/');

		$route = route_detail::Detailbyid($req->i);
		$pool = hubs::find($req->pool . $req->i);

		/* Back */
		$back = '';
		foreach (\Session::get('booked') as $key => $value) {
			$back .= $key . '=' . $value . '&';
		}
		return view('client.seat', [
			'route' => $route,
			'pool' => $pool,
			'req' => $req->all(),
			'back' => rtrim($back, '&')
			]);
	}

	public function getKodebooking(Request $req)
	{
		\Session::put('trans',$req->all());



	//	$trans = array();
		$trans = session('trans');
		$booked = session('booked');

		$ambil = array($trans);
		
		$rute = route_detail::join('routes', 'routes.id', '=', 'route_detail.routes_id')
		->join('travel', 'travel.id', '=', 'routes.travel_id')
		->join('kab_kota AS a', 'a.id', '=', 'route_detail.depart')
		->join('kab_kota AS b', 'b.id', '=', 'route_detail.destination')
		->where('route_detail.id', $trans['route_detail_id'])
		->select(
			'travel.*',
			'a.nm_kab_kota AS nm_depart',
			'b.nm_kab_kota AS nm_destination',
			'routes.*'
			)
		->first();
		$bp = hubs::find($trans['hubs_id']);
		
		/*Harga*/
		$total = 0;
		$kursi = '';

		$kode = strtoupper(substr($rute['nama_travel'], 0,2)) .  strtoupper(base_convert(rand(1111,9999), 30, 36));

		return view('client.kodebooking',[
			'trans' => $trans,
			'booked' => $booked,
			'rute' => $rute,
			'kode' => $kode,
			'ambil' => $ambil
			]);
	}

	public function postReservasi(Request $req){

		$this->dispatch(new CreateReservasiCommand($req->all()));

		/// Tambah email
		return redirect('/reservasi/finish?i=' . $req->kode_booking . '&_t=' . csrf_token());
		
	}

	public function getFinish(Request $req){
	//	$reservasi = reservasi::where('kode_booking','=',$req->i);
		$reservasi = DB::table('reservasi')->where('kode_booking','=',$req->i)->first();
		
		return view('client.finish', [
			'reservasi' => $reservasi
			]);

	}

	public function postFinish(Request $req){
		$this->validate($req, [
			'email' => 'required'
			]);
		\Event::fire(new SendKodeBookingToEmailEvent($req->all()));
		return redirect()->back()->withNotif([
			'label' => 'success',
			'err' => 'Email berhasil terkirim'
			]);
	}

	public function getCarikode(Request $req){

		$rest = reservasi::find($req->kode_booking);
		$rute = DB::table('reservasi')
		->join('route_detail','route_detail.id','=','reservasi.route_detail_id')
		->join('routes', 'routes.id', '=', 'route_detail.routes_id')
		->join('travel', 'travel.id', '=', 'routes.travel_id')
		->join('kab_kota AS a', 'a.id', '=', 'route_detail.depart')
		->join('kab_kota AS b', 'b.id', '=', 'route_detail.destination')
		->join('harga_tiket','harga_tiket.route_detail_id','=','route_detail.id')
		->where('kode_booking','=',$req->kode_booking)
		->select(
			'reservasi.*',
			'travel.*',
			'a.nm_kab_kota AS nm_depart',
			'b.nm_kab_kota AS nm_destination',
			'routes.*',
			'harga_tiket.harga'
			)
		->first();

		
		$ambil = reservasi_detail::join('reservasi','reservasi.id','=','reservasi_detail.reservasi_id')
		->where('reservasi.kode_booking','=',$req->kode_booking)->get();

		$Tot_harga = 0;
		foreach ($ambil as $value) {
			$Tot_harga += $value->harga;
		}


		return view('client.show_booked',[
			'ambil' => $ambil,
			'rute' => $rute,
			'Tot_harga' => $Tot_harga
			]);
	}

	public function getKendaraan(Request $req){

		return json_encode([
			'id' => $req->bus,

			/* -- Template Bus 32 seat 1 */
			'bus1' => '
			<div class="bodi-bus">
				<div class="kendaraan-content">
					<div class="row-kursi">
						<div id="lineA-1" class="kursi"><a href="javascript:void(0);"><img src="' . asset('/images/sopir_24.png') . '" title="JURU MUDI"></a></div>
					</div>

					<div class="row-kursi">
						<div id="lineA-1" class="kursi"><a class="bus-booking" data-kode="4" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">4</span></a></div>
						<div id="lineA-2" class="kursi"><a class="bus-booking" data-kode="3" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">3</span></a></div>
						<div id="lineA-3" class="row-round"></div>
						<div id="lineA-4" class="kursi"><a class="bus-booking" data-kode="2" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">2</span></a></div>
						<div id="lineA-5" class="kursi"><a class="bus-booking" data-kode="1" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">1</span></a></div>
					</div>

					<div class="row-kursi">
						<div id="lineB-1" class="kursi"><a class="bus-booking" data-kode="8" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">8</span></a></div>
						<div id="lineB-2" class="kursi"><a class="bus-booking" data-kode="7" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">7</span></a></div>
						<div id="lineB-3" class="row-round"></div>
						<div id="lineB-4" class="kursi"><a class="bus-booking" data-kode="6" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">6</span></a></div>
						<div id="lineB-5" class="kursi"><a class="bus-booking" data-kode="5" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">5</span></a></div>
					</div>
					<div class="row-kursi">
						<div id="lineC-1" class="kursi"><a class="bus-booking" data-kode="12" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">12</span></a></div>
						<div id="lineC-2" class="kursi"><a class="bus-booking" data-kode="11" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">11</span></a></div>
						<div id="lineC-3" class="row-round"></div>
						<div id="lineC-4" class="kursi"><a class="bus-booking" data-kode="10" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">10</span></a></div>
						<div id="lineC-5" class="kursi"><a class="bus-booking" data-kode="9" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">9</span></a></div>
					</div>
					<div class="row-kursi">
						<div id="lineD-1" class="kursi"><a class="bus-booking" data-kode="16" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">16</span></a></div>
						<div id="lineD-2" class="kursi"><a class="bus-booking" data-kode="15" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">15</span></a></div>
						<div id="lineD-3" class="row-round"></div>
						<div id="lineD-4" class="kursi"><a class="bus-booking" data-kode="14" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">14</span></a></div>
						<div id="lineD-5" class="kursi"><a class="bus-booking" data-kode="13" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">13</span></a></div>
					</div>
					<div class="row-kursi">
						<div id="lineE-1" class="kursi"><a class="bus-booking" data-kode="20" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">20</span></a></div>
						<div id="lineE-2" class="kursi"><a class="bus-booking" data-kode="19" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">19</span></a></div>
						<div id="lineE-3" class="row-round"></div>
						<div id="lineE-4" class="kursi"><a class="bus-booking" data-kode="18" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">18</span></a></div>
						<div id="lineE-5" class="kursi"><a class="bus-booking" data-kode="17" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">17</span></a></div>
					</div>
					<div class="row-kursi">
						<div id="lineF-1" class="kursi"><a class="bus-booking" data-kode="24" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">24</span></a></div>
						<div id="lineF-2" class="kursi"><a class="bus-booking" data-kode="23" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">23</span></a></div>
						<div id="lineF-3" class="row-round"></div>
						<div id="lineF-4" class="kursi"><a class="bus-booking" data-kode="22" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">22</span></a></div>
						<div id="lineF-5" class="kursi"><a class="bus-booking" data-kode="21" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">21</span></a></div>
					</div>
					<div class="row-kursi">
						<div id="lineG-1" class="kursi"><a class="bus-booking" data-kode="28" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">28</span></a></div>
						<div id="lineG-2" class="kursi"><a class="bus-booking" data-kode="27" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">27</span></a></div>
						<div id="lineG-3" class="row-round"></div>
						<div id="lineG-4" class="kursi"><a class="bus-booking" data-kode="26" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">26</span></a></div>
						<div id="lineG-5" class="kursi"><a class="bus-booking" data-kode="25" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">25</span></a></div>
					</div>
					<div class="row-kursi">
						<div id="lineH-1" class="kursi"><a class="bus-booking" data-kode="30" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">30</span></a></div>
						<div id="lineH-2" class="kursi"><a class="bus-booking" data-kode="29" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">29</span></a></div>
						<div id="lineH-3" class="row-round"></div>
						<div id="lineI-3" class="row-empty"></div>
						<div id="lineI-3" class="row-empty"></div>
						<div id="lineI-3" class="row-empty"></div>
						<div id="lineH-4" class="kursi"></div>
						<div id="lineH-5" class="kursi" style="border-bottom:5px solid javascript:void(0);333;"></div>
					</div>
					<div class="row-kursi">
						<div id="lineI-1" class="kursi"><a class="bus-booking" data-kode="32" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">32</span></a></div>
						<div id="lineI-2" class="kursi"><a class="bus-booking" data-kode="31" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">31</span></a></div>
						<div id="lineI-3" class="row-round"></div>
						<div id="lineI-3" class="row-empty"></div>
						<div id="lineI-3" class="row-empty"></div>
						<div id="lineI-4" class="kursi"></div>
						<div id="lineI-5" class="kursi"><img src="http://localhost:8000/images/toilet.png" title="Toilet"></div>
					</div>
				</div>
			</div>
			',
			/* -- Template Bus 32 seat 1 */
			'bus2' => '
			<div class="bodi-bus">
				<div class="kendaraan-content">
					<div class="row-kursi">
						<div id="lineA-1" class="kursi"><a class="bus-booking" data-kode="0" href="javascript:void(0);"><img src="' . asset('/images/sopir_24.png') . '" title="JURU MUDI"></a></div>
					</div>

					<div class="row-kursi">
						<div id="lineA-1" class="kursi"><a class="bus-booking" data-kode="4A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">4A</span></a></div>
						<div id="lineA-2" class="kursi"><a class="bus-booking" data-kode="3A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">3A</span></a></div>
						<div id="lineA-3" class="row-round"></div>
						<div id="lineA-4" class="kursi"><a class="bus-booking" data-kode="2A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">2A</span></a></div>
						<div id="lineA-5" class="kursi"><a class="bus-booking" data-kode="1A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">1A</span></a></div>
					</div>

					<div class="row-kursi">
						<div id="lineB-1" class="kursi"><a class="bus-booking" data-kode="8A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">8A</span></a></div>
						<div id="lineB-2" class="kursi"><a class="bus-booking" data-kode="7A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">7A</span></a></div>
						<div id="lineB-3" class="row-round"></div>
						<div id="lineB-4" class="kursi"><a class="bus-booking" data-kode="6A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">6A</span></a></div>
						<div id="lineB-5" class="kursi"><a class="bus-booking" data-kode="5A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">5A</span></a></div>
					</div>
					<div class="row-kursi">
						<div id="lineC-1" class="kursi"><a class="bus-booking" data-kode="12A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">12A</span></a></div>
						<div id="lineC-2" class="kursi"><a class="bus-booking" data-kode="11A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">11A</span></a></div>
						<div id="lineC-3" class="row-round"></div>
						<div id="lineC-4" class="kursi"><a class="bus-booking" data-kode="10A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">10A</span></a></div>
						<div id="lineC-5" class="kursi"><a class="bus-booking" data-kode="9A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">9A</span></a></div>
					</div>
					<div class="row-kursi">
						<div id="lineD-1" class="kursi"><a class="bus-booking" data-kode="16A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">16A</span></a></div>
						<div id="lineD-2" class="kursi"><a class="bus-booking" data-kode="15A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">15A</span></a></div>
						<div id="lineD-3" class="row-round"></div>
						<div id="lineD-4" class="kursi"><a class="bus-booking" data-kode="14A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">14A</span></a></div>
						<div id="lineD-5" class="kursi"><a class="bus-booking" data-kode="13A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">13A</span></a></div>
					</div>
					<div class="row-kursi">
						<div id="lineE-1" class="kursi"><a class="bus-booking" data-kode="20A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">20A</span></a></div>
						<div id="lineE-2" class="kursi"><a class="bus-booking" data-kode="19A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">19A</span></a></div>
						<div id="lineE-3" class="row-round"></div>
						<div id="lineE-4" class="kursi"><a class="bus-booking" data-kode="18A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">18A</span></a></div>
						<div id="lineE-5" class="kursi"><a class="bus-booking" data-kode="17A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">17A</span></a></div>
					</div>
					<div class="row-kursi">
						<div id="lineF-1" class="kursi"><a class="bus-booking" data-kode="24A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">24A</span></a></div>
						<div id="lineF-2" class="kursi"><a class="bus-booking" data-kode="23A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">23A</span></a></div>
						<div id="lineF-3" class="row-round"></div>
						<div id="lineF-4" class="kursi"><a class="bus-booking" data-kode="22A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">22A</span></a></div>
						<div id="lineF-5" class="kursi"><a class="bus-booking" data-kode="21A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">21A</span></a></div>
					</div>
					<div class="row-kursi">
						<div id="lineG-1" class="kursi"><a class="bus-booking" data-kode="28A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">28A</span></a></div>
						<div id="lineG-2" class="kursi"><a class="bus-booking" data-kode="27A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">27A</span></a></div>
						<div id="lineG-3" class="row-round"></div>
						<div id="lineG-4" class="kursi"><a class="bus-booking" data-kode="26A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">26A</span></a></div>
						<div id="lineG-5" class="kursi"><a class="bus-booking" data-kode="25A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">25A</span></a></div>
					</div>
					<div class="row-kursi">
						<div id="lineH-1" class="kursi"><a class="bus-booking" data-kode="30A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">30A</span></a></div>
						<div id="lineH-2" class="kursi"><a class="bus-booking" data-kode="29A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">29A</span></a></div>
						<div id="lineH-3" class="row-round"></div>
						<div id="lineI-3" class="row-empty"></div>
						<div id="lineI-3" class="row-empty"></div>
						<div id="lineI-3" class="row-empty"></div>
						<div id="lineH-4" class="kursi"></div>
						<div id="lineH-5" class="kursi" style="border-bottom:5px solid javascript:void(0);333;"></div>
					</div>
					<div class="row-kursi">
						<div id="lineI-1" class="kursi"><a class="bus-booking" data-kode="32A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">32A</span></a></div>
						<div id="lineI-2" class="kursi"><a class="bus-booking" data-kode="31A" href="javascript:void(0);"><img src="' . asset('/images/ava.png') . '" title="kursi kosong"><span class="no-kursi">31A</span></a></div>
						<div id="lineI-3" class="row-round"></div>
						<div id="lineI-3" class="row-empty"></div>
						<div id="lineI-3" class="row-empty"></div>
						<div id="lineI-4" class="kursi"></div>
						<div id="lineI-5" class="kursi"><img src="http://localhost:8000/images/toilet.png" title="Toilet"></div>
					</div>
				</div>
			</div>
			'
			]);
}
}
