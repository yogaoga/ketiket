<?php namespace App\Http\Requests\Otojasa;

use App\Http\Requests\Request;

class CreateOtojasaRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'nama_travel' => 'unique:travel|required',
			'alamat' => 'required',
			'no_tlp' => 'required|numeric',
			'provinsi_id' => 'required',
			'provinsi_id' => 'required'
		];
	}

}
