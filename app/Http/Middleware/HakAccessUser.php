<?php namespace App\Http\Middleware;

use App\Events\StatusLoginUser;
use Closure;

class HakAccessUser {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next){

		$set = new StatusLoginUser;
		$set->refresh();

		if(\Auth::check()){
			$menu = \Menu::HakAkses();
			
			if($menu == false)
				return redirect('/');
		}
		
		return $next($request);
	}

}
