<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class relasi_travel extends Model {

	protected $table = 'relasi_travel';
	protected $fillable = [
		'user_id',
		'travel_id',
		'level',
		'status',
		'token'
	];


	//////////////////////////////////// MODEL TRAVEL ///////////////////////
	/*
		Digunakan untuk mengambil data Travel sesuai dengan session login
		@access Public
		@param Auth::user()->id
		@return Travel_id
	*/
	public function scopeMetravel($query){
		$id = \Auth::check() ? \Auth::user()->id : 0;
		return $query->join('travel', function($join){
			return $join->on('travel.id', '=', 'relasi_travel.travel_id');
		})
		->where('relasi_travel.user_id', $id)
		->where('relasi_travel.status', 1)
		->where('travel.tipe', 1) #<---- Tipe untuk Otojasa / Travel
		->select('relasi_travel.*');
	}

	public function scopeAdmintravel($query, $id){
		return $query->join('users', function($join){
			return $join->on('users.id', '=', 'relasi_travel.user_id');
		})
		->where('relasi_travel.travel_id', $id)
		->where('relasi_travel.level', 1)
		->where('relasi_travel.status', 1)
		->select('users.*');
	}

	/*
		Digunakan untuk mengambil data admin dan Operator pada setiap Otojasa / Travel
		@access Public
		@param travel_id
		@return data users dalam Array object
	*/
	public function scopeUserstravel($query, $limit = 20){
		return $query->join('users', function($join){
			return $join->on('users.id', '=', 'relasi_travel.user_id');
		})
		->where('relasi_travel.travel_id', \Travel::data()->id)
		->where('relasi_travel.status', 1)
		->select('users.*', 'relasi_travel.level', 'relasi_travel.id as id_relasi')
		->paginate($limit);
	}

	//////////////////////////////////// MODEL AGEN ///////////////////////
	/*
		Digunakan untuk mengambil data Agen sesuai dengan session login
		@access Public
		@param Auth::user()->id
		@return Travel_id / disebit juga sebagai agen_id
	*/
	public function scopeMeagen($query){
		$id = \Auth::check() ? \Auth::user()->id : 0;
		return $query->join('travel', function($join){
			return $join->on('travel.id', '=', 'relasi_travel.travel_id');
		})
		->where('relasi_travel.user_id', $id)
		->where('relasi_travel.status', 1)
		->where('travel.tipe', 2) #<---- Tipe untuk Agen
		->select('relasi_travel.*');
	}

	/*
		Digunakan untuk mengambil data admin dan Operator pada setiap Agen
		@access Public
		@param travel_id / disebut juga sebagai agen_id
		@return data users dalam Array object
	*/
	public function scopeUsersagen($query, $limit = 20){
		return $query->join('users', function($join){
			return $join->on('users.id', '=', 'relasi_travel.user_id');
		})
		->where('relasi_travel.travel_id', \Agen::data()->id)
		->where('relasi_travel.status', 1)
		->select('users.*', 'relasi_travel.level', 'relasi_travel.id as id_relasi')
		->paginate($limit);
	}

}
