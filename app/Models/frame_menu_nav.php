<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class frame_menu_nav extends Model {

	protected $table = 'frame_menu_nav';
	protected $fillable = [
		'parent_id',
		'title',
		'slug',
		'class',
		'class_id',
		'seri',
		'ket',
		'status',
		'auth',
		'guest'
	];


}
