<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class relasi_facility extends Model {

	protected $table = 'relasi_facility';
	protected $fillable = [
		'routes_id',
		'ref_facility_id'
	];

}
