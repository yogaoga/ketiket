<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class reservasi extends Model {

	protected $table = 'reservasi';
	protected $fillable = [
		'users_id',
		'tgl_reservasi',
		'tgl_limit',
		'tgl_bayar',
		'keterangan',
		'kode_booking',
		'no_telp',
		'status_reservasi',
		'reservasi_by',
		'hubs_id',
		'route_detail_id'
	];

	public function detailkursi(){
		$this->hasOne('App\Models\reservasi_detail');
	}

}