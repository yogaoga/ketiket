<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ketiket_pages extends Model {

	protected $table = 'ketiket_pages';
	protected $fillable = [
		'about_me',
		'privacy_policy',
		'disclaimer',
		'about_front',
		'twitter',
		'facebook',
		'googleplus',
		'email_cs',
		'telp_cs'
	];

}
