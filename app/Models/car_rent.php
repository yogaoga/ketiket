<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\Paginator;
use Auth;
use DB;

use App\Models\car_img;
use App\Models\car_price;
use App\Models\car_booking;

class car_rent extends Model {
    
    protected $table = 'data_car_rent';
    protected $primaryKey = 'id_rentcar';
    protected $fillable = ['id_car_merk','id_car_type','id_car_tranms','id_car_fuel','plat_nomor','pintu','penumpang','ac','audio',
        'thn_model','kapasitas_bbm','komisi','id_kab_kota','lokasi','catatan','status','id_user','wajibdriver'];
    
    public static function get($user='',$status='',$req='',$sort='',$sortdir=''){
        
        $datas = car_rent::select(DB::raw('data_car_rent.*,nm_car_mnfcr,nm_car_merk,nm_car_fuel,nm_kab_kota,nm_car_type,nm_car_trnms'))
                    ->join('ref_car_merk','ref_car_merk.id_car_merk','=','data_car_rent.id_car_merk')
                    ->join('ref_car_manufacture','ref_car_manufacture.ref_car_mnfcr','=','ref_car_merk.id_manufacture')
                    ->join('ref_car_transmission','ref_car_transmission.id_car_trnms','=','data_car_rent.id_car_tranms')
                    ->join('ref_car_fuel','ref_car_fuel.id_car_fuel','=','data_car_rent.id_car_fuel')
                    ->join('ref_car_type','ref_car_type.id_car_type','=','data_car_rent.id_car_type')
                    ->join('kab_kota','kab_kota.id','=','data_car_rent.id_kab_kota');
        if($status != ''){
            if($status=='book'){
                $and = "";
                
                $ardate = explode("/", $req->tgl);
                $date=$ardate[2]."-".$ardate[0]."-".$ardate[1];

                $ardate = explode("/",$req->tgl1);
                $date1=$ardate[2]."-".$ardate[0]."-".$ardate[1];
                $ids=car_rent::to_dbarr(car_booking::get_booked($date." ".$req->jam.":".$req->menit.":00",$date1." ".$req->jam1.":".$req->menit1.":00"));
                if($req->cartype){
                    $and.=" AND data_car_rent.id_car_type='".$req->cartype."'";
                }
                if($req->manuf){
                    $manuf = car_rent::form_to_dbarr($req->manuf);
                    $and.=" AND ref_car_merk.id_manufacture IN(".$manuf.")";
                }
                if($req->trans){
                    $manuf = car_rent::form_to_dbarr($req->trans);
                    $and.=" AND data_car_rent.id_car_tranms IN(".$manuf.")";
                }
                $datas = $datas->whereRaw("(data_car_rent.status='1' OR data_car_rent.status='2') AND id_kab_kota='".$req->depart."' AND id_rentcar NOT IN($ids) $and");
            }else{
                $datas = $datas->where('data_car_rent.status','=',$status);
            }
        }
        if($user != ''){
            $datas = $datas->where('id_user','=',$user);
        }
        if($sort != ''){
            $datas = $datas->orderBy($sort,$sortdir);
        }
        return $datas;
    }
    public static function get_paging($curpage,$limit,$user='',$status='',$req='',$sort='',$sortdir=''){
        Paginator::currentPageResolver(function() use ($curpage){
            return $curpage;
        });
        $data = car_rent::get($user,$status,$req,$sort,$sortdir);
        
        $datacol = $data->get();
        $grouped = $datacol->groupBy('nm_car_mnfcr');
        
        $datas = $data->paginate($limit);
        $ndata = count($datas);
        for($i=0;$i<$ndata;$i++){
            $id = $datas[$i]['id_rentcar'];
            $dataprice = car_price::get($id);
            $datas[$i]['foto']=  car_img::get_primer($id);
            $datas[$i]['price']=  $dataprice;
            //$datas[$i]['harga']=  $dataprice[0]['price'];
        }
        
        $datas->pabrik = $grouped;
        return $datas;
        
    }
    
    public static function get_terkini(){
        $datas = car_rent::orderBy('id_rentcar','desc')->take(8)->get();
        $ndata = count($datas);
        for($i=0;$i<$ndata;$i++){
            $id = $datas[$i]['id_rentcar'];
            $datas[$i]['foto']=  car_img::get_primer($id);
        }
        return $datas;
    }
    public static function get_detail($id){
        $datas = car_rent::select(DB::Raw('data_car_rent.*,nm_car_mnfcr,nm_car_merk'))
                ->join('ref_car_merk','ref_car_merk.id_car_merk','=','data_car_rent.id_car_merk')
                ->join('ref_car_manufacture','ref_car_manufacture.ref_car_mnfcr','=','ref_car_merk.id_manufacture')
                ->where('id_rentcar','=',$id)->first();
        return $datas;
    }

    public static function cekCar($nomor){
        return car_rent::where('plat_nomor','=',$nomor)->count();
    }
    public static function cekCar_wtid($nomor,$id){
        return car_rent::where('plat_nomor','=',$nomor)->where('id_rentcar','<>',$id)->count();
    }
    public static function insert($request){
        $plat_nomor = str_replace(" ","",$request->plat);
        $id_car_merk = $request->merk;
        $id_car_type = $request->tipe;
        $id_car_tranms = $request->trans;
        $id_car_fuel = $request->bbm;
        
        $pintu = $request->pintu;
        $penumpang = $request->penumpang;
        $ac = $request->ac;
        $audio = $request->audio;
        $thn_model = $request->tahun;
        $kapasitas_bbm = $request->kapasitas;
        if($request->komisi > 0){$komisi = $request->komisi;}else{$komisi = 0;}
        $id_kab_kota = $request->kota;
        $lokasi = $request->lok;
        $catatan = $request->cat;
        $status = 0;
        $id_user = Auth::user()->id;
        $wajibdriver = $request->driver;
        $car = car_rent::create(compact('id_car_merk','id_car_type','id_car_tranms','id_car_fuel','plat_nomor','pintu','penumpang','ac','audio','thn_model','kapasitas_bbm','komisi','id_kab_kota','lokasi','catatan','status','id_user','wajibdriver'));
        return DB::getPdo()->lastInsertId();
        
    }
    public static function to_dbarr($arr){
        if(count($arr)>0){
            $ls="'".$arr[0]->id."'";
            for($i=1;$i<count($arr);$i++){
                $ls.=",'".$arr[$i]->id."'";
            }
            return $ls;
        }else{
            return "''";
        }
    }
    public static function form_to_dbarr($arr){
        if(count($arr)>0){
            $ls="'".$arr[0]."'";
            for($i=1;$i<count($arr);$i++){
                $ls.=",'".$arr[$i]."'";
            }
            return $ls;
        }else{
            return "''";
        }
    }
    public static function updet($req){
        return car_rent::where('id_rentcar','=',$req->id)->update(['id_car_merk'=>$req->merk,'id_car_type'=>$req->tipe,'id_car_tranms'=>$req->trans,
            'id_car_fuel'=>$req->bbm,'plat_nomor'=>$req->plat,'pintu'=>$req->pintu,'penumpang'=>$req->penumpang,'ac'=>$req->ac,'audio'=>$req->audio,
        'thn_model'=>$req->tahun,'kapasitas_bbm'=>$req->kapasitas,'komisi'=>$req->komisi,'id_kab_kota'=>$req->kota,'lokasi'=>$req->lok,
            'catatan'=>$req->cat,'wajibdriver'=>$req->driver]);
    }
    
}
