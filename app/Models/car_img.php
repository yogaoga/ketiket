<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Storage;
use DB;
class car_img extends Model {

    protected $table = 'data_car_img';
    protected $primaryKey = 'id_car_img';
    public $timestamps = false;
    protected $fillable = ['id_rentcar','filesize','img','primer'];
    
    public static function get($idrent){
        return car_img::where('id_rentcar','=',$idrent)->get();
    }
    public static function get_primer($idrent){
        $data = car_img::where('id_rentcar','=',$idrent)->where('primer','=',1)->first();
        if($data)
            return $data->img;
        else
            return "nofoto.png";
    }
    public static function get_last_nama($idrent){
        $data = car_img::select(DB::raw("SUBSTRING_INDEX(SUBSTRING_INDEX(img,'-',-1),'.',1) idx"))->where('id_rentcar','=',$idrent)->orderBy('idx','desc')->first();
        if($data){
            return $data->idx;
        }else{
            return 0;
        }
    }

    public static function updet($req){
        DB::beginTransaction();//SUPAYA BISA DI ROLLBACK JIKA ADA SALAH SATU TRANSAKSI DB YANG GAGAL
            $id_rentcar = $req->idrent;
            $path='images/rental/';
            $fullpath=storage_path().'/app/'.$path.'/';
            $n = car_img::get_last_nama($id_rentcar);
            if($req->file('foto')){
                foreach($req->file('foto') as $fl){
                    if($fl){
                        $n++;
                        $ext = $fl->getClientOriginalExtension();
                        $img = 'foto-'.$id_rentcar.'-'.$n.".".$ext;//NAMA FOTO STANDAR
                        $imgth = 'foto-'.$id_rentcar.'-'.$n.".thumb.".$ext;//NAMA FOTO THUMB
                        $fileori = file_get_contents($fl);
                        \Image::make($fileori)->resize(520, 293)->save($fullpath.$img);//RESIZE FOTO STANDAR
                        \Image::make($fileori)->resize(50, 50)->save($fullpath.$imgth);//RESIZE FOTO THUMB
                        $filesize=Storage::size($path.$img);
                        car_img::create(compact('id_rentcar','filesize','img','primer'));//INSERT KE DB
                    }
                }
            }
            if($req->file('fotoedit')){
                $n = 0;
                foreach($req->file('fotoedit') as $fl){
                    if($fl){
                        $ext = $fl->getClientOriginalExtension();
                        $img = $req->namafoto[$n];//NAMA FOTO STANDAR
                        $eximg = explode(".", $img);
                        $imgth = $eximg[1].".thumb.".$ext;//NAMA FOTO THUMB
                        $fileori = file_get_contents($fl);
                        \Image::make($fileori)->resize(520, 293)->save($fullpath.$img);//RESIZE FOTO STANDAR
                        \Image::make($fileori)->resize(50, 50)->save($fullpath.$imgth);//RESIZE FOTO THUMB
                        $filesize=Storage::size($path.$img);
                        car_img::where('id_car_img','=',$req->idfoto[$n])->update(['filesize'=>$filesize]);
                    }
                    $n++;
                }
            }
        DB::commit();
    }

    /*
    Fungsi insert untuk menyimpan foto mobil ke database dan mengunggah foto ke folder images/rental
    Dipakai di CarRentController
    */    
    public static function insert($file,$id_rentcar,$primerval){

        $path='images/rental/';
        $fullpath=storage_path().'/app/'.$path.'/';

        //MEMASTIKAN FOLDER ADA, JIKA TIDAK ADA MAKA DIBUAT FOLDERNYA
        $folderexist=Storage::disk('local')->exists($path);
        if(!$folderexist){
            Storage::makeDirectory($path);
        }
        $n=0;$nama="";$nprimer=0;
        //LOOPING FOTO YANG DIUNGGAH USER
        foreach($file as $fl){
            if($fl){
                
                $ext = $fl->getClientOriginalExtension();
                $img = 'foto-'.$id_rentcar.'-'.$n.".".$ext;//NAMA FOTO STANDAR
                $imgth = 'foto-'.$id_rentcar.'-'.$n.".thumb.".$ext;//NAMA FOTO THUMB
                $fileori = file_get_contents($fl);
                \Image::make($fileori)->resize(520, 293)->save($fullpath.$img);//RESIZE FOTO STANDAR
                \Image::make($fileori)->resize(50, 50)->save($fullpath.$imgth);//RESIZE FOTO THUMB
                $filesize=Storage::size($path.$img);
                if($primerval==$n){$primer=1;$nprimer++;}else{$primer=0;}
                car_img::create(compact('id_rentcar','filesize','img','primer'));//INSERT KE DB
            }
            $n++;
        }
        //MEMASTIKAN ADA FOTO PRIMER, JIKA TIDAK ADA MAKA FOTO YANG PERTAMA DIUNGGAH DIJADIKAN FOTO PRIMER
        if($nprimer==0){
            car_img::where('id_rentcar','=',$id_rentcar)->limit(1)->update(['primer'=>1]);
        }
        
    }
    public static function updetPrimer($req){
        car_img::where('id_rentcar','=',$req->idrent)->update(['primer'=>0]);
        car_img::where('id_car_img','=',$req->id)->update(['primer'=>1]);
    }
}
