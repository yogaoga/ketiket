<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class route_detail extends Model {

	protected $table = 'route_detail';
	protected $fillable = [
		'routes_id',
		'depart',
		'destination',
		'waktu',
		'boarding_point'
	];

	public function scopeListroute($query, $routes_id){
		return $query->join('routes', function($join){
			return $join->on('routes.id', '=', 'route_detail.routes_id');
		})
		->join('harga_tiket', function($join){
			return $join->on('harga_tiket.route_detail_id', '=', 'route_detail.id');
		})
		->join('kab_kota AS a', function($join){
			return $join->on('a.id', '=', 'route_detail.depart');
		})
		->join('kab_kota AS b', function($join){
			return $join->on('b.id', '=', 'route_detail.destination');
		})
		->where('routes.travel_id', \Travel::data()->id)
		->where('route_detail.routes_id', $routes_id)
		->where('route_detail.status', 1)
		->select(
			'route_detail.*', 
			'harga_tiket.harga', 
			'a.nm_kab_kota AS nm_depart', 
			'b.nm_kab_kota AS nm_destination', 
			'routes.depart AS master_depart', 
			'routes.destination AS master_destination'
		);
	}

	/* Mengabil boarding point */
	public function boardingpoint(){
		return $this->hasOne('App\Models\hubs');
	}

	/*
	Menampilkan data hsil pencarian dari Agen
	*/
	public function scopeAgensrc($query,array $src){

		$tgl = date('Y-m-d', strtotime($src['tgl']));

		if(isset($src['diskon']))
			$res = $query->join('persenharga', 'persenharga.route_detail_id', '=', 'route_detail.id');
		else
			$res = $query->leftJoin('persenharga', 'persenharga.route_detail_id', '=', 'route_detail.id');
		/* -- Joins -- */
		$res->join('kab_kota AS a', 'a.id', '=', 'route_detail.depart')
		->join('kab_kota AS b', 'b.id', '=', 'route_detail.destination')
		->join('routes', 'routes.id', '=', 'route_detail.routes_id')
		->join('travel', 'travel.id', '=', 'routes.travel_id')
		->join('harga_tiket', 'harga_tiket.route_detail_id', '=', 'route_detail.id')
		->join('template_mobil', 'template_mobil.id', '=', 'routes.template_mobile');
		/* -- Where -- */
		$res->where('route_detail.depart', $src['depart'])
		->where('route_detail.destination', $src['destination'])
		->where('routes.status', 1)
		->where('routes.tgl_aktif', '<=', $tgl)
		->where('routes.tgl_limit', '>=', $tgl)
		->where('persenharga.tgl_aktif', '<=', $tgl)
		->where('persenharga.tgl_limit', '>=', $tgl)
		->orderby('persenharga.persen', $src['sort'])
		->select(
			'route_detail.*',
			'persenharga.persen',
			'a.nm_kab_kota AS nm_depart',
			'b.nm_kab_kota AS nm_destination',
			'routes.kode_routes',
			'routes.day_route',
			'routes.kelas',
			'travel.nama_travel',
			'travel.logo',
			'harga_tiket.harga',
			'template_mobil.nm_templatemobil AS bus'
		);
		
	}

	public function scopeDetailbyid($query, $id){
		return $query->join('routes', 'routes.id', '=', 'route_detail.routes_id')
		->join('travel', 'travel.id', '=', 'routes.travel_id')
		->join('kab_kota AS a', 'a.id', '=', 'route_detail.depart')
		->join('kab_kota AS b', 'b.id', '=', 'route_detail.destination')
		->join('harga_tiket', 'harga_tiket.route_detail_id', '=', 'route_detail.id')
		->join('template_mobil', 'template_mobil.id', '=', 'routes.template_mobile')
		->leftJoin('persenharga', 'persenharga.route_detail_id', '=', 'route_detail.id')
		->where('route_detail.id', $id)
		->select(
			'route_detail.*',
			'routes.*',
			'harga_tiket.harga',
			'persenharga.persen',
			'travel.nama_travel', 
			'travel.logo', 
			'a.nm_kab_kota AS nm_depart',
			'b.nm_kab_kota AS nm_destination',
			'template_mobil.nm_templatemobil AS bus'
		)
		->first();
	}

	public function scopeAgen($query,array $src){
		$tgl = date('Y-m-d', strtotime($src['tgl']));
		/* -- Joins -- */
		$query->join('kab_kota AS a', 'a.id', '=', 'route_detail.depart')
		->join('kab_kota AS b', 'b.id', '=', 'route_detail.destination')
		->join('routes', 'routes.id', '=', 'route_detail.routes_id')
		->join('travel', 'travel.id', '=', 'routes.travel_id')
		->join('harga_tiket', 'harga_tiket.route_detail_id', '=', 'route_detail.id')
		->join('template_mobil', 'template_mobil.id', '=', 'routes.template_mobile');
		/* -- Where -- */
		$query->where('route_detail.depart', $src['depart'])
		->where('route_detail.destination', $src['destination'])
		->where('routes.status', 1)
		->where('routes.tgl_aktif', '<=', $tgl)
		->where('routes.tgl_limit', '>=', $tgl)
		->select(
			'route_detail.*',
			'a.nm_kab_kota AS nm_depart',
			'b.nm_kab_kota AS nm_destination',
			'routes.kode_routes',
			'routes.day_route',
			'routes.kelas',
			'travel.nama_travel',
			'travel.logo',
			'harga_tiket.harga',
			'template_mobil.nm_templatemobil AS bus'
		);
	}

}
