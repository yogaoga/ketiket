<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class jadwal extends Model {

	protected $table = 'routes';
	protected $fillable = [
		'kode_routes',
		'depart',
		'destination',
		'day_route',
		'status',
		'tgl_aktif',
		'travel_id',
		'tgl_limit',
		'template_mobile',
		'kelas',
		'keterangan',
		'created_by'
	];

   /**
	* @author yogaoga [yoga@valdlabs.com]
	* 
    */

	public function scopeTampil($query,$jadwal,$depart,$destination){

		return $query->join('travel','travel.id','=','routes.travel_id')
		->join('route_detail','route_detail.routes_id','=','routes.id')
		->join('harga_tiket','harga_tiket.route_detail_id','=','route_detail.id')
		->join('template_mobil','template_mobil.id','=','routes.template_mobile')
		->join('hubs','route_detail.id','=','hubs.route_detail_id')
		->where('route_detail.depart','=',$depart)
		->where('route_detail.destination','=',$destination)
		->where('routes.tgl_limit','>=', $jadwal)
		->select('routes.*','travel.nama_travel','hubs.waktu','hubs.lokasi','harga_tiket.harga','template_mobil.nm_templatemobil','template_mobil.jumlah_kursi');

	}

}
