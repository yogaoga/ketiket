<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class travel extends Model {

	protected $table = 'travel';
	protected $fillable = [
		'nama_travel',
		'information',
		'hint',
		'alamat',
		'no_tlp',
		'provinsi_id',
		'hashtag',
		'logo',
		'website',
		'api_key',
		'rate_index',
		'slug',
		'tipe'
	];

	/*
		Digunakan untuk mengambil data travel sesuai id login
		@access public 
		@param user_id
		@return data lengkap travel + level dari travel
	*/
	public function scopeMe($query, $id){
		return $query->join('relasi_travel', function($join){
			return $join->on('relasi_travel.travel_id', '=', 'travel.id');
		})
		->where('travel.id', $id)
		->where('relasi_travel.user_id', \Auth::user()->id)
		->select('travel.*', 'relasi_travel.level')
		->first();
	}

	/*
	digunakan untuk mengecek data travel yang belum ter verifikasi oleh admin
	@access Admin/superadmin
	@param status 0
	@return daftar semua travel yang tida aktif
	*/
	public function scopeListnewtravel($query, $src){
		$res = $query->join('provinsi', function($join){
			return $join->on('provinsi.id', '=', 'travel.provinsi_id');
		})
		->join('relasi_travel', function($join){
			return $join->on('relasi_travel.travel_id', '=', 'travel.id');
		})
		->join('users', function($join){
			return $join->on('users.id', '=', 'relasi_travel.user_id');
		})
		->where('travel.status', 0)
		->where('travel.tipe', 1);

		/* Saat melakukan pencarian nama travel dari Parameter GET['src'] /?src=keyword */
		if(!empty($src))
			$res->where('travel.nama_travel', 'LIKE', '%' . $src . '%');

		$res->select(
			'travel.*',
			'travel.no_tlp',
			'provinsi.nm_provinsi',
			'users.name'
		);
	}
		/*
	digunakan untuk mengecek data agen yang belum ter verifikasi oleh admin
	@access Admin/superadmin
	@param status 0
	@return daftar semua agen yang tida aktif
	*/
	public function scopeListnewagen($query, $src){
		$res = $query->join('provinsi', function($join){
			return $join->on('provinsi.id', '=', 'travel.provinsi_id');
		})
		->join('relasi_travel', function($join){
			return $join->on('relasi_travel.travel_id', '=', 'travel.id');
		})
		->join('users', function($join){
			return $join->on('users.id', '=', 'relasi_travel.user_id');
		})
		->where('travel.status', 0)
		->where('travel.tipe', 2);

		/* Saat melakukan pencarian nama agen dari Parameter GET['src'] /?src=keyword */
		if(!empty($src))
			$res->where('travel.nama_travel', 'LIKE', '%' . $src . '%');

		$res->select(
			'travel.*',
			'travel.no_tlp',
			'provinsi.nm_provinsi',
			'users.name'
		);
	}
}
