<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class harga_tiket extends Model {

	protected $table = 'harga_tiket';
	protected $fillable = [
		'route_detail_id',
		'tgl_aktif',
		'tgl_limit',
		'status_harga',
		'harga'
	];


	public function scopeHargabyroute($query, $from, $to){
		return $query->join('route_detail', function($join){
			return $join->on('route_detail.id', '=', 'harga_tiket.route_detail_id');
		})
		->join('routes', function($join){
			return  $join->on('routes.id', '=', 'route_detail.routes_id');
		})
		->where('routes.travel_id', \Travel::data()->id)
		->where('route_detail.depart', $from)
		->where('route_detail.destination', $to)
		->select('harga_tiket.harga AS price')
		->first();
	}


}
