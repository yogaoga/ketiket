<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class kab_kota extends Model {

	protected $table = 'kab_kota';
	protected $fillable = [
		'nm_kab_kota',
		'provinsi_id',
		'status'
	];

}
