<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class transaksi extends Model {

	
	protected $table = 'transaksi';
	protected $fillable = [
		'users_id',
		'reservasi_id',
		'tgl_transaksi',
		'nominal',
		'jenisbayar',
		'penambahan',
		'total',
		'status_transaksi',
		'keterangan',
		'pengurangan'
	];

}
