<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class routes extends Model {

	protected $table = 'routes';
	protected $fillable = [
		'kode_routes',
		'depart',
		'destination',
		'day_route',
		'status',
		'tgl_aktif',
		'travel_id',
		'tgl_limit',
		'template_mobile',
		'kelas',
		'keterangan',
		'created_by'
	];
	/*
	Menampilkan daftar Rute berdasarkan session
	*/
	public function scopeDaftar($query, $src){
		$daftar = $query->join('kab_kota AS a', function($join){
			return $join->on('a.id', '=', 'routes.depart');
		})
		->join('kab_kota AS b', function($join){
			return $join->on('b.id', '=', 'routes.destination');
		})
		->join('users', function($join){
			return $join->on('users.id', '=', 'routes.created_by');
		});

		if(!empty($src))
			$daftar->where('routes.kode_routes', 'LIKE', '%' . $src . '%');

		$daftar->where('travel_id', \Travel::data()->id)
		->whereIn('routes.status', [0,1]) /* <--- Aktif dan tidak aktif*/
		->select(
			'a.nm_kab_kota AS dari',
			'b.nm_kab_kota AS ke',
			'routes.*',
			'users.name AS oleh'
		)
		->orderBy('routes.id', 'desc');
		

		return $daftar;

	}

	/*
	Menampilkan Rute berdasarkan Travel dan Kode rute
	*/
	public function scopeShow($query, $travel_id, $kode){
		$route = $query->join('kab_kota AS a', function($join){
			return $join->on('a.id', '=', 'routes.depart');
		})
		->join('kab_kota AS b', function($join){
			return $join->on('b.id', '=', 'routes.destination');
		})
		->where('routes.kode_routes', $kode)
		->where('routes.travel_id', $travel_id);
		/* Permission */
		if(\Travel::check() && \Travel::data()->id == $travel_id)
			$route->whereIn('routes.status', [0,1]);
		else
			$route->where('routes.status', 1);
		
		$route->select('routes.*', 'a.nm_kab_kota AS nm_depart', 'b.nm_kab_kota AS nm_destination');
	}

	/*
	Menampilkan Rute Berdasarkan ID dan session
	*/
	public function scopeDetailbysession($query, $id){
		return $query->join('kab_kota AS a', function($join){
				return $join->on('a.id', '=', 'routes.depart');
			})
			->join('kab_kota AS b', function($join){
				return $join->on('b.id', '=', 'routes.destination');
			})
			->where('routes.id', $id)
			->where('routes.travel_id', \Travel::data()->id)
			->select('routes.*', 'a.nm_kab_kota AS nm_depart', 'b.nm_kab_kota AS nm_destination');
	}

	/*
	Mengambil rute detail
	*/
	public function detail(){
		return $this->hasOne('App\Models\route_detail');
	}

	/* Mengambil data Fasility */
	public function facility(){
		return $this->hasOne('App\Models\relasi_facility');
	}

}
