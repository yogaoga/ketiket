<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Pagination\Paginator;

class car_booking extends Model {

    protected $table = 'data_car_booking';
    protected $primaryKey = 'id_car_booking';
    protected $fillable = ['id_rental','pickup_location','return_location','pickup_datetime','return_datetime','id_car_extra',
        'diskon','adjustment','total_price','nama','contact','alamat','id_card','no_idcard','catatan','status','id_user','price',
        'no_invoice','email','parent','catatan_admin','car_time','grand_total','id_car_time','lama_sewa','jml_paket_harga'];
    
    /* yoga@valdlabs.com */
    public function scopeDetail($query)
    {
        $car_booking = $query->join('data_car_rent','data_car_rent.id_rentcar','=','data_car_booking.id_rental')
        ->join('ref_car_merk','ref_car_merk.id_car_merk','=','data_car_rent.id_car_merk')
        ->join('ref_car_manufacture','ref_car_manufacture.ref_car_mnfcr','=','ref_car_merk.id_manufacture')
        ->leftJoin('ref_idcard','data_car_booking.id_card','=','ref_idcard.id')
        ->select('data_car_booking.*','data_car_rent.plat_nomor','ref_car_merk.nm_car_merk','ref_car_manufacture.nm_car_mnfcr','data_car_rent.id_user');
        
        return $car_booking;

    }
    /////////////////////////////
    
    public static function get_detail($no_invoice,$id=0) {
        $data = car_booking::detail();
        if($id>0){
            $data = $data->where('id_car_booking','=',$id);
        }else{
            $data = $data->where('no_invoice','=',$no_invoice);
        }
        $data = $data->orderBy('parent')->get();
        if($data[0]['parent']!=0){
            $parent = car_booking::detail()->where('no_invoice','=',$no_invoice)->where('parent','=',0)->first();
            if($parent){$data->parent=$parent;}
            else{$data->parent=$data[0];}
        }else{
            $data->parent=$data[0];
        }
        $adjust= car_booking::select('adjustment','diskon')->where('no_invoice','=',$no_invoice)->where('status','<>',4)->first();
        if(!$adjust){
            $adjust=(object)array();
            $adjust->adjustment=0;
            $adjust->diskon=0;
        }
        $data->adjust=$adjust;
        
        $cancel= car_booking::where('no_invoice','=',$no_invoice)->where('status','=',4)->sum('adjustment');
        if($cancel)$data->cancelfee=$cancel;
        else $data->cancelfee=0;
        return $data;
    }
    public static function get_paging($curpage,$limit,$status,$user='',$search='',$fieldsearch){
        Paginator::currentPageResolver(function() use ($curpage){
            return $curpage;
        });
        $data = car_booking::detail();
        if($status !=0){
            $data = $data->where('data_car_booking.status','=',$status);
        }
        if($user !=''){
            $data = $data->where('data_car_rent.id_user','=',$user);
        }
        if($search !='' && $fieldsearch!='all'){
            $data = $data->where($fieldsearch,'LIKE','%'.$search.'%');
        }
        $data = $data->orderBy('pickup_datetime')->orderBy('no_invoice')->orderBy('parent');
        $datas = $data->paginate($limit);
        return $datas;
        
    }
    public static function get_booked($pickuptime,$returntime){
        return car_booking::select('id_rental as id')
                ->whereRaw("status <>4 AND (pickup_datetime<='$pickuptime' OR pickup_datetime<='$returntime') AND (return_datetime>='$returntime' OR return_datetime>='$pickuptime')")
                ->get();
    }
    public static function get_noinvoice(){
        $tahun = date('Y');
        $bulan = date('m');
        $book = car_booking::select('no_invoice')->whereRaw("YEAR(created_at)='".$tahun."' AND MONTH(created_at)='".$bulan."'")->orderBy('no_invoice','desc')->first();
        if($book){
            $nomor = $book->no_invoice;
            $arnomor = explode(".", $nomor);
            $id=(int)$arnomor[1];
            $id++;
            if(strlen($id)==1){$id="00".$id;}
            elseif(strlen($id)==2){$id="0".$id;}
        }else{$id='001';}
        return date('Ym').".".$id;
    }

    //MENGINSERT DATA BOOKING SEWA MOBIL
    public static function insert($req){
        $ncar = count($req->car);//MENGHITUNG JUMLAH MOBIL YANG DISEWA
        $status = 1;//STATUS BOOKING BARU
        $parent=0;
        $pickup_location=$req->depart;
        if($req->destination && $req->return==1){$return_location=$req->destination;}
        else{$return_location=$req->depart;}
        $grand_total=$req->total;//GRAND TOTAL 1 INVOICE (TOTAL SEMUA MOBIL)
        $no_invoice=  car_booking::get_noinvoice();
        for($i=0;$i<$ncar;$i++){//LOOPING MOBIL YANG DISEWA
            //DATA PER MOBIL : TGL BERANGKAT, TGL KEMBALI, SATUAN HARGA SEWA, EXTRA, SUBTOTAL=TOTAL PRICE
            $id_rental = $req->car[$i];
            
            $ardate = explode("/", $req->tgl[$i]);
            $date=$ardate[2]."-".$ardate[0]."-".$ardate[1];
            
            $ardate = explode("/",$req->tgl1[$i]);
            $date1=$ardate[2]."-".$ardate[0]."-".$ardate[1];
            $pickup_datetime = $date.' '.$req->jam[$i].':'.$req->menit[$i].":00";
            $return_datetime = $date1.' '.$req->jam1[$i].':'.$req->menit1[$i].":00";
            $car_time = $req->cartime[$i];
            $id_car_time = $req->idcartime[$i];
            $total_price=$req->subtotal[$i];
            $lama_sewa = $req->lamasewa[$i];
            $jml_paket_harga = $req->nprice[$i];
            $id_car_extra="";
            $price = $req->price[$i];
            
            if(isset($req->extras[$i])){//DATA FASILITAS EXTRA MOBIL
                $nextra = count($req->extras[$i]);
                for($j=0;$j<$nextra;$j++){
                    if($j==0){$kom="";}else{$kom=",";}
                    $id_car_extra.=$kom.$req->extras[$i][$j];
                    $price.=",".$req->pricex[$i][$j];
                }
            }
            if($i==0){//DATA USER HANYA DIINSERT DI PARENT
                $nama = $req->nama." ".$req->nama1;
                $contact = $req->nokontak;
                $alamat = $req->alamat;
                $id_card = $req->idcard;
                $no_idcard = $req->idno;
                $catatan = $req->note;
                $email = $req->email;
            }else{
                $nama = "";
                $contact = "";
                $alamat = "";
                $id_card = 0;
                $no_idcard = "";
                $catatan = "";
                $email = "";
            }
            
            car_booking::create(compact('id_rental','pickup_location','return_location','pickup_datetime','return_datetime',
                    'id_car_extra','diskon','adjustment','total_price','nama','contact','alamat','id_card','no_idcard','catatan',
                    'status','id_user','price','no_invoice','email','parent','catatan_admin','car_time','grand_total',
                    'id_car_time','lama_sewa','jml_paket_harga'));
            if($i==0){$parent=DB::getPdo()->lastInsertId();}//DATA MOBIL PERTAMA MENJADI PARENT
        }
        return $no_invoice;
    }

    
    
   
    
}
