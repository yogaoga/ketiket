<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ref_facility extends Model {

	protected $table = 'ref_facility';
	protected $fillable = [
		'kode',
		'keterangan',
		'catatan',
		'logo'
	];

}
