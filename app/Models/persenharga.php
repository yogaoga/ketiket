<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class persenharga extends Model {

	protected $table = 'persenharga';
	protected $fillable = [
		'route_detail_id',
		'tgl_aktif',
		'tgl_limit',
		'persen'
	];

	public function scopeMe($query, $src = ''){
		$persen = $query->join('route_detail', 'route_detail.id', '=', 'persenharga.route_detail_id')
		->join('routes', 'routes.id', '=', 'route_detail.routes_id')
		->join('kab_kota AS a', 'a.id', '=', 'route_detail.depart')
		->join('kab_kota AS b', 'b.id', '=', 'route_detail.destination')
		->join('harga_tiket', 'harga_tiket.route_detail_id', '=', 'route_detail.id')
		->where('routes.travel_id', \Travel::data()->id)
		->where('route_detail.status', 1);

		if(!empty($sec))
			$persen->where('routes.kode_rute', 'LIKE', '%' . $src . '%');

		$persen->select(
			'persenharga.*',
			'a.nm_kab_kota AS nm_depart',
			'b.nm_kab_kota AS nm_destination',
			'harga_tiket.harga',
			'routes.kode_routes',
			'routes.id AS routes_id',
			'route_detail.id AS route_detail_id'
		);
		return $persen;
	}

}