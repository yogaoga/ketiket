<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class reservasi_detail extends Model {

	protected $table = "reservasi_detail";
	protected $fillable = [
		'users_id',
		'reservasi_id',
		'nama_penumpang',
		'kursi',
		'harga',
		'status_bayar',
		'tlp',
		'discount'
	];

}
