<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class frame_relasi_akses_user extends Model {

	protected $table = 'frame_relasi_akses_user';
	protected $fillable = [
		'frame_menu_nav_id',
		'frame_level_user_id'
	];

}
