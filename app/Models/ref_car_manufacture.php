<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
class ref_car_manufacture extends Model {

    protected $table = 'ref_car_manufacture';
    protected $primaryKey = 'ref_car_mnfcr';
    public $timestamps = false ;
    
    public static function getWithMerk(){
        $data = ref_car_manufacture::where('status','=',1)->orderBy('nm_car_mnfcr')->get();
        $ndata = count($data);
        for($i=0;$i<$ndata;$i++){
            $merk = DB::table('ref_car_merk')->where('id_manufacture','=',$data[$i]['ref_car_mnfcr'])->where('status','=',1)->orderBy('nm_car_merk')->get();
            $data[$i]['merk'] = $merk;
        }
        return $data;
    }

}
