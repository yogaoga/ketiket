<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class frame_level_user extends Model {

	protected $table = 'frame_level_user';
	protected $fillable = [
		'nm_level',
		'status'
	];

}
