<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class comment extends Model {

	protected $table = 'comment';
	protected $fillable = [
		'comment',
		'user_id',
		'nama',
		'email',
		'status',
		'travel_id'
	];
}