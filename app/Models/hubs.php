<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class hubs extends Model {

	protected $table = 'hubs'; 
	protected $fillable = [
		'route_detail_id',
		'kota_id',
		'lokasi',
		'lang',
		'lon',
		'waktu'
	];

}
