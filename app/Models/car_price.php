<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class car_price extends Model {

    protected $table = 'data_car_price';
    protected $fillable = ['id_rentcar','price','id_car_time'];
    public $timestamps = false;
    
    public static function get($id_rentcar){
        return car_price::select('data_car_price.id','price','nm_car_time','data_car_price.id_car_time')->join('ref_car_time','ref_car_time.id_car_time','=','data_car_price.id_car_time')
            ->where('id_rentcar','=',$id_rentcar)->get();
    }
    public static function get_price($id_rentcar,$id_cartime){
        return car_price::select('data_car_price.id','price','nm_car_time','data_car_price.id_car_time')
                ->join('ref_car_time','ref_car_time.id_car_time','=','data_car_price.id_car_time')
                ->where('id_rentcar','=',$id_rentcar)
                ->where('data_car_price.id_car_time','=',$id_cartime)
                ->first();
    }
    public static function get_detail($id){
        return car_price::where('id','=',$id)->first();
    }
    //FUNGSI UNTUK MENGINSERT HARGA SEWA MOBIL KE DB
    public static function insert($prices,$unit,$id_rentcar){
        $n=0;
        //HARGA DI LOOPING, BISA LEBIH DARI 1 HARGA SEWA
        foreach($prices as $price){
            if($price>=0){
                $id_car_time=$unit[$n];
                car_price::create(compact('id_rentcar','price','id_car_time'));
            }
            $n++;
        }
    }

}
