<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ref_car_time extends Model {

    protected $table = 'ref_car_time';
    
    //FUNGSI UNTUK MENGHITUNG LAMA SEWA :
    public static function get_rekomtime($date,$date1){
        $dif = $date1->diff($date, true);
        $bulan = $dif->m;
        $hari = $dif->d;
        $jam = $dif->h;
        $menit = $dif->i;
        if($menit>0){$jam++;}

        if($bulan > 0){
            $satuan="m";
            $lamasewa=$bulan." bulan";
        }elseif($hari > 0){
            $satuan="d";
            $lamasewa=$hari." hari";
        }else{
            $satuan="h";
            $lamasewa=$jam." jam";
        }
        $rekom = ref_car_time::where('php_var','=',$satuan)->where('periode','>=',$dif->$satuan)->orderBy('periode')->first();//MENGAMBIL PAKET HARGA TERDEKAT
        if($rekom){$rekomtime=$rekom->nm_car_time;}
        else{
            $rekom = ref_car_time::where('php_var','=',$satuan)->where('periode','<',$dif->$satuan)->orderBy('periode','desc')->first();//MENGAMBIL PAKET HARGA TERDEKAT
            if($rekom){$rekomtime=$rekom->nm_car_time;}
            else{
                $rekomtime="8 jam";
            }
        }
        $data = Array();
        $data[0] = $dif;//PERBEDAAN WAKTU JEMPUT & WAKTU KEMBALI, FORMAT OBJECT
        $data[1] = $rekomtime;//REKOMENDASI PAKET HARGA YANG DIPILIHKAN OLEH SISTEM
        $data[2] = $lamasewa;//LAMA SEWA UNTUK DIPERLIHATKAN KE PENYEWA
        return $data;
    }
    public static function get_detail($id,$date,$date1){
        $data = ref_car_time::where('id_car_time','=',$id)->first();
        $sat = $data->php_var;
        $dif = $date1->diff($date, true);
        if($sat=='h'){
            $lamasewa = ($dif->y*8760)+($dif->m*730)+($dif->d*24)+$dif->h;
        }elseif($sat=='d'){
            $lamasewa = ($dif->y*8760)+($dif->m*730)+$dif->d;
        }elseif($sat=='m'){
            $lamasewa = ($dif->y*8760)+$dif->m;
        }else{
            $lamasewa = $dif->$sat;
        }
        
        if($lamasewa==0){
            $lamasewa = 1;
        }
        $data->lamasewa = $lamasewa;
        return $data;
    }
}
