<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class template_mobil extends Model {

	protected $table = 'template_mobil';
	protected $fillable = [
		'nm_templatemobil',
		'jumlah_kursi'
	];

}
