<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class users_verifikasi extends Model {

	protected $table = 'users_verifikasi';
	protected $fillable = [
		'user_id',
		'kode_verifikasi',
		'status'
	];

}
