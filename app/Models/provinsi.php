<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\kab_kota;
class provinsi extends Model {

    protected $table = 'provinsi';
    protected $fillable = [
            'nm_provinsi'
    ];

    public function kab_kota(){
            return $this->hasOne('App\Models\kab_kota');
    }
    
    public static function getWithKota(){
        $data = provinsi::get();
        $ndata = count($data);
        for($i=0;$i<$ndata;$i++){
            $kota = kab_kota::where('provinsi_id','=',$data[$i]['id'])->where('status','=',1)->get();
            $data[$i]['kota'] = $kota;
        }
        return $data;
    }
}
