<?php namespace App\Providers;

use App\Classes\Travel\TravelClasses;
use Illuminate\Support\ServiceProvider;

class TravelServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind('Travel', function(){
			return new TravelClasses;
		});
	}

}
