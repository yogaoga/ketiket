<?php namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

use App\Events\Otojasa\CreatedTravelEvent;
use App\Handlers\Events\UploadAndNotifikasiWasCreatedTravelEvent;

use App\Events\Agen\SendKodeBookingToEmailEvent;
use App\Handlers\Events\Agen\SendCodeBookingToEmailEventHandler;

class EventServiceProvider extends ServiceProvider {

	/**
	 * The event handler mappings for the application.
	 *
	 * @var array
	 */
	protected $listen = [
		SendKodeBookingToEmailEvent::class => [
			SendCodeBookingToEmailEventHandler::class,
		],
	];

	/**
	 * Register any other events for your application.
	 *
	 * @param  \Illuminate\Contracts\Events\Dispatcher  $events
	 * @return void
	 */
	public function boot(DispatcherContract $events)
	{
		parent::boot($events);

		//
	}

}
