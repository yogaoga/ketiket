<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Classes\Menus\Menu;

class MenuServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind('Menu', function(){
			return new Menu;
		});
	}

}
