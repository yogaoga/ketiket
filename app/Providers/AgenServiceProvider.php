<?php namespace App\Providers;

use App\Classes\Agen\AgenClasses;
use Illuminate\Support\ServiceProvider;

class AgenServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register(){
		
		$this->app->bind('Agen', function(){
			return new AgenClasses;
		});
	}

}
