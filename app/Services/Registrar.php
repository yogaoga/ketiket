<?php namespace App\Services;

use App\User;
use App\Models\users_verifikasi;
use Validator;
use Illuminate\Contracts\Auth\Registrar as RegistrarContract;

class Registrar implements RegistrarContract {

	public $user;

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	public function validator(array $data)
	{
		return Validator::make($data, [
			'name' => 'required|max:255',
			'email' => 'required|email|max:255|unique:users',
			'password' => 'required|confirmed|min:6',
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	public function create(array $data){
		
		$this->user = User::create([
			'name' => $data['name'],
			'email' => $data['email'],
			'password' => bcrypt($data['password']),
		]);

		$kode = md5($this->user->id);

		users_verifikasi::create([
			'user_id' => $this->user->id,
			'kode_verifikasi' => $kode
		]);

		// Mengirim pesan
		\Mail::send('emails.register', ['user' => $this->user], function($msg){
			$msg->to($this->user->email, $this->user->name);
		});

		return $this->user;
	}

}
