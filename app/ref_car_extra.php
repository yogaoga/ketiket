<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ref_car_extra extends Model {
    
    protected $table = 'ref_car_extra';
    
    public static function get_detail($id){
        return ref_car_extra::where('in_car_extra','=',$id)->first();
    }
    public static function get_detail_bynama($nama){
        return ref_car_extra::where('nm_car_extra','=',$nama)->first();
    }
}
