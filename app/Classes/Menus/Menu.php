<?php namespace App\Classes\Menus;
	
	use App;
	use App\Models\ketiket_pages;
	use App\Models\frame_menu_nav;
	use App\Models\frame_relasi_akses_user;

	class Menu {
			
		private $path;
		private $ids;
	    public function __construct(){
	        $this->path = explode('/', \Request::path());
	    }

	    public function HakAkses(){
	        $path = \Request::path();

	        $menu = frame_relasi_akses_user::whereFrame_level_user_id(\Auth::user()->frame_level_user_id)->get();
	        $access = [];
	        foreach ($menu as $val) {
	            $access[] = $val->frame_menu_nav_id;
	        }
	        $slug = frame_menu_nav::whereIn('id', $access)->select('slug')->get();

	        $slugs = [];
	        foreach ($slug as $aces) {
	            $slugs[] = $aces->slug;
	        }
	        
	        if(!empty(\Request::path())):

	            $cek = empty($path) ? 0 : frame_menu_nav::whereSlug($path)->count();
	            if(!empty($path) && !in_array($path, $slugs) && $cek > 0){
	                return false;
	            }
	            $cek = empty($path) ? 0 : frame_menu_nav::whereSlug($path)->count();
	            if(!empty($path) &&  !in_array($path, $slugs) && $cek > 0){
	                return false;
	            }

	        endif;

	        return true;

	    }


	    public function about(){
	    	return ketiket_pages::find(1);
	    }

	    public function titlePage($icon, $title = ''){

	    	return '<div class="page-title-container">
				    <div class="container">
				        <div class="page-title">
				            <h2 class="entry-title"><i class="' . $icon . '"></i> ' . $title . '</h2>
				        </div>
				    </div>
				</div>';
	         
	    }

		public function mainMenu($ids = ''){
        	
        	$this->ids = $ids;
        	
	        $menus = frame_menu_nav::whereStatus(1)->orderby('seri', 'asc')->get();

	        $permission = [];
	        if(\Auth::check()):
		        foreach (frame_relasi_akses_user::whereFrame_level_user_id(\Auth::user()->frame_level_user_id)->get() as $access) {
		            $permission[] = $access->frame_menu_nav_id;
		        }
		    endif;

	        $menu = [];
	        foreach ($menus as $row) {
	            if($row->guest == 1 && \Auth::guest()){
	            	$menu[$row->parent_id][] = $row;
	            }

	            if($row->auth == 1 && \Auth::check()){
		            if(in_array($row->id, $permission)){
		                $menu[$row->parent_id][] = $row;
		            }
	        	}
	        }


	        return $this->get_menu($menu);
	    }

		private function get_menu($data, $parent = 0, $url = null){
	        static $i = 1;
	        $tab = str_repeat(' ', $i);
	        
	        if(isset($data[$parent])){
	            
	            if($parent == 0){
	                $classUl = 'menu'; # <-- Class dari style / template -> tidak memiliki sub menu
	                $ids = $this->ids;
	            }else{
	                $classUl = ''; # <-- Class dari style / template -> pada saat memiliki sub menu
	                $ids ='';
	            }
	            
	            if($url != null && $url != '#'){
	                $link = $url . '/';
	            }else{
	                $link = null;
	            }
	            $activeLi = '';
	            
	            $html = $tab.'<ul class="' . $classUl . '" id="' . $ids .'">';
	            $i++;
	            
	            foreach($data[$parent] as $v){
	                
	                $l = $link == null ? $v->slug : explode('/', ltrim($link, '/'))[0];

	                $child = $this->get_menu($data, $v->id, $l);
	                
	                if($child){
	                    $liClass = ''; # <- menu aktif
	                    $icon = '';
	                    $href = '#';
	                }else{
	                    $liClass = 'menu-item-has-children';
	                    $icon = '';
	                    $href = url() . '/' . $v->slug;
	                }
	                
	                if($this->path != null){
	                    foreach($this->path as $r){
	                        
	                        if($r == $v->slug){
	                            $activeLi = '';
	                        }
	                        
	                    }
	                }
	                $classId = '';
	                if(!empty($v->class_id)){
	                    $classId = 'id="' . $v->class_id . '"';
	                }
	                
	                $html .= $tab.'<li class="' . $activeLi . $liClass . '" ' . $classId . '>';
	                $activeLi = '';
	                $class = $v->class;
	                $html .= '<a href="' . $href . '"><i class="' . $class . '"></i> <span>' . $v->title . '</span> ' . $icon . '</a>';
	                
	                if($child){
	                    $i--;
	                    $html .= $child;
	                    $html .= $tab;
	                }
	                
	                $html .= '</li>';
	            }
	            
	            $html .= $tab.'</ul>';
	            return $html;
	        
	        }else{
	            
	            return false;
	        
	        }
	        
	    }


	    public function menuPosition(){
	        $menus = frame_menu_nav::orderby('seri', 'asc')->get();
	        $menu = [];
	        foreach ($menus as $row) {
	            $menu[$row->parent_id][] = $row;
	        }
	        return $this->formatMenuPosition($menu);
	    }

	    public function formatMenuPosition($data, $parent = 0){
	        static $i = 1;
	        $tab = str_repeat(' ', $i);
	        
	        if(isset($data[$parent])){
	            
	            $html = $tab.'<ol class="dd-list">';
	            $i++;
	            
	            foreach($data[$parent] as $v){
	                
	                $label = $v->status > 0 ? '' : '<span class="label label-danger">Disable</span>';

	                $child = $this->formatMenuPosition($data, $v->id);
	                $html .= $tab.'<li class="dd-item dd3-item" data-id="'.$v->id.'">';
	                $html .= '<div class="dd-handle dd3-handle">
	                            </div>
	                                <div class="dd3-content">
	                                    <i class="' . $v->class . '"></i> 
	                                    <span>'.$v->title.'</span>
	                                    <div class="pull-right">
	                                        ' . $label . '
	                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	                                        <a href="' . url('/menu/edit/' . $v->id ) . '" data-toggle="tooltip" data-placement="left" title="Edit Menu"><i class="fa fa-pencil"></i></a>
	                                    </div>
	                                    
	                                    ';
	                
	                $html .= '</div>';
	                if($child){
	                    $i--;
	                    $html .= $child;
	                    $html .= $tab;
	                }
	                
	                $html .= '</li>';
	            }
	            $html .= $tab.'</ol>';
	            return $html;
	        
	        }else{
	            
	            return false;
	        
	        }
	    }

	    // Kases menu user
	    public function MenuAkses($id = 0){
	         $menus = frame_menu_nav::orderby('seri', 'asc')
	         	->whereAuth(1)
	         	->get();
	        $menu = [];
	        foreach ($menus as $row) {
	            $menu[$row->parent_id][] = $row;
	        }

	        return $this->formatAksesUser($menu, 0, $id);
	    }

	    public function formatAksesUser($data , $parent = 0, $id){
	            
	            if(isset($data[$parent])){
	               
	               $html = '<ul id="tree">';
	              
	                foreach($data[$parent] as $menu){
	                    $child = $this->formatAksesUser($data, $menu['id'], $id);
	                    
	                    $co = frame_relasi_akses_user::whereFrame_level_user_id($id)
	                    		->whereFrame_menu_nav_id($menu['id'])
	                    		->count();
	                    
	                    if($co >= 1){
	                        $checked = 'checked="checked"';
	                    }else{
	                        $checked = '';
	                    }
	                    
	                    $html .= '<li title="'.$menu['ket'].'">';
	                    $html .=    '<label>';
	                    $html .=        '<input type="checkbox" id="update_akses" '.$checked.' name="id_menu[]" value="'.$menu['id'].'" /> ';
	                    $html .=        '<b>'.$menu['title'].'</b> - <small class="text-danger">' . $menu['ket'] . '</small>';
	                    $html .=    '</label>';
	                    if(($child)){
	                        $html .= $child;
	                    }
	                    $html .= '</li>';
	                }
	               
	               $html .= '</ul>';
	               return $html;
	                
	            }else{
	                return false;
	            }
	            
	        }

	}