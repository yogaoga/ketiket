<?php namespace App\Classes\Format;

	class Format{

		////////////////////////////////////// WAKTU /////////////////////////////////////////////////////
		public function indoDate($waktu){
			$bulan = $this->nama_bulan(date('m', strtotime($waktu)));
			$tanggal = date('d', strtotime($waktu)) . ' ' . $bulan . ' ' . date('Y', strtotime($waktu));
			return $tanggal;
		}

		public function time_stamp($ptime){
			$etime = time() - strtotime($ptime);

		    if ($etime < 1)
		    {
		        return 'Baru saja';
		    }
		    $a = array( 12 * 30 * 24 * 60 * 60  =>  'tahun',
		                30 * 24 * 60 * 60       =>  'bulan',
		                24 * 60 * 60            =>  'hari',
		                60 * 60                 =>  'jam',
		                60                      =>  'menit',
		                1                       =>  'detik'
		                );
		    foreach ($a as $secs => $str)
		    {
		        $d = $etime / $secs;
		        if ($d >= 1)
		        {
		            $r = round($d);
		            return $r . ' ' . $str . ($r > 1 ? '' : '') . ' yang lalu';
		        }
		    }
		}

		public function hari($waktu){
			$day = strtotime($waktu);
			return $this->nama_hari(date('N', $day));
		}


		public function nama_hari($no){

			switch($no):
				case 1:
					return 'Senin';
					break;
				case 2:
					return 'Selasa';
					break;
				case 3:
					return 'Rabu';
					break;
				case 4:
					return 'Kamis';
					break;
				case 5:
					return 'Jumat';
					break;
				case 6:
					return 'Sabtu';
					break;
				case 7:
					return 'Minggu';
					break;
			endswitch;
		}


		private function nama_bulan($no_bulan) {
			switch($no_bulan) {
				case 1:
					$nama_bulan = 'Januari';
					break;
				case '01':
					$nama_bulan = 'Januari';
					break;
				case 2:
					$nama_bulan = 'Februari';
					break;
				case '02':
					$nama_bulan = 'Februari';
					break;
				case 3:
					$nama_bulan = 'Maret';
					break;
				case '03':
					$nama_bulan = 'Maret';
					break;
				case 4:
					$nama_bulan = 'April';
					break;
				case '04':
					$nama_bulan = 'April';
					break;
				case 5:
					$nama_bulan = 'Mei';
					break;
				case '05':
					$nama_bulan = 'Mei';
					break;
				case 6:
					$nama_bulan = 'Juni';
					break;
				case '06':
					$nama_bulan = 'Juni';
					break;
				case 7:
					$nama_bulan = 'Juli';
					break;
				case '07':
					$nama_bulan = 'Juli';
					break;
				case 8:
					$nama_bulan = 'Agustus';
					break;
				case '08':
					$nama_bulan = 'Agustus';
					break;
				case 9:
					$nama_bulan = 'September';
					break;
				case '09':
					$nama_bulan = 'September';
					break;
				case 10:
					$nama_bulan = 'Oktober';
					break;
				case 11:
					$nama_bulan = 'Nopember';
					break;
				case 12:
					$nama_bulan = 'Desember';
					break;
			}
			return $nama_bulan;
		}

		////////////////////////////////////// END WAKTU //////////////////////////////////////////////////
		
	}