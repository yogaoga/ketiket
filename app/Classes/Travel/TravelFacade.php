<?php 
	namespace App\Classes\Travel;

	use Illuminate\Support\Facades\Facade;

	class TravelFacade extends Facade {
	    
	    protected static function getFacadeAccessor() { return 'Travel'; }

	}