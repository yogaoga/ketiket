<?php namespace App\Classes\Travel;
	
	use App\Models\relasi_travel;
	use App\Models\travel;

	class TravelClasses{

		/*
		Mengambil data Travel sesuai session
		@access admin/operator Travel
		@param user_id
		@return data Travel berdasarkan session
		@implement Travel::data()
					+- Travel::data()->nama_travel
		*/
		public function data(){
			if($this->check()){
				$relasi = relasi_travel::metravel()->first();
				return travel::me($relasi->travel_id);
			}
			return [];
		}

		/*
		digunakan untuk melakukan pengecekan bahwa users terdaftar sebagai Admin Travel/ atau Operator Travel
		@access Public
		@return true / false
		@implement Travel::check()
		*/
		public function check(){
			$chekc = relasi_travel::metravel()->count();
			return $chekc > 0 ? true : false;
		}

		/*
		digunakan untuk melakukan pengecekan bahwa user tidak terdaftar sebagai Admin Travel/ atau Operator Travel
		@access Public
		@return true / false
		@implement Travel::guest()
		*/
		public function guest(){
			$guest = relasi_travel::metravel()->count();
			return empty($guest) ? true : false;
		}

		/*
		digunakan untuk mengambil data Admin dan operator dari travel berdasarkan session
		@access Admin travel dan operator Travel
		@return users dalam bentuk array
		@implement Travel::users(20) <-- 20 di sini sebagai Limit
		*/
		public function users(){
			return relasi_travel::Userstravel();
		}

	}