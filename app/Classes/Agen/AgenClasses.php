<?php  namespace App\Classes\Agen; 
	
	use App\Models\relasi_travel;
	use App\Models\travel;
	
	class AgenClasses{

		/*
		Mengambil data Agen sesuai session
		@access admin/operator Agen
		@param user_id
		@return data Agen berdasarkan session
		@implement Agen::data()
					+- Agen::data()->nama_travel <-- pembanggilan nama field sesuai dengan tabel travel di database
		*/
		public function data(){
			if($this->check()){
				$relasi = relasi_travel::meagen()->first();
				return travel::me($relasi->travel_id);
			}
			return [];
		}

		/*
		digunakan untuk melakukan pengecekan bahwa user mterdaftar sebagai Admin Agen/ atau Operator Agen
		@access Public
		@return true / false
		@implement Agen::check()
		*/
		public function check(){
			$chekc = relasi_travel::meagen()->count();
			return $chekc > 0 ? true : false;
		}

		/*
		digunakan untuk melakukan pengecekan bahwa user tidak terdaftar sebagai Admin Agen/ atau Operator Agen
		@access Public
		@return true / false
		@implement Agen::guest()
		*/
		public function guest(){
			$guest = relasi_travel::meagen()->count();
			return empty($guest) ? true : false;
		}

		/*
		digunakan untuk mengambil data Admin dan operator dari Agen berdasarkan session
		@access Admin Agen dan operator Agen
		@return users dalam bentuk array
		@implement Agen::users(20) <-- 20 di sini sebagai Limit
		*/
		public function users(){
			return relasi_travel::Usersagen();
		}
	}