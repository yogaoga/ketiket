<?php  namespace App\Classes\Agen;

	use Illuminate\Support\Facades\Facade;

	class AgenFacade extends Facade {
	    
	    protected static function getFacadeAccessor() { return 'Agen'; }

	}