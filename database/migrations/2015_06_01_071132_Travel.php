<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Travel extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('travel', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nama_travel');
			$table->text('information');
			$table->text('hint');
			$table->text('alamat');
			$table->text('no_tlp');
			$table->integer('provinsi_id');
			$table->string('hashtag');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('travel');
	}

}
