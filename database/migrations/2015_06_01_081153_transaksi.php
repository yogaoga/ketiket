<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Transaksi extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transaksi', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('users_id');
			$table->integer('reservasi_id');
			$table->date('tgl_transaksi');
			$table->decimal('nominal',10,2);
			$table->tinyInteger('jenisbayar');
			$table->decimal('penambahan',10,2);
			$table->decimal('total', 10,2);
			$table->integer('status_transaksi');
			$table->text('keterangan');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transaksi');
	}

}
