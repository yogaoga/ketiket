<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Keberangkatan extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('keberangkatan', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('routes_id');
			$table->date('tgl_berangkat');
			$table->tinyInteger('tambahan')->default(0);
			$table->string('status_berangkat')->default(1);
			$table->text('keterangan');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('keberangkatan');
	}

}
