<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReservasiDetail extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reservasi_detail', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('users_id');
			$table->integer('reservasi_id');
			$table->string('nama_penumpang');
			$table->tinyInteger('kursi');
			$table->decimal('harga', 10,2);
			$table->decimal('discount', 10,1);
			$table->decimal('penambahan', 10,2);
			$table->text('keterangan');
			$table->integer('status_bayar');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('reservasi_detail');
	}

}
