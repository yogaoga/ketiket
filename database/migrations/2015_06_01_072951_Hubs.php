<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Hubs extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('hubs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('route_detail_id');
			$table->integer('kota_id');
			$table->text('lokasi');
			$table->string('lang');
			$table->string('lon');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('hubs');
	}

}
