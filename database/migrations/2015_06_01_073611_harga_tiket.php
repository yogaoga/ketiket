<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HargaTiket extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('harga_tiket', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('route_detail_id');
			$table->date('tgl_aktif');
			$table->date('tgl_limit');
			$table->boolean('status_harga');
			$table->decimal('harga',10,2);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('harga_tiket');
	}

}
