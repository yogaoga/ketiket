<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Reservasi extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reservasi', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('users_id');
			$table->date('tgl_reservasi');
			$table->date('tgl_limit');
			$table->date('tgl_bayar');
			$table->text('keterangan');
			$table->string('kode_booking');
			$table->string('no_telp', 20);
			$table->integer('status_reservasi');
			$table->integer('dt_routes');
			$table->integer('keberangkatan_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('reservasi');
	}

}
