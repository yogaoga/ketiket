<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Routes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('routes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('kode_routes',12);
			$table->string('depart',30);
			$table->string('destination',30);
			$table->string('day_route',30);
			$table->integer('status_route');
			$table->date('tgl_aktif');
			$table->integer('travel_id');
			$table->date('tgl_limit');
			$table->string('template_mobile', 3);
			$table->string('kelas', 25);
			$table->text('keterangan');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('routes');
	}

}
