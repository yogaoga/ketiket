<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RouteDetail extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('route_detail', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('routes_id');
			$table->integer('depart');
			$table->integer('destinatin');
			$table->time('waktu');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('route_detail');
	}

}
