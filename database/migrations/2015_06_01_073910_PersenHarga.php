<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PersenHarga extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('persenharga', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('route_detail_id');
			$table->date('tgl_aktif');
			$table->date('tgl_limit');
			$table->decimal('persen',10,1);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('persenharga');
	}

}
