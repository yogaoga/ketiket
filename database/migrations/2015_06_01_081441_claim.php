<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Claim extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('claim', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('users_id');
			$table->integer('reservasi_id');
			$table->date('tgl_claim');
			$table->text('keterangan');
			$table->integer('status_claim');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('claim');
	}

}
