<div class="footer-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <h2>Menu Lainnya</h2>
                <ul class="discover triangle hover row">
                <li ><a href="{{ url('/') }}">HOME</a></li>
                    <li ><a href="{{ url('/tentang-kami') }}">ABOUT ME</a></li>
                    <li ><a href="{{ url('/privacy-policy') }}">PRIVACY POLICE</a></li>
                    <li ><a href="{{ url('/disclaimer') }}">DISCLAIMER</a></li>

                </ul>
            </div>

           
            <div class="col-sm-6">
                <p>{{ Menu::about()->about_front }}</p>
                <br />
                <address class="contact-details">
                    <span class="contact-phone"><i class="soap-icon-phone"></i> {{ Menu::about()->telp_cs }}</span>
                    <br />
                    <a href="mailto:{{ Menu::about()->email_cs }}" class="contact-email">{{ Menu::about()->email_cs }}</a>
                </address>
                <ul class="social-icons clearfix">
                    <li class="twitter"><a title="twitter" target="_blank" href="{{ Menu::about()->twitter }}" data-toggle="tooltip"><i class="soap-icon-twitter"></i></a></li>
                    <li class="googleplus"><a title="googleplus" target="_blank" href="{{ Menu::about()->googleplus }}" data-toggle="tooltip"><i class="soap-icon-googleplus"></i></a></li>
                    <li class="facebook"><a title="facebook" target="_blank" href="{{ Menu::about()->facebook }}" data-toggle="tooltip"><i class="soap-icon-facebook"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>