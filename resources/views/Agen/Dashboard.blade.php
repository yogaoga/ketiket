@extends('Master.template')

@section('content')
	<div class="container">
		<div id="main">
	        <div class="tab-container full-width-style arrow-left dashboard">
	            @include('Agen.Sidebar')
	            <div class="tab-content">
	                <div id="dashboard" class="tab-pane fade in active">
	                    <h1 class="no-margin skin-color">{{ Agen::data()->nama_travel }}</h1>
	                    <p>{{ Agen::data()->hint }}</p>
	                    
	                    @if(Agen::data()->status == 0)
	                    	<br /><br />
	                    	<center><h1>{{ Agen::data()->nama_travel }} Dalam Moderator</h1></center>
	                    @endif

	                </div>
	            </div>
	        </div>
	    </div>
	</div>
@stop