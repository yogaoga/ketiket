<ul class="tabs">
    <li><a href="{{ url('/agen/dashboard') }}"><i class="fa fa-dashboard circle"></i>Dashboard</a></li>

    @if(Agen::data()->status > 0)
    
	    <li><a href="{{ url('/agen/route') }}"><i class="fa fa-wheelchair circle"></i>Pesan Tiket</a></li>
	    @if(Agen::data()->level == 1)
	    <li><a href="{{ url('/agen/users') }}"><i class="fa fa-users circle"></i>Users</a></li>
	    <li><a href="{{ url('/agen/settings') }}"><i class="fa fa-cog circle"></i>Settings</a></li>
	    @endif

    @endif
</ul>