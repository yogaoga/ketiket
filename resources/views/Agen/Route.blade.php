@extends('Master.template')

@section('meta')
	<script type="text/javascript" src="{{ asset('/vendor/jquery-select2/select2.min.js') }}"></script>
	<script type="text/javascript">
		tjq(function(){
			tjq('select.form-select2').select2();

			tjq('[type="number"]').change(function(){
				var val = tjq(this).val();
				if(val < 1){
					tjq(this).val(1);
				}else if(val > 32){
					tjq(this).val(1);
					alert('Penumpang melebihi batas maksimum!');

				}
			});
		});
	</script>
	<link rel="stylesheet" type="text/css" href="{{ asset('/vendor/jquery-select2/select2.min.css') }}">
@stop

@section('content')
	<div class="container">
		<div id="main">
	        <div class="tab-container full-width-style arrow-left dashboard">
	            @include('Agen.Sidebar')
	            <div class="tab-content">
	                <div id="dashboard" class="tab-pane fade in active">
	                  <ul class="wizard">
                         <li class="active">1. Cari Rute</li>
                         <li>2. Daftar Rute</li>
                         <li>3. Pilih Kursi</li>
                         <li>4. Kode Booking</li>
                         <li>5. Terimakasih</li>
                     </ul>
                     <hr />
	                <!-- start content -->
	                <form method="get" action="{{ url('/agen/listroutes') }}">
		                <div class="row">
		                	<div class="col-sm-3">
		                		<label>Depart *</label>
	                            <select name="depart" class="form-control form-select2" required>
	                                <option value="">Pilih Lokasi</option>
	                                @foreach($provinsi as $prov)
	                                    <optgroup label="{{ $prov->nm_provinsi }}">
	                                        @foreach(App\Models\provinsi::find($prov->id)->kab_kota()->get() as $kab)
	                                            <option value="{{ $kab->id }}">{{ $kab->nm_kab_kota }}</option>
	                                        @endforeach
	                                    </optgroup>
	                                @endforeach
	                            </select>
		                	</div>
		                	<div class="col-sm-3">
		                		<label>Destination *</label>
	                            <select name="destination" class="form-control form-select2" required>
	                                <option value="">Pilih Lokasi</option>
	                                @foreach($provinsi as $prov)
	                                    <optgroup label="{{ $prov->nm_provinsi }}">
	                                        @foreach(App\Models\provinsi::find($prov->id)->kab_kota()->get() as $kab)
	                                            <option value="{{ $kab->id }}">{{ $kab->nm_kab_kota }}</option>
	                                        @endforeach
	                                    </optgroup>
	                                @endforeach
	                            </select>
		                	</div>
		                	<div class="col-sm-2">
		                		<label>Tanggal *</label>
		                		<div class="datepicker-wrap">
	                                <input type="text" name="tgl" class="input-text full-width" value="{{ old('tgl') }}" placeholder="mm/dd/yyyy" required readonly="readonly"/>
	                            </div>
		                	</div>
		                	<div class="col-sm-2">
		                		<label>Infant *</label>
	                            <input type="number" name="infant" class="input-text full-width text-right" value="{{ old('infant') ? old('infat') : 1 }}" required/>
		                	</div>
		                	<div class="col-sm-2">
		                		<br />
		                		<button class="full-width">Tampilkan</button>
		                	</div>
		                </div>
		                 <label class="checkbox checkbox-inline">
                            <input type="checkbox" name="diskon" value="1" checked="checked"> Hanya tampilkan Harga Diskon
                        </label>
                        <input type="hidden" name="sort" value="desc">
		                <input type="hidden" name="_t" value="{{ csrf_token() }}">
	                </form>
	                <hr >

	                <p>
	                	<b>Keteangan di di sini</b><br />
	                	Proin eget libero vel nunc cursus mollis ac eget sapien.
	                	Aenean porttitor euismod leo sed ultrices.
	                	Ut porta iaculis lorem ut placerat. Morbi non vulputate turpis. 
	                	Aenean a tempor nulla. Maecenas nec pharetra felis, eget varius orci. 
	                	Mauris vehicula, elit nec tincidunt mattis, nisi velit dignissim libero vel commodo eros lectus vel leo.
	                	Donec condimentum suscipit laoreet. Proin scelerisque mattis tristique.odio vel aliquam volutpat.
	                	in euismod sem lectus. ligula vehicula enenatis semper.
	                </p>
	                <!-- End start content -->

	                </div>
	            </div>
	        </div>
	    </div>
	</div>
@stop