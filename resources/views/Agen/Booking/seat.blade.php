@extends('Master.template')

@section('meta')
	<script type="text/javascript" src="{{ asset('/vendor/jquery-select2/select2.min.js') }}"></script>
	<script type="text/javascript">
		tjq(function(){

			var infant = '{{ $req["infant"] }}';

			tjq('select.form-select2').select2();

			tjq('[type="number"]').change(function(){
				var val = tjq(this).val();
				if(val < 1){
					tjq(this).val(1);
				}else if(val > 32){
					tjq(this).val(1);
					alert('Penumpang melebihi batas maksimum!');

				}
			});

			tjq.getJSON(_base_url + '/agen/kendaraan', {bus : '{{ $route->template_mobile }}'}, function(res){
				var template;
				switch(res.id){
					case '1':
						template = res.bus1;
						break;
					case '2':
						template = res.bus2;
						break;
					case '3':
						template = res.elf14;
						break;
					case '4':
						template = res.elf10;
						break;
				}

				tjq('.seats').html(template);

				//////////////////////////// Event Reservasi //////////////////////////
				tjq('.bus-booking').click(function(){
					tjq('.panel-penumpang').removeClass('hide');
					var codes = [];

					var kode = tjq(this).data('kode');
					var seat = tjq('.penumpang');

					var max = tjq('[data-role="booked"]').length;

					if(max > 0){
						tjq('[data-role="booked"]').each(function(i){
							codes[i] = tjq(this).data('kode');
						});
					}

					var htm  =  '\
						<div class="row kode_' + kode + '" data-role="booked" data-kode="' + kode + '">\
	                		<div class="col-sm-3">\
	                			<strong>Kode</strong>  <input type="text" name="kode[]" class="input-text text-right" value="' + kode + '" required readonly="readonly">\
	                		</div>\
	                		<div class="col-sm-5">\
	                			<input type="text" class="input-text full-width input_' + kode + '" name="nama[]" placeholder="Masukan Nama Penumpang" required>\
	                		</div>\
	                		<div class="col-sm-4">\
	                			<input type="text" class="input-text full-width" name="hp[]" placeholder="Masukan No Handphone" required>\
	                		</div>\
	                	</div>\
					';

					if(max < infant && codes.indexOf(kode) == -1){
						seat.append(htm);
						tjq('.input_' + kode).focus();

						tjq(this).find('img').attr('src', _base_url + '/images/book.png')

					}else{
						if(codes.indexOf(kode) == -1){
							alert('Maaf, Anda memesan kursi hanya untuk ' + infant + ' Orang');
						}else{
							tjq('.kode_' + kode).remove();
							tjq(this).find('img').attr('src', _base_url + '/images/ava.png')
						}
					}

				});
			});
			
			tjq('#form-submit').submit(function(){
				if( tjq(':input[name="kode[]"]').length < 1 ){
					alert('Maaf, Silahkan untuk memesan tempat duduk');
					return false;
				} else if(tjq(':input[name="kode[]"]').length < infant){
					alert('Maaf, Anda memesan ' + infant + ' kursi dan Anda baru memesan ' + tjq(':input[name="kode[]"]').length + ' kursi');
					return false;
				}
			});

		});
	</script>
	<link rel="stylesheet" type="text/css" href="{{ asset('/vendor/jquery-select2/select2.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/seat.css') }}">
@stop

@section('content')
	<div class="container">
		<div id="main">
	        <div class="tab-container full-width-style arrow-left dashboard">
	            @include('Agen.Sidebar')
	            <div class="tab-content">
	                <div id="dashboard" class="tab-pane fade in active">
		                <ul class="wizard">
	                        <li>1. Cari Rute</li>
	                        <li>2. Daftar Rute</li>
	                        <li class="active">3. Pilih Kursi</li>
	                        <li>4. Kode Booking</li>
	                        <li>5. Terimakasih</li>
	                    </ul>
	                    <hr />

	                    <h1 class="no-margin skin-color">{{ $route->nm_depart}} to {{ $route->nm_destination}} </h1>
	                    <span><strong>Boarding Point :</strong> <i class="fa fa-clock-o"></i> {{ date('h:i A', strtotime($pool->waktu)) }} - {{ $pool->lokasi }}</span>
	                    <hr />
		                <!-- start content -->
		                <h4 class="text-muted"><strong>{{ $route->bus }}</strong></h4>
		                <div class="panel panel-default">
		                	<div class="panel-body">
		                		<center class="seats">Memuat...</center>
		                	</div>
		                	<div class="panel-footer">
		                		<center>
		                			<div class="row">
		                				<div class="col-sm-3 col-sm-offset-2">
		                					<img src="{{ asset('/images/book.png') }}"> Booked
		                				</div>
		                				<div class="col-sm-3">
		                					<img src="{{ asset('/images/ava.png') }}"> Available
		                				</div>

		                				<div class="col-sm-3">
		                					<img src="{{ asset('/images/toilet.png') }}"> Toilet
		                				</div>
		                			</div>
		                		</center>
		                	</div>
		                </div>

		                <form method="post" action="{{ url('/agen/seat') }}" id="form-submit">
			                <div class="panel panel-default hide panel-penumpang">
			                	<div class="panel-heading"><strong>Masukan Data Penumpang</strong></div>
				                <div class="panel-body penumpang">
				                	<!-- Daftar penumpang -->
				                </div>
			                </div>
			                <hr />
			                <div class="text-right">
			                	<a href="{{ url('/agen/listroutes?' . $back) }}" class="btn-link pull-left">Kembali</a>
			                	<button type="submit">Lanjut</button>
			                </div>
			                <input type="hidden" name="route_detail_id" value="{{ $req['i'] }}">
			                <input type="hidden" name="hubs_id" value="{{ $req['pool' . $req['i']] }}">
							<input type="hidden" name="tgl_reservasi" value="{{ $req['tgl'] }}">
			                <input type="hidden" name="harga" value="{{ $route->harga }}">
			                <input type="hidden" name="diskon" value="{{ $route->persen }}">
			                <input type="hidden" name="_token" value="{{ csrf_token() }}">
		                </form>

		                <!-- End start content -->

	                </div>
	            </div>
	        </div>
	    </div>
	</div>
@stop