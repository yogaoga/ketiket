@extends('Master.template')

@section('meta')
	<style type="text/css">
		.kode-row{
			display: table;
			width: 100%;
		}
		.kode-col{
			display: table-cell;
		}
		.kode-col-left{
			width: 70%;
			padding: 5px;
		}
		.kode-col-right{
			width: 30%;
			background: #0078c7;
			color: #fff;
			padding: 5px;
		}
		.kode-col-right h1{
			color: #fff;
		}
	</style>
	<script type="text/javascript">
		tjq(function(){
			tjq(':input[name="status_reservasi"]').click(function(){
				var val = tjq(this).val();
				if(val == 1){
					tjq('.keterangan').removeClass('hide');
					tjq('.total').attr('required', 'required');
				}else{
					tjq('.keterangan').addClass('hide');
					tjq('.total').removeAttr('required');
				}
			});
		});
	</script>
@stop

@section('content')
	<div class="container">
		<div id="main">
	        <div class="tab-container full-width-style arrow-left dashboard">
	            @include('Agen.Sidebar')
	            <div class="tab-content">
	                <div id="dashboard" class="tab-pane fade in active">
		                  <ul class="wizard">
	                         <li>1. Cari Rute</li>
	                         <li>2. Daftar Rute</li>
	                         <li>3. Pilih Kursi</li>
	                         <li class="active">4. Kode Booking</li>
	                         <li>5. Kode Booking</li>
	                     </ul>
	                     <hr />
		                <!-- start content -->
		                <div class="panel panel-default">
		                	<div class="kode-row">
		                		<div class="kode-col kode-col-left">
		                			<!-- <h3 style="margin:3px;"><strong>{{ $rute->nm_depart }} to {{ $rute->nm_destination }}</strong></h3>
		                			<span><strong>Boarding Point :</strong> <i class="fa fa-clock-o"></i> {{ date('h:i A', strtotime($pool->waktu)) }} - {{ $pool->lokasi }}</span> -->
		                			<div class="media">
									  	<a class="pull-left" href="#">
									    	<img class="media-object" src="{{ env('IMG') }}/travel/avatar/{{ $rute->logo }}" width="50" height="50">
									  	</a>
									  	<div class="media-body">
									    	<h4 class="media-heading"><strong>{{ $rute->nama_travel }}</strong></h4>
									    	<p>{{ $rute->hint }}</p>
									  	</div>
									</div>
		                		</div>
		                		<div class="kode-col kode-col-right">
		                			<center>
		                				<small>Kode Booking</small>
		                				<h1><strong>{{ $data['reservasi']->kode_booking }}</strong></h1>
		                			</center>
		                		</div>
		                	</div>
		                </div>
		                <div class="panel panel-default">
		                	<div class="panel-body">
		                		<div class="row">
		                			<div class="col-sm-4">
		                				<address>
		                					<strong>Rute</strong>
		                					<p>{{ $rute->nm_depart }} to {{ $rute->nm_destination }}</p>

		                					<strong>Travel</strong>
		                					<p>{{ $rute->nama_travel }}</p>

		                					<strong>Kelas</strong>
		                					<p>{{ $rute->kelas }}</p>

		                					<strong>Jadwal Berangkat</strong>
		                					<p><i class="fa fa-calendar"></i> {{ Format::indoDate($data['reservasi']->tgl_reservasi) }}  <i class="fa fa-clock-o"></i> {{ date('h:i A', strtotime($pool->waktu)) }}, {{ $pool->lokasi }}</p>
		                				</address>
		                			</div>
		                			<div class="col-sm-4">
		                				<address>
		                					<strong>Nomor Kursi</strong>
		                					<p>{{ $rute->kode_routes }} / {{ $other['kursi'] }}</p>

		                					<strong>Jumlah Penumpang</strong>
		                					<p>{{ count($data['detail']) }} orang</p>

		                					<strong>Harga per Tiket</strong>
		                					<p>{{ count($data['detail']) }} orang x IDR {{ number_format($data['detail'][0]->harga,0,',','.') }}</p>

		                					<strong>Harga Total</strong>
		                					<p>IDR {{ number_format($other['total'],0,',','.') }}</p>
		                				</address>
		                			</div>
		                			<div class="col-sm-4">
		                				<h3><strong>Keterangan Diskon</strong></h3>
		                				<address>
		                					<strong>Diskon per Rute {{ $data['detail'][0]->discount }} %</strong>
		                					<div>IDR {{ number_format(($data['detail'][0]->harga * $data['detail'][0]->discount / 100),0,',','.') }}</div>

		                					<strong>Harga Tiket per Diskon</strong>
		                					<div>( IDR {{ number_format($data['detail'][0]->harga,0,',','.') }} - IDR {{ number_format(($data['detail'][0]->harga * $data['detail'][0]->discount / 100),0,',','.') }} ) x  {{ count($data['detail']) }} orang</div>

		                					<strong>Total</strong>
		                					<div>
		                						IDR {{ number_format(
		                							($data['detail'][0]->harga - ($data['detail'][0]->harga * $data['detail'][0]->discount / 100)) * count($data['detail']),
		                							0,',','.'
		                						) }}
		                					</div>

		                					<strong>Jumlah Keuntungan</strong>
		                					<div>
		                						IDR {{ number_format($other['total'],0,',','.') }} - IDR {{ number_format(($data['detail'][0]->harga - ($data['detail'][0]->harga * $data['detail'][0]->discount / 100)) * count($data['detail']),0,',','.') }}
		                					</div>

		                					<strong>Total</strong>
		                					<div>
		                						IDR {{ number_format(
		                							$other['total'] - ($data['detail'][0]->harga - ($data['detail'][0]->harga * $data['detail'][0]->discount / 100)) * count($data['detail']),
		                							0,',','.'
		                						) }}
		                					</div>
		                				</address>
		                			</div>
		                		</div>
		                	</div>
		                </div>

		                <div class="panel panel-default">
		                	<table class="table table-striped">
		                		<tr>
		                			<th class="text-center" width="10%">No Kursi</th>
		                			<th width="50%">Nama Penumpang</th>
		                			<th width="40%">Nomor Telpon</th>
		                		</tr>

		                		@foreach($data['detail'] as $user)
		                			<tr>
		                				<td class="text-center">{{ $user->kursi }}</td>
		                				<td>{{ $user->nama_penumpang }}</td>
		                				<td>{{ $user->tlp }}</td>
		                			</tr>
		                		@endforeach
		                	</table>
		                </div>

		                <form method="post" action="{{ url('/agen/save') }}">
			                <div class="panel panel-default">
			                	<div class="panel-body">
			                		<div class="form-group">
					                	<p>Apakan penumpang sudah melakukan pambayaran Tiket ?</p>
					                	<label class="radio checkbox-inline">
				                            <input type="radio" name="status_reservasi" value="1" required> Sudah
				                        </label>	
				                        <label class="radio checkbox-inline">
				                            <input type="radio" name="status_reservasi" value="0" required> Belum
				                        </label>
			                        </div>

			                        <div class="form-group hide keterangan">
			                        	<label>Jumlah Pembayaran</label>
			                        	<input type="text" placeholder="{{ $other['total'] }}" name="total" class="input-text text-right total">
			                        </div>

			                        <div class="form-group hide keterangan">
				                        <br />
				                        <div class="form-group">
					                        <textarea name="keterangan" class="input-text full-width " placeholder="Tambahkan keterangan di sini..."></textarea>

				                        </div>
			                        </div>
			                	</div>
			                </div>
			                
			                
			                <input type="hidden" value="{{ $data['reservasi']->id }}" name="id">
			                <input type="hidden" value="{{ count($data['detail']) }}" name="penumpang">
			                <input type="hidden" value="{{ csrf_token() }}" name="_token">
			                <hr />
			                <div class="text-right">
		                	<button>Simpan</button>
	                	</form>
	                </div>
		                <!-- End start content -->

	                </div>
	            </div>
	        </div>
	    </div>
	</div>
@stop