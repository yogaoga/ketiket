@extends('Master.template')

@section('content')
	<div class="container">
		<div id="main">
	        <div class="tab-container full-width-style arrow-left dashboard">
	            @include('Agen.Sidebar')
	            <div class="tab-content">
	                <div id="dashboard" class="tab-pane fade in active">
	                  <ul class="wizard">
                         <li>1. Cari Rute</li>
                         <li class="active">2. Daftar Rute</li>
                         <li>3. Pilih Kursi</li>
                         <li>4. Kode Booking</li>
                         <li>5. Terimakasih</li>
                     </ul>
                     <hr />
	                <!-- start content -->
	                <div class="pull-right">
	                	<form method="get" action="">
	                        <select class="full-width" name="sort" onchange="submit();">
	                            <option value="desc" {{ $req['sort'] == 'desc' ? 'selected' : '' }}>Diskon Desc</option>
	                            <option value="asc" {{ $req['sort'] == 'asc' ? 'selected' : '' }}>Diskon Asc</option>
	                        </select>
	                        @foreach($req as $key=>$res)
	                        	@if($key != 'sort')
	                        	<input type="hidden" name="{{ $key }}" value="{{ $res }}">
	                        	@endif
	                        @endforeach
                        </form>
	                </div>
	                <h1 class="no-margin skin-color">{{ $routes->total() }} hasil ditemukan</h1>
	                <br />
	                <div class="panel panel-default">
		                <div class="car-list listing-style3 car">
		                	@forelse($routes as $route)
		                		<form method="get" action="{{ url('/agen/seat') }}">
				                	<article class="box" style="margin:0;padding-bottom:0;">
			                            <div class="details col-xs-12 clearfix">
			                                <div class="col-sm-7">
			                                    <div class="clearfix">
			                                        <h4 class="box-title">{{ $route->nm_depart }} to {{ $route->nm_destination }}<small>{{ $route->nama_travel }} | Kode : {{ $route->kode_routes }}</small></h4>
			                                        <div class="logo">
			                                            <img src="{{ env('IMG') }}/travel/avatar/thumb/{{ $route->logo }}" alt="" />
			                                        </div>
			                                    </div>
			                                    <div class="amenities">
			                                        <ul>
			                                        	@foreach($facilitys[$route->id] as $fac)
			                                        	 <li data-toggle="tooltip" data-placement="bottom" title="{{ $fac->keterangan }}">
			                                        	 	<img width="18" height="18" src="{{ env('IMG') }}/facility/{{ $fac->logo }}"><br />
			                                        	 	{{ $fac->kode }}
			                                        	 </li>
			                                            @endforeach
			                                        </ul>
			                                    </div>
			                                </div>
			                                <div class="col-xs-6 col-sm-2 character">
			                                    <dl class="">
			                                        <dt class="skin-color">Kelas</dt><dd>{{ $route->kelas }}</dd>
			                                        <dt class="skin-color">Harga Diskon</dt><dd>Rp {{ number_format($route->harga - (($route->harga * $route->persen) / 100),0,',','.') }}</dd>
			                                        <dt class="skin-color">Diskon</dt><dd>{{ number_format($route->persen,0,',','.') }}%</dd>
			                                    </dl>
			                                </div>
			                                <div class="action col-xs-6 col-sm-3">
			                                    <span class="price"><small>Harga</small>Rp {{ number_format($route->harga,0,',','.') }}</span>
			                                    <br />
			                                    @if(in_array(date('N', strtotime($req['tgl'])), explode(',', $route->day_route)))
		                                    		<button class="full-width" type="submit">PILIH RUTE</button>
		                                    	@else
		                                    		<div class="well text-danger">
		                                    		Maaf, Rute ini tidak tersedia untuk hari <br >
		                                    		{{ Format::nama_hari(date('N', strtotime($req['tgl']))) }}, {{ Format::indoDate($req['tgl']) }}
		                                    		</div>
		                                    	@endif
		                                    	<input type="hidden" name="rute" value="{{ $route->nm_depart }}-{{ $route->nm_destination }}">
		                                    	<input type="hidden" name="i" value="{{ $route->id }}">
		                                    	<input type="hidden" name="travel" value="{{ $route->nama_travel }}">
		                                    	<input type="hidden" name="infant" value="{{ $req['infant'] }}">
			                                </div>
			                            </div>
			                        </article>

			                        
		                        	<div class="panel-footer">
		                        		<strong>Kendaraan : </strong> <span class="label label-default">{{ $route->bus }}</span> |
		                        		<strong><i class="fa fa-calendar"></i> Hari beroprasi : </strong>
		                        		@foreach(explode(',', $route->day_route) as $day)
		                        			@if(date('N', strtotime($req['tgl'])) == $day)
		                        				<span class="label label-primary">{{ Format::nama_hari($day) }}</span>
		                        			@else
		                        				<span class="label label-default">{{ Format::nama_hari($day) }}</span>
		                        			@endif
		                        		@endforeach
		                        	</div>
			                        <table class="table table-striped" style="border-bottom:solid 1px #ddd;">
			                        	@foreach($pools[$route->id] as $pool)
				                        	<tr>
				                        		<td>
				                        			<label for="f_{{ $pool->id }}" class="radio radio-inline" style="cursor:pointer;">
		                                                <input type="radio" name="pool{{ $route->id }}" value="{{ $pool->id }}" id="f_{{ $pool->id }}" data-id="{{ $pool->id }}"  required/>
		                                            </label>
				                        		</td>
				                        		<td>
				                        			<label for="f_{{ $pool->id }}" style="cursor:pointer;">
				                        				<div>{{ date('h:i A', strtotime($pool->waktu)) }}</div>
				                        			</label>
				                        		</td>
				                        		<td>
				                        			<label for="f_{{ $pool->id }}" style="cursor:pointer;">
				                        				<div>{{ $pool->lokasi }}</div>
				                        			</label>
				                        		</td>
				                        	</tr>
			                        	@endforeach
			                        </table>
			                        <input type="hidden" name="tgl" value="{{ $req['tgl'] }}">
			                        <input type="hidden" name="i" value="{{ $route->id }}">
			                        <input type="hidden" name="_t" value="{{ csrf_token() }}">
		                        </form>
	                        @empty
	                        	<div class="panel-body">
	                        		<div class="well">
			                        	Rute tidak ditemukan !
			                        </div>
	                        	</div>
	                        @endforelse
		                </div>

		                <div class="text-right">
		                	{!! $routes->appends($req)->render() !!}
		                </div>
	                </div>
	                <!-- End start content -->

	                </div>
	            </div>
	        </div>
	    </div>
	</div>
@stop