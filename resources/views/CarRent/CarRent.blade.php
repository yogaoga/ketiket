@extends('Master.template')

@section('header')
	{!! Menu::titlePage('fa fa-car', 'Mobil untuk Disewakan') !!}
@stop

@section('content')
<div class="container">
        <div id="main">
        <div class="tab-container full-width-style arrow-left dashboard">
            @include('Otojasa.Sidebar')
            <div class="tab-content">
                <div id="dashboard" class="tab-pane fade in active">
                    <ul class="nav nav-tabs" id="state-tab">
                        <li class="load active"><a href="/car-rent">Daftar Mobil</a></li>
                        <li class="input"><a href="/input-car-rent/0/0" >Input</a></li>
                        <li class="book"><a href="{{ url('/new-book') }}">Daftar Booking</a></li>
                    </ul>
                    <div class="row">
                        <div class="col-xs-6">
                            <h1 class="skin-color">Daftar Mobil</h1>
                        </div>

<!--                                <div style="text-align: right" class="col-xs-6">
                            Urutkan : 
                            <select class="sort">
                                <option value="harga">Harga</option>
                                <option value="nm_car_mnfcr">Manufaktur</option>
                                <option value="nm_car_merk">Merk</option>
                            </select>
                        </div>-->
                    </div>
                   <div class="data" >

                   </div> 
                    <div class="modal fade" id="Modal" tabindex="-1" role="dialog">
                    
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
tjq(document).ready(function(){
    load_data(1);
});

function load_data(noPage){
    var limit = 5;
    //var search = tjq('input:text#strsearch').val();
   tjq('div.data').load('load-car-rent/'+noPage+'/'+limit);

}
function numberWithCommas(x) {
    x = x.toString();
    var pattern = /(-?\d+)(\d{3})/;
    while (pattern.test(x))
        x = x.replace(pattern, "$1.$2");
    return x;
}
</script>	
@stop
