@extends('Master.template')
@section('meta')
<script type="text/javascript" src="{{ asset('/vendor/jquery-select2/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/create-route.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('/vendor/jquery-select2/select2.min.css') }}">
@stop
@section('header')
	{!! Menu::titlePage('fa fa-car', 'Mobil untuk Disewakan') !!}
@stop

@section('content')
<div class="container">
        <div id="main">
        <div class="tab-container full-width-style arrow-left dashboard">
            @include('Otojasa.Sidebar')
            <div class="tab-content">
                <div id="dashboard" class="tab-pane fade in active">
                    <ul class="nav nav-tabs" id="state-tab">
                        <li class="load"><a href="/car-rent">Daftar Mobil</a></li>
                        <li class="input @if($edit==0) active @endif"><a href="/input-car-rent/0/0">Input</a></li>
			@if($edit==1)
                        <li class="active"><a href="#">Edit</a></li>
			@endif
                        <li class="book"><a href="{{ url('/new-book') }}">Daftar Booking</a></li>
                    </ul>
                       <div class="input">
                       
@if(Auth::user()->status == 0)
        <center>
                <p>
                        Akun anda belum teraktifasi silahkan cek email anda untuk melakukan Aktifasi Akun anda.
                </p>
        </center>
@elseif(empty(Auth::user()->rekening) || empty(Auth::user()->hp) || empty(Auth::user()->alamat) || empty(Auth::user()->cabang) || empty(Auth::user()->bank))
        <center>
                <p>
                        Untuk menyewakan mobil anda diwajibkan untuk melengkapi data diri anda secara lengkap
                        <a href="{{ url('/user/edit') }}" class="btn btn-info btn-xs">Perbaharui Akun</a>
                </p>
        </center>
@else
<h1 class="no-margin skin-color">@if($edit==0) Input @elseif($edit==1) Edit @endif Mobil untuk Disewakan</h1>
<div class="container">
    <div class="row">
        <div class="col-sm-10">
            <div class="travelo-box book-with-us-box">
            
                <form class="form-horizontal" style="padding-top: 10px" role="form" id="form-input" method="post" action="/input-car-rent" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                     <input type="hidden" name="edit" value="{{$edit}}">
                      <input type="hidden" name="id" value="{{$id}}">
                    <div class="form-group">
                        <label for="merk" class="col-sm-2 control-label">Merk Mobil *</label>
                        <div class="col-sm-9">
                            <select name="merk" class="form-control form-select2" required>
                                    <option value="">Pilih Merk</option>
                                    @foreach($armerk as $mnf)
                                    <optgroup label="{{ $mnf->nm_car_mnfcr }}">
                                        @foreach($mnf->merk as $merk)
                                        <option value="{{ $merk->id_car_merk }}" <?php if($edit==1 && $merk->id_car_merk==$odata->id_car_merk){echo "selected='selected'";}?>>{{ $merk->nm_car_merk }}</option>
                                        @endforeach
                                    </optgroup>
                                    @endforeach
                                </select>
                            
                        </div>
                    </div>

                    <div class="form-group">
                    <label for="tipe" class="col-sm-2 control-label">Tipe *</label>
                    <div class="col-sm-9">
                            <select name="tipe" class="form-control form-select2" required>
                                @foreach($artype as $type)
                                <option value="{{ $type->id_car_type }}" <?php if($edit==1 && $type->id_car_type==$odata->id_car_type){echo "selected='selected'";}?>>{{ $type->nm_car_type }}</option>
                                @endforeach
                            </select>
                    </div>
                    </div>

                    <div class="form-group">
                    <label for="trans" class="col-sm-2 control-label">Transmisi *</label>
                    <div class="col-sm-9">
                            <select name="trans" class="form-control form-select2" required>
                                @foreach($artrans as $trans)
                                <option value="{{ $trans->id_car_trnms }}" <?php if($edit==1 && $trans->id_car_trnms==$odata->id_car_tranms){echo "selected='selected'";}?>>{{ $trans->nm_car_trnms }}</option>
                                @endforeach
                            </select>
                    </div>
                    </div>

                    <div class="form-group">
                    <label for="bbm" class="col-sm-2 control-label">Bahan Bakar *</label>
                    <div class="col-sm-9">
                            <select name="bbm" class="form-control form-select2" required>
                                @foreach($arfuel as $fuel)
                                <option value="{{ $fuel->id_car_fuel }}" <?php if($edit==1 && $fuel->id_car_fuel==$odata->id_car_fuel){echo "selected='selected'";}?>>{{ $fuel->nm_car_fuel }}</option>
                                @endforeach
                            </select>
                    </div>
                    </div>

                    <div class="form-group">
                    <label for="plat" class="col-sm-2 control-label">Plat Nomor *</label>
                    <div class="col-sm-9">
                            <input type="text" class="input-text full-width" name="plat" id="plat" value="<?php if($edit==1){echo $odata->plat_nomor;}else{echo old('plat');}?>" required>
                    </div>
                    </div>

                    <div class="form-group">
                    <label for="pintu" class="col-sm-2 control-label">Pintu *</label>
                    <div class="col-sm-9">
                        <input type="number" min='1' max='5' name="pintu" class="input-text full-width" value="<?php if($edit==1){echo $odata->pintu;}else{echo "4";}?>" required>
                      
                    </div>
                    </div>

                    <div class="form-group">
                    <label for="penumpang" class="col-sm-2 control-label">Penumpang *</label>
                    <div class="col-sm-9">
                        <input type="number" min='1' max='15' name="penumpang" value="<?php if($edit==1){echo $odata->penumpang;}else{echo "4";}?>" class="input-text full-width" required>
                      
                    </div>
                    </div>

                    <div class="form-group">
                    <label for="ac" class="col-sm-2 control-label">AC *</label>
                    <div class="col-sm-9">
                                <input type="radio" name="ac" value="1" checked="checked" <?php if($edit==1 && $odata->ac==1){echo 'checked="checked"';}?>>ada
                                &nbsp;
                                <input type="radio" name="ac" value="2" <?php if($edit==1 && $odata->ac==2){echo 'checked="checked"';}?>>tidak ada
                            
                    </div>
                    </div>

                    <div class="form-group">
                    <label for="audio" class="col-sm-2 control-label">Audio *</label>
                    <div class="col-sm-9">
                        <input type="radio" value="1" name="audio" checked="checked" <?php if($edit==1 && $odata->audio==1){echo 'checked="checked"';}?>>ada
                        &nbsp;                                                                
                        <input type="radio" name="audio" value="2" <?php if($edit==1 && $odata->audio==2){echo 'checked="checked"';}?>>tidak ada
                    </div>
                    </div>
                    <div class="form-group">
                    <label for="tahun" class="col-sm-2 control-label">Tahun Model *</label>
                    <div class="col-sm-9">
                        <input type="number" min='1980' max='{{date('Y')}}' value="<?php if($edit==1){echo $odata->thn_model;}else{echo date('Y');}?>" name="tahun" class="input-text full-width" required>
                         
                    </div>
                    </div>

                    <div class="form-group">
                    <label for="kapasitas" class="col-sm-2 control-label">Kapasitas bbm (km/l)*</label>
                    <div class="col-sm-9">
                        <input type="number" min="1" class="input-text full-width" name="kapasitas" id="kapasitas" value="<?php if($edit==1){echo $odata->kapasitas_bbm;}else{echo old('kapasitas');}?>" required>
                    </div>
                    </div>
                    @if($edit==0)
                    <div class="form-group">
                    <label for="harga" class="col-sm-2 control-label">Harga Sewa (Rp.)*</label>

                    <div class="col-sm-4">
                        <input type="number" class="input-text full-width" min="0"  name="harga[]" id="harga" value="{{ old('harga') }}" required>
                    </div>
                    <div class="col-sm-1" style="text-align: right"> satuan :</div>
                    <div class="col-sm-2">
                        <select name="unit[]" class="form-control form-select2" required>
                                @foreach($artime as $time)
                                <option value="{{ $time->id_car_time }}">{{ $time->nm_car_time }}</option>
                                @endforeach
                            </select>
                    </div>
                    <div class="col-sm-1">
                          <button type="button" class="btn btn-xs btn-info addharga" title="tambah foto">Tambah Harga</button>
                      </div>
                    </div>
                    <div class="div-addharga"></div>  
                    @endif
                    <div class="form-group">
                    <label for="driver" class="col-sm-2 control-label">Driver*</label>
                    <div class="col-sm-3">
                        <select name="driver" class="form-control" required>
                            <option value="1" <?php if($edit==1 && $odata->wajibdriver==1){echo 'selected="selected"';}?>>Harus menggunakan supir</option>
                            <option value="2" <?php if($edit==1 && $odata->wajibdriver==2){echo 'selected="selected"';}?>>Tidak harus menggunakan supir</option>
                        </select>
                        
                    </div>
                    <div class="alert-danger col-sm-6 ">
                        <strong>Peringatan :</strong>
                        <br/>
                        Menyewakan Unit Mobil tanpa menyertakan supir sangat berisiko, anda bisa kehilangan mobil dan KETIKET.COM 
                        TIDAK bertanggung jawab atas kejadian ini
                    </div>
                    </div>
                    <div class="form-group">
                    <label for="komisi" class="col-sm-2 control-label">Komisi untuk Agen (%)</label>
                    <div class="col-sm-9">
                        <input type="number" min="0" max="100" class="input-text full-width" name="komisi" id="komisi" value="<?php if($edit==1){echo $odata->komisi;}else{echo old('komisi');}?>">
                    </div>
                    </div>
                    <div class="form-group">
                    <label for="kota" class="col-sm-2 control-label">Kab/Kota *</label>
                    <div class="col-sm-9">
                            <select name="kota" class="form-control form-select2" required>
                                <option value="">Pilih Kota</option>
                                @foreach($arkota as $prov)
                                <optgroup label="{{ $prov->nm_provinsi }}">
                                    @foreach($prov->kota as $kota)
                                    <option value="{{ $kota->id }}" <?php if($edit==1 && $kota->id==$odata->id_kab_kota){echo "selected='selected'";}?>>{{ $kota->nm_kab_kota }}</option>
                                    @endforeach
                                </optgroup>
                                @endforeach
                            </select>
                    </div>
                    </div>

                    <div class="form-group">
                    <label for="lok" class="col-sm-2 control-label">Lokasi *</label>
                    <div class="col-sm-9">
                            <input type="text" class="input-text full-width" name="lok" id="lok" value="<?php if($edit==1){echo $odata->lokasi;}else{echo old('lok');}?>" required>
                    </div>
                    </div>
                    <div class="form-group">
                    <label for="cat" class="col-sm-2 control-label">Catatan *</label>
                    <div class="col-sm-9">
                        <textarea name="cat" id="cat" class="input-text full-width" required=""><?php if($edit==1){echo $odata->catatan;}else{echo old('cat');}?></textarea>

                    </div>
                    </div>
                     @if($edit==0)
                    <div class="form-group">
                      <label for="foto" class="col-sm-2 control-label">Foto Mobil (max 1MB)</label>
                      <div class="col-sm-6 ">
                          <input type='file' name='foto[]' id='foto' accept="image/*">

                      </div>
                      <div class='col-sm-1'><input name='primer' id='primer' type='radio' checked="true" value='0'>Primer</div>
                      <div class="col-sm-1">
                          <button type="button" class="btn btn-xs btn-info add" title="tambah foto">Tambah Foto</button>
                      </div>

                    </div>
                    <div class="div-add"></div>  
                    @endif
                    <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-3">
                            <button class="button btn-large sky-blue1">Simpan</button>
                    </div>
                    </div>


            </form>
            </div>
        </div>
    </div>
</div>
 </div>
      
                   
                    
   
<script type="text/javascript">

tjq('button.addharga').click(function(e){

    var isi = "<div class='form-group'>\n\
                <div class='col-sm-4 col-sm-offset-2'><input type='number' min='0' class='input-text full-width' name='harga[]' id='harga'></div>\n\
                <div class='col-sm-1' style='text-align: right'> satuan :</div>\n\
                <div class='col-sm-2'><select name='unit[]' class='form-control form-select2'>";
                                        @foreach($artime as $time)
        isi +=                              "<option value='{{ $time->id_car_time }}'>{{ $time->nm_car_time }}</option>";
                                        @endforeach
        isi +="                            </select></div>\n\
               </div>";
    tjq('div.div-addharga').append(isi);

});
tjq('button.add').click(function(e){
    var nfoto = countFoto()+1;
    var isi = "<div class='form-group'>\n\
                <div class='col-sm-6 col-sm-offset-2'><input type='file' name='foto[]' id='foto' accept='image/*'></div>\n\
                <div class='col-sm-1'><input name='primer' id='primer' type='radio' value='"+nfoto+"'>Primer</div>\n\
              </div>";
    tjq('div.div-add').append(isi);

});
 
function countFoto(){
    var listfoto=tjq('div.div-add > div.form-group');
    return listfoto.length;
}
</script>
@endif
             </div>
            </div>
        </div>
    </div>
</div>
@stop
