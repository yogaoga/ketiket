
@foreach($arcar as $car)
<div class="thumbnail well">
    <div class="row">
        <div class="col-xs-4">
        <?php 
        $fotos = App\Models\car_img::get($car->id_rentcar);
        $nfotos = count($fotos);
        if($nfotos<2){
        ?>
        
            <img src="{{env('IMG').'/rental/'.$car->foto}}" width="300">
       
        <?php
        }else{?>
        <div id="foto{{$car->id_rentcar}}" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
              @for($i=0;$i<$nfotos;$i++)  
              <li data-target="#foto{{$car->id_rentcar}}" data-slide-to="{{$i}}" <?php if($fotos[$i]['primer']==1)echo 'class="active"';?>></li>
              @endfor
          </ol>
            
          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
            @for($i=0;$i<$nfotos;$i++)   
            <div class="item <?php if($fotos[$i]['primer']==1)echo "active";?>">
                <img src="{{env('IMG').'/rental/'.$fotos[$i]['img']}}" alt="" width="300">
            </div>
            @endfor
          </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#foto{{$car->id_rentcar}}" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#foto{{$car->id_rentcar}}" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <?php
    }?>
           
            <div class="row">
                <div class="col-xs-5"><button type="button" onclick="setting_foto('{{$car->id_rentcar}}')" data-toggle="modal" data-target="#Modal" class="button btn-small dark-blue2 seting-foto"><i class="fa fa-edit"></i> Atur Foto &nbsp;</button></div>
                
                @if($car->wajibdriver==1)
                <div class="col-xs-7 alert-danger" style="text-align:right;"><h5>*disewakan dengan supir</h5></div>
                @endif
               
                
            </div>
            
    </div>
        <div class="col-xs-2">
            <h4>{{$car->nm_car_mnfcr.' '.$car->nm_car_merk}} <small>{{$car->plat_nomor}}</small></h4>
            <div><i class="fa fa-group"></i> {{$car->penumpang.' penumpang'}}</div>
            <div><i class="<?php if($car->id_car_tranms==2){echo "soap-icon-automatic-transmission";}else{echo "soap-icon-user";}?>"></i> {{$car->nm_car_trnms}}</div>
            @if($car->ac==1)
            <div><i class="fa soap-icon-aircon"></i> AC</div>
            @endif
            @if($car->audio==1)
            <div><i class="fa soap-icon-entertainment"></i> Audio</div>
            @endif
            <div><i class="fa soap-icon-fueltank"></i> {{$car->nm_car_fuel.' '.number_format($car->kapasitas_bbm,0,',','.').' km/l'}}</div>
            <div><i class="soap-icon-departure"></i> {{$car->lokasi.' - '.$car->nm_kab_kota}}</div>
            
            <div><button type="button" class="btn btn-mini  btn-info detail" data-det="{{$car}}"><i class="fa fa-plus"> Detail</i></button></div>
            <div class="divdetail{{$car->id_rentcar}}"></div>
        </div>
        <div class="col-xs-3" style="text-align:center;">
            <h3>Tarif <button type="button" onclick="edit_harga(0,0,'{{$car->id_rentcar}}')" class="button btn-xs green"><i class="fa fa-plus"></i></button></h3>
            <div> 
          @foreach($car->price as $price)
          <p>
              <input type="radio"> Rp. {{number_format($price->price,0,',','.').' / '.$price->nm_car_time}}</input> 
              <button type="button" onclick="edit_harga('{{$price->id}}',1,'{{$car->id_rentcar}}')" class="button btn-xs dark-blue2"><i class="fa fa-edit"></i></button>
              <button type="button" onclick="del_harga('{{$price->id}}')" class="button btn-xs red"><i class="fa fa-trash-o"></i></button>
          </p>
          @endforeach
            </div>
            <?php 
            if($car->status==3){
                $status= "Sedang diperbaiki ";
                $cls="yellow";
            }elseif($car->status==2){
                $status= "Reserved";
                $cls="dark-blue1";
            }elseif($car->status==1){
                $status= "Available";
                $cls="green";
            }else{
                $status= "Non Aktif";
                $cls="silver";
            }
            ?>
            <p><button type="button" class="button btn-small {{$cls}}">{{$status}}</button></p>
            
            <p><a href="input-car-rent/1/{{$car->id_rentcar}}" class="button btn-small dark-blue2 edit"><i class="fa fa-edit"></i> Edit</a></p>
        </div>
        <div class="col-xs-3 btn-group">
            <button type="button" data-id="{{$car->id_rentcar}}" data-state="1" class="btn green status">ON</button>
            <button type="button" data-id="{{$car->id_rentcar}}" data-state="0" class="btn silver status">OFF</button>
            <button type="button" data-id="{{$car->id_rentcar}}" data-state="3" class="btn yellow status">SERVIS</button>
        </div>
    </div>
    
</div>
@endforeach

<?php 
$total=$arcar->total();
$limit=$arcar->perPage();
$noPage=$arcar->currentPage();
$jumpage = $arcar->lastPage();
$showPage = "";?>
<div class="row">

    <div class="col-md-3">
        <div class="alert alert-info" id="page-info">Total <strong>{{$arcar->total()}}</strong> mobil</div>
    </div> 
    <div class="col-md-7" style="text-align: right;">
            <div class="pagination" style="margin: 0;">
                <ul class="pagination">
                    
                    @if( $total > $limit)
                        @if($noPage>1)
                            <li><a href="#" data-page='{{$noPage-1}}' id="nav_paging">&laquo;</a></li>
                        @else
                            <li class="disabled"><a href="#">&laquo;</a></li>
                        @endif
                         
                        @for($page = 1; $page <= $jumpage; $page++)
                        
                            @if ((($page >= $noPage - 3) && ($page <= $noPage + 3)) || ($page == 1) || ($page == $jumpage)) 
                                @if (($showPage == 1) && ($page != 2)) 
                                    <li class="disabled"><a href="#" >...</a></li>
                                @endif
                                @if (($showPage != ($jumpage - 1)) && ($page == $jumpage))
                                    <li class="disabled"><a href="#" >...</a></li>
                                @endif
                                @if ($page == $noPage)
                                    <li class="active"><a href="#">{{$page}}</a></li>
                                @else
                                    <li><a href="#" data-page='{{$page}}' id="nav_paging">{{$page}}</a></li>
                                @endif
                                <?php $showPage=$page;  ?>
                            @endif
                        @endfor
                        @if ($noPage < $jumpage)
                            <li><a href="#" data-page='{{$noPage+1}}'  id="nav_paging" >&raquo;</a></li>
                        @else
                            <li class="disabled"><a href="#">&raquo;</a></li>
                        @endif

                    @endif
                </ul>
            </div>
    </div>
</div>
 <script type="text/javascript">   
    tjq(document).ready(function(){
        tjq('li a#nav_paging').click(function(e){
            var np=tjq(this).attr('data-page');
            load_data(np);
            return false;
        });
        
        tjq('button.detail').click(function(e){
            var data=tjq(this).attr('data-det');
             data=tjq.parseJSON(data);
            if(tjq(this).find('i').hasClass('fa-plus')){
                tjq(this).find('i').removeClass('fa-plus').addClass('fa-minus');
                var isi = "" ;
                isi += "<div>"+data['nm_car_type']+"</div>";
                isi += "<div>"+data['pintu']+" pintu</div>";
                isi += "<div>Tahun "+data['thn_model']+"</div>";
                isi += "<div>Komisi agen "+data['komisi']+" %</div>";
                isi += "<div>"+data['catatan']+"</div>";
                tjq('div.divdetail'+data['id_rentcar']).html(isi);
            }else{
                tjq(this).find('i').removeClass('fa-minus').addClass('fa-plus');
                tjq('div.divdetail'+data['id_rentcar']).html('');
            }
            
        });
        
        tjq('button.status').click(function(e){
            var np=tjq(this).attr('data-state');
            var id=tjq(this).attr('data-id');
            tjq.ajax(
                {
                    type:'POST', 
                    url: 'status-car-rent', 
                    data:{
                        state:np,
                        id:id,
                        _token:'<?php echo csrf_token(); ?>'
                    },
                    success: function(response) {
                        load_data({{$noPage}});
                    }
            });
        });
        
        
    });
    
function setting_foto(id){
    tjq('.modal').load('foto-car-rent/'+id,function(){
        tjq('.modal').modal('show');
    }).on('hide.bs.modal',function(e){
        load_data({{$noPage}});
    });
}

function edit_harga(id,edit,idcar){
    tjq('.modal').load('harga-car-rent/'+id+'/'+edit+'/'+idcar,function(){
        tjq('.modal').modal('show');
    }).on('hide.bs.modal',function(e){
        load_data({{$noPage}});
    });
}

function del_harga(id){
    if (confirm('Data ini akan dihapus dan tidak dapat dikembalikan. Apakah anda yakin?')) {
        tjq.ajax(
         {
                 type:'POST', 
                 url: 'delet-harga-car-rent', 
                 data: {
                     _token:'<?php echo csrf_token(); ?>',
                     id:id
                 },
                 success: function(response) {
                     if(response==1){
                         load_data({{$noPage}});
                     }else{
                         alert("Data gagal dihapus!");
                     }
                 }
         });

     }
}
</script>
