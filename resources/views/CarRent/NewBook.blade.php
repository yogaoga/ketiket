@extends('Master.template')
@section('meta')
<script type="text/javascript" src="{{ asset('/js/booked.js') }}"></script>
<style type="text/css">
  #progressbox {
    border: 1px solid #0099CC;
    padding: 1px; 
    position:relative;
    width:400px;
    border-radius: 3px;
    margin: 10px;
    display:none;
    text-align:left;
  }
  #progressbar {
    height:20px;
    border-radius: 3px;
    background-color: #003333;
    width:1%;
  }
  #statustxt {
    top:3px;
    left:50%;
    position:absolute;
    display:inline-block;
    color: #000000;
  }
  .items:hover td .tbl-opsi{
    display: block !important;
  }
</style>

@stop
@section('header')
{!! Menu::titlePage('fa fa-car', 'Mobil untuk Disewakan') !!}
@stop

@section('content')
<div class="container">
  <div id="main">
    <div class="tab-container full-width-style arrow-left dashboard">
      @include('Otojasa.Sidebar')
      <div class="tab-content">
        <div id="dashboard" class="tab-pane fade in active">
          <ul class="nav nav-tabs" id="state-tab">
            <li class="load"><a href="/car-rent">Daftar Mobil</a></li>
            <li class="input"><a href="/input-car-rent/0/0" >Input</a></li>
            <li class="book active"><a href="{{ url('/new-book') }}">Daftar Booking</a></li>
            
          </ul>
        
        
          <h1 class="s-title">Daftar Booking Baru</h1>
          <div class="row">
              <div class="col-xs-5 btn-group">
                <button type="button" data-state="1" class="btn status btn-primary">Baru</button>
                <button type="button" data-state="2" class="btn status">Booked</button>
                <button type="button" data-state="3" class="btn status">Selesai</button>
                <button type="button" data-state="4" class="btn status">Cancel</button>
              </div>
              <div class="col-xs-7 input-group" style="text-align: right;">
                <select name="field" id="field">
                    <option value="all"></option>
                    <option value="no_invoice">no Invoice</option>
                    <option value="nama">nama</option>
                    <option value="plat_nomor">no polisi</option>
                    <option value="nm_car_mnfcr">Manufaktur Mobil</option>
                    <option value="nm_car_merk">Merk Mobil</option>
                    <option value="alamat">alamat</option>
                 </select> 
                <input class="" type="text" id="strsearch" name="strsearch">
                <button type="button" class="btn search"><i class="fa fa-search"></i> Search</button>
                <button type="button" class="btn sky-blue1 refres"><i class="fa fa-refresh"></i> Refresh</button>
              </div>
          </div>
       
          <div class="divdata thumbnail well">
              <table class="table table-condensed data" width="100%">
                <thead style="background-color: grey; color: white">
                  <tr>
                    <th>No</th>
                    <th>No Invoice</th>
                    <th>Nama Penyewa</th>
                    <th>Unit Sewa</th>
                    <th>No Polisi</th>
                    <th>Jemput (Waktu) - Kembali(Waktu)</th>
                    <th>Alamat</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
              <div class="hidden" id="page-no-info">1</div>
              <div class="row">
             
                <div class="col-md-3">
                   <div class="alert alert-info" id="page-info"></div>
                </div> 
                <div class="col-md-9" style="text-align: right;">
                        <div class="pagination" style="margin: 0;"><ul class="pagination"></ul></div>
                </div>
            </div>
          </div>
          </div>
            
        </div>
      </div>
    </div>
    <div class="modal fade" id="Modal" tabindex="-1" role="dialog"></div>
    


  </div>
<script type="text/javascript">
tjq(document).ready(function(){
    load_data();
    tjq('button.status').click(function(){
       tjq('button.status').removeClass('btn-primary');
       tjq(this).addClass('btn-primary');
       load_data();
    });
    tjq('button.search').click(function(){
       load_data();
    });
    tjq('button.refres').click(function(){
       tjq('input#strsearch').val('');
       tjq('select#field').val('');
       load_data();  
    });
});
function load_data(){
    var status =tjq('button.status.btn-primary').attr('data-state');
    var noPage=tjq('div#page-no-info').html();
    var search = tjq('input:text#strsearch').val();
    var fieldSearch = tjq('select#field').val();
    if(!noPage)noPage=1;
    
    tjq('table.data > tbody').html('');
    var data=tjq.ajax({
        url:'load-car-book/'+noPage+'/'+status+'/'+fieldSearch+'/'+search,
        global:false,
        async:false,
        type:'GET',
        complete:function(rdata)
        {
            return rdata;
        }
    }).responseText;
   
    var data=tjq.parseJSON(data);
    var jdata=data['data'];
    var tr='',i,tgl,tgl1,disp;
    var no=(noPage-1)*data['per_page'];
    var jmldata = jdata.length;
    for(i=0;i<jmldata;i++)
    {  
        no++;
        tgl = jdata[i]['pickup_datetime'].split(' ');
        tgl1 = jdata[i]['return_datetime'].split(' ');
        tr +='<tr data-id="'+jdata[i]['id']+'">';
            tr +='<td style="text-align:center;">'+no+'</td>';
            tr +='<td>'+jdata[i]['no_invoice']+'</td>';
            tr +='<td>'+jdata[i]['nama']+'<br><small>'+jdata[i]['contact']+'</small></td>';
            tr +='<td>'+jdata[i]['nm_car_mnfcr']+' '+jdata[i]['nm_car_merk']+'</td>';
            tr +='<td>'+jdata[i]['plat_nomor']+'</td>';
            tr +='<td>'+myDate(tgl[0])+' - '+myDate(tgl1[0])+'<br><small>'+tgl[1]+' - '+tgl1[1]+'</small></td>';
            tr +='<td>'+jdata[i]['alamat']+'</td>';
            tr +='<td>';
            if(status==1 || status==2){//STATUS BARU ATAU BOOK
                
                if(status==2){
                    disp = 'Edit';
                    tr +='<button type="button" class="button btn-mini dark-blue1" data-inv="'+jdata[i]['no_invoice']+'" data-id="'+jdata[i]['id_car_booking']+'" data-state="3">Selesai</button>';
                }else{
                    disp = 'Book';
                }
                tr +='<button type="button" class="button btn-mini" data-inv="'+jdata[i]['no_invoice']+'" data-id="0" data-state="2">'+disp+'</button>';
                tr +='<button type="button" class="button btn-mini red" data-inv="'+jdata[i]['no_invoice']+'" data-id="'+jdata[i]['id_car_booking']+'" data-state="4">Cancel</button>';
            }
            tr +='<a href="/print-car-book/'+jdata[i]['no_invoice']+'" target="_blank" class="button btn-mini light-purple">Invoice</a>';
          
            tr +='</td>';
        tr +='</tr>';
    }
   
    var jumpage=data['last_page'];
    var nav="";
    var noPage=data['current_page'];
    if(data['total'] > data['per_page'] )
    {
        if(noPage>1)
        {
            nav += "<li><a href=\"#\" data-page='"+(parseFloat(noPage)-1)+"' id=\"nav_paging\">&laquo;</a></li>";
        }else
        {
            nav += '<li class="disabled"><a href="#">&laquo;</a></li>';
        }
        var showPage;   
        for(var page = 1; page <= jumpage; page++)
        {
             if (((page >= noPage - 3) && (page <= noPage + 3)) || (page == 1) || (page == jumpage)) 
              {

                  if ((showPage == 1) && (page != 2)) 
                      {
                            nav += '<li class="disabled"><a href="#" >...</a></li>';
                      }
                  if ((showPage != (jumpage - 1)) && (page == jumpage))
                      {
                           nav += '<li class="disabled"><a href="#" >...</a></li>';
                      } 
                  if (page == noPage)
                      {
                          nav += '<li class="active"><a href="#">'+page+'</a></li>';
                      }else
                          {
         nav += "<li><a href=\"#\" data-page='"+page+"' id=\"nav_paging\">"+page+"</a></li>";
                          }
                      showPage=page;  
              }
        }
        if (noPage < jumpage)
        {
            nav += "<li><a href=\"#\" data-page='"+(parseFloat(noPage)+1)+"'  id=\"nav_paging\" >&raquo;</a></li>";
        }else
        {
             nav += ' <li class="disabled"><a href="#">&raquo;</a></li>';
        }

        tjq('div.pagination > ul').html(nav).find('li a#nav_paging').click(function(e){
            var np=tjq(this).attr('data-page');
            tjq('div#page-no-info').html(np);
            load_data();
            return false;
        });
    }else
    {
        tjq('div.pagination > ul').html('');
    }
    var info;
    info=jmldata+' dari '+ data['total'];
    tjq('div#page-info').html(info);
    tjq('div#page-no-info').html(noPage);
    tjq('table.data > tbody').html(tr).find('tr >td > button').click(function(e){
        var state = tjq(this).attr('data-state');
        var id = tjq(this).attr('data-id');
        var noinv = tjq(this).attr('data-inv');
        if(state=='inv'){
            book(id,state);
        }else{
            book(id,noinv,state);
        }
    });
}
function myDate(date){
    var arrdate1 = date.split('-');
    var namabulan = ("NULL Januari Februari Maret April Mei Juni Juli Agustus September Oktober Nopember Desember"); 
    namabulan = namabulan.split(" "); 
    return arrdate1[2]+'/'+namabulan[arrdate1[1]]+'/'+arrdate1[0];
}
function book(id,inv,status){
    
    tjq('.modal').load('view_invoice/'+id+'/'+inv+'/'+status,function(){
        tjq('.modal').modal('show');
    });
    
}
</script>
  @stop
