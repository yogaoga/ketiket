<div class="modal-dialog" style="width: 800px" role="document">
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="exampleModalLabel">
            @if($status==2)
                Booking Transaksi 
            @elseif($status==3)
                Menyelesaikan 
            @else
                Cancel Transaksi 
            @endif
              Invoice #{{ $data[0]->no_invoice }}
          </h4>
        </div>
        <div class="modal-body">
            @include('client.car_rent_invoice')
            <form id="form-input" onsubmit="return submitForm();">
             <input type="hidden" name="status" id="status" value="{{$status}}">
             <input type="hidden" name="noinvoice" value="{{ $data[0]->no_invoice }}">
             <input type="hidden" name="id" value="{{ $data[0]->id_car_booking}}">
             <input type="hidden" name="_token" value="{{ csrf_token() }}">
             @if($status==4)
                <input type="hidden" name="rpcancel" id="rpcancel"/>
                <input type="hidden" name="ototal" value="{{$data[0]['total_price']}}"/>
                <input type="hidden" name="ograndtotal" value="{{$data[0]['grand_total']}}"/>
            @endif
             <table class="table">
                 @if($status==2)
               <tr align="right">
                 <td width="75%">Diskon (%)</td>
                 <td>
                 <input type="number" max="100" name="diskon" onchange="hitung()" id="txtdiskon" class="form-control" placeholder="Diskon" value="{{ $data->adjust->diskon }}">
                 </td>
               </tr>
               <tr align="right">
                 <td width="75%">Rp. Diskon</td>
                 <td>
                     <input type="text" id="rpdiskon" readonly="readonly">
                 </td>
               </tr>
               @endif
                @if($status!=3)
               <tr align="right">
                 <td width="75%">@if($status==2) Adjusment @elseif($status==4) Cancel Fee (%) @endif (Rp.)</td>
                 <td>
                     <input type="number" name="adj" id="txtadj" onchange="hitung()" id="adj" class="form-control" required @if($status==2 && $data->adjust->adjustment>0) value="{{ $data->adjust->adjustment }}" @else value="0" @endif/>
                 </td>
               </tr>
               @endif
                
               @if($status!=3)
               <tr align="right">
                 <td width="75%">@if($status==2) Jumlah yang harus dibayar @elseif($status==4) Rp. Cancel Fee @endif</td>
                 <td>
                     <input type="text" readonly="readonly" value="" id="total"/>
                     
                 </td>
               </tr>
               @endif
               <tr align="right">
                 <td width="75%"> </td>
                 <td>
                   <button type="submit" class="btn btn-primary"> Simpan</button>
                   <button type="button" class="dark-blue1" data-dismiss="modal" aria-label="Close">Close</button>
                 </td>
               </tr>
               
             </table>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    function numberWithCommas(x) {
    x = x.toString();
    var pattern = /(-?\d+)(\d{3})/;
    while (pattern.test(x))
        x = x.replace(pattern, "$1.$2");
    return x;
}  
    function hitung(){
        var status = tjq('input:hidden#status').val();
        var adj = parseFloat(tjq('#txtadj').val());
        var total,diskon,rpdiskon,cancel;
        if(status==2){//MENGHITUNG UNTUK STATUS BOOK
            total = '<?php echo $data[0]->grand_total?>';
            cancel = '<?php echo $data->cancelfee?>';
            total = parseFloat(total);
            diskon = parseFloat(tjq('#txtdiskon').val());
            if(diskon){
                rpdiskon = diskon/100*total;
                total = total-rpdiskon;
                 tjq('input:text#rpdiskon').val(numberWithCommas(rpdiskon));
            }
            if(adj)total = total+adj;
            total+=parseFloat(cancel);
        }else if(status==4){//MENGHITUNG UNTUK STATUS CANCEL
            total = '<?php echo $data[0]->total_price?>';
            total = parseFloat(total);
            if(adj){
                rpdiskon = adj/100*total;
                total = rpdiskon;
            }else{total=0;}
            tjq('input:hidden#rpcancel').val(total);
        }
        tjq('input:text#total').val(numberWithCommas(total));
    }
    
    function submitForm() {//MENYIMPAN STATUS BOOKING
        var msg;
        var status = '<?php echo $status?>';
        if(status==2){
            msg="MELANJUTKAN";
        }else if(status==3){
            msg = "MENYELESAIKAN";
        }else{
            msg = "MEMBATALKAN";
        }
        if (confirm('Apakah anda yakin akan '+msg+' transaksi ini?')){
            tjq.ajax(
            {
                    type:'POST', 
                    url:'saveBooking', 
                    data:tjq('#form-input').serialize(),
                    success: function(response) {
                        if(response==1){
                            tjq('.modal').modal('hide');
                            load_data();
                        }else{
                            alert('Error!');
                        }
                    }
            });
        }
        

        return false;
    }
</script>