<div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Pengaturan Foto</h4>
      </div>
        <form id="form-input" enctype="multipart/form-data" class="form-horizontal" onsubmit="return submitForm();">
            <input type='hidden' name='idrent' value='{{$idrent}}'>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="modal-body">
                <div class="div-add"></div>  
                <div>
                    <button type="button" class="button btn-small green add" title="tambah foto"><i class="fa fa-plus"></i> Tambah Foto</button>
                </div>

                
                @foreach($data as $fotos)   
                <div class='row'>
                    <div class="col-xs-6">
                        <img src="{{env('IMG').'/rental/'.$fotos['img']}}" alt="" width="260">
                    </div>
                    <div class="col-xs-6 foto{{$fotos['id_car_img']}}">
                        <div class="div-edit"></div>
                        <button type="button" class="button btn-small dark-blue2" onclick="edit_foto('{{$fotos['id_car_img']}}','{{$fotos['img']}}')"><i class="fa fa-edit"></i> Ganti</button>
                        <button type="button" class="button btn-small red" onclick="hapus('{{$fotos['id_car_img']}}')"><i class="fa fa-trash-o"></i> Hapus</button>
                        @if($fotos['primer']==0)
                        <button type="button" class="button btn-small dark-blue1" onclick="primer('{{$fotos['id_car_img']}}')"><i class="fa fa-edit"></i> Set as Primer</button>
                        @endif
                        
                    </div>
                </div>

                @endforeach



            </div>

            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary save">Save</button>
            </div>
       </form>
    </div>
  </div>
<script type="text/javascript">
tjq('button.add').click(function(e){
    var isi = "<div>\n\
                <input type='file' name='foto[]' accept='image/*'>\n\
              </div>";
    tjq('div.div-add').append(isi);

});

function edit_foto(id,nama){
    var isi = "<div>\n\
               <input type='file' name='fotoedit[]' id='foto' accept='image/*'>\n\
                <input type='hidden' name='idfoto[]' value='"+id+"'>\n\
                <input type='hidden' name='namafoto[]' value='"+nama+"'>\n\
            </div>";
    tjq('div.foto'+id+' > div.div-edit').html(isi);

}

function primer(id){
    tjq.ajax(
        {
            type:'POST', 
            url: 'primer-foto-car-rent', 
            data:{
                _token:'<?php echo csrf_token(); ?>',
                id:id,
                idrent:'<?php echo $idrent?>'
            }, 
            success: function(response) {
                if(response==1)
                    tjq('.modal').modal('hide');
                else
                    alert('Error!');
            }
    });

}

function hapus(id){
    tjq.ajax(
        {
            type:'POST', 
            url: 'delet-foto-car-rent', 
            data:{
                _token:'<?php echo csrf_token(); ?>',
                id:id
            }, 
            success: function(response) {
                if(response==1)
                    tjq('.modal').modal('hide');
                else
                    alert('Error!');
            }
    });

}

function submitForm() {
  
    var fd = new FormData(document.getElementById("form-input"));
    tjq.ajax(
        {
            type:'POST', 
            url: 'foto-car-rent', 
            data:fd, 
            processData: false,
            contentType: false,
            success: function(response) {
                if(response==1)
                    tjq('.modal').modal('hide');
                else
                    alert('Error!');
            }
    });
    
    return false;
}
</script>