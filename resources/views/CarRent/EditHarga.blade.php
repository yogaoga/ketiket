<div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Tarif Sewa</h4>
      </div>
        <form id="form-input" class="form-horizontal" onsubmit="return submitForm();">
            <input type='hidden' name='id' value='{{$id}}'>
            <input type='hidden' name='edit' value='{{$edit}}'>
            <input type='hidden' name='idcar' value='{{$idcar}}'>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="modal-body">
                
                <div class="form-group">
                    <label for="harga" class="col-sm-1 control-label">Rp.</label>

                    <div class="col-sm-4">
                        <input type="number" class="input-text full-width" min="0"  name="harga" id="harga" value="<?php if($edit==1)echo $data->price?>" required>
                    </div>
                    <div class="col-sm-2" style="text-align: right"> satuan :</div>
                    <div class="col-sm-5">
                        <select name="unit" class="form-control" required>
                                @foreach($artime as $time)
                                <option value="{{ $time->id_car_time }}" <?php if($edit==1 && $time->id_car_time==$data->id_car_time){echo "selected='selected'";}?>>{{ $time->nm_car_time }}</option>
                                @endforeach
                            </select>
                    </div>
                </div>



            </div>

            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary save">Save</button>
            </div>
       </form>
    </div>
  </div>
<script type="text/javascript">
function submitForm() {
    tjq.ajax(
        {
            type:'POST', 
            url:'harga-car-rent', 
            data:tjq('#form-input').serialize(),
            success: function(response) {
                if(response==1)
                    tjq('.modal').modal('hide');
                else
                    alert('Error!');
            }
    });
    
    return false;
}
</script>