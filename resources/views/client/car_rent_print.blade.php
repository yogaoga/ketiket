<!DOCTYPE html>
<!--[if IE 8]>          <html class="ie ie8"> <![endif]-->
<!--[if IE 9]>          <html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->  <html> <!--<![endif]-->
<head>
    <!-- Page Title -->
    <title>{{ !empty($title) ? $title : 'Ketiket Travel' }}</title>
    
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="keywords" content="{{ !empty($keywords) ? $keywords : 'Ketiket, Jasa Tiketing' }}" />
    <meta name="description" content="{{ !empty($description) ? $description : 'Ketiket.com merupakan situs yang memiliki akses dengan 876 operator travel yang ada di indonesia.' }}">
    <meta name="author" content="{{ !empty($author) ? $author : 'Ketiket' }}">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Theme Styles -->
    <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/font-awesome.min.css') }}">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
   
</head>
<body>
     <div style="padding-left: 30px;padding-top: 10px;width: 950px; font-size: 11pt ;font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;">
         
         <div class="row" style="border-bottom: #000000; border-bottom-style: solid;">
            <div class="col-xs-3" style="padding-top: 5px;">
                <img alt="Ketiket" src="{{ asset('/images/logo.png') }}">
            </div>
            <div class="col-xs-4">
                Intiland Tower 3rd Floor
                Jl. Panglima Sudirman 101-103
                Surabaya 60271
            </div>
            <div class="col-xs-4">
                Telp (031) 600 38 328
                Fax (031) 600 38 329
                Ketiket.com
            </div>
        </div>
         <h4 class="s-title">Invoice <strong>#{{$data[0]->no_invoice}}</strong></h4>
        @include('client.car_rent_invoice')
        <table width="100%" class="table table-condensed no-border">
        <tr class="active">
            <td colspan="2">
                Catatan :
                <br />
                1. Pembayaran sewa dilakukan 5 jam sebelum waktu penjemputan.
                <br />
                2. Pembatalan setelah pembayaran akan dikenakan potongan 10% dari transaksi yang sudah dibayarkan. 
                <br />
                3. Bila pembatalan tidak dilakukan setelah waktu penjemputan dilakukan, maka akan dianggap hangus.
            </td>
        </tr>
        </table>
        <br />
        <div class="col-xs-offset-9" style="text-align: center">
             Malang, {{date('d F Y')}}
             <br />
             <br />
             <br />
             <br />
            
             Customer Service Ketiket.com
         </div>
    </div>
    
</body>
</html>




