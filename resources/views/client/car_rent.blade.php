@extends('Master.template')
@section('meta')
<script type="text/javascript" src="{{ asset('/vendor/jquery-select2/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/create-route.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('/vendor/jquery-select2/select2.min.css') }}">
@stop
@section('header')
<div class="page-title-container">
	<div class="container">
		<div class="page-title pull-left">
			<h2 class="entry-title">Booked</h2>
		</div>
		<ul class="breadcrumbs pull-right">
			<li><a href="#">HOME</a></li>
			<li><a href="#">Pages</a></li>
			<li class="active">booked</li>
		</ul>
	</div>
</div>
@stop
@section('content')
<div class="container">
    <div id="main">
        <div class="row">
            <div id="main" class="col-sm-8 col-md-9">
                <div class="toggle-container filters-container">
                    <div class="panel style1 arrow-right">
                        <h4 class="panel-title">
                                <a data-toggle="collapse" href="#price-filter" class="collapsed">Ubah Pencarian</a>
                        </h4>
                        <div id="price-filter" class="panel-collapse collapse">
                            <div class="panel-content">
                                <form action="{{ url('/car-book') }}" method="get">
                                    <input type="hidden" name="_t" value="{{ csrf_token() }}">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>Lokasi</label>
                                            <select name="depart" class="form-control form-select2" required>
                                                <option value="">Lokasi</option>
                                                @foreach($arkota as $prov)
                                                <optgroup label="{{ $prov->nm_provinsi }}">
                                                    @foreach($prov->kota as $kota)
                                                    <option value="{{ $kota->id }}" <?php if($kota->id==$_GET['depart']){echo "selected='selected'";}?>>{{ $kota->nm_kab_kota }}</option>
                                                    @endforeach
                                                </optgroup>
                                                @endforeach
                                            </select>
                                            <input type="checkbox" onclick="difreturn()" name="return" <?php if(isset($_GET['return'])){echo "checked='checked'";}?> value="1">Kembali di lokasi yang berbeda
                                         <div class="<?php if(!isset($_GET['return'])){echo "hidden";}?> divreturn">
                                            <label>Lokasi Kembali</label>
                                            <select name="destination" class="form-control form-select2">
                                                <option value="">Pilih Kota</option>
                                                @foreach($arkota as $prov)
                                                <optgroup label="{{ $prov->nm_provinsi }}">
                                                    @foreach($prov->kota as $kota)
                                                    <option value="{{ $kota->id }}" <?php if($kota->id==$_GET['destination']){echo "selected='selected'";}?>>{{ $kota->nm_kab_kota }}</option>
                                                    @endforeach
                                                </optgroup>
                                                @endforeach
                                            </select>
                                            </div> 
                                             
                                        </div>
                                        <div class="col-md-4">
                                            <label>Tanggal Penjemputan</label>
                                            <div class="btn-group">
                                                <div class="col-sm-6" style="padding-left: 0px;padding-right: 0px; ">
                                                    <div class="datepicker-wrap">
                                                        <input type="text" required name="tgl" class="input-text full-width" value="{{ $_GET['tgl'] }}" placeholder="mm/dd/yy" />
                                                    </div> 
                                                </div>
                                                <div class="col-sm-3" style="padding-left: 0px;padding-right: 0px;">
                                                     <input type="number" name="jam" min="0" max="24" class="input-text full-width text-right" value="{{ $_GET['jam'] }}" required/>                                
                                                </div>
                                                <div class="col-sm-3" style="padding-left: 0px; padding-right: 0px;">
                                                    <input type="number" name="menit" min="0" max="60" class="input-text full-width text-right" value="{{ $_GET['menit'] }}" required/>
                                                </div>
                                            </div>
                                            
                                            <label>Car Type</label>
                                            <select name="cartype" class="form-control form-select2">
                                                <option value="">Pilih Tipe Mobil</option>
                                                @foreach($cartype as $car)
                                                    <option value="{{ $car->id_car_type }}" <?php if($car->id_car_type==$_GET['cartype']){echo "selected='selected'";}?>>{{ $car->nm_car_type }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Tanggal Kembali</label>
                                            <div class="btn-group">
                                                <div class="col-sm-6" style="padding-left: 0px;padding-right: 0px; ">
                                                    <div class="datepicker-wrap">
                                                        <input type="text" required name="tgl1" class="input-text full-width" value="{{ $_GET['tgl1'] }}" placeholder="mm/dd/yy" />
                                                    </div> 
                                                </div>
                                                <div class="col-sm-3" style="padding-left: 0px;padding-right: 0px;">
                                                     <input type="number" name="jam1"  min="0" max="24" class="input-text full-width text-right" value="{{ $_GET['jam1']}}" required/>                                
                                                </div>
                                                <div class="col-sm-3" style="padding-left: 0px; padding-right: 0px;">
                                                    <input type="number" name="menit1" min="0" max="60" class="input-text full-width text-right" value="{{ $_GET['menit1']}}" required/>
                                                </div>
                                            </div>
                                            <br><br>
                                             <button type="submit" class="full-width icon-check animated" data-animation-type="bounce" data-animation-duration="1">SEARCH</button>
                                        </div>
                                        
                                    </div>
                                    <input type="hidden" name="sort" value="desc">
                                    <input type="hidden" name="_t" value="{{ csrf_token() }}">
                                </form>
                                <div class="clearer"></div>
                            </div><!-- end content -->
                        </div>
                    </div>
                </div>
                <div class="booking-information travelo-box">
                        <div class="tab-content">
                                <div id="dashboard" class="tab-pane fade in active">
                                        <ul class="wizard">
                                                 <li>1. Cari Mobil</li>
                                                <li class="active">2. Daftar Mobil</li>
                                                <li>3. Fasilitas</li>
                                                <li>4. Biodata</li>
                                                <li>5. Selesai</li>
                                        </ul>
                                        <hr />
                                        <div class="contentdata">
                                        <?php 
                                        //HITUNG TANGGAL
                                        $ardate = explode("/", $_GET['tgl']);
                                        $date=$ardate[2]."-".$ardate[0]."-".$ardate[1].' '.$_GET['jam'].':'.$_GET['menit'].':00';
                                        $date = new DateTime($date);
                                        
                                        $ardate = explode("/", $_GET['tgl1']);
                                        $date1=$ardate[2]."-".$ardate[0]."-".$ardate[1].' '.$_GET['jam1'].':'.$_GET['menit1'].':00';
                                        $date1 = new DateTime($date1);
                                        $rekomtime = App\Models\ref_car_time::get_rekomtime($date,$date1);
                                        echo '<h2>Anda akan menyewa mobil selama '.$rekomtime[2].'!<br>Kami merekomendasikan anda untuk memilih paket harga per <strong>'.$rekomtime[1].'</strong></h2>';
                                        
                                        ?>
                                        <h1 class="no-margin skin-color">hasil ditemukan ({{count($arcar)}} dari {{ $arcar->total() }}) </h1>
                                        <br />
                                        <form action="{{ url('/extra-car-book') }}" method="get">
                                            <input type="hidden" name="depart" value="{{$_GET['depart']}}">
                                            <input type="hidden" name="destination" value="{{$_GET['destination']}}">
                                            <input type="hidden" name="tgl" value="{{$_GET['tgl']}}">
                                            <input type="hidden" name="tgl1" value="{{$_GET['tgl1']}}">
                                            <input type="hidden" name="jam" value="{{$_GET['jam']}}">
                                            <input type="hidden" name="jam1" value="{{$_GET['jam1']}}">
                                            <input type="hidden" name="menit" value="{{$_GET['menit']}}">
                                            <input type="hidden" name="menit1" value="{{$_GET['menit1']}}">
                                            <input type="hidden" name="return" value="<?php if(isset($_GET['return'])){echo $_GET['return'];}?>">
                                            <input type="hidden" name="cartype" value="{{$_GET['cartype']}}">
                                            <input type="hidden" name="precar" value="<?php if(isset($_GET['car']))echo $_GET['car'] ?>">
                                            
                                           <?php 
                                                                                       
                                           $pagecar="";if(isset($_GET['car']) && !empty($_GET['car']))$pagecar="&car=".$_GET['car'] ?>
                                            @foreach($arcar as $car)
                                            <?php 
                                            $ada = 0;
                                            if(isset($_GET['car']) && !empty($_GET['car'])){
                                                $precars=explode("--", $_GET['car']);
                                                foreach($precars as $precar){
                                                    $isiprecar=explode("||",$precar);
                                                    if($car->id_rentcar == $isiprecar[0]){
                                                        $ada=1;
                                                        break;
                                                    }
                                                }
                                            }
                                            if($ada==0){
                                            ?>
                                            <div class="thumbnail well">
                                                <div class="row">
                                                    <div class="col-xs-5">
                                                    <?php 
                                                    $fotos = App\Models\car_img::get($car->id_rentcar);
                                                    $nfotos = count($fotos);
                                                    if($nfotos<2){
                                                    ?>
                                                    
                                                        <img src="{{env('IMG').'/rental/'.$car->foto}}" width="310">
                                                    
                                                    <?php
                                                    }else{?>
                                                    <div id="foto{{$car->id_rentcar}}" class="carousel slide" data-ride="carousel">
                                                        <!-- Indicators -->
                                                        <ol class="carousel-indicators">
                                                          @for($i=0;$i<$nfotos;$i++)  
                                                          <li data-target="#foto{{$car->id_rentcar}}" data-slide-to="{{$i}}" <?php if($fotos[$i]['primer']==1)echo 'class="active"';?>></li>
                                                          @endfor
                                                      </ol>

                                                      <!-- Wrapper for slides -->
                                                      <div class="carousel-inner" role="listbox">
                                                        @for($i=0;$i<$nfotos;$i++)   
                                                        <div class="item <?php if($fotos[$i]['primer']==1)echo "active";?>">
                                                            <img src="{{env('IMG').'/rental/'.$fotos[$i]['img']}}" alt="" width="310">
                                                        </div>
                                                        @endfor
                                                      </div>

                                                    <!-- Controls -->
                                                    <a class="left carousel-control" href="#foto{{$car->id_rentcar}}" role="button" data-slide="prev">
                                                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                                        <span class="sr-only">Previous</span>
                                                    </a>
                                                    <a class="right carousel-control" href="#foto{{$car->id_rentcar}}" role="button" data-slide="next">
                                                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                        <span class="sr-only">Next</span>
                                                    </a>
                                                </div>
                                                <?php
                                                }?>
                                                    @if($car->wajibdriver==1)
                                                    <div class="alert-danger">
                                                        <h4>* disewakan dengan supir</h4>
                                                    </div>
                                                    @endif
                                                    </div>
                                                    <div class="col-xs-3">
                                                        <h4>{{$car->nm_car_mnfcr.' '.$car->nm_car_merk}} <small>({{$car->plat_nomor}})</small></h4>
                                                        <div><i class="fa fa-group"></i> {{$car->penumpang.' penumpang'}}</div>
                                                        <div><i class="<?php if($car->id_car_tranms==2){echo "soap-icon-automatic-transmission";}else{echo "soap-icon-user";}?>"></i> {{$car->nm_car_trnms}}</div>
                                                        @if($car->ac==1)
                                                        <div><i class="fa soap-icon-aircon"></i> AC</div>
                                                        @endif
                                                        @if($car->audio==1)
                                                        <div><i class="fa soap-icon-entertainment"></i> Audio</div>
                                                        @endif
                                                        <div><i class="fa soap-icon-fueltank"></i> {{$car->nm_car_fuel.' '.number_format($car->kapasitas_bbm,0,',','.').' km/l'}}</div>
                                                        <div><i class="soap-icon-departure"></i> {{$car->lokasi.' - '.$car->nm_kab_kota}}</div>
                                                        <div><button type="button" class="btn btn-mini  btn-info detail" data-det="{{$car}}"><i class="fa fa-plus"> Detail</i></button></div>
                                                        <div class="divdetail{{$car->id_rentcar}}"></div>
                                                    </div>
                                                    <div class="col-xs-3" style="text-align:center;">
                                                        <h3>Tarif</h3>
                                                        <div> 
                                                      @foreach($car->price as $price)
                                                      <p><input type="radio" name="car" required="" value="{{$car->id_rentcar.'||'.$price->price.'||'.$price->nm_car_time.'||'.$price->id_car_time}}"> Rp. {{number_format($price->price,0,',','.').' / '.$price->nm_car_time}}</input></p>
                                                      
                                                      @endforeach
                                                        </div>

                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <?php }?>
                                            @endforeach
					<div class="row">
					    <div class="col-xs-9">
						<?php 
                                            $total=$arcar->total();
                                            $limit=$arcar->perPage();
                                            $noPage=$arcar->currentPage();
                                            $jumpage = $arcar->lastPage();
                                            $showPage = "";
                                            if(isset($_GET['return'])){$return = $_GET['return'];}else{$return="";}
                                            $pageatr="?depart=".$_GET['depart']."&destination=".$_GET['destination']."&return=".$return."&tgl=".$_GET['tgl']."&jam=".$_GET['jam']."&menit=".$_GET['menit']."&tgl1=".$_GET['tgl1']."&jam1=".$_GET['jam1']."&menit1=".$_GET['menit1']."&cartype=".$_GET['cartype'].$pagecar."&page=";
                                            ?>

                                                        <div class="pagination" style="margin: 0;">
                                                            <ul class="pagination">

                                                                @if( $total > $limit)
                                                                    @if($noPage>1)
                                                                        <li><a href="car-book{{$pageatr.($noPage-1)}}" data-page='{{$noPage-1}}' id="nav_paging">&laquo;</a></li>
                                                                    @else
                                                                        <li class="disabled"><a href="#">&laquo;</a></li>
                                                                    @endif

                                                                    @for($page = 1; $page <= $jumpage; $page++)

                                                                        @if ((($page >= $noPage - 3) && ($page <= $noPage + 3)) || ($page == 1) || ($page == $jumpage)) 
                                                                            @if (($showPage == 1) && ($page != 2)) 
                                                                                <li class="disabled"><a href="#" >...</a></li>
                                                                            @endif
                                                                            @if (($showPage != ($jumpage - 1)) && ($page == $jumpage))
                                                                                <li class="disabled"><a href="#" >...</a></li>
                                                                            @endif
                                                                            @if ($page == $noPage)
                                                                                <li class="active"><a href="#">{{$page}}</a></li>
                                                                            @else
                                                                                <li><a href="car-book{{$pageatr.$page}}" data-page='{{$page}}' id="nav_paging">{{$page}}</a></li>
                                                                            @endif
                                                                            <?php $showPage=$page;  ?>
                                                                        @endif
                                                                    @endfor
                                                                    @if ($noPage < $jumpage)
                                                                        <li><a href="car-book{{$pageatr.($noPage+1)}}" data-page='{{$noPage+1}}'  id="nav_paging" >&raquo;</a></li>
                                                                    @else
                                                                        <li class="disabled"><a href="#">&raquo;</a></li>
                                                                    @endif

                                                                @endif
                                                            </ul>
                                                        </div>
					    </div>
                                            <div class="col-xs-3" style="text-align: right">
                                                <button type="submit" class="button btn-large sky-blue1">Lanjutkan &raquo;</button>                                    
                                            </div>
					</div>
                                            </form>
                                        
                                    </div>
                                </div>
                        </div>
                </div>
            </div>
            <div class="sidebar col-sm-4 col-md-3">
                <div class="travelo-box contact-box">
                        <h4>Need Ketiket Help?</h4>
                        <p>We would be more than happy to help you. Our team advisor are 24/7 at your service to help you.</p>
                        <address class="contact-details">
                                <span class="contact-phone"><i class="soap-icon-phone"></i> 1-800-123-HELLO</span>
                                <br>
                                <a class="contact-email" href="#">help@ketiket.com</a>
                        </address>
                </div>
                <div class="travelo-box book-with-us-box">
                    <form action="{{ url('/car-book') }}" method="get">
                        <input type="hidden" name="depart" value="{{$_GET['depart']}}">
                        <input type="hidden" name="destination" value="{{$_GET['destination']}}">
                        <input type="hidden" name="tgl" value="{{$_GET['tgl']}}">
                        <input type="hidden" name="tgl1" value="{{$_GET['tgl1']}}">
                        <input type="hidden" name="jam" value="{{$_GET['jam']}}">
                        <input type="hidden" name="jam1" value="{{$_GET['jam1']}}">
                        <input type="hidden" name="menit" value="{{$_GET['menit']}}">
                        <input type="hidden" name="menit1" value="{{$_GET['menit1']}}">
                        <input type="hidden" name="return" value="<?php if(isset($_GET['return'])){echo $_GET['return'];}?>">
                        <input type="hidden" name="cartype" value="{{$_GET['cartype']}}">
                    <div class="alert alert-success">Filter Result</div>
                        <h5>Manufaktur </h5>
                    
                        @foreach($armanuf as $data)
                            <?php
                                if(isset($arcar->pabrik[$data->nm_car_mnfcr]))
                                    $jmlpabrik = count($arcar->pabrik[$data->nm_car_mnfcr]);
                                else
                                    $jmlpabrik = 0;
                            ?>    
                        <input type="checkbox" name="manuf[]" value="{{$data->ref_car_mnfcr}}">{{$data->nm_car_mnfcr.' ('.$jmlpabrik.')'}} <br/>                               
                        @endforeach
                    
                        <br>
                        <h5>Transmisi</h5>
                        @foreach($artrans as $data)
                        <input type="checkbox" name="trans[]" value="{{$data->id_car_trnms}}">{{$data->nm_car_trnms}} <br/>                               
                        @endforeach
                        
                            <button type="submit" class="full-width">SEARCH</button>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
tjq('button.detail').click(function(e){
    var data=tjq(this).attr('data-det');
     data=tjq.parseJSON(data);
    if(tjq(this).find('i').hasClass('fa-plus')){
        tjq(this).find('i').removeClass('fa-plus').addClass('fa-minus');
        var isi = "" ;
        isi += "<div>"+data['nm_car_type']+"</div>";
        isi += "<div>"+data['pintu']+" pintu</div>";
        isi += "<div>Tahun "+data['thn_model']+"</div>";
        isi += "<div>"+data['catatan']+"</div>";
        tjq('div.divdetail'+data['id_rentcar']).html(isi);
    }else{
        tjq(this).find('i').removeClass('fa-minus').addClass('fa-plus');
        tjq('div.divdetail'+data['id_rentcar']).html('');
    }

});
function difreturn(){
    var cek = tjq('div.divreturn').hasClass('hidden');
    if(cek){
        tjq('div.divreturn').removeClass('hidden');
    }else{
        tjq('div.divreturn').addClass('hidden');
    }
}
</script>
@stop
