@extends('Master.template')

@section('meta')
<style type="text/css">
	.kode-row{
		display: table;
		width: 100%;
	}
	.kode-col{
		display: table-cell;
	}
	.kode-col-left{
		width: 70%;
		padding: 5px;
	}
	.kode-col-right{
		width: 30%;
		background: #0078c7;
		color: #fff;
		padding: 5px;
	}
	.kode-col-right h1{
		color: #fff;
	}
</style>
<script type="text/javascript">
	tjq(function(){
		tjq(':input[name="status_reservasi"]').click(function(){
			var val = tjq(this).val();
			if(val == 1){
				tjq('.keterangan').removeClass('hide');
				tjq('.total').attr('required', 'required');
			}else{
				tjq('.keterangan').addClass('hide');
				tjq('.total').removeAttr('required');
			}
		});
	});
</script>
@stop

@section('content')
<div class="container">
	<div id="main">
		<div id="main" class="col-sm-8 col-md-9">
			<div class="booking-information travelo-box">
				<div class="tab-content">
					<div id="dashboard" class="tab-pane fade in active">
						<ul class="wizard">
							<li>1. Cari Rute</li>
							<li>2. Daftar Rute</li>
							<li>3. Pilih Kursi</li>
							<li class="active">4. Kode Booking</li>
							<li>5. Selesai</li>
						</ul>
						<hr />
						<!-- start content -->
						<div class="panel panel-default">
							<div class="kode-row">
								<div class="kode-col kode-col-left">
									<div class="media">
										<a class="pull-left" href="#">
											<img class="media-object" src="" width="50" height="50">
										</a>
										<div class="media-body">
											<h4 class="media-heading"><strong>{{ $rute->nama_travel }}</strong></h4>
											<p>{{ $rute->hint }}</p>
										</div>
									</div>
								</div>
								<div class="kode-col kode-col-right">
									<center>
										<small>Kode Booking</small>
										<h1><strong>{{$kode}}</strong></h1>
									</center>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="row">
									<div class="col-sm-6">
										<address>
											<strong>Rute</strong>
											<p>{{ $rute['nm_depart'] }} to {{ $rute['nm_destination'] }}</p>

											<strong>Travel</strong>
											<p>{{ $rute['nama_travel'] }}</p>

											<strong>Kelas</strong>
											<p>{{ $rute['kelas'] }}</p>

											<strong>Jadwal Berangkat</strong>
											<p><i class="fa fa-calendar"></i> {{ Format::indoDate($rute['tgl_reservasi']) }}  </p>
										</address>
									</div>
									<div class="col-sm-6	">
										<address>
											<strong>Nomor Kursi</strong>
											<p>{{ $rute['kode_routes'] }}</p>

											<strong>Jumlah Penumpang</strong>
											<p>{{ $booked['infant'] }}</p>

											<strong>Harga per Tiket</strong>
											<p>{{ number_format($trans['harga']) }}</p>

											<strong>Harga Total</strong>
											<p>{{ number_format($trans['harga']) }}</p>
										</address>
									</div>
								</div>
							</div>
						</div>

						<hr />
						<form method="post" action="{{ url('/reservasi/reservasi') }}">
						<div class="panel panel-default">
							<table class="table table-striped">
								<tr>
									<th>No</th>
									<th class="text-center" width="10%">No Kursi</th>
									<th width="50%">Nama Penumpang</th>
									<th width="40%">Nomor Telpon</th>
								</tr>
								<?php 
									$no = 1;
								?>
								@foreach($ambil[0]['nama'] as $i => $value)
								<tr>
									<td>{{ $no }}</td>
									<td class="text-center">{{ $ambil[0]['kode'][$i] }} <input type="hidden" value="{{ $ambil[0]['kode'][$i] }}" name="kode[]" ></td>
									<td>{{ $ambil[0]['nama'][$i] }}<input type="hidden" value="{{ $ambil[0]['nama'][$i] }}" name="nama[]" ></td>
									<td>{{ $ambil[0]['hp'][$i] }}<input type="hidden" value="{{ $ambil[0]['hp'][$i] }}" name="hp[]" ></td>
								</tr>
								<?php $no++; ?>
								@endforeach
							</table>
						</div>
							<input type="hidden" value="{{ $trans['harga'] }}" name="harga">
							<input type="hidden" value="{{ csrf_token() }}" name="_token">
							<input type="hidden" value="{{ $kode }}" name="kode_booking">
							<input type="hidden" value="{{ $trans['route_detail_id'] }}" name="route_detail_id">
							<input type="hidden" value="{{ $trans['hubs_id'] }}" name="hubs_id">
							<input type="hidden" value="{{ $trans['hubs_id'] }}" name="tgl_reservasi">

							<hr />
							<div class="text-right">
								<button>Simpan</button>
							</div>

						</form>

					</div>
				</div>
			</div>
		</div>
		<div class="sidebar col-sm-4 col-md-3">
			<div class="travelo-box contact-box">
				<h4>Need Ketiket Help?</h4>
				<p>We would be more than happy to help you. Our team advisor are 24/7 at your service to help you.</p>
				<address class="contact-details">
					<span class="contact-phone"><i class="soap-icon-phone"></i> 1-800-123-HELLO</span>
					<br>
					<a class="contact-email" href="#">help@ketiket.com</a>
				</address>
			</div>
			<div class="travelo-box book-with-us-box">
				<h4>Why Book with us?</h4>
				<ul>
					<li>
						<i class="soap-icon-hotel-1 circle"></i>
						<h5 class="title"><a href="#">135,00+ Hotels</a></h5>
						<p>Nunc cursus libero pur congue arut nimspnty.</p>
					</li>
					<li>
						<i class="soap-icon-savings circle"></i>
						<h5 class="title"><a href="#">Low Rates &amp; Savings</a></h5>
						<p>Nunc cursus libero pur congue arut nimspnty.</p>
					</li>
					<li>
						<i class="soap-icon-support circle"></i>
						<h5 class="title"><a href="#">Excellent Support</a></h5>
						<p>Nunc cursus libero pur congue arut nimspnty.</p>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
@stop