@extends('Master.template')

@section('content')
<div class="container">
	<div class="row">
		<div id="main" class="col-sm-8 col-md-9">
			<div class="row">
				<div class="col-sm-12">
					<div class="page-title-container style5">
						<div class="container">
							<ul class="breadcrumbs">
								<li><a href="#">PILIH JADWAL KEBERANGKATAN </a></li>
								<li><a href="#">PILIH KURSI</a></li>
								<li><a href="#">INFORMASI PENUMPANG</a></li>
								<li class="active"><a href="#">PEMBAYARAN</a></li>
								<li><a href="#">SELESAI</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="booking-information travelo-box">
				<h2>Booking Confirmation</h2>
				<hr />
				<h1>Kode Booking : <i>{{ $reservasi['kode_booking'] }}</i></h1>
				
				
				<div class="row">
					<div class="col-sm-6">
						<p>Nama Travel : <b></b> </p>
						<p>Telp Travel : <b></b> </p>
						<p>Jam : <b></b> </p>
					</div>
					<div class="col-sm-6">
						<p>Depart : <b>{{ $travel['pergi_dari'] }}</b> </p>
						<p>Destination : <b>{{ $travel['tujuan_ke'] }}</b> </p>
						<p>Harga : <b></b> </p>
					</div>
				</div>

				<table class="table">
					<tr>
						<th>Nomor Kursi</th>
						<th>Nama</th>
						<th>No HP</th>
					</tr>
					@for($i = 0; $i < count($reservasi['nomer']); $i++)

					<tr>
						<td>{{ $reservasi['nomer'][$i] }}</td>
						<td>{{ $reservasi['nama'][$i] }}</td>
						<td>{{ $reservasi['no_telp'][$i] }}</td>
					</tr>
					@endfor
				</table>

				<blockquote class="style1">
				<span class="triangle"></span>Total. 100000
				</blockquote>
				<form action="{{ url('/saveBooking') }}" method="post">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					

					<button type="submit" >Simpan</button>
				</form>


			</div>
		</div>
		<div class="sidebar col-sm-4 col-md-3">
			<div class="travelo-box contact-box">
				<h4>Need Travelo Help?</h4>
				<p>We would be more than happy to help you. Our team advisor are 24/7 at your service to help you.</p>
				<address class="contact-details">
					<span class="contact-phone"><i class="soap-icon-phone"></i> 1-800-123-HELLO</span>
					<br>
					<a class="contact-email" href="#">help@travelo.com</a>
				</address>
			</div>
			<div class="travelo-box book-with-us-box">
				<h4>Why Book with us?</h4>
				<ul>
					<li>
						<i class="soap-icon-hotel-1 circle"></i>
						<h5 class="title"><a href="#">135,00+ Hotels</a></h5>
						<p>Nunc cursus libero pur congue arut nimspnty.</p>
					</li>
					<li>
						<i class="soap-icon-savings circle"></i>
						<h5 class="title"><a href="#">Low Rates &amp; Savings</a></h5>
						<p>Nunc cursus libero pur congue arut nimspnty.</p>
					</li>
					<li>
						<i class="soap-icon-support circle"></i>
						<h5 class="title"><a href="#">Excellent Support</a></h5>
						<p>Nunc cursus libero pur congue arut nimspnty.</p>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
@stop