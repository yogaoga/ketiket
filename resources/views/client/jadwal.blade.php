@extends('Master.template')
@section('meta')
<script type="text/javascript" src="{{ asset('/vendor/jquery-select2/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/create-route.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('/vendor/jquery-select2/select2.min.css') }}">
@stop
@section('header')
<div class="page-title-container">
	<div class="container">
		<div class="page-title pull-left">
			<h2 class="entry-title">Booked</h2>
		</div>
		<ul class="breadcrumbs pull-right">
			<li><a href="#">HOME</a></li>
			<li><a href="#">Pages</a></li>
			<li class="active">booked</li>
		</ul>
	</div>
</div>
@stop
@section('content')
<div class="container">
	<div id="main">

		<div class="row">
			<div id="main" class="col-sm-8 col-md-9">
				<div class="toggle-container filters-container">
					<div class="panel style1 arrow-right">
						<h4 class="panel-title">
							<a data-toggle="collapse" href="#price-filter" class="collapsed">Ubah Pencarian</a>
						</h4>
						<div id="price-filter" class="panel-collapse collapse">
							<div class="panel-content">
								<form action="{{ url('/reservasi/jadwal') }}" method="get">
									<div class="row">
										<div class="form-group col-sm-6 col-md-5">
											<div class="row">
												<div class="col-sm-6">
													<label>Pergi Ke</label>
													<select name="depart" class="form-control form-select2" required>
														<option value="">Pilih Lokasi</option>
														@foreach($provinsi as $prov)
														<optgroup label="{{ $prov->nm_provinsi }}">
															@foreach(App\Models\provinsi::find($prov->id)->kab_kota()->get() as $kab)
															<option value="{{ $kab->id }}" {{ $kab->id == $req['depart'] ? 'selected="selected"' : '' }}>{{ $kab->nm_kab_kota }}</option>
															@endforeach
														</optgroup>
														@endforeach
													</select>	
												</div>
												<div class="col-sm-6">
													<label>Tujuan Ke</label>

													<select name="destination" class="form-control form-select2" required>
														<option value="">Pilih Lokasi</option>
														@foreach($provinsi as $prov)
														<optgroup label="{{ $prov->nm_provinsi }}">
															@foreach(App\Models\provinsi::find($prov->id)->kab_kota()->get() as $kab)
															<option value="{{ $kab->id }}" {{ $kab->id == $req['destination'] ? 'selected="selected"' : '' }}>{{ $kab->nm_kab_kota }}</option>
															@endforeach
														</optgroup>
														@endforeach
													</select>
												</div>
											</div>

										</div>

										<div class="form-group col-sm-6 col-md-4">
											<div class="row">
												<div class="col-xs-6">
													<label>Check Out</label>
													<div class="datepicker-wrap">
														<input type="text" required name="tgl" class="input-text full-width" value="{{ $req['tgl'] }}" placeholder="mm/dd/yy" />
													</div>
												</div>
												<div class="col-xs-4">
													<label>Infant</label>
													<div class="selector full-width">
														<select name="infant">
															<option value="1">1</option>
															<option value="2">2</option>
															<option value="3">3</option>
															<option value="4">4</option>
														</select>
													</div>
												</div>
											</div>
										</div>

										<div class="form-group col-sm-6 col-md-3">
											<div class="row">
												<div class="col-xs-10">
													<label class="hidden-xs">&nbsp;</label>
													<button type="submit" class="full-width icon-check animated" data-animation-type="bounce" data-animation-duration="1">SEARCH NOW</button>
												</div>
											</div>
										</div>
									</div>
									<input type="hidden" name="sort" value="desc">
									<input type="hidden" name="_t" value="{{ csrf_token() }}">
								</form>
								<div class="clearer"></div>
							</div><!-- end content -->
						</div>
					</div>
				</div>
				<div class="booking-information travelo-box">
					<div class="tab-content">
						<div id="dashboard" class="tab-pane fade in active">
							<ul class="wizard">
								 <li>1. Cari Rute</li>
								<li class="active">2. Daftar Rute</li>
								<li>3. Pilih Kursi</li>
								<li>4. Kode Booking</li>
								<li>4. Selesai</li>
							</ul>
							<hr />
							<h1 class="no-margin skin-color">{{ $routes->total() }} hasil ditemukan</h1>
							<br />
							<div class="panel panel-default">
								<div class="car-list listing-style3 car">
									@forelse($routes as $route)
									<form method="get" action="{{ url('/reservasi/seat') }}">
										<article class="box" style="margin:0;padding-bottom:0;">
											<div class="details col-xs-12 clearfix">
												<div class="col-sm-7">
													<div class="clearfix">
														<h4 class="box-title">{{ $route->nm_depart }} to {{ $route->nm_destination }}<small>{{ $route->nama_travel }} | Kode : {{ $route->kode_routes }}</small></h4>
														<div class="logo">
															<img src="{{ env('IMG') }}/travel/avatar/thumb/{{ $route->logo }}" alt="" />
														</div>
													</div>
													<div class="amenities">
														<ul>
															@foreach($facilitys[$route->id] as $fac)
															<li data-toggle="tooltip" data-placement="bottom" title="{{ $fac->keterangan }}">
																<img width="18" height="18" src="{{ env('IMG') }}/facility/{{ $fac->logo }}"><br />
																{{ $fac->kode }}
															</li>
															@endforeach
														</ul>
													</div>
												</div>
												<div class="col-xs-6 col-sm-2 character">
													<dl class="">
														<dt class="skin-color">Kelas</dt><dd>{{ $route->kelas }}</dd>
													</dl>
												</div>
												<div class="action col-xs-6 col-sm-3">
													<span class="price"><small>Harga</small>Rp {{ number_format($route->harga,0,',','.') }}</span>
													<br />
													@if(in_array(date('N', strtotime($req['tgl'])), explode(',', $route->day_route)))
													<button class="full-width" type="submit">PILIH KURSI</button>
													@else
													<div class="well text-danger">
														Maaf, Rute ini tidak tersedia untuk hari <br >
														{{ Format::nama_hari(date('N', strtotime($req['tgl']))) }}, {{ Format::indoDate($req['tgl']) }}
													</div>
													@endif
													<input type="hidden" name="rute" value="{{ $route->nm_depart }}-{{ $route->nm_destination }}">
													<input type="hidden" name="i" value="{{ $route->id }}">
													<input type="hidden" name="travel" value="{{ $route->nama_travel }}">
													<input type="hidden" name="infant" value="{{ $req['infant'] }}">
												</div>
											</div>
										</article>


										<div class="panel-footer">
											<strong>Kendaraan : </strong> <span class="label label-default">{{ $route->bus }}</span> |
											<strong><i class="fa fa-calendar"></i> Hari beroprasi : </strong>
											@foreach(explode(',', $route->day_route) as $day)
											@if(date('N', strtotime($req['tgl'])) == $day)
											<span class="label label-primary">{{ Format::nama_hari($day) }}</span>
											@else
											<span class="label label-default">{{ Format::nama_hari($day) }}</span>
											@endif
											@endforeach
										</div>
										<table class="table table-striped" style="border-bottom:solid 1px #ddd;">
											@foreach($pools[$route->id] as $pool)
											<tr>
												<td>
													<label for="f_{{ $pool->id }}" class="radio radio-inline" style="cursor:pointer;">
														<input type="radio" name="pool{{ $route->id }}" value="{{ $pool->id }}" id="f_{{ $pool->id }}" data-id="{{ $pool->id }}"  required/>
													</label>
												</td>
												<td>
													<label for="f_{{ $pool->id }}" style="cursor:pointer;">
														<div>{{ date('h:i A', strtotime($pool->waktu)) }}</div>
													</label>
												</td>
												<td>
													<label for="f_{{ $pool->id }}" style="cursor:pointer;">
														<div>{{ $pool->lokasi }}</div>
													</label>
												</td>
											</tr>
											@endforeach
										</table>
										<input type="hidden" name="tgl" value="{{ $req['tgl'] }}">
										<input type="hidden" name="i" value="{{ $route->id }}">
										<input type="hidden" name="_t" value="{{ csrf_token() }}">
									</form>
									@empty
									<div class="panel-body">
										<div class="well">
											Rute tidak ditemukan !
										</div>
									</div>
									@endforelse
								</div>

								<div class="text-right">
									{!! $routes->appends($req)->render() !!}
								</div>
							</div>


						</div>
					</div>
				</div>
			</div>
			<div class="sidebar col-sm-4 col-md-3">
				<div class="travelo-box contact-box">
					<h4>Need Ketiket Help?</h4>
					<p>We would be more than happy to help you. Our team advisor are 24/7 at your service to help you.</p>
					<address class="contact-details">
						<span class="contact-phone"><i class="soap-icon-phone"></i> 1-800-123-HELLO</span>
						<br>
						<a class="contact-email" href="#">help@ketiket.com</a>
					</address>
				</div>
				<div class="travelo-box book-with-us-box">
					<h4>Why Book with us?</h4>
					<ul>
						<li>
							<i class="soap-icon-hotel-1 circle"></i>
							<h5 class="title"><a href="#">135,00+ Hotels</a></h5>
							<p>Nunc cursus libero pur congue arut nimspnty.</p>
						</li>
						<li>
							<i class="soap-icon-savings circle"></i>
							<h5 class="title"><a href="#">Low Rates &amp; Savings</a></h5>
							<p>Nunc cursus libero pur congue arut nimspnty.</p>
						</li>
						<li>
							<i class="soap-icon-support circle"></i>
							<h5 class="title"><a href="#">Excellent Support</a></h5>
							<p>Nunc cursus libero pur congue arut nimspnty.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
@stop