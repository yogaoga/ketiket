@extends('Master.template')

@section('header')
<div class="page-title-container">
	<div class="container">
		<div class="page-title pull-left">
			<h2 class="entry-title">Booked</h2>
		</div>
		<ul class="breadcrumbs pull-right">
			<li><a href="#">HOME</a></li>
			<li><a href="#">Pages</a></li>
			<li class="active">booked</li>
		</ul>
	</div>
</div>
@stop
@section('content')
<div class="container">
    <div id="main">
        <div class="row">
            <div id="main" class="col-sm-8 col-md-9">
                <div class="booking-information travelo-box">
                    <div class="tab-content">
                        <div id="dashboard" class="tab-pane fade in active">
                            <ul class="wizard">
                                     <li>1. Cari Mobil</li>
                                    <li>2. Daftar Mobil</li>
                                    <li>3. Fasilitas</li>
                                    <li>4. Biodata</li>
                                    <li class="active">5. Selesai</li>
                            </ul>
                            <hr />
                            
                            <div class="contentdata">

                                <div class="alert alert-success">
                                    <h3>Terima kasih anda baru saja melakukan transaksi sewa mobil di <strong>ketiket.com</strong>
                                        dengan nomor invoice {{$no_invoice}}</h3>
                                </div>

                                <h5><strong>Detail Transaksi</strong></h5>
                                @include('client.car_rent_invoice')
                                <table width="100%" class="table table-condensed no-border">
                                <tr class="active">
                                    <td colspan="2">
                                        Catatan :
                                        <br />
                                        1. Pembayaran sewa dilakukan 5 jam sebelum waktu penjemputan.
                                        <br />
                                        2. Pembatalan setelah pembayaran akan dikenakan potongan 10% dari transaksi yang sudah dibayarkan. 
                                        <br />
                                        3. Bila pembatalan tidak dilakukan setelah waktu penjemputan dilakukan, maka akan dianggap hangus.
                                    </td>
                                </tr>
                                </table>
                                <div style="text-align: right"><a href='/print-car-book/{{$no_invoice}}'>(Print Halaman ini)</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sidebar col-sm-4 col-md-3">
                <div class="travelo-box book-with-us-box">
                    <h5><strong>Pembayaran Ditransfer ke Rekening</strong> </h5>
                    <h5>KETIKET.COM (PT. Maltatravelindo)</h5>
                    @foreach($npwp as $npwp)
                    <h5>{{$npwp->npwp." : ".$npwp->nomor}}</h5>
                    @endforeach
                    <br />
                    <h5>Alamat : Gedung Intiland Lt.3, Surabaya, 43456 - Jawa Timur</h5>
                    <br />
                    @foreach($norek as $rek)
                    <h5>Rek. {{$rek->bank." : ".$rek->norek}}</h5>
                    @endforeach
                </div>
                <div class="travelo-box contact-box">
                        <h4>Need Ketiket Help?</h4>
                        <p>We would be more than happy to help you. Our team advisor are 24/7 at your service to help you.</p>
                        <address class="contact-details">
                                <span class="contact-phone"><i class="soap-icon-phone"></i> 1-800-123-HELLO</span>
                                <br>
                                <a class="contact-email" href="#">help@ketiket.com</a>
                        </address>
                </div>
                
            </div>
        </div>
    </div>
</div>

@stop
