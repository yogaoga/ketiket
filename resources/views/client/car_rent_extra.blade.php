@extends('Master.template')
@section('meta')
<script type="text/javascript" src="{{ asset('/vendor/jquery-select2/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/create-route.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('/vendor/jquery-select2/select2.min.css') }}">
@stop
@section('header')
<div class="page-title-container">
	<div class="container">
		<div class="page-title pull-left">
			<h2 class="entry-title">Booked</h2>
		</div>
		<ul class="breadcrumbs pull-right">
			<li><a href="#">HOME</a></li>
			<li><a href="#">Pages</a></li>
			<li class="active">booked</li>
		</ul>
	</div>
</div>
@stop
@section('content')
<div class="container">
    <div id="main">
        <div class="row">
             <form action="{{ url('/biodata-car-book') }}" method="get">
            <div id="main" class="col-sm-8 col-md-8">
             
                <div class="booking-information travelo-box">
                        <div class="tab-content">
                                <div id="dashboard" class="tab-pane fade in active">
                                        <ul class="wizard">
                                                 <li>1. Cari Mobil</li>
                                                <li>2. Daftar Mobil</li>
                                                <li class="active">3. Fasilitas</li>
                                                <li>4. Biodata</li>
                                                <li>5. Selesai</li>
                                        </ul>
                                        <hr />
                                        <div class="contentdata">
                                        <h1 class="no-margin skin-color">Pilih Fasilitas Tambahan</h1>
                                        <br />
                                       
                                            <input type="hidden" name="depart" value="{{$_GET['depart']}}">
                                            <input type="hidden" name="destination" value="{{$_GET['destination']}}">
                                            <input type="hidden" name="cartype" value="{{$_GET['cartype']}}">
                                            <input type="hidden" name="return" value="<?php if($_GET['return']){echo $_GET['return'];}?>">
                                            <?php $datacar="";
                                            if(!empty($_GET['precar'])){$datacar=$_GET['precar']."--";}
                                            $datacar.=$_GET['car']; ?>
                                           
                                        @foreach($arextra as $car)
                                            <div class="thumbnail well">
                                                <div class="row">
                                                    <div class="col-xs-1">
                                                        <input type="checkbox" class="extra extra{{$car->in_car_extra}}" data-nama="{{$car->nm_car_extra}}" data-price="{{$car->price_extra}}" value="{{$car->in_car_extra}}">
                                                    </div>
                                                    <div class="col-xs-3">

                                                        <img src="<?php echo env('IMG').'/rental/';if($car->img){ echo $car->img;}else{echo "nofoto.png";}?>" width="100">

                                                    </div>
                                                    <div class="col-xs-4">
                                                        <h4>{{$car->nm_car_extra}}</h4>
                                                        <div>{{$car->note}}</div>
                                                    </div>
                                                    <div class="col-xs-3" style="text-align:right;">
                                                        <h3>Rp. {{number_format($car->price_extra,0,',','.')}}</h3>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <?php 
                                                    if(isset($_GET['return'])){$return = $_GET['return'];}else{$return="";}
                                                    $pageatr="?depart=".$_GET['depart']."&destination=".$_GET['destination']."&return=".$return."&tgl=".$_GET['tgl']."&jam=".$_GET['jam']."&menit=".$_GET['menit']."&tgl1=".$_GET['tgl1']."&jam1=".$_GET['jam1']."&menit1=".$_GET['menit1']."&cartype=".$_GET['cartype']."&page=1&car=".$datacar;?>
                                                    <a href="car-book{{$pageatr}}" class="button btn-large sky-blue1">&laquo; Kembali</a>
                                                </div>
                                                <div class="col-xs-6" style="text-align: right">
                                                    <button type="submit" class="button btn-large sky-blue1">Lanjutkan & Checkout &raquo;</button>
                                                </div>
                                            </div>
                                </div>
                        </div>
                </div>
            </div>
            </div>
            <div class="sidebar col-sm-4 col-md-4">
                <div class="travelo-box contact-box">
                        <h4>Need Ketiket Help?</h4>
                        <p>We would be more than happy to help you. Our team advisor are 24/7 at your service to help you.</p>
                        <address class="contact-details">
                                <span class="contact-phone"><i class="soap-icon-phone"></i> 1-800-123-HELLO</span>
                                <br>
                                <a class="contact-email" href="#">help@ketiket.com</a>
                        </address>
                </div>
                <div class="travelo-box book-with-us-box">
                    <a href="car-book{{$pageatr}}" class="button btn-large sky-blue1"><i class="fa fa-plus"></i> Tambah mobil lain</a>
                    
                   
                    <div class="alert alert-success">
                        Order Info
                    </div>     
                            <?php 
                            $cars = explode("--", $datacar);
                            $total = 0;$n=0;
                            //HITUNG TANGGAL
                            $ardate = explode("/", $_GET['tgl']);
                            $date=$ardate[2]."-".$ardate[0]."-".$ardate[1].' '.$_GET['jam'].':'.$_GET['menit'].':00';
                            $date = new DateTime($date);

                            $ardate = explode("/", $_GET['tgl1']);
                            $date1=$ardate[2]."-".$ardate[0]."-".$ardate[1].' '.$_GET['jam1'].':'.$_GET['menit1'].':00';
                            $date1 = new DateTime($date1);
                            
                            foreach($cars as $data){
                               $biayaextra = 0;
                               $totalextra = 0;
                               $cardet = explode("||", $data);
                               $detail = App\Models\car_rent::get_detail($cardet[0]);
                               $lamasewa = App\Models\ref_car_time::get_detail($cardet[3],$date,$date1);
                               $nprice = ceil($lamasewa->lamasewa/$lamasewa->periode);
                               $subtotal = $nprice*$cardet[1];       
                              
                               ?>
                    
                    <div style="padding-top:10px;" class="car car{{$cardet[0]}}" data-id="{{$cardet[0]}}" data-price="{{$cardet[1]}}">
                       <input type="hidden" name="car[]" value="{{$cardet[0]}}">
                        <input type="hidden" name="cartime[]" value="{{$cardet[3]}}">
                        <input type="hidden" name="nprice[]" id="nprice" value="{{$nprice}}">
                        
                        <div class="nmcartime hidden">{{$lamasewa->nm_car_time}}</div>
                        <div class="wajibdriver hidden">{{$detail->wajibdriver}}</div>
                                <div class="panel-footer">
                                <div class="row" >
                                    <div class="col-xs-10">
                                        <h5>{{($n+1).'. '.$detail->nm_car_mnfcr.' '.$detail->nm_car_merk.' - '.$detail->plat_nomor}}</h5>
                                    </div>
                                    <div class="col-xs-2" style="text-align:right">
                                         @if($n>0)
                                            <button type="button" class="btn btn-warning delcar" data-id="{{$cardet[0]}}"><i class="fa fa-times"></i></button>
                                        @endif
                                    </div>
                                </div>
                                </div>
                                <div class="thumbnail">
                                    <img width="100%" alt="" src="{{env('IMG').'/rental/'.App\Models\car_img::get_primer($cardet[0])}}">
                                </div>
                               
                                <div><h5>Tanggal Penjemputan</h5></div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php if($n==0){?>
                                        <input type="text" name="tgl[]" class="input-text full-width"  value="{{ $_GET['tgl'] }}" readonly="readonly"/>
                                        <?php }else{?>
                                            <div class="datepicker-wrap">
                                                <input type="text" name="tgl[]" onchange="lamasewa('{{$cardet[0]}}','{{$cardet[3]}}','{{$cardet[1]}}')" class="input-text full-width tgl" value="{{ $_GET['tgl'] }}" placeholder="mm/dd/yy" />
                                            </div>
                                        <?php }?>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="btn-group">
                                            <div class="col-sm-5" style="padding-left: 0px;padding-right: 0px;">
                                                 <input type="number" name="jam[]"  min="0" max="24" class="input-text full-width text-right jam" value="{{ $_GET['jam'] }}" 
                                                     <?php if($n==0){echo 'readonly="readonly"';}else{echo 'onchange="lamasewa('.$cardet[0].','.$cardet[3].','.$cardet[1].')"';}?>/>                                
                                            </div>
                                            <div class="col-sm-5" style="padding-left: 0px; padding-right: 0px;">
                                                <input type="number" name="menit[]" min="0" max="60" class="input-text full-width text-right menit" value="{{ $_GET['menit'] }}" <?php if($n==0){echo 'readonly="readonly"';}else{echo 'onchange="lamasewa('.$cardet[0].','.$cardet[3].','.$cardet[1].')"';}?>/>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div><h5>Tanggal Kembali</h5></div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php if($n==0){?>
                                        <input type="text" name="tgl1[]" class="input-text full-width" value="{{ $_GET['tgl1'] }}" readonly="readonly"/>
                                        <?php }else{?>
                                            <div class="datepicker-wrap">
                                                <input type="text" name="tgl1[]" onchange="lamasewa('{{$cardet[0]}}','{{$cardet[3]}}','{{$cardet[1]}}')" class="input-text full-width tgl1" value="{{ $_GET['tgl1'] }}" placeholder="mm/dd/yy" />
                                            </div>
                                        <?php }?>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="btn-group">
                                            <div class="col-sm-5" style="padding-left: 0px;padding-right: 0px;">
                                                 <input type="number" name="jam1[]" min="0" max="24" class="input-text full-width text-right jam1" value="{{ $_GET['jam1'] }}" <?php if($n==0){echo 'readonly="readonly"';}else{echo 'onchange="lamasewa('.$cardet[0].','.$cardet[3].','.$cardet[1].')"';}?>/>                                
                                            </div>
                                            <div class="col-sm-5" style="padding-left: 0px; padding-right: 0px;">
                                                <input type="number" name="menit1[]" min="0" max="60" class="input-text full-width text-right menit1" value="{{ $_GET['menit1'] }}" <?php if($n==0){echo 'readonly="readonly"';}else{echo 'onchange="lamasewa('.$cardet[0].','.$cardet[3].','.$cardet[1].')"';}?>/>
                                            </div>
                                        </div>

                                    </div>
                                </div>   
                                <div class="row">
                                    <div class="col-xs-6"><h5>Periode Sewa</h5></div>
                                    <div class="col-xs-6 periode"><h5><strong>{{$lamasewa->lamasewa.' '.$lamasewa->satuan}}</strong></h5></div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-xs-8"><h5>Biaya Rp. {{number_format($cardet[1],0,',','.').' / '.$cardet[2]}}</h5></div>
                                    <div class="col-xs-4 kalisewa"><h5><strong>@if($nprice>1){{'x '.$nprice.' @'.$lamasewa->nm_car_time}}@endif</strong></h5></div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6"><h5>Total</h5></div>
                                
                                    <div class="col-xs-6 totalsewa1" style="text-align:right"><h5>Rp. <strong>{{number_format($subtotal,0,',','.')}}</strong></h5></div>
                                </div>
                                <div class="totalsewa hidden">{{$subtotal}}</div>
                                <div class="panel-footer dataextra">
                                    <h5>Extra</h5>
                                </div>
                                @if($detail->wajibdriver==1)
                                    <div><input type="hidden" name="extras[{{$n}}][]" value="{{$driverdet->in_car_extra}}">
                                        <div class="col-xs-5"><h5>{{$driverdet->nm_car_extra}} (Wajib)</h5></div>
                                        <div class="col-xs-5" style="text-align:right;"><h5>Rp. {{number_format($driverdet->price_extra,0,',','.')}}</h5></div>
                                    </div>
                                <?php 
                                $biayaextra = $driverdet->price_extra;
                                $totalextra = $driverdet->price_extra*$nprice;
                                $subtotal+=$totalextra;
                                ?>
                                @endif
                                <div class="row">
                                    <div class="col-xs-5"><h5>Biaya</h5></div>
                                    <div class="col-xs-7 biayaextra1"><h5>Rp. {{number_format($biayaextra,0,',','.')}}</h5></div>
                                </div>
                               
                                <div class="row">
                                    <div class="col-xs-6"><h5>Total</h5></div>                                
                                    <div class="col-xs-6 totalextra1" style="text-align:right"><h5>Rp. {{number_format($totalextra,0,',','.')}}</h5></div>
                                </div>
                                <div class="hidden biayaextra">{{$biayaextra}}</div>
                                <div class="hidden totalextra">{{$totalextra}}</div>
                                <div class="panel-footer">
                                <div class="row">
                                    <div class="col-xs-6"><h5>Subtotal</h5></div>
                                
                                    <div class="col-xs-6 subtotal1" style="text-align:right"><h5>Rp. {{number_format($subtotal,0,',','.')}}</h5></div>
                                    
                                </div>
                            </div>
                            <div class="subtotal hidden">{{$subtotal}}</div>
                    </div>
                            <?php 
                                $total +=$subtotal;
                                $n++;
                            }?>
                            
                            <div class="alert alert-info">
                                <div class="row">
                                    <div class="col-xs-6"><h4>Total<h4></div>
                                    <div class="col-xs-6 total" style="text-align:right"><h4>Rp. {{number_format($total,0,',','.')}}</h4></div></div>
                            </div>
                         <input type="hidden" name="total" id="total" value="{{$total}}">
                   
                </div>
                
            </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
function numberWithCommas(x) {
    x = x.toString();
    var pattern = /(-?\d+)(\d{3})/;
    while (pattern.test(x))
        x = x.replace(pattern, "$1.$2");
    return x;
}  
tjq('input:checkbox.extra').click(function(e){
    var  id = tjq(this).val();
    var tr="";
    var total = 0;
    var nama = tjq(this).attr('data-nama');
    var varprice =  parseFloat(tjq(this).attr('data-price'));
    var cars = tjq('div.car');
    var subtotal,idcar,biayaextra,totalextra,nprice,satuan,jmlprice;
    if(tjq(this).is(":checked")){
        tjq.each(cars,function(j,obj){
            subtotal = parseFloat(tjq(this).find('div.subtotal').html());
            
            if(id!=3 || (id==3 && tjq(this).find('div.wajibdriver').html()!=1)){
                idcar = tjq(this).attr('data-id');
                satuan = "";
                nprice = parseFloat(tjq(this).find('input:hidden#nprice').val());
                jmlprice = varprice*nprice;

                tr='<div class="row extra'+id+'"><input type="hidden" name="extras['+j+'][]" value="'+id+'">\n\
                    <div class="col-xs-5"><h5>'+nama+'</h5></div>\n\
                    <div class="col-xs-5" style="text-align:right;"><h5>Rp. '+numberWithCommas(varprice)+'</h5></div>\n\
                    <div class=col-xs-1><button type="button" onclick="delextra('+idcar+','+id+','+varprice+')" class="btn btn-default"><i class="fa fa-times"></i></button></div>\n\
                </div>';
                
                subtotal +=jmlprice;
                tjq(this).find('div.subtotal').html(subtotal);
                tjq(this).find('div.subtotal1').html('<h5>Rp. '+numberWithCommas(subtotal)+'</h5>');
                tjq(this).find('div.dataextra').after(tr);


                if(nprice>1){satuan = "<strong> x "+nprice+" @"+tjq(this).find('div.nmcartime').html()+"</strong>";}
                biayaextra = parseFloat(tjq(this).find('div.biayaextra').html())+varprice;
                totalextra = parseFloat(tjq(this).find('div.totalextra').html())+jmlprice;
                tjq(this).find('div.biayaextra1').html('<h5>Rp. '+numberWithCommas(biayaextra)+satuan+'</h5>');
                tjq(this).find('div.totalextra1').html('<h5><strong>Rp. '+numberWithCommas(totalextra)+'</strong></h5>');
                tjq(this).find('div.biayaextra').html(biayaextra);
                tjq(this).find('div.totalextra').html(totalextra);
            }    
            total += subtotal;
            
        });
        tjq('input:hidden#total').val(total);
        tjq('div.total').html('<h4>Rp. '+numberWithCommas(total)+'</h4>');
        

    }else{
        total = parseFloat(tjq('input:hidden#total').val());
        tjq('div.extra'+tjq(this).val()).remove();
        tjq.each(cars,function(j,obj){
            
            if(id!=3 || (id==3 && tjq(this).find('div.wajibdriver').html()!=1)){
                satuan = "";
                nprice = parseFloat(tjq(this).find('input:hidden#nprice').val());
                jmlprice = varprice*nprice;
                subtotal = parseFloat(tjq(this).find('div.subtotal').html());
                subtotal -=jmlprice;
                tjq(this).find('div.subtotal').html(subtotal);
                tjq(this).find('div.subtotal1').html('<h5>Rp. '+numberWithCommas(subtotal)+'</h5>');
                total -= jmlprice;

                biayaextra = parseFloat(tjq(this).find('div.biayaextra').html())-varprice;
                totalextra = parseFloat(tjq(this).find('div.totalextra').html())-jmlprice;
                if(nprice>1){satuan = "<strong> x "+nprice+" @"+tjq(this).find('div.nmcartime').html()+"</strong>";}
                tjq(this).find('div.biayaextra1').html('<h5>Rp. '+numberWithCommas(biayaextra)+satuan+'</h5>');
                tjq(this).find('div.totalextra1').html('<h5><strong>Rp. '+numberWithCommas(totalextra)+'</strong></h5>');
                tjq(this).find('div.biayaextra').html(biayaextra);
                tjq(this).find('div.totalextra').html(totalextra);
            }
        });
        tjq('div.total').html('<h4>Rp. '+numberWithCommas(total)+'</h4>');
        tjq('input:hidden#total').val(total);
    }
    
});
tjq('button.delcar').click(function(e){
    var data=tjq(this).attr('data-id');
    var total = parseFloat(tjq('input:hidden#total').val());
    total -= parseFloat(tjq('div.car'+data+'>div.subtotal').html());
    tjq('div.total').html('<h4>Rp. '+numberWithCommas(total)+'</h4>');
    tjq('input:hidden#total').val(total);
    tjq('div.car'+data).remove();
});
function delextra(idcar,idextra,price){
    var cars = tjq('div.car');
    var subtotal = parseFloat(tjq('div.car'+idcar+'> div.subtotal').html());
    var total = parseFloat(tjq('input:hidden#total').val());
    var nprice = parseFloat(tjq('div.car'+idcar).find('input:hidden#nprice').val());
    var jmlprice = price*nprice;
    var satuan = "";
    total-=jmlprice;
    subtotal-=jmlprice;
    tjq('input:hidden#total').val(total);
    tjq('div.total').html('<h4>Rp. '+numberWithCommas(total)+'</h4>');
    tjq('div.car'+idcar+'> div.subtotal').html(subtotal);
    tjq('div.car'+idcar+'> div >div>div.subtotal1').html('<h5>Rp. '+numberWithCommas(subtotal)+'</h5>');
    
    biayaextra = parseFloat(tjq('div.car'+idcar).find('div.biayaextra').html())-price;
    totalextra = parseFloat(tjq('div.car'+idcar).find('div.totalextra').html())-jmlprice;
    if(nprice>1){satuan = "<strong> x "+nprice+" @"+tjq('div.car'+idcar).find('div.nmcartime').html()+"</strong>";}
    tjq('div.car'+idcar).find('div.biayaextra1').html('<h5>Rp. '+numberWithCommas(biayaextra)+satuan+'</h5>');
    tjq('div.car'+idcar).find('div.totalextra1').html('<h5><strong>Rp. '+numberWithCommas(totalextra)+'</strong></h5>');
    tjq('div.car'+idcar).find('div.biayaextra').html(biayaextra);
    tjq('div.car'+idcar).find('div.totalextra').html(totalextra);
    tjq('div.car'+idcar+'> div.extra'+idextra).remove();
    
    var n = 0;
    tjq.each(cars,function(j,obj){
        if(tjq(this).find('div.extra'+idextra).html())n++;
    });
    if(n==0){tjq('input:checkbox.extra'+idextra).attr('checked', false);}
}
function lamasewa(idcar,cartime,hargasatuan){
    var date = tjq('div.car'+idcar).find('input.tgl').val();
    ardate = date.split("/");
    date = ardate[2]+"-"+ardate[0]+"-"+ardate[1]+' '+tjq('div.car'+idcar).find('input.jam').val()+':'+tjq('div.car'+idcar).find('input.menit').val()+':00';
    var date1 = tjq('div.car'+idcar).find('input.tgl1').val();
    ardate = date1.split("/");
    date1 = ardate[2]+"-"+ardate[0]+"-"+ardate[1]+' '+tjq('div.car'+idcar).find('input.jam1').val()+':'+tjq('div.car'+idcar).find('input.menit1').val()+':00';
    var data = tjq.ajax(
    {
        url: 'lamasewa-car-book/'+date+'/'+date1+'/'+cartime, 
        global:false,
        async:false,
        type:'GET',
        complete:function(rdata)
        {
            return rdata;
        }
    }).responseText;
    var data=tjq.parseJSON(data);
    tjq('div.car'+idcar).find('div.periode').html("<h5><strong>"+data['lamasewa']+" "+data['satuan']+"</strong></h5>");
    var nprice = Math.ceil(parseFloat(data['lamasewa'])/parseFloat(data['periode']));
    var satuan = "";
    var biayaextra = parseFloat(tjq('div.car'+idcar).find('div.biayaextra').html());
    var totalextra = biayaextra*nprice;
    var totalsewa = nprice*parseFloat(hargasatuan);
    var subtotal = totalsewa+totalextra;
    
    if(nprice>1){
        satuan = "<strong> x "+nprice+" @"+data['nm_car_time']+"</strong>";
    }
    
    tjq('div.car'+idcar).find('div.kalisewa').html("<h5>"+satuan+"</h5>");
    tjq('div.car'+idcar).find('div.totalsewa').html(totalsewa);
    tjq('div.car'+idcar).find('div.totalsewa1').html('<h5>Rp. '+numberWithCommas(totalsewa)+'</h5>');
    tjq('div.car'+idcar).find('div.subtotal').html(subtotal);
    tjq('div.car'+idcar).find('div.subtotal1').html('<h5>Rp. '+numberWithCommas(subtotal)+'</h5>');
        
    tjq('div.car'+idcar).find('div.biayaextra1').html('<h5>Rp. '+numberWithCommas(biayaextra)+satuan+'</h5>');
    tjq('div.car'+idcar).find('div.totalextra1').html('<h5><strong>Rp. '+numberWithCommas(totalextra)+'</strong></h5>');
    tjq('div.car'+idcar).find('div.biayaextra').html(biayaextra);
    tjq('div.car'+idcar).find('div.totalextra').html(totalextra);
    tjq('div.car'+idcar).find('input:hidden#nprice').val(nprice);
    
    var cars = tjq('div.car');
    var total = 0;
    tjq.each(cars,function(j,obj){
        subtotal = parseFloat(tjq(this).find('div.subtotal').html());
        total+=subtotal;
    });
    tjq('input:hidden#total').val(total);
    tjq('div.total').html('<h4>Rp. '+numberWithCommas(total)+'</h4>');
    
}
</script>
@stop
