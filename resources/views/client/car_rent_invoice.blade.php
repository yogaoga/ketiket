<div style="font-size: 11pt ;font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;">
    <div class=" row"  >
       <div class="col-xs-3">
           Nama
       </div>
       <div class="">
           : {{$data->parent->nama}}
       </div>
    </div>
    <div class="row">
       <div class="col-xs-3">
           Alamat
       </div>
       <div class="">
           : {{$data->parent->alamat}}
       </div>
    </div>
    <div class="row">
       <div class="col-xs-3">
           No. Kontak
       </div>
       <div class="">
           : {{$data->parent->contact}}
       </div>
    </div>
    <div class="row">
       <div class="col-xs-3">
           Kartu Identitas {{$data->parent->idcard}}
       </div>
       <div class="">
           : {{$data->parent->no_idcard}}
       </div>
    </div>
    <div class="row">
       <div class="col-xs-3">
           Catatan Tambahan
       </div>
       <div class="">
          : {{$data->parent->catatan}}
       </div>
    </div>
    <div style="padding-top: 15px;"> 
       <?php $i=0;$rpcancel=0;$grandtotal=0;?>
       @foreach($data as $car)
       <?php $i++;
       $subtotal = 0;
       $nprice = $car->jml_paket_harga;
       $prices = explode(",", $car->price);
       $extras = explode(",", $car->id_car_extra);
       $totalsewa = $prices[0]*$nprice;
       $nextra = count($extras);
       $totalextra = 0;
       $jmlextra = 0;
       ?>
       <table width="100%" class="table table-condensed no-border">
           <tr class="info">
               <th width="40%">{{$i}}. Unit yang disewa </th>
               <th>{{$car->nm_car_mnfcr." ".$car->nm_car_merk." - Plat Nomor ".$car->plat_nomor}}
                    @if($car->status==4)
                        &nbsp;<span style="text-align: right" class="alert-danger">Dibatalkan</span>
                    @endif
               </th>
           </tr>
           <tr class="active">
               <td><img width="100%" alt="" src="{{env('IMG').'/rental/'.App\Models\car_img::get_primer($car->id_rental)}}"></td>
               <td>
                   <div class="row">
                       <div class="col-xs-5">
                           Tanggal Penjemputan
                       </div>
                       <div class="">
                           : {{$car->pickup_datetime}}
                       </div>
                   </div>
                   <div class="row">
                       <div class="col-xs-5">
                          Tanggal Kembali
                       </div>
                       <div class="">
                           : {{$car->return_datetime}}
                       </div>
                   </div>
                   <div class="row">
                       <div class="col-xs-5">
                           Periode Sewa
                       </div>
                       <div class="">
                           : <strong>{{$car->lama_sewa}}</strong>
                       </div>
                   </div>
                   <div class="row">
                       <div class="col-xs-5">
                          Biaya
                       </div>
                       <div class="">
                           : Rp. {{number_format($prices[0],0,',','.')}} 
                           @if($nprice>1)
                           <strong>x {{$nprice." @".$car->car_time}}</strong>
                           @endif
                       </div>
                   </div>
                   <div class="row">
                       <div class="col-xs-5">
                           Total Biaya Sewa
                       </div>
                       <div class="">
                           : <strong>Rp. {{number_format($totalsewa,0,',','.')}}</strong>
                       </div>
                   </div>
               </td>
           </tr>
           @if($nextra>0 && !empty($extras[0]))
           <tr class="danger">
               <th colspan="2">Biaya Ekstra</th>
           </tr>
           <tr class="active">
               <td colspan="2">
                   @for($j=0;$j<$nextra;$j++)
                       <?php
                       $extradet = App\ref_car_extra::get_detail($extras[$j]);
                       $totalextra+=$prices[($j+1)];
                       ?>
                       <div class="row">
                           <div class="col-xs-3 col-xs-offset-1">
                               {{($j+1).". ".$extradet->nm_car_extra}}
                           </div>
                           <div class="">
                               : Rp. {{number_format($prices[($j+1)],0,',','.')}}
                           </div>
                       </div>
                   @endfor
                   <div class="row">
                       <div class="col-xs-3">
                           Biaya Ekstra
                       </div>
                       <div class="">
                           : Rp. {{number_format($totalextra,0,',','.')}}
                           @if($nprice>1)
                           <strong>x {{$nprice." @".$car->car_time}}</strong>
                           @endif
                           
                       </div>
                   </div>
                   <?php 
                   $jmlextra = $totalextra*$nprice;
                   ?>
                   <div class="row">
                       <div class="col-xs-3">
                           Total Biaya Ekstra
                       </div>
                       <div class="">
                           : Rp. <strong>{{number_format($jmlextra,0,',','.')}}</strong>
                       </div>
                   </div>
               </td>
           </tr>
           @endif
           
           <?php 
           if($car->status==4){
            $rpcancel+=$car->adjustment;
            $subtotal = $car->adjustment;
           ?>
           <tr class="active">
               <th colspan="2" style="text-align: right">Cancel Fee : Rp. <strong>{{number_format($car->adjustment,0,',','.')}}</strong></th>
           </tr>
           <?php
           }else{
                $subtotal = $jmlextra+$totalsewa;
                $grandtotal+=$subtotal;?>
                <tr class="active">
                    <th colspan="2" style="text-align: right">Total Biaya : Rp. <strong>{{number_format($subtotal,0,',','.')}}</strong></th>
                </tr>
<?php
            }
           
           ?>
       @endforeach
       <tr class="success ">
           <th colspan="2" style="text-align: right">Grand Total : Rp. <strong>{{number_format($grandtotal,0,',','.')}}</strong></th>
       </tr>
       <?php $diskon=$data->adjust->diskon/100*$grandtotal;?>
       <tr class="active">
               <th colspan="2" style="text-align: right">Diskon : <strong>{{number_format($data->adjust->diskon,0,',','.')}}% x {{number_format($grandtotal,0,',','.')}} = Rp. ({{number_format($diskon,0,',','.')}})</strong></th>
        </tr>
       <?php 
        if($rpcancel>0){
        ?>
        <tr class="active">
            <th colspan="2" style="text-align: right">Cancel Fee : Rp. <strong>{{number_format($rpcancel,0,',','.')}}</strong></th>
        </tr>
        <?php
        }
        
        ?>
       <tr class="active">
               <th colspan="2" style="text-align: right">Adjustment : Rp. <strong>{{number_format($data->adjust->adjustment,0,',','.')}}</strong></th>
        </tr>
       
        <?php
        $grandtotal=$grandtotal+$data->adjust->adjustment+$rpcancel-$diskon;
        ?>
       <tr class="success ">
           <th colspan="2" style="text-align: right">Jumlah yang harus dibayar : Rp. <strong>{{number_format($grandtotal,0,',','.')}}</strong></th>
       </tr>
       </table>
       
    </div>
</div> 



