@extends('Master.template')
@section('header')
<div class="page-title-container">
	<div class="container">
		<div class="page-title pull-left">
			<h2 class="entry-title">Booked</h2>
		</div>
		<ul class="breadcrumbs pull-right">
			<li><a href="#">HOME</a></li>
			<li><a href="#">Pages</a></li>
			<li class="active">booked</li>
		</ul>
	</div>
</div>
@stop
@section('content')
<form action="{{ url('/biodata-car-book') }}" method="post">
<div class="container">
    <div id="main">
        <div class="row">
            <div id="main" class="col-sm-8 col-md-9">
                <div class="booking-information travelo-box">
                        <div class="tab-content">
                                <div id="dashboard" class="tab-pane fade in active">
                                        <ul class="wizard">
                                                 <li>1. Cari Mobil</li>
                                                <li>2. Daftar Mobil</li>
                                                <li>3. Fasilitas</li>
                                                <li class="active">4. Biodata</li>
                                                <li>5. Selesai</li>
                                        </ul>
                                        <hr />
                                        <div class="contentdata">
                                        <h1 class="no-margin skin-color">Mengisi biodata penyewa mobil</h1>
                                        <br />
                                        
                                            <input type="hidden" name="depart" value="{{$_GET['depart']}}">
                                            <input type="hidden" name="destination" value="{{$_GET['destination']}}">
                                            
                                             <input type="hidden" name="return" value="<?php if($_GET['return']){echo $_GET['return'];}?>">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <div class="panel">
                                                <div class="panel panel-header">
                                                    <h4>Biodata Personal</h4>
                                                </div>
                                                <div class="panel panel-body-d">
                                                    <div class="row">
                                                        <div class="form-group col-sm-6">
                                                             <label for="nama" class="col-sm-3 control-label">Nama Depan</label>
                                                             <div class="col-sm-9">
                                                                     <input type="text" class="input-text full-width" name="nama" id="nama" value="{{ old('nama') }}" required>
                                                             </div>
                                                         </div>
                                                        <div class="form-group col-sm-6">
                                                             <label for="nama1" class="col-sm-3 control-label">Nama Belakang</label>
                                                             <div class="col-sm-9">
                                                                     <input type="text" class="input-text full-width" name="nama1" id="nama1" value="{{ old('nama1') }}">
                                                             </div>
                                                         </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-sm-6">
                                                             <label for="alamat" class="col-sm-3 control-label">Alamat (Sesuai KTP)</label>
                                                             <div class="col-sm-9">
                                                                     <input type="text" class="input-text full-width" name="alamat" id="alamat" value="{{ old('alamat') }}" required>
                                                             </div>
                                                         </div>
                                                        <div class="form-group col-sm-6">
                                                             <label for="nokontak" class="col-sm-3 control-label">Nomor Kontak</label>
                                                             <div class="col-sm-9">
                                                                     <input type="text" class="input-text full-width" name="nokontak" id="nokontak" value="{{ old('nokontak') }}" required>
                                                             </div>
                                                         </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-sm-6">
                                                             <label for="idcard" class="col-sm-3 control-label">Jenis Kartu Identitas</label>
                                                             <div class="col-sm-9">
                                                                 <select name="idcard" class="form-control form-select2" required>
                                                                        @foreach($aridcard as $car)
                                                                            <option value="{{$car->id}}">{{ $car->idcard }}</option>
                                                                        @endforeach
                                                                    </select>
                                                             </div>
                                                         </div>
                                                        <div class="form-group col-sm-6">
                                                             <label for="idno" class="col-sm-3 control-label">Nomor Kartu Identitas</label>
                                                             <div class="col-sm-9">
                                                                     <input type="text" class="input-text full-width" name="idno" id="idno" value="{{ old('idno') }}" required>
                                                             </div>
                                                         </div>
                                                    </div>
                                                    <div class="form-group col-sm-12">
                                                        <label for="note" class="col-sm-3 control-label">Catatan Tambahan</label>
                                                        <div class="col-sm-9">
                                                                <input type="text" class="input-text full-width" name="note" id="note" value="{{ old('note') }}">
                                                        </div>
                                                    </div>
                                                    <p>Bila anda ingin Bukti Notifikasi Telah Melakukan Pemesanan Mobil, masukkan email anda :</p>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                             <label for="email" class="col-sm-3 control-label">Email</label>
                                                             <div class="col-sm-9">
                                                                     <input type="text" class="input-text full-width" name="email" id="email" value="{{ old('email') }}" >
                                                             </div>
                                                         </div>
                                                    
                                                        <div class="col-sm-6">
                                                            <input type="checkbox" name="plat" id="plat" value="" required>
                                                            Saya Setuju dengan Syarat dan Ketentuan yang Berlaku
                                                         </div>
                                                    </div>
                                               
                                                </div>
                                                <div class="panel panel-footer" style="text-align: right">
                                                   
                                                    <button type="submit" class="button btn-large sky-blue1">Pesan Sekarang</button>
                                                
                                                </div>
                                            </div>
                                            
                                    </div>
                                </div>
                        </div>
                </div>
            </div>
            <div class="sidebar col-sm-4 col-md-3">
            
                <div class="travelo-box book-with-us-box">
                    <div class="alert alert-success">
                        Order Info
                    </div>     
                            <?php 
                            $total = 0;$n=0;$ncar = count($_GET['car']);
                            
                            foreach($_GET['car'] as $data){
                               //HITUNG TANGGAL
                                $ardate = explode("/", $_GET['tgl'][$n]);
                                $date=$ardate[2]."-".$ardate[0]."-".$ardate[1].' '.$_GET['jam'][$n].':'.$_GET['menit'][$n].':00';
                                $date = new DateTime($date);

                                $ardate = explode("/", $_GET['tgl1'][$n]);
                                $date1=$ardate[2]."-".$ardate[0]."-".$ardate[1].' '.$_GET['jam1'][$n].':'.$_GET['menit1'][$n].':00';
                                $date1 = new DateTime($date1);
                                
                                $detail = App\Models\car_rent::get_detail($data);
                                $subtotal = 0;
                                $price = App\Models\car_price::get_price($data, $_GET['cartime'][$n]);
                                $lamasewa = App\Models\ref_car_time::get_detail($_GET['cartime'][$n],$date,$date1);
                                $rpprice = $price->price;
                                $cartime = $price->nm_car_time;
                                $totalsewa = ceil($lamasewa->lamasewa/$lamasewa->periode)*$rpprice;
                                $subtotal+= $totalsewa;
                                $nprice = $_GET['nprice'][$n];
                               ?>
                                <input type="hidden" name="car[]" value="{{$data}}">
                                <input type="hidden" name="cartime[]" value="{{$cartime}}">
                                <input type="hidden" name="price[]" value="{{$rpprice}}">
                                <input type="hidden" name="idcartime[]" value="{{$_GET['cartime'][$n]}}">
                                <input type="hidden" name="nprice[]" value="{{$nprice}}">
                                <input type="hidden" name="lamasewa[]" value="{{$lamasewa->lamasewa.' '.$lamasewa->satuan}}">
                                <div class="panel-footer">
                                    <h5>{{($n+1).'. '.$detail->nm_car_mnfcr.' '.$detail->nm_car_merk.' - '.$detail->plat_nomor}}</h5>
                                </div>
                              
                                <div>
                                    <img width="100%" alt="" src="{{env('IMG').'/rental/'.App\Models\car_img::get_primer($data)}}">
                                </div>
                               
                                <div><h5>Tanggal Penjemputan</h5></div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <input type="text" name="tgl[]" class="input-text full-width" value="{{ $_GET['tgl'][$n] }}" readonly="readonly"/>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="btn-group">
                                            <div class="col-sm-6" style="padding-left: 0px;padding-right: 0px;">
                                                 <input type="text" name="jam[]" class="input-text full-width text-right" value="{{ $_GET['jam'][$n] }}" readonly="readonly"/>                                
                                            </div>
                                            <div class="col-sm-6" style="padding-left: 0px; padding-right: 0px;">
                                                <input type="text" name="menit[]" class="input-text full-width text-right" value="{{ $_GET['menit'][$n] }}" readonly="readonly"/>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div><h5>Tanggal Kembali</h5></div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <input type="text" name="tgl1[]" class="input-text full-width" value="{{ $_GET['tgl1'][$n] }}" readonly="readonly"/>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="btn-group">
                                            <div class="col-sm-6" style="padding-left: 0px;padding-right: 0px;">
                                                 <input type="text" name="jam1[]" class="input-text full-width text-right" value="{{ $_GET['jam1'][$n] }}" readonly="readonly"/>                                
                                            </div>
                                            <div class="col-sm-6" style="padding-left: 0px; padding-right: 0px;">
                                                <input type="text" name="menit1[]" class="input-text full-width text-right" value="{{ $_GET['menit1'][$n] }}" readonly="readonly"/>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-xs-6"><h5>Periode Sewa</h5></div>
                                    <div class="col-xs-6"><h5><strong>{{$lamasewa->lamasewa.' '.$lamasewa->satuan}}</strong></h5></div>
                                </div>
                                
                                <div>
                                   <h5>Biaya Rp. {{number_format($rpprice,0,',','.').' / '.$cartime}}</h5>
                                </div>
                                @if($nprice>1)
                                <div style="text-align: right">
                                   <h5><strong>{{'x '.$nprice.' @'.$lamasewa->nm_car_time}}</strong></h5>
                                </div>
                                @endif
                                <div class="row">
                                    <div class="col-xs-6"><h5>Total</h5></div>
                                    <div class="col-xs-6" style="text-align:right"><h5>Rp. <strong>{{number_format($totalsewa,0,',','.')}}</strong></h5></div>
                                </div>
                                 <?php 
                            if(isset($_GET['extras'][$n])){?>
                                <div class="panel-footer dataextra">
                                    <h5>Extra</h5>
                                </div>
                            <?php   
                                $jmlextra = 0;
                                foreach($_GET['extras'][$n] as $datax){
                                    $extra = App\ref_car_extra::get_detail($datax);
                                    $nmextra = $extra->nm_car_extra;
                                    $rpextra = $extra->price_extra;
                                    $jmlextra+=$rpextra;
                                    ?>
                                <div class="row">
                                    <input type="hidden" name="extras[{{$n}}][]" value="{{$datax}}">
                                    <input type="hidden" name="pricex[{{$n}}][]" value="{{$rpextra}}">
                                    <div class="col-xs-5"><h5>{{$nmextra}}</h5></div>
                                    <div class="col-xs-5"><h5>Rp. {{number_format($rpextra,0,',','.')}}</h5></div>
                                </div>
                        <?php }
                            $totalextra = $jmlextra*$nprice;
                            $subtotal+=$totalextra;?>
                                <div class="row">
                                    <div class="col-xs-5"><h5>Biaya</h5></div>
                                    <div class="col-xs-7"><h5>Rp. {{number_format($jmlextra,0,',','.')}}</h5></div>
                                </div>
                                @if($nprice>1)
                                <div style="text-align: right">
                                   <h5><strong>{{'x '.$nprice.' @'.$lamasewa->nm_car_time}}</strong></h5>
                                </div>
                                @endif
                                <div class="row">
                                    <div class="col-xs-6"><h5>Total</h5></div>
                                
                                    <div class="col-xs-6" style="text-align:right"><h5><strong>Rp. {{number_format($totalextra,0,',','.')}}</strong></h5></div>
                                </div>
                        <?php   }?>
                                <div class="panel-footer">
                                <div class="row">
                                    <div class="col-xs-6"><h5>Subtotal</h5></div>
                                    <input type="hidden" name="subtotal[]" value="{{$subtotal}}">
                                    <div class="col-xs-6 " style="text-align:right"><h5>Rp. {{number_format($subtotal,0,',','.')}}</h5></div>
                                </div>
                            </div>
                              
                   
                            <?php 
                                $total+=$subtotal;
                                $n++;
                            }?>
                            
                            <div class="alert alert-info" style="text-align:right">
                                Total <h4>Rp. {{number_format($total,0,',','.')}}</h4>
                            </div>
                            <input type="hidden" name="total" value="{{$total}}">
                   
                </div>
                
            </div>
        </div>
    </div>
</div>
</form>
@stop
