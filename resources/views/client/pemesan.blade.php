@extends('Master.template')

@section('content')

<div class="container">
	<div class="row">
		<div id="main" class="col-sms-6 col-sm-8 col-md-9">
		<div class="row">
				<div class="col-sm-12">
					<div class="page-title-container style5">
						<div class="container">
							<ul class="breadcrumbs">
								<li><a href="#">PILIH JADWAL KEBERANGKATAN </a></li>
								<li><a href="#">PILIH KURSI</a></li>
								<li  class="active"><a href="#">INFORMASI PENUMPANG</a></li>
								<li><a href="#">PEMBAYARAN</a></li>
								<li><a href="#">SELESAI</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="booking-section travelo-box">
				
				<form class="booking-form" action="{{ url('/invoice') }}" method="post">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="person-information">
						<h2>Your Personal Information</h2>
						<div class="form-group row">
						<input type="hidden" name="kode_booking" value="{{ $randPass }}">
							<div class="col-sm-6 col-md-5">
								<label>Nama Pemesan</label>
								<input type="text" required name="nm_pemesan" class="input-text full-width" value="" placeholder="" />
							</div>
							<div class="col-sm-6 col-md-5">
								<label>No Telp</label>
								<input type="text" required name="no_telp" class="input-text full-width" value="" placeholder="" />
							</div>
						</div>
						<div class="form-group row">
							<div class="col-sm-6 col-md-5">
								<label>Alamat</label>
								<textarea name="alamat" required name="alamat" class="input-text full-width"></textarea>
							</div>
							<div class="col-sm-6 col-md-5">
								<label>E-mail</label>
								<input type="email" name="email" required class="input-text full-width" value="" placeholder="" />
							</div>
						</div>
						
					</div>

					<div class="row">
						<table class="table table-bordered">
							<tr>
								<th>
									Nomor Kursi
								</th>
								<th>
									Nama Penumpang
								</th>
								<th>
									No HP
								</th>
							</tr>


							@for($i = 0; $i < $booking['jumlah']; $i++ )
								<tr>
									<td><input type="text" name="nomer[]" class="input-text full-width" value="{{ $no_kursi[$i] }}" /> </td>
									<td><input type="text" name="nama[]" required class="input-text full-width" placeholder="Masukan Nama" /> </td>
									<td><input type="text" name="nohp[]" required class="input-text full-width" placeholder="Masukan No hanphone" /> </td>
								</tr>					

							@endfor

							
						</table>
					</div>

						<div class="form-group">
							<div class="checkbox">
								<label>
									<input type="checkbox"> I want to receive <span class="skin-color">Travelo</span> promotional offers in the future
								</label>
							</div>
						</div>
					<div class="form-group row">
						<div class="col-sm-6 col-md-5">
							

						</div>
					</div>

				<button type="submit" class="full-width btn-large">CONFIRM BOOKING</button>	
				</form>
			</div>
		</div>
		<div class="sidebar col-sms-6 col-sm-4 col-md-3">
			<div class="booking-details travelo-box">
				<h4>Booking Details</h4>
				<article class="car-detail">
					<figure class="clearfix">
						<a title="" href="car-detailed.html" class="middle-block"><img class="middle-item" alt="" src="http://placehold.it/75x75"></a>
						<div class="travel-title">
							<h5 class="box-title">BMW Mini<small>Economy car</small></h5>
							<a href="car-detailed.html" class="button">EDIT</a>
						</div>
					</figure>
					<div class="details">
						<div class="icon-box style11 full-width">
							<div class="icon-wrapper">
								<i class="soap-icon-departure"></i>
							</div>
							<dl class="details">
								<dt class="skin-color">Date</dt>
								<dd>Nov 14, 2013 to nov 15.2013</dd>
							</dl>
						</div>
						<div class="icon-box style11 full-width">
							<div class="icon-wrapper">
								<i class="soap-icon-departure"></i>
							</div>
							<dl class="details">
								<dt class="skin-color">Time</dt>
								<dd>11:00 AM to 11:00am</dd>
							</dl>
						</div>
						<div class="icon-box style11 full-width">
							<div class="icon-wrapper">
								<i class="soap-icon-departure"></i>
							</div>
							<dl class="details">
								<dt class="skin-color">Location</dt>
								<dd>London city to paris orly airport</dd>
							</dl>
						</div>
					</div>
				</article>
				
				<h4>Other Details</h4>
				<dl class="other-details">
					<dt class="feature">Mileage included:</dt><dd class="value">3,000 miles</dd>
					<dt class="feature">Damage Protection:</dt><dd class="value">$0.00 USD</dd>
					<dt class="feature">Per day price:</dt><dd class="value">$45.39</dd>
					<dt class="feature">taxes and fees:</dt><dd class="value">$155.61</dd>
					<dt class="total-price">Total Price</dt><dd class="total-price-value">$201.00</dd>
				</dl>
			</div>
			
			<div class="travelo-box contact-box">
				<h4>Need Travelo Help?</h4>
				<p>We would be more than happy to help you. Our team advisor are 24/7 at your service to help you.</p>
				<address class="contact-details">
					<span class="contact-phone"><i class="soap-icon-phone"></i> 1-800-123-HELLO</span>
					<br>
					<a class="contact-email" href="#">help@travelo.com</a>
				</address>
			</div>
		</div>
	</div>
</div>


@stop