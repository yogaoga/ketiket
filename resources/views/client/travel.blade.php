@extends('Master.template')

@section('content')
<div class="container">
	<div class="row">
		<div id="main" class="col-sm-8 col-md-9">
			<div class="tab-container style1" id="cruise-main-content">
				
				<div class="tab-content">
					<div id="photos-tab" class="tab-pane fade in active">
						<div class="photo-gallery style1" data-animation="slide" data-sync="#photos-tab .image-carousel">
							<ul class="slides">
								<li><img src="http://placehold.it/900x500" alt="" /></li>
								<li><img src="http://placehold.it/900x500" alt="" /></li>
								<li><img src="http://placehold.it/900x500" alt="" /></li>
								<li><img src="http://placehold.it/900x500" alt="" /></li>
								<li><img src="http://placehold.it/900x500" alt="" /></li>
								<li><img src="http://placehold.it/900x500" alt="" /></li>
								<li><img src="http://placehold.it/900x500" alt="" /></li>
								<li><img src="http://placehold.it/900x500" alt="" /></li>
								<li><img src="http://placehold.it/900x500" alt="" /></li>
								<li><img src="http://placehold.it/900x500" alt="" /></li>
								<li><img src="http://placehold.it/900x500" alt="" /></li>
							</ul>
						</div>
						<div class="image-carousel style1" data-animation="slide" data-item-width="70" data-item-margin="10" data-sync="#photos-tab .photo-gallery">
							<ul class="slides">
								<li><img src="http://placehold.it/70x70" alt="" /></li>
								<li><img src="http://placehold.it/70x70" alt="" /></li>
								<li><img src="http://placehold.it/70x70" alt="" /></li>
								<li><img src="http://placehold.it/70x70" alt="" /></li>
								<li><img src="http://placehold.it/70x70" alt="" /></li>
								<li><img src="http://placehold.it/70x70" alt="" /></li>
								<li><img src="http://placehold.it/70x70" alt="" /></li>
								<li><img src="http://placehold.it/70x70" alt="" /></li>
								<li><img src="http://placehold.it/70x70" alt="" /></li>
								<li><img src="http://placehold.it/70x70" alt="" /></li>
								<li><img src="http://placehold.it/70x70" alt="" /></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div id="cruise-features" class="tab-container">
				<ul class="tabs">
					<li class="active"><a href="#cruise-description" data-toggle="tab">Description</a></li>
					
				</ul>
				<div class="tab-content">
					<div class="tab-pane fade in active" id="cruise-description">
						<div class="intro table-wrapper full-width hidden-table-sms">
							<div class="col-sm-7 col-lg-8 features table-cell">
								<div class="travelo-box">
									<h4 class="box-title">Profil</h4>
									<ul>
										<li><label>Nama Travel:</label>{{ $travel->nama_travel }}</li>
										<li><label>Alamat:</label>{{ $travel->alamat }}</li>
										<li><label>No Telp:</label>{{ $travel->no_tlp }}</li>
										<li><label>Website:</label>{{ $travel->website }}</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="long-description">
							<h2>About {{ $travel->nama_travel }}</h2>
							<p>
								{{ $travel->information }}
							</p>
						</div>
					</div>
				</div>

			</div>
			<div class="comments-container block">
				<br>
				<h2>{{ count($comment) }} Testimonial</h2>
				<ul class="comment-list travelo-box">
				@if(count($comment) > 0)
					@foreach($comment as $data)
					<li class="comment depth-1">
						<div class="the-comment">
							<div class="avatar">
								<img src="http://placehold.it/270x263" width="72" height="72" alt="">
							</div>
							<div class="comment-box">
								<div class="comment-author">
									
									<h4 class="box-title"><a href="#">{{ $data->nama }}</a><small>{{ $data->created_at }}</small></h4>
								</div>
								<div class="comment-text">
									<p>{{ $data->comment }}</p>
								</div>
							</div>
						</div>
					</li>
					@endforeach
				@else
					<li><i>Tidak ada Comment</i></li>
				@endif
				</ul>
			</div>
			<div class="post-comment block">
				<h2 class="reply-title">Post a Testimonial</h2>
				<div class="travelo-box">
					<form  action="{{ url('/saveComment') }}" method="post" class="comment-form">
						<input type="hidden" value="{{ csrf_token() }}" name="_token">
						<input type="hidden" value="{{ $travel->id }}" name="travel_id">
						<div class="form-group row">
							<div class="col-xs-6">
								<label>Your Name</label>
								<input type="text" name="nama" class="input-text full-width">
							</div>
							<div class="col-xs-6">
								<label>Your Email</label>
								<input type="text" name="email" class="input-text full-width">
							</div>
						</div>
						<div class="form-group">
							<label>Your Message</label>
							<textarea rows="6" name="message" class="input-text full-width" placeholder="write message here"></textarea>
						</div>
						<input type="hidden" value="{{ $travel->slug }}" name="slug">

						<button type="submit" class="btn-large full-width">SEND TESTIMONIAL</button>
					</form>
				</div>
			</div>
		</div>
		<div class="sidebar col-sm-4 col-md-3">
			<article class="detailed-logo">
				<figure>
					<img width="320" height="80" src="http://placehold.it/320x80" alt="">
				</figure>
				<div class="details">
					<h2 class="box-title">{{ $travel->nama_travel }}</h2>
					
					<div class="feedback clearfix">
						<div title="4 stars" class="five-stars-container" data-toggle="tooltip" data-placement="bottom"><span class="five-stars" style="width: {{ $travel->rate_index }}%;"></span></div>
						<span class="review pull-right">270 reviews</span>
					</div>
					<p class="description">{{ $travel->hint }}</p>
					
				</div>
			</article>
			<div class="travelo-box book-with-us-box">
				<h4>Why Book with us?</h4>
				<ul>
					<li>
						<i class="soap-icon-hotel-1 circle"></i>
						<h5 class="title"><a href="#">135,00+ Hotels</a></h5>
						<p>Nunc cursus libero pur congue arut nimspnty.</p>
					</li>
					<li>
						<i class="soap-icon-savings circle"></i>
						<h5 class="title"><a href="#">Low Rates &amp; Savings</a></h5>
						<p>Nunc cursus libero pur congue arut nimspnty.</p>
					</li>
					<li>
						<i class="soap-icon-support circle"></i>
						<h5 class="title"><a href="#">Excellent Support</a></h5>
						<p>Nunc cursus libero pur congue arut nimspnty.</p>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>


@stop