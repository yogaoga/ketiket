@extends('Master.template')

@section('meta')
<script type="text/javascript" src="{{ asset('/vendor/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('/vendor/bootstrap-tagsinput/bootstrap-tagsinput.min.css') }}">
<script type="text/javascript">
	tjq(function(){
		tjq('[data-role="tagsinput"]').tagsinput('items');
	});
</script>
<style type="text/css">
	.bootstrap-tagsinput{
		margin: 0 !important;
		border-color: #ddd;
		background: #f5f5f5;
	}
</style>
@stop
@section('header')
<div class="page-title-container">
	<div class="container">
		<div class="page-title pull-left">
			<h2 class="entry-title">Booked</h2>
		</div>
		<ul class="breadcrumbs pull-right">
			<li><a href="#">HOME</a></li>
			<li><a href="#">Pages</a></li>
			<li class="active">booked</li>
		</ul>
	</div>
</div>
@stop

@section('content')
<div class="container">
	<div id="main">
		<div class="row">
			<div id="main" class="col-sm-8 col-md-9">
				<div class="tab-container full-width-style arrow-left dashboard">
					<div class="tab-content">
						<div id="dashboard" class="tab-pane fade in active">
							<ul class="wizard">
								<li>1. Cari Rute</li>
								<li>2. Daftar Rute</li>
								<li>3. Pilih Kursi</li>
								<li>4. Kode Booking</li>
								<li class="active">5. Selesai</li>
							</ul>
							<hr />
							<!-- start content -->
							<form method="post" action="{{ url('/agen/finish') }}">
								<center>
									<h1>Terimakasih telah melakukan Pemesanan</h1>
									<p>
										<span>Kode Booking</span>
										<h1>{{ $reservasi->kode_booking }}</h1>
									</p>
									<p>
										Kirim kode Booking ke Email
										<div class="row">
											<div class="col-sm-6 col-sm-offset-3 text-center">
												<input type="text" name="email" data-role="tagsinput" class="input-text email" placeholder="Masukan beberapa Alamat email">
												<small>* Pisahkan dengan menekan Enter</small>
											</div>
										</div>
									</p>
									<p>
										<a class="button btn-medium green" href="{{ url('/agen/route') }}"><i class="glyphicon glyphicon-circle-arrow-left"></i> Kembali</a>
										<button type="submit"><i class="fa fa-envelope"></i> Kirim Email</button>
										<a class="button btn-medium green" href="#"><i class="fa fa-print"></i> Print</a>
										<input type="hidden" value="{{ csrf_token() }}" name="_token">
										<input type="hidden" value="{{ $reservasi->id }}" name="id">
									</p>
								</center>
							</form>
							<!-- End start content -->

						</div>
					</div>
				</div>
			</div>
			<div class="sidebar col-sm-4 col-md-3">
				<div class="travelo-box contact-box">
					<h4>Need Ketiket Help?</h4>
					<p>We would be more than happy to help you. Our team advisor are 24/7 at your service to help you.</p>
					<address class="contact-details">
						<span class="contact-phone"><i class="soap-icon-phone"></i> 1-800-123-HELLO</span>
						<br>
						<a class="contact-email" href="#">help@ketiket.com</a>
					</address>
				</div>
				<div class="travelo-box book-with-us-box">
					<h4>Why Book with us?</h4>
					<ul>
						<li>
							<i class="soap-icon-hotel-1 circle"></i>
							<h5 class="title"><a href="#">135,00+ Hotels</a></h5>
							<p>Nunc cursus libero pur congue arut nimspnty.</p>
						</li>
						<li>
							<i class="soap-icon-savings circle"></i>
							<h5 class="title"><a href="#">Low Rates &amp; Savings</a></h5>
							<p>Nunc cursus libero pur congue arut nimspnty.</p>
						</li>
						<li>
							<i class="soap-icon-support circle"></i>
							<h5 class="title"><a href="#">Excellent Support</a></h5>
							<p>Nunc cursus libero pur congue arut nimspnty.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
@stop