
<div class="main-header">
    
    <a href="#mobile-menu-01" data-toggle="collapse" class="mobile-menu-toggle">
        Mobile Menu Toggle
    </a>

    <div class="container">
        <h1 class="logo navbar-brand">
            <a href="{{ url('/') }}" title="Ketiket - home">
                <img src="{{ asset('/images/logo.png') }}" alt="Ketiket" />
            </a>
        </h1>

        <nav id="main-menu" role="navigation">
                {!! Menu::MainMenu() !!}
        </nav>
    </div>
    
    <nav id="mobile-menu-01" class="mobile-menu collapse">
        {!! Menu::MainMenu('mobile-primary-menu') !!}
        <ul class="mobile-topnav container">
             @if(Auth::check())
            <li><a href="#">MY ACCOUNT</a></li>
            @endif
             @if(Auth::guest())
            <li><a href="#travelo-login" class="soap-popupbox">LOGIN</a></li>
            <li><a href="#travelo-signup" class="soap-popupbox">SIGNUP</a></li>
            @else
            <li><a href="{{ url('/auth/logout') }}">LOGOUT</a></li>
            @endif
        </ul>
        
    </nav>
</div>