@extends('Master.template')

@section('content')
	<div class="container">
		<div class="travelo-box">
			<h1 class="no-margin skin-color">{{ $judul }}</h1>
			<hr />
			{!! $result !!}
		</div>
	</div>
@stop