@extends('Master.template')

@section('meta')
	<link rel="stylesheet" type="text/css" href="{{ asset('/vendor/bootstrap-wysihtml5/css/bootstrap-wysihtml5.css') }}">
	<script type="text/javascript" src="{{ asset('/vendor/bootstrap-wysihtml5/js/wysihtml5-0.3.0.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/vendor/bootstrap-wysihtml5/js/bootstrap-wysihtml5.js') }}"></script>
	<script>
	    tjq(function(){
	        tjq('textarea').wysihtml5({
	            stylesheets: ['dist/assets/plugins/bootstrap-wysihtml5/css/wysiwyg-color.css']
	        });
	        //tjq('.wysihtml5-toolbar .btn-default').removeClass('btn-default').addClass('btn-white');
	    });
	</script>
@stop

@section('content')
	<div class="container">
		<form method="post" action="{{ url('/pages') }}">
		<input type="hidden" value="{{ csrf_token() }}" name="_token">
			<div class="tab-container box">
	            <ul class="tabs">
	                <li class="active"><a href="#satisfied-customers" data-toggle="tab">About Me</a></li>
	                <li><a href="#tours-suggestions" data-toggle="tab">Privacy Policy</a></li>
	                <li><a href="#careers" data-toggle="tab">Disclaimer</a></li>
	                <li><a href="#more" data-toggle="tab">Informasi</a></li>
	            </ul>
	            <div class="tab-content">
	                <div class="tab-pane fade in active" id="satisfied-customers">
	                   <!-- About Me -->
	                   <textarea id="wysiwyg" name="about_me" class="form-control" placeholder="Enter message ..." rows="25">{{ $data->about_me }}</textarea>
	                </div>
	                <div class="tab-pane fade" id="tours-suggestions">
	                 	<!-- Privacy Policy -->
	                 	<textarea id="wysiwyg" name="privacy_policy" class="form-control" placeholder="Enter message ..." rows="25">{{ $data->privacy_policy }}</textarea>
	                </div>
	                <div class="tab-pane fade" id="careers">
	                    <!-- Disclaimer -->
	                    <textarea id="wysiwyg" class="form-control" name="disclaimer" placeholder="Enter message ..." rows="25">{{ $data->disclaimer }}</textarea>
	                </div>
	                <div class="tab-pane fade" id="more">
	                	<div class="row">
	                		<div class="col-sm-6">
	                			<div class="form-group">
				                	<label>Singkat Ketiket</label>
				                	<input type="text" name="about_front" class="form-control" value="{{ $data->about_front }}" maxlength="255">
				                	<small class="text-muted">*Max 255 Karakter</small>
				                </div>
				                <div class="form-group">
				                	<label>Email</label>
				                	<input type="email" name="email_cs" class="form-control" value="{{ $data->email_cs }}" maxlength="255">
				                </div>
				                <div class="form-group">
				                	<label>Telpon</label>
				                	<input type="text" name="telp_cs" class="form-control" value="{{ $data->telp_cs }}" maxlength="255">
				                </div>
	                		</div>

	                		<div class="col-sm-6">
	                			<div class="form-group">
				                	<label>Facebook</label>
				                	<input type="text" name="facebook" class="form-control" value="{{ $data->facebook }}" maxlength="255">
				                	<small>&nbsp;</small>
				                </div>
				                <div class="form-group">
				                	<label>Google+</label>
				                	<input type="text" name="googleplus" class="form-control" value="{{ $data->googleplus }}" maxlength="255">
				                </div>
				                <div class="form-group">
				                	<label>Twitter</label>
				                	<input type="text" name="twitter" class="form-control" value="{{ $data->twitter }}" maxlength="255">
				                </div>
	                		</div>

	                	</div>
	                </div>
	            </div>
	        </div>

	        <div class="travelo-box">
	        	<button type="submit">Simpan Perubahan</button>
	        </div>
	    </form>
	</div>
@stop