@extends('Master.template')

@section('meta')
	<script type="text/javascript">
		tjq(function(){
			tjq('#addUser').submit(function(v){
				v.preventDefault();
				var email = tjq('[name="adduser"]').val();
				tjq('#kirim').button('loading');
				tjq.ajax({
					type : 'POST',
					url : _base_url + '/ajax/addusertrvel',
					data : {email : email},
					cache : false,
					dataType : 'json',
					success : function(res){
						if(res.result == true){
							tjq('.info').html('<div class="alert alert-info" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button>Berhasil terkirim</div>');
							tjq('[name="adduser"]').val('');
						}else{
							tjq('.info').html('<div class="alert alert-info" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button>' + res.err + '</div>');
						}
						tjq('#kirim').button('reset');
					}
				});
				
			});

			del = function(id){
				var c = confirm('Anda yaking ingin menghapus user ini ?');
				if(c == true){
					tjq('.box_' + id).css('opacity', '0.3');
					tjq.ajax({
						type : 'POST',
						url : _base_url + '/ajax/deladdusertrvel',
						data : {id : id},
						cache : false,
						dataType : 'json',
						success : function(res){
							if(res.result == true){
								tjq('.box_' + id).remove();
							}
						}
					});
				}
			}
		});
	</script>
@stop

@section('header')
	{!! Menu::titlePage('glyphicon glyphicon-user','Users ' . Travel::data()->nama_travel) !!}
@stop

@section('content')
	<div class="container">
		<div id="main">
	        <div class="tab-container full-width-style arrow-left dashboard">
	            @include('Otojasa.Sidebar')
	            <div class="tab-content">
	                <div id="dashboard" class="tab-pane fade in active">
	                	@if(Travel::data()->status > 0)
	                	<button class="pull-right" data-toggle="modal" data-target="#user">Tambah Operator</button>
	                	@endif
	                	 <h1 class="no-margin skin-color">{{ $users->total() }} hasil ditemukan</h1>
	                	 <hr />
	                	 
	                    <div class="car-list listing-style3 car">
	                    	@foreach($users as $user)
	                            <article class="box box_{{ $user->id_relasi }}">
	                                <figure class="col-xs-3">
	                                    <span><img src="{{ env('IMG') }}/avatar/thumb/{{ $user->foto }}"></span>
	                                </figure>
	                                <div class="details col-xs-9 clearfix">
	                                    <div class="col-sm-8">
	                                        <div class="clearfix">
	                                            <h4 class="box-title">{{ $user->name }} <small>{!! $user->status == 0 ? '<i class="fa fa-times-circle text-danger"></i> user belum aktifasi email' : '' !!}</small></h4>
	                                            <hr />
	                                        </div>
	                                        <div>
	                                        	<div class="row">
	                                        		<div class="col-sm-6">
		                                        		<strong>Telpon</strong>
		                                        		<p>{{ $user->hp }}</p>
	                                        		</div>
	                                        		<div class="col-sm-6">
		                                        		<strong>Email</strong>
		                                        		<p>{{ $user->email }}</p>
	                                        		</div>
	                                        	</div>
	                                        	<div>{{ trim($user->alamat) }}</div>
	                                        </div>
	                                    </div>
	                                    <div class="action col-xs-6 col-sm-2">
	                                        <span class="price"><small>Jabatan</small>{{ $user->level == 1 ? 'Admin' : 'Operator' }}</span>
	                                        @if($user->id != Auth::user()->id)
	                                        <a href="javascript:;" onclick="del({{ $user->id_relasi }})" class="button btn-small full-width">Hapus</a>
	                                        @endif
	                                    </div>
	                                </div>
	                            </article>
                        	@endforeach
                         </div>

                         <div class="text-right">
                         	{!! $users->render() !!}
                         </div>

	                </div>
	            </div>
	        </div>
	    </div>
	</div>
@stop

@section('footer')
	<!-- Modal -->
	<div class="modal fade" id="user" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	    		<form method="post" action="" id="addUser">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		        		<h4 class="modal-title" id="myModalLabel"><i class="glyphicon glyphicon-user"></i> Tambah Operator</h4>
		      		</div>
		      		<div class="modal-body">
		        		<div class="info"></div>
			      		<div class="form-group">
			      			<input type="email" class="input-text full-width" name="adduser" placeholder="Masukan Email" required>
			      		</div>
			      		<p>
			      			+ Masukan email yang ingin anda undang untuk menjadi Operator
			      		</p>

		      		</div>
			      	<div class="modal-footer">
			        	<button type="button" class="btn btn-link pull-left" data-dismiss="modal">Batal</button>
			        	<button type="submit" id="kirim" data-loading-text="Mengirim...">Kirim</button>
			      	</div>
	      		</form>
	    	</div>
	  	</div>
	</div>
@stop