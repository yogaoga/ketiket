@extends('Master.template')

@section('meta')
	<script type="text/javascript">
	tjq(function(){
		tjq('#active-otojasa').submit(function(){
			var c = confirm('Anda yakin akan mengaktifkan Travel ini ?');
			if(c == true){
				return true;
			}else{
				return false;
			}
		});
	});
	</script>
@stop

@section('header')
	{!! Menu::titlePage('', 'Otojasa Baru') !!}
@stop

@section('content')
	<div class="container">
		<div id="main">
            <div class="row">
                <div class="col-sm-8 col-md-9">
                    <div class="sort-by-section clearfix">
                        <ul class="sort-bar clearfix block-sm pull-right">
                            <li class="sort-by-name">
                            	<form method="get" action="">
                            		<input name="src" class="input-text" placeholder="Cari Travel..." /> <button><i class="fa fa-search"></i></button>
                            	</form>
                            </li>
                        </ul>
                        <h4 class="search-results-title"><b>{{ $items->total() }}</b> hasil ditemukan</h4>
                        
                    </div>
                    <div class="car-list">
                        <div class="row image-box car listing-style1">
                        	@forelse($items as $item)
                            <div class="col-sms-6 col-sm-6 col-md-4">
                                <article class="box">
                                    <figure>
                                        <a href="{{ url($item->slug) }}"><img alt="" src="{{ env('IMG') }}/travel/avatar/{{ $item->logo }}"></a>
                                    </figure>
                                    <div class="details">
                                        <h4 class="box-title">
                                            <a href="{{ url($item->slug) }}">
                                                {{ $item->nama_travel }}
                                            </a>
                                            <small><i class="fa fa-map-marker"></i> {{ $item->nm_provinsi }}</small>
                                        </h4>
                                        <div class="amenities" style="text-transform:none;">
                                        	<div class="text-left"><i class="glyphicon glyphicon-user"></i>
                                                {{ $item->name }}
                                            </div>
                                            <div class="text-left"><i class="glyphicon glyphicon-earphone"></i> {{ $item->no_tlp }}</div>
                                            <div class="text-left">{{ $item->alamat }}</div>
                                        </div>
                                        <p class="mile"><span class="skin-color">Tanggal :</span> {{ date('d F Y', strtotime($item->created_at)) }}</p>
                                        <div class="action">
                                        	<form method="post" action="{{ url('/otojasa/active') }}" id="active-otojasa">
                                        		<button type="submit" class="button btn-small full-width">Aktifkan</button>
                                        		<input type="hidden" value="{{ $item->id }}" name="id">
                                        		<input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        	</form>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            @empty

                            @endforelse
                        </div>
                    </div>
                    
                    <div class="text-right">
                    	{!! $items->appends(['src' => $src])->render() !!}
                    </div>

                </div>
            </div>
        </div>
	</div>
@stop