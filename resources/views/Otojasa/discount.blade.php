@extends('Master.template')

@section('meta')
    <script type="text/javascript">
        tjq(function(){
            getrutes = function(offset, src){
                var _src = src.length > 0 ? src : tjq('[name="route_src"]').val();
                tjq('.rute-content').css('opacity', '.3');
                tjq.ajax({
                    type : 'POST',
                    data : {src : _src},
                    url : _base_url + '/ajax/discountroutes?page=' + offset,
                    cache : false,
                    dataType : 'json',
                    success : function(res){
                        tjq('.rute-content').html(res.data);
                        tjq('.rute-footer').html(res.paginate);
                        tjq('.rute-content').css('opacity', '1');

                        /*--*/
                        tjq('.rute-footer > ul.pagination > li > a').click(function(e){
                            e.preventDefault();
                            var param = tjq(this).attr('href');
                            var pre_offset = param.split('?page=');
                            var _offset = pre_offset[1];
                            getrutes(_offset, '');
                        });
                    }
                });
            }

            tjq('[name="route_src"]').keyup(function(){
                var val = tjq(this).val();
                getrutes(1, val);
            });

            del = function(id){
                var c = confirm('Anda yakin ingin menghapusnya ?');
                if(c == true){
                    tjq('.box_' + id).css('opacity', '.3');
                    tjq.ajax({
                        type : 'POST',
                        data : {id : id},
                        url : _base_url + '/ajax/deldiscount',
                        cache : false,
                        dataType : 'json',
                        success : function(res){
                            tjq('.box_' + id).fadeOut('slow', function(){
                                tjq(this).remove();
                            });
                        }
                    });
                }
            }
        });
    </script>
    <style type="text/css">
        .rute-footer ul.pagination{
            margin: 0;
        }
    </style>
@stop

@section('header')
    {!! Menu::titlePage('glyphicon glyphicon-barcode','Diskon ' . Travel::data()->nama_travel) !!}
@stop

@section('content')
    <div class="container">
        <div id="main">
            <div class="tab-container full-width-style arrow-left dashboard">
                @include('Otojasa.Sidebar')
                <div class="tab-content">
                    <div id="dashboard" class="tab-pane fade in active">
                         <div class="pull-right">
                            <form method="get" action="{{ url('/otojasa/discount') }}">
                            <input type="text" name="src" value="{{ $src }}" class="input-text" placeholder="Cari Kode">
                            <input type="submit" class="hide">
                            <a href="#" data-toggle="modal" data-target="#myModal" class="button btn-medium green" onclick="getrutes(1, '');"><i class="fa fa-plus"></i> Tambah Diskon</a>
                            </form>
                        </div>

                         <h1 class="no-margin skin-color">{{ $rutes->total() }} hasil ditemukan</h1>
                         <hr />
                        <!-- star content -->
                        <div class="flight-list listing-style3 flight">
                            @forelse($rutes as $rute)
                            <article class="box box_{{ $rute->id }}" style="margin:10px 0;">
                                <div class="details col-xs-9 col-sm-12">
                                    <div class="details-wrapper">
                                        <div class="first-row">
                                            <div>
                                                <h4 class="box-title">
                                                    {{ $rute->nm_depart }} <i class="soap-icon-arrow-right"></i> {{ $rute->nm_destination }}
                                                    <small>Kode : {{ $rute->kode_routes }}</small>
                                                </h4>
                                                <a href="javascript:;" style="opacity:0;" class="button btn-mini stop">Detail</a>
                                                <div class="amenities">
                                                    
                                                    <a href="{{ url('/otojasa/creatediscount/' . $rute->routes_id . '#' . $rute->route_detail_id . '-' . $rute->kode_routes ) }}" data-toggle="tooltip" data-placement="bottom" title="Perbaharui diskon"><i class="glyphicon glyphicon-pencil circle"></i></a>
                                                    <a href="javascript:;" onclick="del({{ $rute->id }});" data-toggle="tooltip" data-placement="bottom" title="Hapus"><i class="glyphicon glyphicon-trash circle"></i></a>
                                                
                                                </div>
                                            </div>
                                            <div>
                                                <span class="price"><small>Harga Jual</small>Rp {{ number_format($rute->harga - (($rute->harga * $rute->persen) / 100),0,',','.') }}</span>
                                            </div>
                                        </div>

                                        <div class="second-row">
                                            <div class="time">
                                                <div class="col-sm-4">
                                                    <div class="icon"><i class="fa fa-check yellow-color"></i></div>
                                                    <div>
                                                        <span class="skin-color">Aktif</span><br />{{ Format::hari($rute->tgl_aktif) }}, {{ Format::indoDate($rute->tgl_aktif) }}
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="icon"><i class="fa fa-times yellow-color"></i></div>
                                                    <div>
                                                        <span class="skin-color">Non Aktif</span><br />{{ Format::hari($rute->tgl_limit) }}, {{ Format::indoDate($rute->tgl_limit) }}
                                                    </div>
                                                </div>
                                                <div class="total-time col-sm-4">
                                                    <div class="icon"><i class="soap-icon-features yellow-color"></i></div>
                                                    <div>
                                                        <span class="skin-color">Harga Total</span><br />Rp {{ number_format($rute->harga,0,',','.') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="action" style="width:20%;">
                                                <div>
                                                    <span class="price"><small>Diskon</small> {{ (INT) $rute->persen }} %</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </article>
                            @empty
                            <hr />
                            <article>
                            <div class="well">
                                Dikon Tidak ditemukan ?
                            </div>
                            </article>
                            @endforelse
                        </div>
                        <!-- end content -->

                        <div class="text-right">
                            {!! $rutes->appends([
                                'src' => $src
                            ])->render() !!}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('footer')
    <!-- Daftar Rute -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Daftar Rute</h4>
            </div>
            <div class="modal-body">
                <div class="route-src text-right">
                    <input type="text" name="route_src" placeholder="Cari Kode" class="input-text">
                </div>
                <div class="rute-content flight-list listing-style3 flight"><i class="fa fa-times fa-spin"></i> Memuat Rute...</div>
                <div class="rute-footer"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Keluar</button>
              </div>
            </div>
        </div>
    </div>
@stop