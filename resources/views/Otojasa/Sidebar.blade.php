<ul class="tabs">
    <li><a href="{{ url('/otojasa/dashboard') }}"><i class="fa fa-dashboard circle"></i>Dashboard</a></li>

    @if(Travel::data()->status > 0)

	    <li><a href="{{ url('/otojasa/route') }}"><i class="fa fa-road circle"></i>Rute</a></li>
	    <li><a href="{{ url('/otojasa/discount') }}"><i class="fa fa-tags circle"></i>Diskon Agen</a></li>
	    <li><a href="#"><i class="fa fa-briefcase circle"></i>Booking</a></li>
            <li><a href="{{ url('/car-rent') }}"><i class="fa soap-icon-car"></i>Rental Mobil</a></li>
	    @if(Travel::data()->level == 1)
	    <li><a href="{{ url('/otojasa/users') }}"><i class="fa fa-users circle"></i>Users</a></li>
	    <li><a href="{{ url('/otojasa/settings') }}"><i class="fa fa-cog circle"></i>Setting</a></li>
	    @endif
    
    @endif
</ul>