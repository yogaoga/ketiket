@extends('Master.template')

@section('meta')
	<link rel="stylesheet" type="text/css" href="{{ asset('/vendor/crop/cropper.min.css') }}">
	<script type="text/javascript" src="{{ asset('/vendor/crop/cropper.min.js') }}"></script>
	<style type="text/css">
		.img-container{
			width: 100%;
			height: 350px;
			border: solid 1px #eee;
		}
		.btn-file{
			position: relative;
			overflow: hidden;
			margin-top: 5px;
			background: #0078c7;
			padding: 5px;
			cursor: pointer;
			color: #fff;
		}
		.btn-file:hover{
			opacity: .9;
		}
		.btn-file input{
			position: absolute;
			top: 0;
			right: 0;
			font-size: 300pt;
			opacity: 0;
			cursor: pointer;
		}
	</style>
	<script type="text/javascript">
		tjq(function(){
			tjq('.top-warning').remove();
		});
	</script>
@stop

@section('header')
	{!! Menu::titlePage('glyphicon glyphicon-user', 'Pembaharuan Akun') !!}
@stop

@section('content')
	
	<div class="container">
		<div class="row">
			<div class="col-sm-3">
				<div class="travelo-box book-with-us-box">
					<div>
						<img src="{{ env('IMG') . '/travel/avatar/' . Travel::data()->logo }}" class="img-responsive">	
					</div>
					
					<form method="post" action="{{ url('/otojasa/logo') }}"  enctype="multipart/form-data">
						<div class="btn-file">
				      		<center><i class="glyphicon glyphicon-camera"></i> Pilih Foto</center>
				      		<input type="file" name="logo" id="input-avatar" onchange="submit();">
				      		<input type="hidden" value="{{ csrf_token() }}" name="_token">
				      	</div>
					</form>
				</div>
			</div>
			<div class="col-sm-9">
				<div class="travelo-box book-with-us-box">
					<form class="form-horizontal" role="form" method="post" action="{{ url('/otojasa/settings') }}">
						<input type="hidden" value="{{ csrf_token() }}" name="_token">
					  	<div class="form-group">
					    	<label for="nama" class="col-sm-2 control-label">Nama Travel</label>
					    	<div class="col-sm-10">
					      		<input type="text" class="input-text full-width" name="nama_travel" id="nama" required value="{{ Travel::data()->nama_travel }}">
					    	</div>
					  	</div>
					  	<div class="form-group">
					    	<label for="email" class="col-sm-2 control-label">TItle</label>
					    	<div class="col-sm-10">
					      		<input type="text" class="input-text full-width" id="email" name="hint" value="{{ Travel::data()->hint }}">
					    	</div>
					  	</div>
					  	<div class="form-group">
					    	<label for="hp" class="col-sm-2 control-label">Deskripsi</label>
					    	<div class="col-sm-10">
					      		<input type="text" class="input-text full-width" name="information" id="hp" required value="{{ Travel::data()->information }}">
					    	</div>
					  	</div>
					  	<div class="form-group">
					    	<label for="alamat" class="col-sm-2 control-label">Alamat</label>
					    	<div class="col-sm-10">
					    		<textarea class="input-text full-width" rows="5" name="alamat" id="alamat" required>{{ Travel::data()->alamat }}</textarea>
					    	</div>
					  	</div>

					  	<div class="form-group">
					    	<label for="hp" class="col-sm-2 control-label">Provinsi</label>
					    	<div class="col-sm-10">
					      		<div class="selector">
						      		<select name="provinsi">
						      			@foreach($provinsi as $prov)
						      				<option value="{{ $prov->id }}" {{ Travel::data()->provinsi_id == $prov->id ? 'selected="selected"' : '' }}>{{ $prov->nm_provinsi }}</option>
						      			@endforeach
						      		</select>
					      		</div>
					    	</div>
					  	</div>

					  	<hr />
					  	<div class="form-group">
					    	<label for="bank" class="col-sm-2 control-label">Tlp</label>
					    	<div class="col-sm-10">
					      		<input type="text" class="input-text full-width" name="no_tlp" id="bank" required value="{{ Travel::data()->no_tlp }}">
					    	</div>
					  	</div>
					  	<div class="form-group">
					    	<label for="bank" class="col-sm-2 control-label">Website</label>
					    	<div class="col-sm-10">
					      		<input type="text" class="input-text full-width" name="website" id="cabang" required value="{{ Travel::data()->website }}">
					    	</div>
					  	</div>
					  	<div class="form-group">
					    	<label for="rekening" class="col-sm-2 control-label"></label>
					    	<div class="col-sm-10">
					      		<button class="button btn-large sky-blue1">Simpan Perubahan</button>
					    	</div>
					  	</div>
					 </form>
				</div>
			</div>
		</div>
	</div>

@stop

@section('footer')
	<!-- Modal -->
	<div class="modal fade" id="avatar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	        <h4 class="modal-title" id="myModalLabel">Avatar</h4>
	      </div>
	      <div class="modal-body">
	        
	        <!-- review gambar -->
	      	<div class="img-container"><img id="image-review" /></div>

	      	<!-- Tombol upload -->
	      	

	      </div>
	      <div class="modal-footer">
	        <a class="btn btn-default pull-left btn-link" href="{{ url('/user/edit') }}">Batal</a>
	        <button type="button" class="btn btn-primary">Upload</button>
	      </div>
	    </div>
	  </div>
	</div>
@stop