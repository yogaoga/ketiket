@extends('Master.template')

@section('meta')
    <script type="text/javascript" src="{{ asset('/vendor/jquery-select2/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendor/bootstrap-timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendor/slimScroll/jquery.slimscroll.min.js') }}"></script>

    <script type="text/javascript">
    tjq(function(){
        //tjq('select.form-select2').select2();

        del = function(id){
            var c = confirm('Anda ingin menghapusnya ?');
            if(c == true){
                tjq('.box_' + id).css('opacity', '0.3');
                tjq.ajax({
                    type : 'POST',
                    url : _base_url + '/ajax/delrutedetail',
                    data : {id : id},
                    cache : false,
                    dataType : 'json',
                    success : function(res){
                        if(res.result == true){
                            tjq('.box_' + id).fadeOut('slow', function(){
                                tjq(this).remove();
                            });
                        }
                    }
                });
            }
        }

        edit = function(id){
            tjq('.edit-judul').html('Memuat...');
            tjq('.edit-content').html('Memuat...');
            tjq.ajax({
                type : 'POST',
                url : _base_url + '/ajax/editdetailrute',
                data : {id : id},
                cache : false,
                dataType : 'json',
                success : function(res){
                    if(res.result == true){
                        tjq('.edit-judul').html(res.judul);
                        tjq('.edit-content').html(res.content);
                    }else{
                        tjq('.edit-judul').html('Ops!');
                        tjq('.edit-content').html(res.err);
                    }
                }
            });
        }

        tjq('.boardingpoints').slimscroll({
            height : '100px'
        });

         addpools = function(){
            var htm = '\
                <div class="row">\
                    <button type="button" class="button btn-medium yellow" data-dismiss="alert"><i class="fa fa-times"></i></button>\
                    <div class="col-sm-8 col-sm-offset-1">\
                        <div class="form-group">\
                            <input type="text" class="input-text full-width" name="boardingpoint[]" placeholder="e.g Jl Trs Soekarno Hatta No. 111" required />\
                        </div>\
                    </div>\
                    <div class="col-sm-2">\
                        <input type="text" name="time[]" class="input-text full-width" data-rel="timepicker" data-show-meridian="false"/>\
                    </div>\
                </div>\
            ';
            tjq('.add-pools').append(htm);
            tjq('[data-rel="timepicker"]').timepicker();
         }

        delpool = function(id){
            var c = confirm('Anda yakin ingin menghapusnya ?');
            if(c == true){
                tjq('.pool-' + id).css('opacity', '.3');
                tjq.ajax({
                    type : 'POST',
                    url : _base_url + '/ajax/delpool',
                    data : {id : id},
                    cache : false,
                    dataType : 'json',
                    success : function(res){
                        if(res.result == true){
                           tjq('.pool-' + id).fadeOut('slow', function(){
                                tjq(this).remove();
                           });
                        }
                    }
                });
            }
         }

    });
    </script>

    <link rel="stylesheet" type="text/css" href="{{ asset('/vendor/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/vendor/bootstrap-timepicker/bootstrap-timepicker.min.css') }}">
@stop

@section('header')
    {!! Menu::titlePage('glyphicon glyphicon-road',' Pembaharuan Rute') !!}
@stop

@section('content')
    <div class="container">
        <div id="main">
            <div class="tab-container full-width-style arrow-left dashboard">
                @include('Otojasa.Sidebar')
                <div class="tab-content">
                    <div id="dashboard" class="tab-pane fade in active">
                        @if(Travel::data()->level == 1)
                        <div class="text-right">
                        <a href="#" data-toggle="modal" data-target="#myModal" class="button btn-medium green"><i class="fa fa-plus"></i> Tambah Rute</a>
                        <hr />
                        </div>
                        @endif
                        <!-- star content -->
                            <div class="cruise-list image-box style3 cruise listing-style1">
                                <div class="row">
                                @foreach($routes as $route)
                                    <div class="col-sms-6 col-sm-6 col-md-4 box_{{ $route->id }}">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                @if($route->depart.$route->destination != $route->master_depart.$route->master_destination)
                                                <a href="javascript:;" onclick="del({{ $route->id }});" class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Hapus"><i class="fa fa-times"></i></a>
                                                @endif
                                                <article class="box" style="height:200px;">
                                                    <div class="details">
                                                        <span class="price"><small>harga</small>{{ number_format($route->harga,0,',','.') }}</span>
                                                        <h4 class="box-title">{{ $route->nm_depart }} to {{ $route->nm_destination }}</h4>
                                                        <div class="feedback"></div>
                                                        <!-- <div class="row time">
                                                            <div class="date col-xs-6">
                                                                <i class="soap-icon-clock yellow-color"></i>
                                                                <div>
                                                                    <span class="skin-color">Waktu</span><br />
                                                                </div>
                                                            </div>
                                                            <div class="departure col-xs-6">
                                                                <i class="soap-icon-calendar yellow-color"></i>
                                                                <div>
                                                                    <span class="skin-color">Created at</span><br />
                                                                </div>
                                                            </div>
                                                        </div> -->
                                                        
                                                        <div class="boardingpoints">
                                                        @foreach($pools[$route->id] as $pool)
                                                            <div class="pool-{{ $pool->id }}">
                                                                <button type="button" class="" onclick="delpool({{ $pool->id }})"><span aria-hidden="true">&times;</span></button>
                                                                {{ date('h:i A', strtotime($pool->waktu)) }} | {{ $pool->lokasi }} 
                                                            </div>
                                                        @endforeach
                                                        </div>
                                                        
                                                        <div class="action">
                                                            <a class="button btn-small full-width" onclick="edit({{ $route->id }});" href="#" data-toggle="modal" data-target="#edit">Perbaharui</a>
                                                        </div>
                                                    </div>
                                                </article>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                </div>
                            </div>
                        </div>
                        <!-- end content -->

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('footer')
    <!-- edit rute -->
    <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form method="post" class="form-horizontal" action="{{ url('/otojasa/editroutes') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title edit-judul" id="myModalLabel">Memuat...</h4>
                    </div>
                    <div class="modal-body edit-content">
                        Memuat...
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link pull-left" data-dismiss="modal">Batal</button>
                        <button type="submit" class="button btn-medium green btn-loading" data-loading-text="Progress...">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>



    <!-- add Rute -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <form method="post" action="{{ url('/otojasa/addrutekota') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id" value="{{ $id }}">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Kota Persinggahan & Tujuan</h4>
              </div>
              <div class="modal-body">
                <div class="row">
                    <div class="col-xs-6">
                        <label>Kota Persinggahan *</label>
                        <select name="depart" class="form-control form-select2" required>
                            <option value="">Pilih Lokasi</option>
                            @foreach($provinsi as $prov)
                                <optgroup label="{{ $prov->nm_provinsi }}">
                                    @foreach(App\Models\provinsi::find($prov->id)->kab_kota()->get() as $kab)
                                        <option value="{{ $kab->id }}">{{ $kab->nm_kab_kota }}</option>
                                    @endforeach
                                </optgroup>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-xs-6">
                        <label>Kota Tujuan *</label>
                        <select name="destination" class="form-control form-select2" required>
                            <option value="">Pilih Lokasi</option>
                            @foreach($provinsi as $prov)
                                <optgroup label="{{ $prov->nm_provinsi }}">
                                    @foreach(App\Models\provinsi::find($prov->id)->kab_kota()->get() as $kab)
                                        <option value="{{ $kab->id }}">{{ $kab->nm_kab_kota }}</option>
                                    @endforeach
                                </optgroup>
                            @endforeach
                        </select>
                    </div>
                </div>
                <hr />
                <div class="form-group">
                    <label for="harga">Harga</label>
                    <input type="text" class="input-text full-width text-right" id="harga" name="harga" required>
                </div>

                <button type="button" onclick="addpools();" class="button btn-mini sky-blue1 pull-right">Tambah Boarding Point</button>
                 <hr>
                <div class="add-pools"></div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-link pull-left" data-dismiss="modal">Batal</button>
                <button type="submit">Simpan</button>
              </div>
        </form>
    </div>
  </div>
</div>
@stop