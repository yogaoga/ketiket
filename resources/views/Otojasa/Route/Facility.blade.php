@extends('Master.template')

@section('meta')
    <link rel="stylesheet" type="text/css" href="{{ asset('/vendor/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/vendor/jquery-select2/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/vendor/bootstrap-tagsinput/bootstrap-tagsinput.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/vendor/bootstrap-timepicker/bootstrap-timepicker.min.css') }}">

    <script type="text/javascript" src="{{ asset('/vendor/jquery-select2/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendor/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendor/bootstrap-timepicker/bootstrap-timepicker.min.js') }}"></script>
    
    <script type="text/javascript" src="{{ asset('/js/create-route.js') }}"></script>
    <script type="text/javascript">
        tjq(function(){
            tjq('[name="facilitys[]"]').change(function(){
                var id = tjq(this).data('id');
                var box = tjq(this).prop('checked');
                if(box == true){
                    tjq('.img-' + id).toggleClass('img-active');
                }else{
                    tjq('.img-' + id).toggleClass('img-active');
                }
            });
        });
    </script>
@stop

@section('header')
    {!! Menu::titlePage('glyphicon glyphicon-road','Tambah Rute') !!}
@stop

@section('content')
    <div class="container">
        <div id="main">
            <div class="tab-container full-width-style arrow-left dashboard">
                @include('Otojasa.Sidebar')
                <div class="tab-content">
                    <div id="dashboard" class="tab-pane fade in active create-route">
                         
                        <ul class="wizard">
                             <li>1. Data Rute</li>
                             <li>2. Kota-kota Tujuan</li>
                             <li>3. Boarding Point</li>
                             <li>4. Harga</li>
                             <li class="active">5. Fasilitas</li>
                         </ul>
                         <hr />

                        <!-- star content -->
                       tahap develop...
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop