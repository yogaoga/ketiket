@extends('Master.template')

@section('meta')
    <script type="text/javascript" src="{{ asset('/vendor/jquery-select2/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendor/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendor/bootstrap-timepicker/bootstrap-timepicker.min.js') }}"></script>
    
    <script type="text/javascript" src="{{ asset('/js/create-route.js') }}"></script>

    <script type="text/javascript">
        tjq(function(){
            tjq('.add-rute').click(function(){
                var labels;
                var name = tjq(this).data('name');
                var target = tjq(this).data('target');

                var btn = tjq(this);

                if(target == '.depart')
                    labels = 'Kota yang dilewati';
                else
                    labels = 'Kota tujuan lain';

                btn.button('loading');
                tjq.ajax({
                    type : 'POST',
                    url : _base_url + '/ajax/getkabkota',
                    data : {name : name, label : labels},
                    cache : false,
                    dataType : 'json',
                    success : function(res){
                        tjq(target).append(res.data);

                        tjq('select.form-select2').select2();
                        btn.button('reset');
                    }
                });
            });
        });
    </script>

    <link rel="stylesheet" type="text/css" href="{{ asset('/vendor/jquery-select2/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/vendor/bootstrap-tagsinput/bootstrap-tagsinput.min.css') }}">
@stop

@section('header')
    {!! Menu::titlePage('glyphicon glyphicon-road','Tambah Rute') !!}
@stop

@section('content')
    <div class="container">
        <div id="main">
            <div class="tab-container full-width-style arrow-left dashboard">
                @include('Otojasa.Sidebar')
                <div class="tab-content">
                    <div id="dashboard" class="tab-pane fade in active create-route">
                         <ul class="wizard">
                             <li>1. Data Rute</li>
                             <li class="active">2. Kota-kota Tujuan</li>
                             <li>3. Boarding Point</li>
                             <li>4. Harga</li>
                             <li>5. Fasilitas</li>
                         </ul>
                         <hr />
                        <!-- star content -->
                       tahap develop..
                        <!-- end content -->

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop