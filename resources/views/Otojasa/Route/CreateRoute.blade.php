@extends('Master.template')

@section('meta')
    <script type="text/javascript" src="{{ asset('/vendor/jquery-select2/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendor/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendor/bootstrap-timepicker/bootstrap-timepicker.min.js') }}"></script>
    
    <script type="text/javascript" src="{{ asset('/js/create-route.js') }}"></script>

    <link rel="stylesheet" type="text/css" href="{{ asset('/vendor/jquery-select2/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/vendor/bootstrap-tagsinput/bootstrap-tagsinput.min.css') }}">
@stop

@section('header')
    {!! Menu::titlePage('glyphicon glyphicon-road','Tambah Rute') !!}
@stop

@section('content')
    <div class="container">
        <div id="main">
            <div class="tab-container full-width-style arrow-left dashboard">
                @include('Otojasa.Sidebar')
                <div class="tab-content">
                    <div id="dashboard" class="tab-pane fade in active create-route">
                         <ul class="wizard">
                             <li class="active">1. Data Rute</li>
                             <li>2. Kota-kota Tujuan</li>
                             <li>3. Boarding Point</li>
                             <li>4. Harga</li>
                             <li>5. Fasilitas</li>
                         </ul>
                         <hr />
                        <!-- star content -->
                         <form action="{{ url('/otojasa/routecity') }}" method="post" id="create_route_1">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="search-content">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <label>Kota Keberangkatan *</label>
                                        <select name="depart" class="form-control form-select2" required>
                                            <option value="">Pilih Lokasi</option>
                                            @foreach($provinsi as $prov)
                                                <optgroup label="{{ $prov->nm_provinsi }}">
                                                    @foreach(App\Models\provinsi::find($prov->id)->kab_kota()->get() as $kab)
                                                        <option value="{{ $kab->id }}">{{ $kab->nm_kab_kota }}</option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-xs-4">
                                        <label>Kota Tujuan *</label>
                                        <select name="destination" class="form-control form-select2" required>
                                            <option value="">Pilih Lokasi</option>
                                            @foreach($provinsi as $prov)
                                                <optgroup label="{{ $prov->nm_provinsi }}">
                                                    @foreach(App\Models\provinsi::find($prov->id)->kab_kota()->get() as $kab)
                                                        <option value="{{ $kab->id }}">{{ $kab->nm_kab_kota }}</option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-xs-6 col-md-4">
                                        <label>Tanggal Aktif *</label>
                                        <div class="datepicker-wrap">
                                            <input type="text" name="tgl_aktif" class="input-text full-width" value="{{ old('tgl_aktif') }}" placeholder="Tanggal Aktif" />
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-4">
                                        <label>Tanggal Non Aktif *</label>
                                        <div class="datepicker-wrap">
                                            <input type="text" name="tgl_limit" value="{{ old('tgl_limit') }}" class="input-text full-width" placeholder="Tanggal Non Aktif" />
                                        </div>
                                    </div>
                                </div>
                                <label>Hari Pelaksanaan *</label>
                                <div class="row choose-travel">
                                        <div class="col-xs-12">
                                            <label class="checkbox checkbox-inline">
                                                <input type="checkbox" name="day_route[]" value="1">Senin
                                            </label>
                                        
                                            <label class="checkbox checkbox-inline">
                                                <input type="checkbox" name="day_route[]" value="2">Selasa
                                            </label>
                                       
                                            <label class="checkbox checkbox-inline">
                                                <input type="checkbox" name="day_route[]" value="3">Rabu
                                            </label>
                                       
                                            <label class="checkbox checkbox-inline">
                                                <input type="checkbox" name="day_route[]" value="4">Kamis
                                            </label>
                                        
                                            <label class="checkbox checkbox-inline">
                                                <input type="checkbox" name="day_route[]" value="5">Jum'at
                                            </label>
                                        
                                            <label class="checkbox checkbox-inline">
                                                <input type="checkbox" name="day_route[]" value="6">Sabtu
                                            </label>
                                        
                                            <label class="checkbox checkbox-inline">
                                                <input type="checkbox" name="day_route[]" value="7">Minggu
                                            </label>
                                        </div>
                                    </div>
                                <hr>

                                <div class="row">
                                    <div class="col-xs-4 col-md-4">
                                        <label>Kode Rute *</label>
                                        <div class="input-group">
                                            <input type="text" class="input-text full-width" value="{{ old('kode_routes') }}" name="kode_routes" placeholder="" required/>
                                            <span class="input-group-btn">
                                                <a href="javascript:generate();" class="btn btn-default" data-toggle="tooltip" data-placement="right" title="Generate Kode"><i class="fa fa-refresh"></i></a>
                                            </span>
                                        </div>
                                    </div>
                                
                                    <div class="col-xs-4 col-md-4">
                                        <label>Kelas *</label>
                                        <input type="text" class="input-text full-width" placeholder="e.g Ekonomi" name="kelas" value="{{ old('kelas') }}" required/>
                                    </div>

                                    <div class="col-xs-4 col-md-4">
                                        <label>Jenis Kendaraan *</label>
                                        <div class="selector">
                                            <select name="template_mobile" required>
                                                <option value="">Pilih jenis Kendaraan</option>
                                                @foreach($mobils as $mobil)
                                                <option value="{{ $mobil->id }}">{{ $mobil->nm_templatemobil }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-4 col-md-12">
                                        <label>Keterangan</label>
                                        <textarea class="nput-text full-width" name="keterangan">{{ old('keterangan') }}</textarea>
                                    </div>
                                </div>
                                

                                <hr />
                                <button type="submit" class="btn-medium pull-right">Lanjut <i class="glyphicon glyphicon-circle-arrow-right"></i></button>
                                <a class="btn btn-link" href="{{ url('/otojasa/route') }}">Batal</a>
                            </div>
                        </form>
                        <!-- end content -->

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop