@extends('Master.template')

@section('meta')
    <link rel="stylesheet" type="text/css" href="{{ asset('/vendor/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/vendor/jquery-select2/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/vendor/bootstrap-tagsinput/bootstrap-tagsinput.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/vendor/bootstrap-timepicker/bootstrap-timepicker.min.css') }}">

    <script type="text/javascript" src="{{ asset('/vendor/jquery-select2/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendor/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendor/bootstrap-timepicker/bootstrap-timepicker.min.js') }}"></script>
    
    <script type="text/javascript" src="{{ asset('/js/create-route.js') }}"></script>
    <script type="text/javascript">
        tjq(function(){
            tjq('[name="facilitys[]"]').change(function(){
                var id = tjq(this).data('id');
                var box = tjq(this).prop('checked');
                if(box == true){
                    tjq('.img-' + id).toggleClass('img-active');
                }else{
                    tjq('.img-' + id).toggleClass('img-active');
                }
            });
        });
    </script>
@stop

@section('header')
    {!! Menu::titlePage('glyphicon glyphicon-road','Tambah Rute') !!}
@stop

@section('content')
    <div class="container">
        <div id="main">
            <div class="tab-container full-width-style arrow-left dashboard">
                @include('Otojasa.Sidebar')
                <div class="tab-content">
                    <div id="dashboard" class="tab-pane fade in active create-route">
                        <h3>Kode {{ $route->kode_routes }}</h3>
                        <hr />

                        <!-- star content -->
                        <form method="post" action="{{ url('/otojasa/editfacility') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id" value="{{ $route->id }}">
                            <div class="panel panel-default table-responsive">
                                <table class="table table-hover">
                                    @foreach($items as $item)
                                    @if($item->kode == '3P' || $item->kode == '2P' || $item->kode == '1P')
                                    <tr>
                                        <td width="3%">
                                            <label for="f_{{ $item->id }}" class="radio radio-inline">
                                                <input type="radio" name="facilitys[]" value="{{ $item->id }}" id="f_{{ $item->id }}" data-id="{{ $item->id }}"  required {{ in_array($item->id, $facilitys) ? 'checked' : '' }} />
                                            </label>
                                        </td>
                                        <td width="7%">
                                            <label for="f_{{ $item->id }}" style="cursor:pointer;">
                                                <img src="{{ env('IMG') }}/facility/{{ $item->logo }}" class="img-responsive">
                                            </label>
                                        </td>
                                        <td width="10%"><strong>{{ $item->kode }}</strong></td>
                                        <td width="20%">{{ $item->keterangan }}</td>
                                        <td width="60%">{{ $item->catatan }}</td>
                                    </tr>
                                    @endif
                                    @endforeach
                                </table>    
                            </div>

                            <div class="panel panel-default table-responsive">
                                <table class="table table-hover">
                                    @foreach($items as $item)
                                    @if($item->kode != '3P' && $item->kode !='2P' && $item->kode != '1P')
                                    <tr>
                                        <td width="3%">
                                            <label for="f_{{ $item->id }}" class="checkbox checkbox-inline">
                                                <input type="checkbox" name="facilitys[]" value="{{ $item->id }}" id="f_{{ $item->id }}" data-id="{{ $item->id }}" {{ in_array($item->id, $facilitys) ? 'checked' : '' }} />
                                            </label>
                                        </td>
                                        <td width="7%">
                                            <label for="f_{{ $item->id }}" style="cursor:pointer;">
                                                <img src="{{ env('IMG') }}/facility/{{ $item->logo }}" class="img-responsive">
                                            </label>
                                        </td>
                                        <td width="10%"><strong>{{ $item->kode }}</strong></td>
                                        <td width="20%">{{ $item->keterangan }}</td>
                                        <td width="60%">{{ $item->catatan }}</td>
                                    </tr>
                                    @endif
                                    @endforeach
                                </table>    
                            </div>
                            <!-- end content -->

                             <hr />
                            <button type="submit" class="btn-medium pull-right">Perbaharui <i class="fa fa-check"></i></button>
                            <a class="btn btn-link" href="{{ url('/otojasa/route') }}">Batal</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop