@extends('Master.template')

@section('meta')
    <script type="text/javascript" src="{{ asset('/vendor/jquery-select2/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendor/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendor/bootstrap-timepicker/bootstrap-timepicker.min.js') }}"></script>
    
    <script type="text/javascript" src="{{ asset('/js/create-route.js') }}"></script>

    <script type="text/javascript">
        tjq(function(){
            tjq('.add-rute').click(function(){
                var name = tjq(this).data('name');
                var target = tjq(this).data('target');

                var btn = tjq(this);
                btn.button('loading');
                tjq.ajax({
                    type : 'POST',
                    url : _base_url + '/ajax/getkabkota',
                    data : {name : name, label : 'Kota yang dilewati'},
                    cache : false,
                    dataType : 'json',
                    success : function(res){
                        tjq(target).append(res.data);

                        tjq('select.form-select2').select2();
                        btn.button('reset');
                    }
                });
            });

            addpoint = function(id){
                var _t = tjq('.add-boardingpoint-' + id);
                var htm = '\
                    <div class="row">\
                        <button type="button" class="button btn-medium yellow" data-dismiss="alert"><i class="fa fa-times"></i></button>\
                        <div class="col-sm-8">\
                            <div class="form-group">\
                                <input type="text" class="input-text full-width" name="boardingpoint[' + id + '][]" required />\
                            </div>\
                        </div>\
                        <div class="col-sm-3">\
                            <input type="text" name="time[' + id + '][]" class="input-text full-width" data-rel="timepicker" data-show-meridian="false"/>\
                        </div>\
                    </div>\
                ';
                _t.append(htm);
                tjq('[data-rel="timepicker"]').timepicker();
            }

        });
    </script>

    <link rel="stylesheet" type="text/css" href="{{ asset('/vendor/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/vendor/jquery-select2/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/vendor/bootstrap-tagsinput/bootstrap-tagsinput.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/vendor/bootstrap-timepicker/bootstrap-timepicker.min.css') }}">
@stop

@section('header')
    {!! Menu::titlePage('glyphicon glyphicon-road','Tambah Rute') !!}
@stop

@section('content')
    <div class="container">
        <div id="main">
            <div class="tab-container full-width-style arrow-left dashboard">
                @include('Otojasa.Sidebar')
                <div class="tab-content">
                    <div id="dashboard" class="tab-pane fade in active create-route">
                         
                        <ul class="wizard">
                             <li>1. Data Rute</li>
                             <li>2. Kota-kota Tujuan</li>
                             <li class="active">3. Boarding Point</li>
                             <li>4. Harga</li>
                             <li>5. Fasilitas</li>
                         </ul>
                         <hr />
                        tahap develop..

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop