@extends('Master.template')

@section('meta')
    <script type="text/javascript" src="{{ asset('/vendor/jquery-select2/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendor/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendor/bootstrap-timepicker/bootstrap-timepicker.min.js') }}"></script>
    
    <script type="text/javascript" src="{{ asset('/js/create-route.js') }}"></script>

    <script type="text/javascript">
        tjq(function(){
            tjq('.add-rute').click(function(){
                var name = tjq(this).data('name');
                var target = tjq(this).data('target');

                var btn = tjq(this);
                btn.button('loading');
                tjq.ajax({
                    type : 'POST',
                    url : _base_url + '/ajax/getkabkota',
                    data : {name : name, label : 'Kota yang dilewati'},
                    cache : false,
                    dataType : 'json',
                    success : function(res){
                        tjq(target).append(res.data);

                        tjq('select.form-select2').select2();
                        btn.button('reset');
                    }
                });
            });

            tjq('[data-toggle="popover"]').popover({
                trigger: 'focus',
                html : true
            });
        });
    </script>

    <link rel="stylesheet" type="text/css" href="{{ asset('/vendor/jquery-select2/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/vendor/bootstrap-tagsinput/bootstrap-tagsinput.min.css') }}">
@stop

@section('header')
    {!! Menu::titlePage('glyphicon glyphicon-road','Tambah Rute') !!}
@stop

@section('content')
    <div class="container">
        <div id="main">
            <div class="tab-container full-width-style arrow-left dashboard">
                @include('Otojasa.Sidebar')
                <div class="tab-content">
                    <div id="dashboard" class="tab-pane fade in active create-route">
                         
                         <ul class="wizard">
                             <li>1. Data Rute</li>
                             <li>2. Kota-kota Tujuan</li>
                             <li>3. Boarding Point</li>
                             <li class="active">4. Harga</li>
                             <li>5. Fasilitas</li>
                         </ul>
                         <hr />

                        <!-- star content -->
                       tahap develop..
                        <!-- end content -->

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop