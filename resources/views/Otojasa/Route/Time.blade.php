@extends('Master.template')

@section('meta')
    <link rel="stylesheet" type="text/css" href="{{ asset('/vendor/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/vendor/jquery-select2/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/vendor/bootstrap-tagsinput/bootstrap-tagsinput.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/vendor/bootstrap-timepicker/bootstrap-timepicker.min.css') }}">

    <script type="text/javascript" src="{{ asset('/vendor/jquery-select2/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendor/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendor/bootstrap-timepicker/bootstrap-timepicker.min.js') }}"></script>
    
    <script type="text/javascript" src="{{ asset('/js/create-route.js') }}"></script>
   
@stop

@section('header')
    {!! Menu::titlePage('glyphicon glyphicon-road','Tambah Rute') !!}
@stop

@section('content')
    <div class="container">
        <div id="main">
            <div class="tab-container full-width-style arrow-left dashboard">
                @include('Otojasa.Sidebar')
                <div class="tab-content">
                    <div id="dashboard" class="tab-pane fade in active create-route">
                         
                        <!-- star content -->
                        <h3>Waktu Perkiraan Keberangkatan</h3>
                        <hr />

                        <form class="form-horizontal" role="form" method="post" action="{{ url('/otojasa/createroute') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            @foreach($data['departs'][0] as $from)

                            <div class="form-group">
                                <label for="{{ $from }}" class="col-sm-4 control-label">Waktu {{ App\Models\kab_kota::find($from)->nm_kab_kota }}</label>
                                <div class="col-sm-3">
                                    <div class="input-group bootstrap-timepicker">
                                        <input type="text" name="time[{{ $from }}]" class="input-text full-width" data-rel="timepicker" data-show-meridian="false"/>
                                    </div>
                                </div>
                            </div>

                            @endforeach

                            <hr />
                            <button type="submit" class="btn-medium pull-right">Simpan <i class="fa fa-check"></i></button>
                            <a class="btn btn-link" href="{{ url('/otojasa/route') }}">Batal</a>

                        </form>
                       
                        <!-- end content -->

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop