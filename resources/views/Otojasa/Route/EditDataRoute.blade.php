@extends('Master.template')

@section('meta')
    <script type="text/javascript" src="{{ asset('/vendor/jquery-select2/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendor/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendor/bootstrap-timepicker/bootstrap-timepicker.min.js') }}"></script>
    
    <script type="text/javascript" src="{{ asset('/js/create-route.js') }}"></script>

    <link rel="stylesheet" type="text/css" href="{{ asset('/vendor/jquery-select2/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/vendor/bootstrap-tagsinput/bootstrap-tagsinput.min.css') }}">
@stop

@section('header')
    {!! Menu::titlePage('glyphicon glyphicon-pencil',' Pembaharuan Rute') !!}
@stop

@section('content')
    <div class="container">
        <div id="main">
            <div class="tab-container full-width-style arrow-left dashboard">
                @include('Otojasa.Sidebar')
                <div class="tab-content">
                    <div id="dashboard" class="tab-pane fade in active">
                            
                        <!-- star content -->
                         <form action="{{ url('/otojasa/editdataroute') }}" method="post" id="create_route_1">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="title-container">
                                <h2 class="search-title">Seting Rute</h2>

                            </div>
                            <hr />
                            <div class="search-content">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <label>Kota Keberangkatan</label>
                                        <input type="text"  class="input-text full-width" value="{{ $route->nm_depart }}" readonly="readonly"  />
                                    </div>
                                    <div class="col-xs-4">
                                        <label>Kota Tujuan</label>
                                        <input type="text"  class="input-text full-width" value="{{ $route->nm_destination }}" readonly="readonly"  />
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-xs-6 col-md-4">
                                        <label>Tanggal Aktif *</label>
                                        <div class="datepicker-wrap">
                                            <input type="text" name="tgl_aktif" class="input-text full-width" value="{{ date('m/d/Y', strtotime($route->tgl_aktif)) }}" readonly="readonly" placeholder="Tanggal Aktif" />
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-4">
                                        <label>Tanggal Non Aktif *</label>
                                        <div class="datepicker-wrap">
                                            <input type="text" name="tgl_limit" value="{{ date('m/d/Y', strtotime($route->tgl_limit)) }}" class="input-text full-width" placeholder="Tanggal Non Aktif" readonly="readonly" />
                                        </div>
                                    </div>
                                </div>
                                <label>Hari Pelaksanaan *</label>
                                <div class="row choose-travel">
                                        <div class="col-xs-12">
                                            <label class="checkbox checkbox-inline">
                                                <input type="checkbox" name="day_route[]" value="1" {{ in_array(1, $hari) ? 'checked="checked"' : '' }}>Senin
                                            </label>
                                        
                                            <label class="checkbox checkbox-inline">
                                                <input type="checkbox" name="day_route[]" value="2" {{ in_array(2, $hari) ? 'checked="checked"' : '' }}>Selasa
                                            </label>
                                       
                                            <label class="checkbox checkbox-inline">
                                                <input type="checkbox" name="day_route[]" value="3" {{ in_array(3, $hari) ? 'checked="checked"' : '' }}>Rabu
                                            </label>
                                       
                                            <label class="checkbox checkbox-inline">
                                                <input type="checkbox" name="day_route[]" value="4" {{ in_array(4, $hari) ? 'checked="checked"' : '' }}>Kamis
                                            </label>
                                        
                                            <label class="checkbox checkbox-inline">
                                                <input type="checkbox" name="day_route[]" value="5" {{ in_array(5, $hari) ? 'checked="checked"' : '' }}>Jum'at
                                            </label>
                                        
                                            <label class="checkbox checkbox-inline">
                                                <input type="checkbox" name="day_route[]" value="6" {{ in_array(6, $hari) ? 'checked="checked"' : '' }}>Sabtu
                                            </label>
                                        
                                            <label class="checkbox checkbox-inline">
                                                <input type="checkbox" name="day_route[]" value="7" {{ in_array(7, $hari) ? 'checked="checked"' : '' }}>Minggu
                                            </label>
                                        </div>
                                    </div>
                                <hr>

                                <div class="row">
                                    <div class="col-xs-4 col-md-4">
                                        <label>Kode Rute *</label>
                                        <input type="text" class="input-text full-width" value="{{ $route->kode_routes }}" readonly="readonly" required/>
                                    </div>
                                
                                    <div class="col-xs-4 col-md-4">
                                        <label>Kelas *</label>
                                        <input type="text" class="input-text full-width" placeholder="e.g Ekonomi" name="kelas" value="{{ $route->kelas }}" required/>
                                    </div>

                                    <div class="col-xs-4 col-md-4">
                                        <label>Jenis Kendaraan *</label>
                                        <div class="selector">
                                            <select name="template_mobile" required>
                                                <option value="">Pilih jenis Kendaraan</option>
                                                @foreach($mobils as $mobil)
                                                <option value="{{ $mobil->id }}" {{ $mobil->id == $route->template_mobile ? 'selected="selected"' : '' }}>{{ $mobil->nm_templatemobil }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-4 col-md-12">
                                        <label>Keterangan</label>
                                        <textarea rows="7" class="nput-text full-width" name="keterangan">{{ $route->keterangan }}</textarea>
                                    </div>
                                </div>
                                

                                <hr />
                                <button type="submit" class="btn-medium pull-right">Simpan <i class="fa fa-check"></i></button>
                                <a class="btn btn-link" href="{{ url('/otojasa/route') }}">Batal</a>
                                <input type="hidden" value="{{ $route->id }}" name="id">
                            </div>
                        </form>
                        <!-- end content -->

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop