@extends('Master.template')

@section('header')
	{!! Menu::titlePage('', 'Kode ' . $route->kode_routes . ' ' . $status ) !!}
@stop

@section('content')
	<div class="container">
		<section id="content" class="gray-area">
            <div class="container car-detail-page">
                <div class="row">
                    <div id="main" class="col-md-9">
                        <div class="tab-container">
                            @if(count($facilitys) > 0)
                            <ul class="tabs">
                                <li class="active">
                                    <a href="#car-details" data-toggle="tab">Rute Detail</a>
                                </li>
                                <li>
                                    <a href="#car-upgrade" data-toggle="tab">Pasilitas</a>
                                </li>
                            </ul>
                            @endif
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="car-details">
                                     @if(Travel::check() && Travel::data()->id == $travel->id && Travel::data()->level == 1)
                                        <a href="{{ url('otojasa/editdataroute/' . $route->id ) }}" class="button btn-small green pull-right">Perbaharui Data</a>
                                    @endif

                                    <div class="intro box table-wrapper full-width hidden-table-sms">
                                        <div class="col-sm-4 table-cell travelo-box">
                                            <dl class="term-description">
                                                <dt>Company:</dt><dd>{{ $travel->nama_travel }}</dd>
                                                <dt>Type:</dt><dd>{{ $route->kelas }}</dd>
                                                <dt>Code:</dt><dd>{{ $route->kode_routes }}</dd>
                                                <dt>Route:</dt><dd>{{ count($details) }}</dd>
                                            </dl>
                                        </div>
                                        <div class="col-sm-8 table-cell">
                                            <div class="detailed-features clearfix">
                                                <div class="col-md-6">
                                                    <h4 class="box-title">
                                                    	<small>Depart</small>
                                                        <div>{{ $route->nm_depart }}</div>
                                                    </h4>
                                                    <div class="icon-box style11">
                                                        <div class="icon-wrapper">
                                                            <i class="soap-icon-clock"></i>
                                                        </div>
                                                        <dl class="details">
                                                            <dt class="skin-color">Tanggal Aktif</dt>
                                                            <dd>{{ Format::hari($route->tgl_aktif) }}, {{ Format::indoDate($route->tgl_aktif) }}</dd>
                                                        </dl>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <h4 class="box-title">
                                                    	<small>Destination</small>
                                                        <div>{{ $route->nm_destination }}</div>
                                                    </h4>
                                                    <div class="icon-box style11">
                                                        <div class="icon-wrapper">
                                                            <i class="soap-icon-clock"></i>
                                                        </div>
                                                        <dl class="details">
                                                            <dt class="skin-color">Tanggal non Aktif</dt>
                                                            <dd><dd>{{ Format::hari($route->tgl_limit) }}, {{ Format::indoDate($route->tgl_limit) }}</dd></dd>
                                                        </dl>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    @if(!empty($route->keterangan))
                                    <h2>Keterangan</h2>
                                    <p>{{ $route->keterangan }}</p>
                                    @endif
                                    <br />
                                    
                                    @if(Travel::check() && Travel::data()->id == $travel->id && Travel::data()->level == 1)
                                        <a href="{{ url('otojasa/editroutes/' . $route->id ) }}" class="button btn-small green pull-right">Perbaharui Rute</a>
                                    @endif
                                    <h2>{{ count($details) }} Rute Perjalanan</h2>
                                    <div class="car-list listing-style3 car">
                                        @foreach($details as $detail)
                                        <article class="box">
                                            <div class="details col-xs-12 clearfix">
                                                <div class="col-sm-8">
                                                    <div class="clearfix">
                                                        <h4 class="box-title">{{ $detail->nm_depart }} <i class="soap-icon-arrow-right"></i> {{ $detail->nm_destination }}</h4>
                                                    </div>
                                                    <div class="amenities">
                                                        <div>
                                                        	Hari
                                                        	@foreach(explode(',',$route->day_route) as $day)
                                                        		<span>, {{ Format::nama_hari($day) }}</span>
                                                        	@endforeach
                                                        </div>

                                                         <div>
                                                            @foreach($pools[$detail->id] as $pool)
                                                                <div>
                                                                    <i class="fa fa-clock-o"></i> {{ date('h:i A', strtotime($pool->waktu)) }} | <i class="soap-icon-locations"></i> {{ $pool->lokasi }}
                                                                </div>
                                                            @endforeach
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-xs-6 col-sm-2 character">
                                                    <dl class="">
                                                        <dt class="skin-color">Kelas</dt><dd>{{ $route->kelas }}</dd>
                                                    </dl>
                                                </div>
                                                <div class="action col-xs-6 col-sm-2">
                                                    <span class="price"><small>per Orang</small>Rp. {{ number_format($detail->harga,0,',','.') }}</span>
                                                    <!-- <a href="car-booking.html" class="button btn-small full-width">select</a> -->
                                                </div>
                                            </div>
                                        </article>
                                        <hr />
                                        @endforeach
                                    </div>

                                </div>

                                <div class="tab-pane fade in" id="car-upgrade">
                                    @if(Travel::check() && Travel::data()->id == $travel->id && Travel::data()->level == 1)
                                    <div class="text-right">
                                        <a href="{{ url('otojasa/editfacility/' . $route->id ) }}" class="button btn-small green">Perbaharui Pasilitas</a>
                                    </div>
                                    <hr />
                                    @endif

                                    <div class="panel panel-default">
                                    <table class="table">
                                    @foreach($facilitys as $facility)
                                        <tr>
                                            <td><img src="{{ env('IMG') }}/facility/{{ $facility->logo }}"></td>
                                            <td>{{ $facility->kode }}</td>
                                            <td>{{ $facility->keterangan }}</td>
                                        </tr>
                                    @endforeach
                                    </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="sidebar col-md-3">
                        <article class="detailed-logo">
                            <figure>
                                <img class="img-responsive" src="{{ env('IMG') }}/travel/avatar/{{ $travel->logo }}" title="{{ $travel->nama_travel }}" alt="{{ $travel->nama_travel }}">
                            </figure>
                            <div class="details">
                                <h2 class="box-title">{{ $travel->nama_travel }}<small>{{ $route->kelas }}</small></h2>
                                <p class="description">
                                	{{ $travel->hint }}
                                </p>
                            </div>
                        </article>
                        <div class="travelo-box contact-box">
                            <h4>Need {{ $travel->nama_travel }} Help?</h4>
                            <p>We would be more than happy to help you. Our team advisor are 24/7 at your service to help you.</p>
                            <address class="contact-details">
                                <span class="contact-phone"><i class="soap-icon-phone"></i> {{ $travel->no_tlp }}</span>
                            </address>
                        </div>

                       <!--  <div class="travelo-box book-with-us-box">
                            <h4>Why Book with us?</h4>
                            <ul>
                                <li>
                                    <i class="soap-icon-hotel-1 circle"></i>
                                    <h5 class="title"><a href="#">135,00+ Hotels</a></h5>
                                    <p>Nunc cursus libero pur congue arut nimspnty.</p>
                                </li>
                                <li>
                                    <i class="soap-icon-savings circle"></i>
                                    <h5 class="title"><a href="#">Low Rates &amp; Savings</a></h5>
                                    <p>Nunc cursus libero pur congue arut nimspnty.</p>
                                </li>
                                <li>
                                    <i class="soap-icon-support circle"></i>
                                    <h5 class="title"><a href="#">Excellent Support</a></h5>
                                    <p>Nunc cursus libero pur congue arut nimspnty.</p>
                                </li>
                            </ul>
                        </div> -->
                        
                    </div>
                </div>
            </div>
        </section>
	</div>	
@stop