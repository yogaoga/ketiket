@extends('Master.template')

@section('meta')
    <script type="text/javascript" src="{{ asset('/vendor/bootstrap-switch/bootstrap-switch.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('/vendor/bootstrap-switch/bootstrap-switch.min.css') }}">
    <script type="text/javascript">
    tjq(function(){
        tjq('input.boot-switch').bootstrapSwitch();

        tjq('.boot-switch').on('switchChange.bootstrapSwitch', function(event, state) {
          var status = tjq(this).prop('checked');
          var stat = status == false ? 0 : 1; 
          var id = tjq(this).data('id');
          tjq.ajax({
                type : 'POST',
                url : _base_url + '/ajax/activeroutes',
                data : {routes : id, status : stat},
                cache : false,
                dataType : 'json',
                success : function(res){
                    console.log(res.result);
                }
            });
        });

        del = function(id){
            var c = confirm('Anda yakin ingin menghapus Rute ini ?');
            if(c == true){
                tjq('.box_' + id).css('opacity', '0.3');
                tjq.ajax({
                    type : 'POST',
                    url : _base_url + '/ajax/deleteroutes',
                    data : {routes : id},
                    cache : false,
                    dataType : 'json',
                    success : function(res){
                        if(res.result == true){
                            tjq('.box_' + id).remove();
                        }
                    }
                });
            }
        }
        
    });
    </script>
@stop

@section('header')
    {!! Menu::titlePage('glyphicon glyphicon-road','Rute ' . Travel::data()->nama_travel) !!}
@stop

@section('content')
    <div class="container">
        <div id="main">
            <div class="tab-container full-width-style arrow-left dashboard">
                @include('Otojasa.Sidebar')
                <div class="tab-content">
                    <div id="dashboard" class="tab-pane fade in active">
                        <div class="pull-right">
                            <form method="get" action="{{ url('/otojasa/route') }}">
                            <input type="text" name="src" class="input-text" value="{{ $src }}" placeholder="Cari Kode">
                            <input type="submit" class="hide">
                            @if(Travel::data()->level == 1)
                            <a href="{{ url('/otojasa/createroute') }}" class="button btn-medium green"><i class="fa fa-plus"></i> Tambah Rute</a>
                            @endif
                            </form>
                        </div>
                        
                         <h1 class="no-margin skin-color">{{ $routes->total() }} hasil ditemukan</h1>
                         <hr />

                        <!-- star content -->
                        <div class="flight-list listing-style3 flight">
                            @forelse($routes as $route)
                                <article class="box box_{{ $route->id }}">
                                    <figure class="col-xs-3 col-sm-2">
                                        <center><img src="data:img/png;base64, {{ base64_encode(QrCode::format('png')->size(100)->margin(0)->generate(url(Travel::data()->slug . '/route/' . $route->kode_routes))) }}" title="{{ $route->kode_routes }}"></center>
                                    </figure>
                                    <div class="details col-xs-9 col-sm-10">
                                        <div class="details-wrapper">
                                            <div class="first-row">
                                                <div>
                                                    <h4 class="box-title">{{ $route->dari }} <i class="soap-icon-arrow-right"></i> {{ $route->ke }}<small>Kode : {{ $route->kode_routes }} | Oleh : {{ $route->oleh }}</small></h4>
                                                    <a target="_blank" href="{{ url(Travel::data()->slug . '/route/' . $route->kode_routes) }}" class="button btn-mini stop">Detail</a>
                                                    <div class="amenities">
                                                        <a href="{{ url('/otojasa/creatediscount/' . $route->id) }}" data-toggle="tooltip" data-placement="bottom" title="Set Diskon"><i class="glyphicon glyphicon-barcode circle"></i></a>
                                                        @if(Travel::data()->level == 1)
                                                            <a href="{{ url('/otojasa/editdataroute/' . $route->id) }}" data-toggle="tooltip" data-placement="bottom" title="Perbaharui Data"><i class="glyphicon glyphicon-pencil circle"></i></a>
                                                            <a href="{{ url('/otojasa/editroutes/' . $route->id) }}" data-toggle="tooltip" data-placement="bottom" title="Perbaharui Rute"><i class="glyphicon glyphicon-road circle"></i></a>
                                                            <a href="{{ url('/otojasa/editfacility/' . $route->id) }}" data-toggle="tooltip" data-placement="bottom" title="Perbaharui Pasilitas"><i class="glyphicon glyphicon-music circle"></i></a>
                                                            <a href="javascript:;" onclick="del({{ $route->id }});" data-toggle="tooltip" data-placement="bottom" title="Hapus"><i class="glyphicon glyphicon-trash circle"></i></a>
                                                        @endif
                                                    </div>
                                                    
                                                </div>
                                                <div>
                                                    <span class="price"><small>Harga</small>Rp {{ number_format(App\Models\harga_tiket::hargabyroute($route->depart,$route->destination)->price,0,',','.') }}</span>
                                                </div>
                                            </div>
                                            <div class="second-row">
                                                <div class="time">
                                                    <div class="col-sm-4">
                                                        <div class="icon"><i class="fa fa-check yellow-color"></i></div>
                                                        <div>
                                                            <span class="skin-color">Aktif</span><br />{{ Format::hari($route->tgl_aktif) }}, {{ Format::indoDate($route->tgl_aktif) }}
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="icon"><i class="fa fa-times yellow-color"></i></div>
                                                        <div>
                                                            <span class="skin-color">Non Aktif</span><br />{{ Format::hari($route->tgl_limit) }}, {{ Format::indoDate($route->tgl_limit) }}
                                                        </div>
                                                    </div>
                                                    <div class="total-time col-sm-4">
                                                        <div class="icon"><i class="soap-icon-passenger yellow-color"></i></div>
                                                        <div>
                                                            <span class="skin-color">Kelas</span><br />{{ $route->kelas }}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="action">
                                                    @if(Travel::data()->level == 1)
                                                    <input data-label-text="<i class='soap-icon-car'></i>" data-id="{{ $route->id }}" class="boot-switch" type="checkbox" data-on-color="info" data-off-color="danger" {{ $route->status == 1 ? 'checked' : '' }}>
                                                    @else
                                                    {!! $route->status == 1 ? '<a class="button btn-medium sky-blue1">ON</a>' : '<a class="button btn-medium red">OFF</a>' !!}
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            @empty
                                <div class="well">
                                    <h4>Rute Tidak ditemukan ?</h4>
                                </div>
                            @endforelse
                            
                            <div class="text-right">
                                {!! $routes->appends(['src' => $src])->render() !!}
                            </div>

                        </div>
                        <!-- end content -->

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop