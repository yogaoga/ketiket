@extends('Master.template')

@section('header')
	{!! Menu::titlePage('fa fa-road', 'Daftar Otojasa') !!}
@stop

@section('content')
	<div class="container">
		<div class="travelo-box book-with-us-box">
			
			@if(Auth::user()->status == 0)
				<center>
		  			<p>
			  			Akun anda belum teraktifasi silahkan cek email anda utnuk melakukan Aktifasi Akun anda.
			  		</p>
		  		</center>
		  	@elseif(empty(Auth::user()->rekening) || empty(Auth::user()->hp) || empty(Auth::user()->alamat) || empty(Auth::user()->cabang) || empty(Auth::user()->bank))
		  		<center>
			  		<p>
			  			Untuk mengajuakn Travel / Agen anda diwajibkan untuk melengkapi data diri anda secara lengkap
			  			<a href="{{ url('/user/edit') }}" class="btn btn-info btn-xs">Perbaharui Akun</a>
			  		</p>
		  		</center>
		  	@else
		  		<div class="row">
					<!-- Form input create jasa -->
					<div class="col-sm-6">
						<form class="form-horizontal" role="form" method="post" action="{{ url('/create-otojasa') }}" enctype="multipart/form-data">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
						  	
						  	
						  	<div class="form-group">
						    	<label for="nama_travel" class="col-sm-4 control-label">Nama *</label>
						    	<div class="col-sm-8">
						      		<input type="text" class="input-text full-width" name="nama_travel" value="{{ old('nama_travel') }}" id="nama_travel" required placeholder="Travel / Agen">
						    	</div>
						  	</div>

						  	<div class="form-group">
						    	<label for="nama_travel" class="col-sm-4 control-label">Jenis *</label>
						    	<div class="col-sm-8">
						      		<div class="btn-group" data-toggle="buttons">
									  <label class="btn btn-default btn-sm active">
									    <input type="radio" name="tipe" id="option1" checked value="1"> Travel
									  </label>
									  <label class="btn btn-default btn-sm">
									    <input type="radio" name="tipe" id="option2" value="2"> Agen
									  </label>
									</div>
									<small>* Pilih salah satu</small>
						    	</div>
						  	</div>

						  	<div class="form-group">
						    	<label for="hint" class="col-sm-4 control-label">Title </label>
						    	<div class="col-sm-8">
						      		<input type="text" class="input-text full-width" name="hint" id="hint" value="{{ old('hint') }}">
						    	</div>
						  	</div>

						  	<div class="form-group">
						    	<label for="information" class="col-sm-4 control-label">Deskripsi</label>
						    	<div class="col-sm-8">
						      		<textarea name="information" id="information" rows="5" class="input-text full-width">{{ old('information') }}</textarea>
						    	</div>
						  	</div>

						  	<hr />

						  	<div class="form-group">
						    	<label for="no_tlp" class="col-sm-4 control-label">Telpon *</label>
						    	<div class="col-sm-8">
						      		<input type="text" class="input-text full-width" name="no_tlp" id="no_tlp" value="{{ old('no_tlp') }}" required>
						    	</div>
						  	</div>

						  	<div class="form-group">
						    	<label for="website" class="col-sm-4 control-label">Website</label>
						    	<div class="col-sm-8">
						      		<input type="text" class="input-text full-width" name="website" value="{{ old('website') }}" id="website">
						    	</div>
						  	</div>
						  	<hr />
						  	<div class="form-group">
						    	<label for="website" class="col-sm-4 control-label">Alamat *</label>
						    	<div class="col-sm-8">
						      		<textarea name="alamat" rows="7" class="input-text full-width" required>{{ old('alamat') }}</textarea>
						    	</div>
						  	</div>

						  	<div class="form-group">
						    	<label for="provinsi_id" class="col-sm-4 control-label">Provinsi *</label>
						    	<div class="col-sm-8">
						    		<div class="selector full-width">
						    			<select name="provinsi_id" id="provinsi_id" required>
								      		@foreach($provinsi as $prov)
								      			<option value="{{ $prov->id }}" {{ old('provinsi_id') == $prov->id ? 'selected' : '' }}>{{ $prov->nm_provinsi }}</option>
								      		@endforeach
							      		</select>
						    		</div>
						    	</div>
						  	</div>

						  	<hr />
						  	<div class="form-group">
						    	<label for="logo" class="col-sm-4 control-label">Logo</label>
						    	<div class="col-sm-8">
						      		<div class="fileinput no-float no-padding" style="line-height: 34px;">
	                                    <input id="logo" name="logo" type="file" class="input-text" data-placeholder="select image/s">
	                                    <input type="text" class="custom-fileinput input-text" placeholder="select image/s" required>
	                                </div>
						    	</div>
						  	</div>

						  	<hr />

						  	<div class="form-group">
						    	<div class="col-sm-8 col-sm-offset-4">
						      		<div class="checkbox" style="padding-top:0;">
	                                    <label>
	                                        <input type="checkbox" required> Saya menyetujui sarat dan ketentuan
	                                    </label>
	                                </div>
						    	</div>
						  	</div>
						  	<div class="form-group">
						    	<div class="col-sm-8 col-sm-offset-4">
						      		<button class="button btn-large sky-blue1">Ajukan Otojasa</button>
						    	</div>
						  	</div>
						  	

						</form>
					</div>
					<!-- End Form input create jasa -->

					<!-- Satar da ketentuan -->
					<div class="col-sm-6">
						<p>
							Sarat dan ketentuan silahkan isi di sini
						</p>
					</div>
					<!-- End Satar da ketentuan -->
				</div>
			@endif
			
		</div>
	</div>
@stop