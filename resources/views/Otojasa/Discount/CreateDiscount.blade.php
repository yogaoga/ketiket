@extends('Master.template')

@section('meta')
    <script type="text/javascript">
        tjq(function(){
            tjq('.setall').click(function(){
                var tgl_aktif   = tjq('[name="tgl_aktif"]').val();
                var tgl_limit   = tjq('[name="tgl_limit"]').val();
                var diskon      = tjq('[name="discount"]').val();

                if(tgl_aktif.length > 0)
                    tjq(':input[rel-me="aktif"]').attr('required', 'required').val(tgl_aktif);
                if(tgl_limit.length > 0)
                    tjq(':input[rel-me="limit"]').attr('required', 'required').val(tgl_limit);
                if(diskon.length > 0)
                    tjq(':input[rel-me="diskon"]').val(diskon);

            });

            tjq('[rel-me="diskon"]').change(function(){
                var id = tjq(this).data('id');
                var val = tjq(this).val();
                console.log(id);
                if(val.length > 0){
                    tjq('[name="tgl_aktif[' + id + ']"]').attr('required', 'required');
                }else{
                    tjq('[name="tgl_aktif[' + id + ']"]').remove('required');
                }
            });

            if(window.location.hash){
                var hash = window.location.hash;
                tjq(hash).addClass('diskonfocus');
                console.log(hash);
            }
        });
    </script>
@stop

@section('header')
    {!! Menu::titlePage('glyphicon glyphicon-barcode','Diskon ' . $route->kode_routes) !!}
@stop

@section('content')
    <div class="container">
        <div id="main">
            <div class="tab-container full-width-style arrow-left dashboard">
                @include('Otojasa.Sidebar')
                <div class="tab-content">
                    <div id="dashboard" class="tab-pane fade in active">
                        <!-- star content -->
                        <h3 style="margin:0;"><i class="glyphicon glyphicon-road"></i> {{ $nm_depart }} to {{ $nm_destination }}</h3>
                        <span><i class="fa fa-search"></i> {{ count($details) }} hasil ditemukan</span>
                        <hr />
                        <div class="flight-list listing-style3 flight">
                        @if(count($details) > 1)
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="datepicker-wrap">
                                                <input type="text" name="tgl_aktif" class="input-text full-width" placeholder="Tanggal non Aktif" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="datepicker-wrap">
                                                <input type="text" name="tgl_limit" class="input-text full-width" placeholder="Tanggal non Aktif" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="input-group">
                                                <input type="text" name="discount" class="input-text full-width text-right">
                                                <span class="input-group-addon" style="background:#f5f5f5; border:none;">%</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <button class="button btn-medium light-yellow setall" type="button">Set Semua Form</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        
                            <form method="post" action="{{ url('/otojasa/updatediscount') }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                @foreach($details as $rute)
                                    <article class="box" style="margin:10px 0;" id="{{ $rute->id }}-{{ $route->kode_routes }}">
                                        <div class="details col-xs-9 col-sm-12">
                                            <div class="details-wrapper">
                                                <div class="first-row">
                                                    <div>
                                                        <h4 class="box-title">
                                                            {{ $rute->nm_depart }} <i class="soap-icon-arrow-right"></i> {{ $rute->nm_destination }}
                                                            <small>Kode : {{ $route->kode_routes }}</small>
                                                        </h4>
                                                        <a href="javascript:;" style="opacity:0;" class="button btn-mini stop">Detail</a>
                                                    </div>
                                                    <div>
                                                        <span class="price"><small>Total Harga</small>Rp {{ number_format($rute->harga,0,',','.') }}</span>
                                                    </div>
                                                </div>

                                                <div class="second-row">
                                                    <div class="time">
                                                        <div class="col-sm-4">
                                                            
                                                        </div>
                                                        <div class="col-sm-4 text-right">
                                                            <label>Tangal Aktif</label>
                                                            <div class="datepicker-wrap">
                                                                <input type="text" name="tgl_aktif[{{$rute->id}}]" rel-me="aktif" class="input-text full-width" value="{{ !empty($rute->tgl_d_aktif) ? date('m/d/Y', strtotime($rute->tgl_d_aktif)) : '' }}" placeholder="Tanggal Aktif" />
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4 text-right">
                                                            <label>Tangal Non Aktif</label>
                                                            <div class="datepicker-wrap">
                                                                <input type="text" name="tgl_limit[{{$rute->id}}]" rel-me="limit" class="input-text full-width" value="{{ !empty($rute->tgl_d_limit) ? date('m/d/Y', strtotime($rute->tgl_d_limit)) : '' }}" placeholder="Tanggal non Aktif" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="action" style="width:20%;">
                                                        <label>Diskon</label>
                                                        <div class="input-group">
                                                            <input type="text" name="discount[{{$rute->id}}]" rel-me="diskon" value="{{ (INT) $rute->persen }}" data-id="{{$rute->id}}" class="input-text full-width text-right">
                                                            <span class="input-group-addon" style="background:#f5f5f5; border:none;">%</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                @endforeach
                                <div class="text-right">
                                    <hr />
                                    <a href="{{ url('/otojasa/discount') }}" class="btn btn-link pull-left">Batal</a>
                                    <button type="submit">Simpan Diskon</button>
                                </div>
                            </form>
                        
                        </div>
                        <!-- end content -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop