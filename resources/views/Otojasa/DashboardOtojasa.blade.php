@extends('Master.template')

@section('header')
	{!! Menu::titlePage('fa fa-dashboard','Dashboard ' . Travel::data()->nama_travel) !!}
@stop

@section('content')
	<div class="container">
		<div id="main">
	        <div class="tab-container full-width-style arrow-left dashboard">
	            @include('Otojasa.Sidebar')
	            <div class="tab-content">
	                <div id="dashboard" class="tab-pane fade in active">
	                    <h1 class="no-margin skin-color">{{ Travel::data()->nama_travel }}</h1>
	                    <p>{{ Travel::data()->hint }}</p>
	                    
	                    @if(Travel::data()->status == 0)
	                    	<br /><br />
	                    	<center><h1>{{ Travel::data()->nama_travel }} Dalam Moderator</h1></center>
	                    @endif

	                </div>
	            </div>
	        </div>
	    </div>
	</div>
@stop