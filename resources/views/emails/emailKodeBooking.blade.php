@extends('Master.email_template')

@section('content')
<div mc:repeatable="service_1s">
   <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="wrap" style="border-collapse: collapse; background-image: none; background-color: #ffffff;">
      <tr>
         <td align="center" valign="top" class="module-td">
            <table align="center" width="600" border="0" cellpadding="0" cellspacing="0" class="row" style="border-collapse: collapse;">
               <tr>
                  <td height="50"></td>
               </tr>
               <tr>
                  <td valign="top">
                     <!--title in module-->
                     <table align="left" width="75%" border="0" cellpadding="0" cellspacing="0" class="title-module" style="border-collapse: collapse;border: none;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                        <tr>
                           <td align="left" class=" h3 uppercase highlight" mc:edit="subtitle" style="font-weight: 400; color: #149ff3; font-family: Raleway, Tahoma, Arial; font-size: 21px; line-height: 27px; text-transform: uppercase;">{{ $reservasi->nm_depart }} to {{ $reservasi->nm_destination }}</td>
                        </tr>
                        <tr>
                           <td valign="top">
                              <table align="left" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;border: none;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                 <tr>
                                    <td align="left" class=" underline h2 bold uppercase highlight" mc:edit="main-title" style="font-weight: 700; color: #149ff3; font-family: Raleway, Tahoma, Arial; font-size: 27px; line-height: 32px; text-transform: uppercase; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #149ff3;">Kode : {{ $reservasi->kode_booking }}</td>
                                 </tr>
                              </table>
                           </td>
                        </tr>
                        <tr>
                           <td height="20"></td>
                        </tr>
                        <tr>
                           <td align="left" class=" content" mc:edit="after-title" style="font-family: Raleway, Tahoma, Arial; font-size: 13px; line-height: 18px; font-weight: 400; color: #5f5f5f; -webkit-font-smoothing: antialiased;">Terimakasih telah melakukan Pemesanan tiket di {{ Agen::data()->nama_travel }}.<br />Di atas merupakan Kode Booking yang akan anda gunakan.</td>
                        </tr>
                        <tr>
                           <td height="30"></td>
                        </tr>
                     </table>
                  </td>
               </tr>
               <tr>
                  <td valign="top">
                     <!--col3 left-->
                     <table align="left" width="180" border="0" cellpadding="0" cellspacing="0" class="col3 mid40" style="border-collapse: collapse;border: none;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                        <tr>
                           <td align="left">
                              <br>
                           </td>
                        </tr>
                        <tr>
                           <td align="left" height="50" class="h4 bold valign uppercase" mc:edit="title1" style="vertical-align: middle; font-weight: 700; color: #20394d; font-family: Raleway, Tahoma, Arial; font-size: 17px; line-height: 24px; text-transform: uppercase;">No Kursi</td>
                        </tr>
                        <tr>
                           <td align="left" class="content" mc:edit="content1" style="font-family: Raleway, Tahoma, Arial; font-size: 13px; line-height: 18px; font-weight: 400; color: #5f5f5f; -webkit-font-smoothing: antialiased;">{{ $kursi }}</td>
                        </tr>
                     </table>
                     <!--col3 left-->
                     <table align="left" width="180" border="0" cellpadding="0" cellspacing="0" class="col3 mid40" style="border-collapse: collapse;border: none;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                        <tr>
                           <td align="left">
                              <br>
                           </td>
                        </tr>
                        <tr>
                           <td align="left" height="50" class="h4 bold valign uppercase" mc:edit="title2" style="vertical-align: middle; font-weight: 700; color: #20394d; font-family: Raleway, Tahoma, Arial; font-size: 17px; line-height: 24px; text-transform: uppercase;">Boarding Point</td>
                        </tr>
                        <tr>
                           <td align="left" class="content" mc:edit="content2" style="font-family: Raleway, Tahoma, Arial; font-size: 13px; line-height: 18px; font-weight: 400; color: #5f5f5f; -webkit-font-smoothing: antialiased;">{{ $reservasi->lokasi }}</td>
                        </tr>
                     </table>
                     <!--col3 left-->
                     <table align="right" width="180" border="0" cellpadding="0" cellspacing="0" class="col3" style="border-collapse: collapse;border: none;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                        <tr>
                           <td align="left">
                              <br>
                           </td>
                        </tr>
                        <tr>
                           <td align="left" height="50" class="h4 bold valign uppercase" mc:edit="title3" style="vertical-align: middle; font-weight: 700; color: #20394d; font-family: Raleway, Tahoma, Arial; font-size: 17px; line-height: 24px; text-transform: uppercase;">Waktu</td>
                        </tr>
                        <tr>
                           <td align="left" class="content" mc:edit="content3" style="font-family: Raleway, Tahoma, Arial; font-size: 13px; line-height: 18px; font-weight: 400; color: #5f5f5f; -webkit-font-smoothing: antialiased;"> {{ date('h:i A', strtotime($reservasi->waktu)) }} </td>
                        </tr>
                     </table>
                  </td>
               </tr>
               <tr>
                  <td height="50"></td>
               </tr>
            </table>
         </td>
      </tr>
   </table>
</div>
@stop