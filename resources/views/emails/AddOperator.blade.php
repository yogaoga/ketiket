@extends('Master.email_template')

@section('content')
	<!--start title-->
      <tr>
         <td align="center" class="title-td" mc:edit="subtitle">
            <h3 class="h3 bold highlight" style="font-family: Montserrat, Tahoma; font-weight: 700; color: #e54d24; font-size: 27px; line-height: 32px; margin: 0px 0px 8px !important;">
            	Hi, {{ $data->name }}
            </h3>
            <table align="center" width="90" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse;">
               <tr>
                  <td height="1" class="small-img line2" style="font-size: 0px;line-height: 0px;border-collapse: collapse;background-color: #252525;"><img  src="http://template/email/email_builder/html/orange/images/spacer.gif" width="1" height="1" style="border: 0;display: block;-ms-interpolation-mode: bicubic;"></td>
               </tr>
            </table>
         </td>
      </tr>
      <!--end title-->
	<!--start content-->
      <tr>
         <td align="center" class="content b" mc:edit="content1" style="font-family: 'Playfair Display', Arial; font-weight: 400; font-size: 15px; line-height: 21px; color: #252525; -webkit-font-smoothing: antialiased; font-style: italic;">
            <p style="font-family: 'Playfair Display', Arial; font-weight: 400; font-size: 15px; line-height: 21px; color: #252525; -webkit-font-smoothing: antialiased; font-style: italic; margin: 0px !important;">
            	
            </p>
         </td>
      </tr>
      <!--end content-->
      <tr>
         <td height="38"></td>
      </tr>
      <!--start 2 columns-->
      <tr>
         <td>
            <table width="290" align="right" border="0" cellspacing="0" cellpadding="0" class="col2 mid" style="border-collapse: collapse;border: none;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
               <tr>
                  <td><img  src="{{ env('IMG') }}/travel/avatar/{{ Travel::data()->logo }}" width="290" height="170" alt="image" class="img img290" mc:edit="image"  style="border: 0;display: block;-ms-interpolation-mode: bicubic;width: 290px;height: auto;max-width: 290px;"></td>
               </tr>
            </table>
            <table width="290" align="left" border="0" cellspacing="0" cellpadding="0" class="col2" style="border-collapse: collapse;border: none;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
               <tr>
                  <td align="left" class="content gray" mc:edit="content2" style="font-family: Raleway, Arial; font-weight: 400; font-size: 13px; line-height: 19px; color: #585858; -webkit-font-smoothing: antialiased;">
                     <p style="font-family: Raleway, Arial; font-weight: 400; font-size: 13px; line-height: 19px; color: #585858; -webkit-font-smoothing: antialiased; margin: 0px !important;">

                     	<!-- Keterangan, darat dan kondidi di sini -->
                     		Anda telah di undang olah {{ Travel::data()->nama_travel }} untuk bergabung sebagai pengelola dari jasa Travel {{ Travel::data()->nama_travel }}
                     	<!-- End Keterangan, darat dan kondidi di sini -->

                     </p>
                  </td>
               </tr>
               <tr><td height="28"></td></tr>
               	<tr>
					<td class="in colorbg" mc:edit="button text1" style="font-family: Raleway; font-size: 14px; line-height: 14px; font-weight: 400; padding: 11px 19px; border-radius: 0px; background-color: #3291db;">
						<a  href="{{ url('/invite/' . md5($data->id . Travel::data()->id)) }}" style="color: #fff;text-decoration: none !important;outline: none;"><center>Terima</center></a>
					</td>
				</tr>

            </table>
         </td>
      </tr>
      <!--end 2 columns-->
@stop