@extends('Master.email_template')

@section('content')
	
	<!-- start ◆full_module_7s◆ -->
<div mc:repeatable="full_module_7s">
   <table width="650" align="center" border="0" cellspacing="0" cellpadding="0" class="wrap wrapbg" style="border-collapse: collapse; width: 650px; margin: 0px auto; background-image: none; background-color: #ffffff;">
      <tr>
         <td align="center" class="module-td1" style="padding: 10px 0 0;">
            <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="row" style="border-collapse: collapse;">
               
               <!--start content-->
               <tr>
                  <td align="center" class="content" mc:edit="content" style="font-family: Raleway, Arial; font-weight: 400; font-size: 13px; line-height: 19px; color: #252525; -webkit-font-smoothing: antialiased;">
                     <p style="font-family: Raleway, Arial; font-weight: 400; font-size: 13px; line-height: 19px; color: #252525; -webkit-font-smoothing: antialiased; margin: 0px !important;">

                     	<p>
                     		Hi, {{ $user->name }} terimakasih telah bergabung di Ketiket.
                     		<br />Untuk melakukan aktifasi akun silahkan klik link Verifikasi di bawah ini
                     	</p>
                     	<a href="{{ url('/aktivasi?t=' . md5($user->id) ) }}" target="_blank">
                     		<h3 class="h3 bold highlight" style="font-family: Montserrat, Tahoma; font-weight: 700; color: #0078c7; font-size: 27px; line-height: 32px; margin: 0px 0px 8px !important;">
		                     	Verifikasi Akun
		                     </h3>
                     	</a>

                     	<p>
                     		Dapatkan kemudahan dalam menentukan rute perjalanan Anda bersama Ketiket
                     	</p>

                     </p>
                  </td>
               </tr>
               <!--end content-->
            </table>
         </td>
      </tr>
   </table>
</div>
<!-- end - ◆full_module_7s◆ -->

@stop