@extends('Master.template')

@section('meta')
	<link rel="stylesheet" type="text/css" href="{{ asset('/lib/dragdrop/drugdrop.css') }}">
	<script type="text/javascript" src="{{ asset('/lib/dragdrop/jquery.nestable.js') }}"></script>
	<script type="text/javascript">
    	tjq(function(){
    		var updateOutput = function(e){

	            var list   = e.length ? e : tjq(e.target),
	                output = list.data('output');
	            if (window.JSON) {
	                output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));			
	            } else {
	                output.val('JSON browser support required for this demo.');
	            }
	        }
	        
	        tjq('#nestable').nestable({
	        	group: 1
	        })
	        .on('change', updateOutput);
	        updateOutput(tjq('#nestable').data('output', tjq('#nestable-output')));
    	});
	</script>

@stop

@section('content')
	
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				
					@if(empty($menu))
						<form class="form-horizontal form-bordered" action="" method="post" role="form">
					@else
						<form class="form-horizontal form-bordered" action="{{ url('/menu/update') }}" method="post" role="form">
					@endif

					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="travelo-box">
					  	<div class="panel-heading">
					  		<h4 class="panel-title">{{ !empty($menu) ? 'Update' : 'Add' }} Menu</h4>
					  	</div>
					  	<div class="panel-body">

					  	@if((Session::get('sess')))
					  		<div class="alert alert-block alert-info">
		                        <button data-dismiss="alert" class="close" type="button">&times;</button>
		                        {{ Session::get('sess') }}
		                    </div>
					  	@endif

					  		<div class="form-group">
			                    <label for="title" class="control-label col-sm-3">Title *</label>

			                    <div class="controls col-sm-9">
			                        <input type="text" value="{{ empty($menu->title) ? '' : $menu->title }}" class="input-text full-width" id="title" name="title" required="required" />
			                    </div>
			                </div>

			                <div class="form-group">
			                    <label for="slug" class="control-label col-sm-3">Slug</label>

			                    <div class="controls col-sm-9">
			                        <input type="text" value="{{ empty($menu->slug) ? '' : $menu->slug }}" placeholder="controller/method" class="input-text full-width" id="slug" name="slug" />
		                        </div>
			                </div>

			                <div class="form-group">
			                    <label for="icon" class="control-label col-sm-3">Icon</label>

			                    <div class="controls col-sm-9">
			                        <input type="text" value="{{ empty($menu->class) ? '' : $menu->class }}" class="input-text full-width" placeholder="fa " id="icon" name="class" />
			                        <small class="text-muted">* fa fa-fw fa-caret-right</small>
			                        <a href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blank" class="pull-right">Check icon here!</a>
			                    </div>
			                </div>

			                <div class="form-group">
			                    <label for="id" class="control-label col-sm-3">#Id</label>

			                    <div class="controls col-sm-9">
			                        <input type="text" value="{{ empty($menu->class_id) ? '' : $menu->class_id }}" class="input-text full-width" id="id" name="id" />
			                    </div>
			                </div>

			                <div class="form-group">
			                    <label for="parent" class="control-label col-sm-3">Parent</label>

			                    <div class="controls col-sm-9">
			                        <select class="input-text full-width" id="parent" name="idparent">
			                        	<option value="0">None</option>
			                        	@foreach($parent as $menus)
			                        		<option value="{{ $menus->id }}" {{ empty($menu) ? '' : $menus->id == $menu->parent_id ? 'selected="selected"' : '' }}>- {{ $menus->title }}</option>
			                        	@endforeach
			                        </select>
			                    </div>
			                </div>

			                <div class="form-group">
			                    <label class="control-label col-sm-3">Status</label>

			                    <div class="controls col-sm-9">

			                     	<div class="controls">
			                     		 <label class="radio radio-inline radio-square">
			                                <input type="radio" value="1" name="status" class="icheck minimal" {{ !empty($menu) && $menu->status == 1 ? 'checked="checked"' : empty($menu) ? 'checked="checked"' : ''  }}>Enable
			                            </label>

			                             <label class="radio radio-inline radio-square">
			                                <input type="radio" name="status" value="0" class="icheck minimal" {{ !empty($menu) && $menu->status == 0 ? 'checked="checked"' : '' }}>Disable
			                            </label>
				                        
				                    </div>

			                    </div>
			                </div>

			                <div class="form-group">
			                    <label class="control-label col-sm-3">View</label>

			                    <div class="controls col-sm-9">

			                     	<div class="controls">
			                     		 <label class="checkbox checkbox-inline checkbox-square" data-toggle="tooltip" data-placement="bottom" title="Tampil pada saat login saja">
			                                <input type="checkbox" value="1" name="auth" class="icheck minimal" {{ !empty($menu) && $menu->auth == 1 ? 'checked="checked"' : empty($menu) ? 'checked="checked"' : ''  }}>Auth
			                            </label>

			                             <label class="checkbox checkbox-inline checkbox-square" data-toggle="tooltip" data-placement="bottom" title="Tampil pada saat tidak login saja">
			                                <input type="checkbox" name="guest" value="1" class="icheck minimal" {{ !empty($menu) && $menu->guest == 1 ? 'checked="checked"' : '' }}>Guest
			                            </label>
				                        
				                    </div>

			                    </div>
			                </div>


			                @if(!empty($menu))
			                 <div class="form-group">
			                    <label class="control-label col-sm-3">Delete</label>

			                    <div class="controls col-sm-9">
			                          <label class="checkbox checkbox-inline checkbox-square">
		                                <input name="del" type="checkbox" class="icheck line-red">
		                                Yes
		                            </label>
			                    </div>
			                </div>
			                @endif

			                 <div class="form-group">
			                    <label for="note" class="control-label col-sm-3">Note</label>

			                    <div class="controls col-sm-9">
			                        <input value="{{ empty($menu->ket) ? '' : $menu->ket }}" type="text" class="input-text full-width" id="note" placeholder="Admin, Superadmin, User" name="ket" />
			                    </div>
			                </div>

			                <div class="form-group">
			                    <label class="control-label col-sm-3"></label>

			                    
			                    <div class="controls col-sm-9">
			                    	<button type="submit" class="btn btn-flat btn-primary">{{ !empty($menu) ? 'Update' : 'Add' }} Menu</button>
			                    	@if(!empty($menu))
			                    		<a href="{{ url('/menu/setting') }}" class="btn btn-link pull-right"><strong>Cancel</strong></a>
			                    		<input type="hidden" value="{{ $menu->id }}" name="id" />
			                    	@endif
			                    </div>
			                    
			                </div>

					  	</div>
					</div>
				</form>
			</div>
			<div class="col-md-6">
				
					<div class="travelo-box">
					  	<div class="panel-heading">
					  		<h4 class="panel-title">Menu Position</h4>
					  	</div>
					  	<div class="panel-body">
					  		
					  		<div class="dd" id="nestable">
					  			{!! Menu::menuPosition() !!}
					  		</div>
					  	</div>
					</div>

					<div class="travelo-box">
					  	<div class="panel-body">
					  		
					  		<div>
					  			<form method="post" action="{{ url('/menu/saveposition') }}">
						            <button type="submit" name="simpan" class="btn btn-flat btn-primary">Save Position</button>
						            <input type="hidden" value="" name="update" id="nestable-output">
						            <input type="hidden" name="_token" value="{{ csrf_token() }}">
						        </form>
					  		</div>

					  	</div>
					</div>
					
			</div>
		</div>
	</div>

@stop