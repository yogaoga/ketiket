@extends('Master.template')

@section('meta')
<script type="text/javascript" src="{{ asset('/vendor/jquery-select2/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/create-route.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('/vendor/jquery-select2/select2.min.css') }}">
@stop

@section('header')
<div class="container">
    <div class="row">
        <div id="main" class="col-md-6" style="padding-left: 0px; padding-right: 0px;">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
              <!-- Indicators -->
              <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
              <div class="item active">
                <img src="{{ asset('images/cipaganti.jpg') }}" alt="" width="100%">
              </div>
              <div class="item">
                  <img src="{{ asset('images/cipaganti.jpg') }}" alt="" width="100%">
              </div>
              <div class="item">
                  <img src="{{ asset('images/cipaganti.jpg') }}" alt="" width="100%">
              </div>
            </div>

          <!-- Controls -->
          <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

</div>
<div class="col-sm-6" style="padding-left: 0px;">
    <div class="col-md-12">
        <div class="travelo-box">
            <ul class="nav nav-tabs" id="state-tab">
                <li class="rental"><a href="#home" onclick="rental()"><h4 class="box-title">Sewa Mobil</h4></a></li>
                <li class="travel"><a href="#home" onclick="travel()"><h4 class="box-title">Search Travel</h4></a></li>
            </ul>
            <div class="divtravel hidden">
                <form action="{{ url('/reservasi/jadwal') }}" method="get">
                    <div class="form-group">
                        <label>Kota Asal</label>
                        <select name="depart" class="form-control form-select2" required>
                            <option value="">Pilih Lokasi</option>
                            @foreach($provinsi as $prov)
                            <optgroup label="{{ $prov->nm_provinsi }}">
                                @foreach(App\Models\provinsi::find($prov->id)->kab_kota()->get() as $kab)
                                <option value="{{ $kab->id }}">{{ $kab->nm_kab_kota }}</option>
                                @endforeach
                            </optgroup>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Kota Tujuan</label>
                        <select name="destination" class="form-control form-select2" required>
                            <option value="">Pilih Lokasi</option>
                            @foreach($provinsi as $prov)
                            <optgroup label="{{ $prov->nm_provinsi }}">
                                @foreach(App\Models\provinsi::find($prov->id)->kab_kota()->get() as $kab)
                                <option value="{{ $kab->id }}">{{ $kab->nm_kab_kota }}</option>
                                @endforeach
                            </optgroup>
                            @endforeach
                        </select>
                    </div>
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form-group">
                                <label>Tanggal</label>
                                <div class="datepicker-wrap">
                                    <input type="text" required name="tgl" class="input-text full-width" value="{{ date('m/d/Y') }}" placeholder="mm/dd/yy" />
                                </div>
                                
                            </div>
                        </div>    
                        <div class="col-sm-5">
                            <div class="form-group">
                                <label>Infant *</label>
                                <input type="number" name="infant" class="input-text full-width text-right" value="{{ old('infant') ? old('infat') : 1 }}" required/>
                            </div>        
                        </div>

                    </div>

                    <input type="hidden" name="sort" value="desc">
                    <input type="hidden" name="_t" value="{{ csrf_token() }}">
                    <button class="icon-check full-width">TAMPILKAN</button>
                </form>
            </div>
            
            <div class="divrental hidden">
                <form action="{{ url('/car-book') }}" method="get">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Lokasi</label>
                                <select name="depart" class="form-control form-select2" required>
                                    <option value="">Pilih Lokasi</option>
                                    @foreach($provinsi as $prov)
                                    <optgroup label="{{ $prov->nm_provinsi }}">
                                        @foreach(App\Models\provinsi::find($prov->id)->kab_kota()->get() as $kab)
                                        <option value="{{ $kab->id }}">{{ $kab->nm_kab_kota }}</option>
                                        @endforeach
                                    </optgroup>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                             <div class="form-group divreturn hidden">
                                <label>Lokasi Kembali</label>
                                <select name="destination" class="form-control form-select2">
                                    <option value="">Pilih Lokasi</option>
                                    @foreach($provinsi as $prov)
                                    <optgroup label="{{ $prov->nm_provinsi }}">
                                        @foreach(App\Models\provinsi::find($prov->id)->kab_kota()->get() as $kab)
                                        <option value="{{ $kab->id }}">{{ $kab->nm_kab_kota }}</option>
                                        @endforeach
                                    </optgroup>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" name="return" value="1" onclick="difreturn()">Kembali di lokasi yang berbeda
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-6" style="padding-right: 0px;">
                            <label>Tanggal Penjemputan</label>
                            <div class="btn-group">
                                <div class="col-sm-6" style="padding-left: 0px;padding-right: 0px; ">
                                    <div class="datepicker-wrap">
                                        <input type="text" required name="tgl" class="input-text full-width" value="{{ date('m/d/Y') }}" placeholder="mm/dd/yy hh" />
                                    </div> 
                                </div>
                                <div class="col-sm-3" style="padding-left: 0px;padding-right: 0px;">
                                     <input type="number" name="jam" min="0" max="24" class="input-text full-width text-right" value="{{ date('G')}}" required/>                                
                                </div>
                                <div class="col-sm-3" style="padding-left: 0px; padding-right: 0px;">
                                    <input type="number" name="menit" min="0" max="60" class="input-text full-width text-right" value="{{ date('i')}}" required/>
                                </div>
                            </div>
                        </div>  
                        <div class="col-sm-6" style="padding-right: 0px;">
                            <label>Tanggal Kembali</label>
                            <div class="btn-group">
                                <div class="col-sm-6" style="padding-left: 0px;padding-right: 0px; ">
                                    <div class="datepicker-wrap">
                                        <input type="text" required name="tgl1" class="input-text full-width" value="{{ date('m/d/Y') }}" placeholder="mm/dd/yy hh" />
                                    </div> 
                                </div>
                                <div class="col-sm-3" style="padding-left: 0px;padding-right: 0px;">
                                     <input type="number" name="jam1"  min="0" max="24" class="input-text full-width text-right" value="{{ date('G')}}" required/>                                
                                </div>
                                <div class="col-sm-3" style="padding-left: 0px; padding-right: 0px;">
                                    <input type="number" name="menit1" min="0" max="60" class="input-text full-width text-right" value="{{ date('i')}}" required/>
                                </div>
                            </div>
                        </div> 

                    </div>
                    <div class="form-group">
                        <label>Car Type</label>
                        <select name="cartype" class="form-control form-select2">
                            <option value="">Pilih Tipe Mobil</option>
                            @foreach($cartype as $car)
                                <option value="{{ $car->id_car_type }}">{{ $car->nm_car_type }}</option>
                            @endforeach
                        </select>
                    </div> 
                    <input type="hidden" name="page" value="1">
                    <input type="hidden" name="_t" value="{{ csrf_token() }}">
                    <button class="icon-check full-width">TAMPILKAN</button>
                </form>
            </div>
        </div>
    </div>
</div>        
</div>
</div>
@stop

@section('content')
<div class="page-title-container style4">
   <div class="container">
       <div class="row">
           <div class="col-md-4">
            <div class="page-title">
                <i class="soap-icon-plane-right soap-icon-search"></i>
                <h2 class="entry-title">Cari Kode Booking</h2>
            </div>       
        </div>
        <form action="{{ url('/reservasi/carikode') }}" method="get">
            <div class="col-md-4">

                <input type="text" required class="input-text full-width" name="kode_booking" placeholder="Kode Booking" />    


            </div>
            <div class="col-md-1">
                <button class="button btn-medium sky-blue2">CARI</button>
            </div>
        </form>
    </div>
    
</div>
</div>
<!-- Popuplar Destinations -->
<div class="destinations section">
    <div class="container shortcode">
        <h1>Operation</h1>
        <div class="row image-box hotel listing-style1">
            @foreach($data as $datas)
            <div class="col-sm-6 col-md-3">
                <article class="box">
                    <figure>
                        <a href="{{ url($datas->slug) }}" class="hover-effect"><img width="270" height="160" alt="" src="{{ url (env('IMG') . '/travel/avatar/' . $datas->logo) }}"></a>
                    </figure>
                    <div class="details">
                        <h4 class="box-title">{{ $datas->nama_travel }}</h4>
                        <div class="feedback">
                            <div data-placement="bottom" data-toggle="tooltip" class="five-stars-container" title=""><span style="width: {{ $datas->rate_index }}% ;" class="five-stars"></span></div>
                            
                        </div>
                    </div>
                </article>
            </div>
            @endforeach
        </div>
    </div>
    <div class="container shortcode">
        <h1>Rental Mobil</h1>
        <div class="row image-box car listing-style1">
            @foreach($carrent as $car)
            <div class="col-sm-6 col-md-3">
                <article class="box">
                    <figure>
                        <a href="#" class="hover-effect"><img width="270" height="160" alt="" src="{{env('IMG').'/rental/'.$car->foto}}"></a>
                    </figure>
                </article>
            </div>
            @endforeach
        </div>
    </div>
</div>

<!-- Route Populer -->
<div class="page-route">
   <div class="container">
       <div class="row ">
       <br>
           <div class="col-md-6">

            <h2>Route Populer</h2>
            <ul class="discover triangle hover row">
                @foreach($route_populer as $row)
                <li class="col-xs-5"><a href="{{ url('/reservasi/jadwal?depart='.$row['depart'].'&destination='.$row['destination'].'&tgl='.date('m/d/Y').'&infant='.'1'.'&sort='.'desc') }}">{{$row['nm_depart']}} - {{$row['nm_destination']}}</a></li>

                @endforeach
                <li class="col-xs-5"><a href="#">Read More</a></li>
            </ul>
        </div>
        <div class="col-md-6">
            <h2>Latest Route</h2>
            <ul class="discover triangle hover row">
                @foreach($route_last as $row)
                <li class="col-xs-5"><a href="#">{{$row['nm_depart']}} - {{$row['nm_destination']}}</a></li>
                @endforeach
            </ul>
        </div>
    </div>
    <br>
</div>
</div>
<script type="text/javascript">

  rental();

function refres(){
    tjq('ul#state-tab >li').removeClass('active');
    tjq('div.divtravel').addClass('hidden');
    tjq('div.divrental').addClass('hidden');
}
function travel(){
    refres();
    tjq('div.divtravel').removeClass('hidden');
    tjq('ul#state-tab >li.travel').addClass('active');
}
function rental(){
    refres();
    tjq('div.divrental').removeClass('hidden');
    tjq('ul#state-tab >li.rental').addClass('active');
}
function difreturn(){
    var cek = tjq('div.divreturn').hasClass('hidden');
    if(cek){
        tjq('div.divreturn').removeClass('hidden');
    }else{
        tjq('div.divreturn').addClass('hidden');
    }
}
//function add(operator,target){
//    var oldnum = tjq('select.'+target).val();
//    var result;
//    if(operator=='-'){
//        result=parseFloat(oldnum)-1;
//    }else{
//        result=parseFloat(oldnum)+1;
//    }
//    tjq('select.'+target).val(result);
//}
</script>
@stop

@section('footer')

@stop