<!DOCTYPE html>
<!--[if IE 8]>          <html class="ie ie8"> <![endif]-->
<!--[if IE 9]>          <html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->  <html> <!--<![endif]-->
<head>
    <!-- Page Title -->
    <title>{{ !empty($title) ? $title : 'Ketiket Travel' }}</title>
    
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="keywords" content="{{ !empty($keywords) ? $keywords : 'Ketiket, Jasa Tiketing' }}" />
    <meta name="description" content="{{ !empty($description) ? $description : 'Ketiket.com merupakan situs yang memiliki akses dengan 876 operator travel yang ada di indonesia.' }}">
    <meta name="author" content="{{ !empty($author) ? $author : 'Ketiket' }}">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Theme Styles -->
    <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/font-awesome.min.css') }}">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ asset('/css/animate.min.css') }}">
    
    <!-- Current Page Styles -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/components/revolution_slider/css/settings.css') }}" media="screen" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/components/revolution_slider/css/style.css') }}" media="screen" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/components/jquery.bxslider/jquery.bxslider.css') }}" media="screen" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/components/flexslider/flexslider.css') }}" media="screen" />
    
    <!-- Main Style -->
    <link id="main-style" rel="stylesheet" href="{{ asset('/css/style.css') }}">
    
    <!-- Custom Styles -->
    <link rel="stylesheet" href="{{ asset('/css/custom.css') }}">

    <!-- Updated Styles -->
    <link rel="stylesheet" href="{{ asset('/css/updates.css') }}">

    <!-- Updated Styles -->
    <link rel="stylesheet" href="{{ asset('/css/updates.css') }}">
    
    <!-- Responsive Styles -->
    <link rel="stylesheet" href="{{ asset('/css/responsive.css') }}">
    
    <!-- CSS for IE -->
    <!--[if lte IE 9]>
        <link rel="stylesheet" type="text/css" href="css/ie.css" />
    <![endif]-->
    
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script type='text/javascript' src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
    <![endif]-->

    <!-- Javascript -->
    <script type="text/javascript" src="{{ asset('/js/jquery-1.11.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.noconflict.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/modernizr.2.7.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery-migrate-1.2.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.placeholder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery-ui.1.10.4.min.js') }}"></script>
    
    <!-- Twitter Bootstrap -->
    <script type="text/javascript" src="{{ asset('/js/bootstrap.min.js') }}"></script>
    
    <!-- load revolution slider scripts -->
    <script type="text/javascript" src="{{ asset('/components/revolution_slider/js/jquery.themepunch.plugins.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/components/revolution_slider/js/jquery.themepunch.revolution.min.js') }}"></script>
    
    <!-- load BXSlider scripts -->
    <script type="text/javascript" src="{{ asset('/components/jquery.bxslider/jquery.bxslider.min.js') }}"></script>

    <!-- Flex Slider -->
    <script type="text/javascript" src="{{ asset('/components/flexslider/jquery.flexslider.js') }}"></script>

    <!-- parallax -->
    <script type="text/javascript" src="{{ asset('/js/jquery.stellar.min.js') }}"></script>
    
    <!-- waypoint -->
    <script type="text/javascript" src="{{ asset('/js/waypoints.min.js') }}"></script>

    <!-- load page Javascript -->
    <script type="text/javascript" src="{{ asset('/js/theme-scripts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scripts.js') }}"></script>
    
    <script type="text/javascript">
        tjq(document).ready(function() {
            tjq('.revolution-slider').revolution(
            {
                dottedOverlay:"none",
                delay:8000,
                startwidth:1170,
                startheight:646,
                onHoverStop:"on",
                hideThumbs:10,
                fullWidth:"on",
                forceFullWidth:"on",
                navigationType:"none",
                shadow:0,
                spinner:"spinner4",
                hideTimerBar:"on",
            });
        });
    </script>

    @if(Auth::check())
        <script type="text/javascript">
            tjq(function(){
                setNotifications({
                    complite_user : '{{ Auth::user()->id }}'
                }, function(res){
                    if(res.result == true){
                        tjq('.top-warning').show('slow');
                        tjq('.top-warning-body').html(res.err);
                    }
                });
            });
        </script>
    @endif

    @yield('meta')
</head>
<body>
    <div class="top-warning">
        <div class="container top-warning-body"></div>
    </div>

    @if(Session::get('notif'))
    <div class="top-notif animated animated-fadeIn">
        <div class="container">
            <div class="alert alert-{{ Session::get('notif')['label'] }}" role="alert">
                <button type="button" class="close close-top-notif"></button>
                <p>{{ Session::get('notif')['err'] }}</p>
            </div>
        </div>
    </div>
    @endif
    
    @if(count($errors->all()) > 0)
        <div class="top-notif animated animated-fadeIn">
            <div class="container">
                <div class="alert alert-info" role="alert">
                    <button type="button" class="close close-top-notif"></button>
                    <ul>
                    @foreach($errors->all() as $err)
                        <li>{{ $err }}</li>
                    @endforeach
                    </ul>
                </div>
            </div>
        </div>    
    @endif

    <div id="page-wrapper">
        
         <header id="header" class="navbar-static-top">
            <div class="topnav hidden-xs">
                <div class="container">
                    @if(Auth::check())
                    <ul class="quick-menu pull-left">
                        <li><a href="{{ url('/home') }}">Hi, {{ Auth::user()->name }}</a></li>
                    </ul>
                    @endif
                    <ul class="quick-menu pull-right">
                    @if(Auth::guest())
                        <li><a href="#ketiket-login" class="soap-popupbox">LOGIN</a></li>
                        <li><a href="{{ url('/auth/register') }}" >SIGNUP</a></li>
                    @else
                        
                        <!-- menu travel -->
                        @if(Travel::check())
                        <li data-toggle="tooltip" data-placement="bottom" title="Admin Travel"><a href="{{ url('/otojasa/dashboard') }}">Dashboard {{ Travel::data()->nama_travel }}</a></li>
                        @endif

                        <!-- Menu Agen -->
                        @if(Agen::check())
                        <li data-toggle="tooltip" data-placement="bottom" title="Admin Agen"><a href="{{ url('/agen/dashboard') }}">Dashboard {{ Agen::data()->nama_travel }}</a></li>
                        @endif

                        <li><a href="{{ url('/auth/logout') }}">LOGOUT</a></li>
                        @if(Travel::guest() && Agen::guest())
                        <li><a href="{{ url('/create-otojasa') }}">Daftar Travel / Agen</a></li>
                        @endif
                    @endif
                    </ul>

                </div>
            </div>
            
            @include('navtop')
            <div id="ketiket-login" class="travelo-login-box travelo-box">
                <div class="login-social">
                    <a href="{{ url('gotofb') }}" class="button login-facebook"><i class="soap-icon-facebook"></i>Login with Facebook</a>
                    <a href="#" class="button login-googleplus"><i class="soap-icon-googleplus"></i>Login with Google+</a>
                </div>
                <div class="seperator"><label>OR</label></div>
                <form method="POST" action="{{ url('/auth/login') }}">
                    <div class="form-group">
                        <input type="text" class="input-text full-width" placeholder="email address" name="email" value="{{ old('email') }}">
                    </div>
                    <div class="form-group">
                        <input type="password" class="input-text full-width" placeholder="password" name="password">
                    </div>
                    <div class="form-group">
                        <a class="btn btn-link" href="{{ url('/password/email') }}">Forgot password?</a>
                        <div class="checkbox checkbox-inline">
                            <label>
                                <input type="checkbox" name="remember"> Remember Me
                            </label>
                        </div>
                    </div>
                     <div class="form-group">
                        <button type="submit" class="btn-large full-width sky-blue1">LOGIN TO YOUR ACCOUNT</button>
                    </div>

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </form>
                <div class="seperator"></div>
                <p>Don't have an account? <a href="#travelo-signup" class="goto-signup soap-popupbox">Sign up</a></p>
            </div>
        </header>

        @yield('header')

        <section id="content">
            @yield('content')
        </section>
        
        <footer id="footer">
            @yield('footer')
            @include('footer')
            <div class="bottom gray-area">
                <div class="container">
                    <div class="logo pull-left">
                        <a href="{{ url('/') }}" title="Ketiket - home">
                            <img src="{{ asset('/images/logo.png') }}" alt="Ketiket HTML5 Template" />
                        </a>
                    </div>
                    <div class="pull-right">
                        <a id="back-to-top" href="#" class="animated" data-animation-type="bounce"><i class="soap-icon-longarrow-up circle"></i></a>
                    </div>
                    <div class="copyright pull-right">
                        <p>&copy; 2015 Ketiket</p>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    
    <script type="text/javascript">var _base_url = '{{ url() }}';</script>
</body>
</html>

