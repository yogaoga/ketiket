<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
      <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"/>
      <title>{{ empty($title) ? 'Ketiket' : $title }}</title>
      <link href="http://fonts.googleapis.com/css?family=Playfair+Display:400italic|Montserrat:400,700" rel="stylesheet" type="text/css">
      <style type="text/css">
         /*Client-specific Styles*/
         #outlook a { padding:0;}
         .ReadMsgBody { width:100%;}
         .ExternalClass { width:100%;}
         body { -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; -webkit-font-smoothing:antialiased;}
         .yshortcuts, .yshortcuts a, .yshortcuts a:link, .yshortcuts a:visited, .yshortcuts a:hover, .yshortcuts a span { text-decoration:none !important; border-bottom:none !important; background:none !important;}
         /*link styles & hover*/
         a { color:#3291db; text-decoration:none; outline:none;}
         a:hover { text-decoration:underline !important;}
         /*media query*/
         @media only screen and (max-width: 649px) {
         table[class~=wrap], td[class~=wrap], table[class~=divider] { width:100% !important;}
         table[class~=cw4] { width:88px !important;}
         table[class~=cw1].a { width:295px !important;}
         table[class~=cw2].a { width:320px !important;}
         table[class~=cw3] { width:40px !important;}
         table[class~=cw5] { width:95px !important;}
         }
         @media only screen and (max-width: 629px) {
         table[class~=wrap], td[class~=wrap], table[class~=divider] { width:440px !important;}
         table[class~=row], td[class~=row] { width:400px !important;}
         table[class~=col2], table[class~=col3] { width:100% !important;}
         table[class~=cw1].a { width:360px !important;}
         table[class~=cw2].a { width:360px !important;}
         table[class~=cw3] { width:80px !important;}
         table[class~=cw5] { width:80px !important;}
         table[class~=cw6] { width:80px !important;}
         td[class~=cw7] { width:10px !important;}
         table[class~=ch1] { height:auto !important;}
         td[class~=ch2] { height:200px !important;}
         td[class~=tch] { height:0px !important;}
         td[class~=mch] { height:30px !important;}
         table[class~=fn] { float:none !important;}
         img { height:auto !important;}
         table[class~=hide], td[class~=hide] { display:none !important;}
         table[class~=mid] { float:none !important; margin-bottom:30px !important;}
         table[class~=mid2] { float:none !important; margin-bottom:18px !important;}
         table[class~=mid3] { float:none !important; margin-bottom:50px !important;}
         table[class~=mid4] { float:none !important; margin-bottom:30px !important;}
         img[class~=img] { width:100% !important; max-width:100% !important;}
         td[class~=block] { width:100% !important; display:block !important; float:left !important; margin:0 auto !important; padding:0 !important;}
         td[class~=center] { text-align:center !important;}
         table[class~=logo] td { padding:40px 0 0 !important;}
         img[class~=logo-img] { width:160px !important; height:auto !important; max-width: 200px !important;}
         td[class~=headerbanner] .title-td { padding:0 10px !important;}
         td[class~=headerbanner] .content-td { padding:0 10px !important;}
         td[class~pad-td1] { padding:0 0 0 10px !important;}
         td[class~=cprt].a { padding:22px 0 0 !important;}
         td[class~=border-white2] { border:none !important;}
         td[class~=border-top].a { border-top:none !important;}
         td[class~=border-gray-left] { border-left:none !important;}
         td[class~=border-gray-right] { border-right:none !important;}
         td[class~=border-t], table[class~=border-t] { border-top-width:1px !important;}
         td[class~=border-l], table[class~=border-l] { border-left-width:1px !important;}
         td[class~=border-r], table[class~=border-r] { border-right-width:1px !important;}
         td[class~=border-b], table[class~=border-b] { border-bottom-width:1px !important;}
         }
         @media only screen and (max-width: 439px) {
         table[class~=wrap], td[class~=wrap], table[class~=divider] { width:360px !important;}
         table[class~=row] { width:330px !important;}
         table[class~=cw1].a { width:330px !important;}
         table[class~=cw2].a { width:330px !important;}
         table[class~=ch1] { width:330px !important;}
         td[class~=block3] { width:100% !important; display:block !important; float:left !important; margin:0 auto !important; padding:0 !important;}
         table[class~=hide3], td[class~=hide3] { display:none !important;}
         td[class~=sh1] { font-size:38px !important; line-height:40px !important;}
         h1[class~=h1] { font-size:37px !important; line-height:40px !important;}
         h1[class~=h1].b { font-size:48px !important; line-height:72px !important;}
         img[class~=logo-img] { width:145px !important; height:auto !important; max-width: 200px !important;}
         }
         @media only screen and (max-width: 359px) {
         table[class~=wrap], td[class~=wrap], table[class~=divider] { width:320px !important;}
         table[class~=row] { width:300px !important;}
         table[class~=cw1].a { width:300px !important;}
         table[class~=cw2].a { width:300px !important;}
         table[class~=ch1] { width:300px !important;}
         table[class~=mid4] { float:none !important; margin-bottom:18px !important;}
         table[class~=hide4], td[class~=hide4] { display:none !important;}
         td[class~=block2] { width:100% !important; height:50px !important; display:block !important; float:left !important; margin:0 auto !important; padding:25px 0 !important;}
         td[class~=border-dark].a { border-top-width:0px !important; border-bottom-width:0px !important;}
         table[class~=menu] td { padding:40px 4px !important;}
         img[class~=logo-img] { max-width: 200px !important;}
         }
      </style>

      @yield('meta')
   </head>
   <body style="margin:0;padding:0;width:100%;height:100%;">
      <div class="preheader" style="display:none; visibility:hidden; height:0px; font-size:0px; line-height:0px;">Email Newsletter of this month.</div>
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="BGtable" style="border-collapse: collapse;margin: 0;padding: 0;table-layout: fixed;background-color: #474747;width: 100% !important;height: 100% !important;">
         <tr>
            <td align="center" valign="top" class="BGtable-inner">
               <!-- start ◆header◆ -->
               <div mc:repeatable="header">
                  <table width="650" align="center" border="0" cellpadding="0" cellspacing="0" class="wrap header" style="border-collapse: collapse;width: 650px;margin: 0 auto;">
                     <tr>
                        <td height="50" class="tch"></td>
                     </tr>
                     <tr>
                        <td align="center" class="wrapbg" style="background-image: none; background-color: #ffffff;">
                           <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="row" style="border-collapse: collapse;">
                              <tr>
                                 <td align="center">
                                    <!--start logo img-->
                                    <table align="left" border="0" cellspacing="0" cellpadding="0" class="logo fn" style="border-collapse: collapse;border: none;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                       <tr>
                                          <td style="padding: 28px 0 24px;"><a  href="#" style="color: #3291db;text-decoration: none;outline: none;"><img  src="{{ asset('/images/logo.png') }}" width="138" height="39" alt="logo" class="logo-img" mc:edit="logo"  style="border: 0;display: block;-ms-interpolation-mode: bicubic;"></a></td>
                                       </tr>
                                    </table>
                                    <!--end logo img-->
                                    <!--start menu-->
                                    <table align="right" border="0" cellspacing="0" cellpadding="0" class="menu fn" style="border-collapse: collapse;border: none;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                       <tr>
                                          <td mc:edit="about" style="font-family: Montserrat;font-size: 13px;line-height: 13px;font-weight: 700;color: #262424;padding: 40px 8px;"><a  href="{{ url('/') }}" style="color: #242424;text-decoration: none;outline: none;">HOME</a></td>
                                          <td style="font-family: Montserrat;font-size: 13px;line-height: 13px;font-weight: 700;color: #262424;padding: 40px 8px;">|</td>
                                          <td mc:edit="services" style="font-family: Montserrat;font-size: 13px;line-height: 13px;font-weight: 700;color: #262424;padding: 40px 8px;"><a  href="{{ url('/auth/login') }}" style="color: #242424;text-decoration: none;outline: none;">LOGIN</a></td>
                                          <td style="font-family: Montserrat;font-size: 13px;line-height: 13px;font-weight: 700;color: #262424;padding: 40px 8px;">|</td>
                                          <td mc:edit="contact" style="font-family: Montserrat;font-size: 13px;line-height: 13px;font-weight: 700;color: #262424;padding: 40px 8px;"><a  href="{{ url('/auth/register') }}" style="color: #242424;text-decoration: none;outline: none;">REGISTER</a></td>
                                       </tr>
                                    </table>
                                    <!--end menu-->
                                 </td>
                              </tr>
                           </table>
                        </td>
                     </tr>
                  </table>
               </div>
               <!-- end - ◆header◆ -->
               <!-- start ◆full_module_7s◆ -->
               <div mc:repeatable="full_module_7s">
                  <table width="650" align="center" border="0" cellspacing="0" cellpadding="0" class="wrap wrapbg" style="border-collapse: collapse; width: 650px; margin: 0px auto; background-image: none; background-color: #ffffff;">
                     <tr>
                        <td align="center" class="module-td1" style="padding: 0 0 0;">
                           <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="row" style="border-collapse: collapse;">
                              <tr>
                                 <td height="20"></td>
                              </tr>
                              <!--start content-->
                              <tr>
                                 <td align="center" class="content" mc:edit="content" style="font-family: Raleway, Arial; font-weight: 400; font-size: 13px; line-height: 19px; color: #252525; -webkit-font-smoothing: antialiased;">
                                    @yield('content')
                                 </td>
                              </tr>
                              <!--end content-->
                           </table>
                        </td>
                     </tr>
                  </table>
               </div>
               <!-- end - ◆full_module_7s◆ -->
               <!-- start ◆divider_70px_height◆ -->
               <div mc:repeatable="divider_70px_height">
                  <table width="650" align="center" border="0" cellpadding="0" cellspacing="0" class="divider wrapbg" style="border-collapse: collapse; width: 650px; margin: 0px auto; background-image: none; background-color: #ffffff;">
                     <tr>
                        <td height="70" class="small-img" style="font-size: 0px;line-height: 0px;border-collapse: collapse;"><img  src="http://template/email/email_builder/html/blue/images/spacer.gif" width="1" height="1" mc:edit="do not edit"  style="border: 0;display: block;-ms-interpolation-mode: bicubic;"></td>
                     </tr>
                  </table>
               </div>
               <!-- end - ◆divider_70px_height◆ -->
               <!-- start ◆footer◆ -->
               <table width="650" align="center" border="0" cellspacing="0" cellpadding="0" class="wrap footer" style="border-collapse: collapse;width: 650px;margin: 0 auto;">
                  <tr>
                     <td align="center" class="colorbg-gray" style="background-image: none; background-color: #f2f2f2;">
                        <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="row" style="border-collapse: collapse;">
                           <tr>
                              <td align="center">
                                @yield('footer')
                              </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
                  <tr>
                     <td height="1" class="small-img line2" style="font-size: 0px;line-height: 0px;border-collapse: collapse;background-color: #252525;"><img  src="http://template/email/email_builder/html/blue/images/spacer.gif" width="1" height="1" style="border: 0;display: block;-ms-interpolation-mode: bicubic;"></td>
                  </tr>
                  <tr>
                     <td align="center" class="colorbg-gray" style="background-image: none; background-color: #f2f2f2;">
                        <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="row" style="border-collapse: collapse;">
                           <tr>
                              <td height="30"></td>
                           </tr>
                           <tr>
                              <td align="center"><img  src="http://template/email/email_builder/html/blue/images/icon-35-10.png" width="35" height="35" alt="icon" class="icon35" mc:edit="icon"  style="border: 0;display: block;-ms-interpolation-mode: bicubic;width: 35px;height: auto;max-width: 35px;max-height: 35px;"></td>
                           </tr>
                           <tr>
                              <td height="12" class="small-img" style="font-size: 0px;line-height: 0px;border-collapse: collapse;"><img  src="http://template/email/email_builder/html/blue/images/spacer.gif" width="1" height="1" style="border: 0;display: block;-ms-interpolation-mode: bicubic;"></td>
                           </tr>
                           <tr>
                              <td align="center" class="h5" mc:edit="address" style="font-family: Montserrat,Tahoma;font-weight: 400;color: #262424;font-size: 17px;line-height: 17px;">Unit No.100, Park Name, Area Name,
                                 <br>State, Country Name
                              </td>
                           </tr>
                           <tr>
                              <td height="40"></td>
                           </tr>
                           <tr>
                              <td>
                                 <table border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse;">
                                    <tr>
                                       <td align="center" width="300" class="block">
                                          <table align="right" border="0" cellspacing="0" cellpadding="0" class="mid" style="border-collapse: collapse;border: none;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                             <tr>
                                                <td><img  src="http://template/email/email_builder/html/blue/images/icon-15-3.png" width="15" height="15" alt="icon" class="icon15" mc:edit="icon"  style="border: 0;display: block;-ms-interpolation-mode: bicubic;width: 15px;height: auto;max-width: 15px;max-height: 15px;"></td>
                                                <td width="15"></td>
                                                <td class="h5" mc:edit="email" style="font-family: Montserrat,Tahoma;font-weight: 400;color: #262424;font-size: 17px;line-height: 17px;"><a  href="#" class="dark" style="color: #252525;text-decoration: none;outline: none;">hello@digiage.com</a></td>
                                                <td width="15" class="hide"></td>
                                             </tr>
                                          </table>
                                       </td>
                                       <td align="center" width="300" class="block">
                                          <table align="left" border="0" cellspacing="0" cellpadding="0" class="fn" style="border-collapse: collapse;border: none;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                             <tr>
                                                <td width="15" class="hide"></td>
                                                <td><img  src="http://template/email/email_builder/html/blue/images/icon-15-4.png" width="15" height="15" alt="icon" class="icon15" mc:edit="icon"  style="border: 0;display: block;-ms-interpolation-mode: bicubic;width: 15px;height: auto;max-width: 15px;max-height: 15px;"></td>
                                                <td width="15"></td>
                                                <td class="h5" mc:edit="website" style="font-family: Montserrat,Tahoma;font-weight: 400;color: #262424;font-size: 17px;line-height: 17px;"><a  href="#" class="dark" style="color: #252525;text-decoration: none;outline: none;">www.digiage.com</a></td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td height="38"></td>
                           </tr>
                        </table>
                     </td>
                  </tr>
                  <tr>
                     <td align="center" height="120" class="colorbg-dark ch2" style="background-image: none; background-color: #252525;">
                        <!--start social icons-->
                        <table align="center" border="0" cellspacing="0" cellpadding="0" class="social-icons" style="border-collapse: collapse;">
                           <tr>
                              <td align="center" class="block">
                                 <table align="center" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse;">
                                    <tr>
                                       <td>
                                          <table border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse;">
                                             <tr>
                                                <td align="center" width="59" height="59" class="icon-border" style="border: 2px solid #fff;"><a  href="#" style="color: #3291db;text-decoration: none;outline: none;"><img  src="http://template/email/email_builder/html/blue/images/icon-35-11.png" width="35" height="35" alt="icon" class="icon35" mc:edit="icon1"  style="border: 0;display: block;-ms-interpolation-mode: bicubic;width: 35px;height: auto;max-width: 35px;max-height: 35px;"></a></td>
                                             </tr>
                                          </table>
                                       </td>
                                       <td width="12"></td>
                                       <td>
                                          <table border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse;">
                                             <tr>
                                                <td align="center" width="59" height="59" class="icon-border" style="border: 2px solid #fff;"><a  href="#" style="color: #3291db;text-decoration: none;outline: none;"><img  src="http://template/email/email_builder/html/blue/images/icon-35-12.png" width="35" height="35" alt="icon" class="icon35" mc:edit="icon2"  style="border: 0;display: block;-ms-interpolation-mode: bicubic;width: 35px;height: auto;max-width: 35px;max-height: 35px;"></a></td>
                                             </tr>
                                          </table>
                                       </td>
                                       <td width="12"></td>
                                       <td>
                                          <table border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse;">
                                             <tr>
                                                <td align="center" width="59" height="59" class="icon-border" style="border: 2px solid #fff;"><a  href="#" style="color: #3291db;text-decoration: none;outline: none;"><img  src="http://template/email/email_builder/html/blue/images/icon-35-13.png" width="35" height="35" alt="icon" class="icon35" mc:edit="icon3"  style="border: 0;display: block;-ms-interpolation-mode: bicubic;width: 35px;height: auto;max-width: 35px;max-height: 35px;"></a></td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                              <td width="12" height="12" class="block"></td>
                              <td align="center" class="block">
                                 <table align="center" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse;">
                                    <tr>
                                       <td>
                                          <table border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse;">
                                             <tr>
                                                <td align="center" width="59" height="59" class="icon-border" style="border: 2px solid #fff;"><a  href="#" style="color: #3291db;text-decoration: none;outline: none;"><img  src="http://template/email/email_builder/html/blue/images/icon-35-14.png" width="35" height="35" alt="icon" class="icon35" mc:edit="icon4"  style="border: 0;display: block;-ms-interpolation-mode: bicubic;width: 35px;height: auto;max-width: 35px;max-height: 35px;"></a></td>
                                             </tr>
                                          </table>
                                       </td>
                                       <td width="12"></td>
                                       <td>
                                          <table border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse;">
                                             <tr>
                                                <td align="center" width="59" height="59" class="icon-border" style="border: 2px solid #fff;"><a  href="#" style="color: #3291db;text-decoration: none;outline: none;"><img  src="http://template/email/email_builder/html/blue/images/icon-35-15.png" width="35" height="35" alt="icon" class="icon35" mc:edit="icon5"  style="border: 0;display: block;-ms-interpolation-mode: bicubic;width: 35px;height: auto;max-width: 35px;max-height: 35px;"></a></td>
                                             </tr>
                                          </table>
                                       </td>
                                       <td width="12"></td>
                                       <td>
                                          <table border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse;">
                                             <tr>
                                                <td align="center" width="59" height="59" class="icon-border" style="border: 2px solid #fff;"><a  href="#" style="color: #3291db;text-decoration: none;outline: none;"><img  src="http://template/email/email_builder/html/blue/images/icon-35-16.png" width="35" height="35" alt="icon" class="icon35" mc:edit="icon6"  style="border: 0;display: block;-ms-interpolation-mode: bicubic;width: 35px;height: auto;max-width: 35px;max-height: 35px;"></a></td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                        </table>
                        <!--end social icons-->
                     </td>
                  </tr>
                  <tr>
                     <td align="center" class="colorbg-gray" style="background-image: none; background-color: #f2f2f2;">
                        <table align="center" width="600" border="0" cellspacing="0" cellpadding="0" class="row" style="border-collapse: collapse;">
                           <tr>
                              <td align="center">
                                 <!--copyright-->
                                 <table align="left" border="0" cellspacing="0" cellpadding="0" class="fn" style="border-collapse: collapse;border: none;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                    <tr>
                                       <td align="center" class="cprt a" mc:edit="cprt" style="font-family: Raleway;font-size: 11px;line-height: 11px;color: #2c2c2c;font-weight: 400;padding: 22px 0 20px;">Copyright@2014 email Template, All rights reserved</td>
                                    </tr>
                                 </table>
                                 <!--unsubscribed-->
                                 <table align="right" border="0" cellspacing="0" cellpadding="0" class="fn" style="border-collapse: collapse;border: none;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                    <tr>
                                       <td align="center" class="cprt" mc:edit="func" style="font-family: Raleway;font-size: 11px;line-height: 11px;color: #2c2c2c;font-weight: 400;padding: 22px 0 20px;"><a  href="*|UNSUB|*" style="color: #2c2c2c;text-decoration: none;outline: none;">Un Subscribed Here</a> / <a  href="#" style="color: #2c2c2c;text-decoration: none;outline: none;">Update Profile</a></td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
               </table>
               <!-- end - ◆footer◆ -->
            </td>
         </tr>
      </table>
   </body>
</html>