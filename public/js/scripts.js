/*
 * Title:   Travelo | Responsive HTML5 Travel Template - Custom Javascript file
 * Author:  http://themeforest.net/user/soaptheme
 */

tjq(document).ready(function() {
    tjq.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': tjq('meta[name="csrf-token"]').attr('content')
        }
    });

    // Function untuk memanggil notifikasi yang masih aktif
    setNotifications = function(option, next){
    	tjq.ajax({type : 'GET',url : _base_url + '/ajax/notif',data : option,cache : false,dataType : 'json',success : function(res){
    			return next(res);
    		}
    	});
    }

    tjq('.close-top-notif').click(function(){
        tjq('.top-notif').hide();
    });

});

