tjq(function(){
    tjq('select.form-select2').select2();
    tjq('[data-rel="timepicker"]').timepicker();

    generate = function(){
        tjq('[name="kode_routes"]').val('');
        tjq('[name="kode_routes"]').attr('placeholder', 'Generating...');
        tjq.ajax({
            type : 'POST',
            url : _base_url + '/ajax/generatecode',
            data : {},
            cache : false,
            dataType : 'json',
            success : function(res){
                tjq('[name="kode_routes"]').val(res.result);
            }
        });
    };

    var options = { 
        target:   '#output', 
        beforeSubmit:  beforeSubmit,
            uploadProgress: OnProgress, //upload progress callback 
            success:       afterSuccess,
            resetForm: true  
        }; 
        $('#foto').change(function() { 
            alert("a");
            $(this).ajaxSubmit(options);            
            return false; 
        });

    }); 
function OnProgress(event, position, total, percentComplete)
{
    //Progress bar
    progressbar.width(percentComplete + '%') //update progressbar percent complete
    statustxt.html(percentComplete + '%'); //update status text
    if(percentComplete>50)
    {
            statustxt.css('color','#fff'); //change status text to white after 50%
        }
    }